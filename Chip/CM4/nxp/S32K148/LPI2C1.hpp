#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//The LPI2C Memory Map/Register Definition can be found here.
    namespace Lpi2c1Verid{    ///<Version ID Register
        using Addr = Register::Address<0x40067000,0x00000000,0x00000000,unsigned>;
        ///Feature Specification Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> feature{}; 
        ///Minor Version Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> minor{}; 
        ///Major Version Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> major{}; 
    }
    namespace Lpi2c1Param{    ///<Parameter Register
        using Addr = Register::Address<0x40067004,0xfffff0f0,0x00000000,unsigned>;
        ///Master Transmit FIFO Size
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> mtxfifo{}; 
        ///Master Receive FIFO Size
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> mrxfifo{}; 
    }
    namespace Lpi2c1Mcr{    ///<Master Control Register
        using Addr = Register::Address<0x40067010,0xfffffcf0,0x00000000,unsigned>;
        ///Master Enable
        enum class MenVal : unsigned {
            v0=0x00000000,     ///<Master logic is disabled.
            v1=0x00000001,     ///<Master logic is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,MenVal> men{}; 
        namespace MenValC{
            constexpr Register::FieldValue<decltype(men)::Type,MenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(men)::Type,MenVal::v1> v1{};
        }
        ///Software Reset
        enum class RstVal : unsigned {
            v0=0x00000000,     ///<Master logic is not reset.
            v1=0x00000001,     ///<Master logic is reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,RstVal> rst{}; 
        namespace RstValC{
            constexpr Register::FieldValue<decltype(rst)::Type,RstVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rst)::Type,RstVal::v1> v1{};
        }
        ///Doze mode enable
        enum class DozenVal : unsigned {
            v0=0x00000000,     ///<Master is enabled in Doze mode.
            v1=0x00000001,     ///<Master is disabled in Doze mode.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,DozenVal> dozen{}; 
        namespace DozenValC{
            constexpr Register::FieldValue<decltype(dozen)::Type,DozenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dozen)::Type,DozenVal::v1> v1{};
        }
        ///Debug Enable
        enum class DbgenVal : unsigned {
            v0=0x00000000,     ///<Master is disabled in debug mode.
            v1=0x00000001,     ///<Master is enabled in debug mode.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,DbgenVal> dbgen{}; 
        namespace DbgenValC{
            constexpr Register::FieldValue<decltype(dbgen)::Type,DbgenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dbgen)::Type,DbgenVal::v1> v1{};
        }
        ///Reset Transmit FIFO
        enum class RtfVal : unsigned {
            v0=0x00000000,     ///<No effect.
            v1=0x00000001,     ///<Transmit FIFO is reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RtfVal> rtf{}; 
        namespace RtfValC{
            constexpr Register::FieldValue<decltype(rtf)::Type,RtfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rtf)::Type,RtfVal::v1> v1{};
        }
        ///Reset Receive FIFO
        enum class RrfVal : unsigned {
            v0=0x00000000,     ///<No effect.
            v1=0x00000001,     ///<Receive FIFO is reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,9),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RrfVal> rrf{}; 
        namespace RrfValC{
            constexpr Register::FieldValue<decltype(rrf)::Type,RrfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rrf)::Type,RrfVal::v1> v1{};
        }
    }
    namespace Lpi2c1Msr{    ///<Master Status Register
        using Addr = Register::Address<0x40067014,0xfcff80fc,0x00000000,unsigned>;
        ///Transmit Data Flag
        enum class TdfVal : unsigned {
            v0=0x00000000,     ///<Transmit data not requested.
            v1=0x00000001,     ///<Transmit data is requested.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,TdfVal> tdf{}; 
        namespace TdfValC{
            constexpr Register::FieldValue<decltype(tdf)::Type,TdfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tdf)::Type,TdfVal::v1> v1{};
        }
        ///Receive Data Flag
        enum class RdfVal : unsigned {
            v0=0x00000000,     ///<Receive Data is not ready.
            v1=0x00000001,     ///<Receive data is ready.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RdfVal> rdf{}; 
        namespace RdfValC{
            constexpr Register::FieldValue<decltype(rdf)::Type,RdfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rdf)::Type,RdfVal::v1> v1{};
        }
        ///End Packet Flag
        enum class EpfVal : unsigned {
            v0=0x00000000,     ///<Master has not generated a STOP or Repeated START condition.
            v1=0x00000001,     ///<Master has generated a STOP or Repeated START condition.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::ReadWriteAccess,EpfVal> epf{}; 
        namespace EpfValC{
            constexpr Register::FieldValue<decltype(epf)::Type,EpfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(epf)::Type,EpfVal::v1> v1{};
        }
        ///STOP Detect Flag
        enum class SdfVal : unsigned {
            v0=0x00000000,     ///<Master has not generated a STOP condition.
            v1=0x00000001,     ///<Master has generated a STOP condition.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,9),Register::ReadWriteAccess,SdfVal> sdf{}; 
        namespace SdfValC{
            constexpr Register::FieldValue<decltype(sdf)::Type,SdfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sdf)::Type,SdfVal::v1> v1{};
        }
        ///NACK Detect Flag
        enum class NdfVal : unsigned {
            v0=0x00000000,     ///<Unexpected NACK not detected.
            v1=0x00000001,     ///<Unexpected NACK was detected.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,10),Register::ReadWriteAccess,NdfVal> ndf{}; 
        namespace NdfValC{
            constexpr Register::FieldValue<decltype(ndf)::Type,NdfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ndf)::Type,NdfVal::v1> v1{};
        }
        ///Arbitration Lost Flag
        enum class AlfVal : unsigned {
            v0=0x00000000,     ///<Master has not lost arbitration.
            v1=0x00000001,     ///<Master has lost arbitration.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,AlfVal> alf{}; 
        namespace AlfValC{
            constexpr Register::FieldValue<decltype(alf)::Type,AlfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(alf)::Type,AlfVal::v1> v1{};
        }
        ///FIFO Error Flag
        enum class FefVal : unsigned {
            v0=0x00000000,     ///<No error.
            v1=0x00000001,     ///<Master sending or receiving data without START condition.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,12),Register::ReadWriteAccess,FefVal> fef{}; 
        namespace FefValC{
            constexpr Register::FieldValue<decltype(fef)::Type,FefVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fef)::Type,FefVal::v1> v1{};
        }
        ///Pin Low Timeout Flag
        enum class PltfVal : unsigned {
            v0=0x00000000,     ///<Pin low timeout has not occurred or is disabled.
            v1=0x00000001,     ///<Pin low timeout has occurred.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,13),Register::ReadWriteAccess,PltfVal> pltf{}; 
        namespace PltfValC{
            constexpr Register::FieldValue<decltype(pltf)::Type,PltfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pltf)::Type,PltfVal::v1> v1{};
        }
        ///Data Match Flag
        enum class DmfVal : unsigned {
            v0=0x00000000,     ///<Have not received matching data.
            v1=0x00000001,     ///<Have received matching data.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,14),Register::ReadWriteAccess,DmfVal> dmf{}; 
        namespace DmfValC{
            constexpr Register::FieldValue<decltype(dmf)::Type,DmfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dmf)::Type,DmfVal::v1> v1{};
        }
        ///Master Busy Flag
        enum class MbfVal : unsigned {
            v0=0x00000000,     ///<I2C Master is idle.
            v1=0x00000001,     ///<I2C Master is busy.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,MbfVal> mbf{}; 
        namespace MbfValC{
            constexpr Register::FieldValue<decltype(mbf)::Type,MbfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(mbf)::Type,MbfVal::v1> v1{};
        }
        ///Bus Busy Flag
        enum class BbfVal : unsigned {
            v0=0x00000000,     ///<I2C Bus is idle.
            v1=0x00000001,     ///<I2C Bus is busy.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,BbfVal> bbf{}; 
        namespace BbfValC{
            constexpr Register::FieldValue<decltype(bbf)::Type,BbfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(bbf)::Type,BbfVal::v1> v1{};
        }
    }
    namespace Lpi2c1Mier{    ///<Master Interrupt Enable Register
        using Addr = Register::Address<0x40067018,0xffff80fc,0x00000000,unsigned>;
        ///Transmit Data Interrupt Enable
        enum class TdieVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,TdieVal> tdie{}; 
        namespace TdieValC{
            constexpr Register::FieldValue<decltype(tdie)::Type,TdieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tdie)::Type,TdieVal::v1> v1{};
        }
        ///Receive Data Interrupt Enable
        enum class RdieVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,RdieVal> rdie{}; 
        namespace RdieValC{
            constexpr Register::FieldValue<decltype(rdie)::Type,RdieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rdie)::Type,RdieVal::v1> v1{};
        }
        ///End Packet Interrupt Enable
        enum class EpieVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::ReadWriteAccess,EpieVal> epie{}; 
        namespace EpieValC{
            constexpr Register::FieldValue<decltype(epie)::Type,EpieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(epie)::Type,EpieVal::v1> v1{};
        }
        ///STOP Detect Interrupt Enable
        enum class SdieVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,9),Register::ReadWriteAccess,SdieVal> sdie{}; 
        namespace SdieValC{
            constexpr Register::FieldValue<decltype(sdie)::Type,SdieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sdie)::Type,SdieVal::v1> v1{};
        }
        ///NACK Detect Interrupt Enable
        enum class NdieVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,10),Register::ReadWriteAccess,NdieVal> ndie{}; 
        namespace NdieValC{
            constexpr Register::FieldValue<decltype(ndie)::Type,NdieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ndie)::Type,NdieVal::v1> v1{};
        }
        ///Arbitration Lost Interrupt Enable
        enum class AlieVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,AlieVal> alie{}; 
        namespace AlieValC{
            constexpr Register::FieldValue<decltype(alie)::Type,AlieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(alie)::Type,AlieVal::v1> v1{};
        }
        ///FIFO Error Interrupt Enable
        enum class FeieVal : unsigned {
            v0=0x00000000,     ///<Interrupt enabled.
            v1=0x00000001,     ///<Interrupt disabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,12),Register::ReadWriteAccess,FeieVal> feie{}; 
        namespace FeieValC{
            constexpr Register::FieldValue<decltype(feie)::Type,FeieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(feie)::Type,FeieVal::v1> v1{};
        }
        ///Pin Low Timeout Interrupt Enable
        enum class PltieVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,13),Register::ReadWriteAccess,PltieVal> pltie{}; 
        namespace PltieValC{
            constexpr Register::FieldValue<decltype(pltie)::Type,PltieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pltie)::Type,PltieVal::v1> v1{};
        }
        ///Data Match Interrupt Enable
        enum class DmieVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,14),Register::ReadWriteAccess,DmieVal> dmie{}; 
        namespace DmieValC{
            constexpr Register::FieldValue<decltype(dmie)::Type,DmieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dmie)::Type,DmieVal::v1> v1{};
        }
    }
    namespace Lpi2c1Mder{    ///<Master DMA Enable Register
        using Addr = Register::Address<0x4006701c,0xfffffffc,0x00000000,unsigned>;
        ///Transmit Data DMA Enable
        enum class TddeVal : unsigned {
            v0=0x00000000,     ///<DMA request disabled.
            v1=0x00000001,     ///<DMA request enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,TddeVal> tdde{}; 
        namespace TddeValC{
            constexpr Register::FieldValue<decltype(tdde)::Type,TddeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tdde)::Type,TddeVal::v1> v1{};
        }
        ///Receive Data DMA Enable
        enum class RddeVal : unsigned {
            v0=0x00000000,     ///<DMA request disabled.
            v1=0x00000001,     ///<DMA request enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,RddeVal> rdde{}; 
        namespace RddeValC{
            constexpr Register::FieldValue<decltype(rdde)::Type,RddeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rdde)::Type,RddeVal::v1> v1{};
        }
    }
    namespace Lpi2c1Mcfgr0{    ///<Master Configuration Register 0
        using Addr = Register::Address<0x40067020,0xfffffcf8,0x00000000,unsigned>;
        ///Host Request Enable
        enum class HrenVal : unsigned {
            v0=0x00000000,     ///<Host request input is disabled.
            v1=0x00000001,     ///<Host request input is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,HrenVal> hren{}; 
        namespace HrenValC{
            constexpr Register::FieldValue<decltype(hren)::Type,HrenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(hren)::Type,HrenVal::v1> v1{};
        }
        ///Host Request Polarity
        enum class HrpolVal : unsigned {
            v0=0x00000000,     ///<Active low.
            v1=0x00000001,     ///<Active high.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,HrpolVal> hrpol{}; 
        namespace HrpolValC{
            constexpr Register::FieldValue<decltype(hrpol)::Type,HrpolVal::v0> v0{};
            constexpr Register::FieldValue<decltype(hrpol)::Type,HrpolVal::v1> v1{};
        }
        ///Host Request Select
        enum class HrselVal : unsigned {
            v0=0x00000000,     ///<Host request input is pin HREQ.
            v1=0x00000001,     ///<Host request input is input trigger.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,HrselVal> hrsel{}; 
        namespace HrselValC{
            constexpr Register::FieldValue<decltype(hrsel)::Type,HrselVal::v0> v0{};
            constexpr Register::FieldValue<decltype(hrsel)::Type,HrselVal::v1> v1{};
        }
        ///Circular FIFO Enable
        enum class CirfifoVal : unsigned {
            v0=0x00000000,     ///<Circular FIFO is disabled.
            v1=0x00000001,     ///<Circular FIFO is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::ReadWriteAccess,CirfifoVal> cirfifo{}; 
        namespace CirfifoValC{
            constexpr Register::FieldValue<decltype(cirfifo)::Type,CirfifoVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cirfifo)::Type,CirfifoVal::v1> v1{};
        }
        ///Receive Data Match Only
        enum class RdmoVal : unsigned {
            v0=0x00000000,     ///<Received data is stored in the receive FIFO as normal.
            v1=0x00000001,     ///<Received data is discarded unless the RMF is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,9),Register::ReadWriteAccess,RdmoVal> rdmo{}; 
        namespace RdmoValC{
            constexpr Register::FieldValue<decltype(rdmo)::Type,RdmoVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rdmo)::Type,RdmoVal::v1> v1{};
        }
    }
    namespace Lpi2c1Mcfgr1{    ///<Master Configuration Register 1
        using Addr = Register::Address<0x40067024,0xf8f8f8f8,0x00000000,unsigned>;
        ///Prescaler
        enum class PrescaleVal : unsigned {
            v000=0x00000000,     ///<Divide by 1.
            v001=0x00000001,     ///<Divide by 2.
            v010=0x00000002,     ///<Divide by 4.
            v011=0x00000003,     ///<Divide by 8.
            v100=0x00000004,     ///<Divide by 16.
            v101=0x00000005,     ///<Divide by 32.
            v110=0x00000006,     ///<Divide by 64.
            v111=0x00000007,     ///<Divide by 128.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,PrescaleVal> prescale{}; 
        namespace PrescaleValC{
            constexpr Register::FieldValue<decltype(prescale)::Type,PrescaleVal::v000> v000{};
            constexpr Register::FieldValue<decltype(prescale)::Type,PrescaleVal::v001> v001{};
            constexpr Register::FieldValue<decltype(prescale)::Type,PrescaleVal::v010> v010{};
            constexpr Register::FieldValue<decltype(prescale)::Type,PrescaleVal::v011> v011{};
            constexpr Register::FieldValue<decltype(prescale)::Type,PrescaleVal::v100> v100{};
            constexpr Register::FieldValue<decltype(prescale)::Type,PrescaleVal::v101> v101{};
            constexpr Register::FieldValue<decltype(prescale)::Type,PrescaleVal::v110> v110{};
            constexpr Register::FieldValue<decltype(prescale)::Type,PrescaleVal::v111> v111{};
        }
        ///Automatic STOP Generation
        enum class AutostopVal : unsigned {
            v0=0x00000000,     ///<No effect.
            v1=0x00000001,     ///<STOP condition is automatically generated whenever the transmit FIFO is empty and LPI2C master is busy.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::ReadWriteAccess,AutostopVal> autostop{}; 
        namespace AutostopValC{
            constexpr Register::FieldValue<decltype(autostop)::Type,AutostopVal::v0> v0{};
            constexpr Register::FieldValue<decltype(autostop)::Type,AutostopVal::v1> v1{};
        }
        ///IGNACK
        enum class IgnackVal : unsigned {
            v0=0x00000000,     ///<LPI2C Master will receive ACK and NACK normally.
            v1=0x00000001,     ///<LPI2C Master will treat a received NACK as if it was an ACK.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,9),Register::ReadWriteAccess,IgnackVal> ignack{}; 
        namespace IgnackValC{
            constexpr Register::FieldValue<decltype(ignack)::Type,IgnackVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ignack)::Type,IgnackVal::v1> v1{};
        }
        ///Timeout Configuration
        enum class TimecfgVal : unsigned {
            v0=0x00000000,     ///<Pin Low Timeout Flag will set if SCL is low for longer than the configured timeout.
            v1=0x00000001,     ///<Pin Low Timeout Flag will set if either SCL or SDA is low for longer than the configured timeout.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,10),Register::ReadWriteAccess,TimecfgVal> timecfg{}; 
        namespace TimecfgValC{
            constexpr Register::FieldValue<decltype(timecfg)::Type,TimecfgVal::v0> v0{};
            constexpr Register::FieldValue<decltype(timecfg)::Type,TimecfgVal::v1> v1{};
        }
        ///Match Configuration
        enum class MatcfgVal : unsigned {
            v000=0x00000000,     ///<Match disabled.
            v010=0x00000002,     ///<Match enabled (1st data word equals MATCH0 OR MATCH1).
            v011=0x00000003,     ///<Match enabled (any data word equals MATCH0 OR MATCH1).
            v100=0x00000004,     ///<Match enabled (1st data word equals MATCH0 AND 2nd data word equals MATCH1).
            v101=0x00000005,     ///<Match enabled (any data word equals MATCH0 AND next data word equals MATCH1).
            v110=0x00000006,     ///<Match enabled (1st data word AND MATCH1 equals MATCH0 AND MATCH1).
            v111=0x00000007,     ///<Match enabled (any data word AND MATCH1 equals MATCH0 AND MATCH1).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(18,16),Register::ReadWriteAccess,MatcfgVal> matcfg{}; 
        namespace MatcfgValC{
            constexpr Register::FieldValue<decltype(matcfg)::Type,MatcfgVal::v000> v000{};
            constexpr Register::FieldValue<decltype(matcfg)::Type,MatcfgVal::v010> v010{};
            constexpr Register::FieldValue<decltype(matcfg)::Type,MatcfgVal::v011> v011{};
            constexpr Register::FieldValue<decltype(matcfg)::Type,MatcfgVal::v100> v100{};
            constexpr Register::FieldValue<decltype(matcfg)::Type,MatcfgVal::v101> v101{};
            constexpr Register::FieldValue<decltype(matcfg)::Type,MatcfgVal::v110> v110{};
            constexpr Register::FieldValue<decltype(matcfg)::Type,MatcfgVal::v111> v111{};
        }
        ///Pin Configuration
        enum class PincfgVal : unsigned {
            v000=0x00000000,     ///<LPI2C configured for 2-pin open drain mode.
            v001=0x00000001,     ///<LPI2C configured for 2-pin output only mode (ultra-fast mode).
            v010=0x00000002,     ///<LPI2C configured for 2-pin push-pull mode.
            v011=0x00000003,     ///<LPI2C configured for 4-pin push-pull mode.
            v100=0x00000004,     ///<LPI2C configured for 2-pin open drain mode with separate LPI2C slave.
            v101=0x00000005,     ///<LPI2C configured for 2-pin output only mode (ultra-fast mode) with separate LPI2C slave.
            v110=0x00000006,     ///<LPI2C configured for 2-pin push-pull mode with separate LPI2C slave.
            v111=0x00000007,     ///<LPI2C configured for 4-pin push-pull mode (inverted outputs).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,24),Register::ReadWriteAccess,PincfgVal> pincfg{}; 
        namespace PincfgValC{
            constexpr Register::FieldValue<decltype(pincfg)::Type,PincfgVal::v000> v000{};
            constexpr Register::FieldValue<decltype(pincfg)::Type,PincfgVal::v001> v001{};
            constexpr Register::FieldValue<decltype(pincfg)::Type,PincfgVal::v010> v010{};
            constexpr Register::FieldValue<decltype(pincfg)::Type,PincfgVal::v011> v011{};
            constexpr Register::FieldValue<decltype(pincfg)::Type,PincfgVal::v100> v100{};
            constexpr Register::FieldValue<decltype(pincfg)::Type,PincfgVal::v101> v101{};
            constexpr Register::FieldValue<decltype(pincfg)::Type,PincfgVal::v110> v110{};
            constexpr Register::FieldValue<decltype(pincfg)::Type,PincfgVal::v111> v111{};
        }
    }
    namespace Lpi2c1Mcfgr2{    ///<Master Configuration Register 2
        using Addr = Register::Address<0x40067028,0xf0f0f000,0x00000000,unsigned>;
        ///Bus Idle Timeout
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::ReadWriteAccess,unsigned> busidle{}; 
        ///Glitch Filter SCL
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,unsigned> filtscl{}; 
        ///Glitch Filter SDA
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,24),Register::ReadWriteAccess,unsigned> filtsda{}; 
    }
    namespace Lpi2c1Mcfgr3{    ///<Master Configuration Register 3
        using Addr = Register::Address<0x4006702c,0xfff000ff,0x00000000,unsigned>;
        ///Pin Low Timeout
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,8),Register::ReadWriteAccess,unsigned> pinlow{}; 
    }
    namespace Lpi2c1Mdmr{    ///<Master Data Match Register
        using Addr = Register::Address<0x40067040,0xff00ff00,0x00000000,unsigned>;
        ///Match 0 Value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> match0{}; 
        ///Match 1 Value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> match1{}; 
    }
    namespace Lpi2c1Mccr0{    ///<Master Clock Configuration Register 0
        using Addr = Register::Address<0x40067048,0xc0c0c0c0,0x00000000,unsigned>;
        ///Clock Low Period
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,unsigned> clklo{}; 
        ///Clock High Period
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,8),Register::ReadWriteAccess,unsigned> clkhi{}; 
        ///Setup Hold Delay
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(21,16),Register::ReadWriteAccess,unsigned> sethold{}; 
        ///Data Valid Delay
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,24),Register::ReadWriteAccess,unsigned> datavd{}; 
    }
    namespace Lpi2c1Mccr1{    ///<Master Clock Configuration Register 1
        using Addr = Register::Address<0x40067050,0xc0c0c0c0,0x00000000,unsigned>;
        ///Clock Low Period
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,unsigned> clklo{}; 
        ///Clock High Period
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,8),Register::ReadWriteAccess,unsigned> clkhi{}; 
        ///Setup Hold Delay
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(21,16),Register::ReadWriteAccess,unsigned> sethold{}; 
        ///Data Valid Delay
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,24),Register::ReadWriteAccess,unsigned> datavd{}; 
    }
    namespace Lpi2c1Mfcr{    ///<Master FIFO Control Register
        using Addr = Register::Address<0x40067058,0xfffcfffc,0x00000000,unsigned>;
        ///Transmit FIFO Watermark
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,0),Register::ReadWriteAccess,unsigned> txwater{}; 
        ///Receive FIFO Watermark
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,16),Register::ReadWriteAccess,unsigned> rxwater{}; 
    }
    namespace Lpi2c1Mfsr{    ///<Master FIFO Status Register
        using Addr = Register::Address<0x4006705c,0xfff8fff8,0x00000000,unsigned>;
        ///Transmit FIFO Count
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> txcount{}; 
        ///Receive FIFO Count
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(18,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxcount{}; 
    }
    namespace Lpi2c1Mtdr{    ///<Master Transmit Data Register
        using Addr = Register::Address<0x40067060,0xfffff800,0x00000000,unsigned>;
        ///Transmit Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> data{}; 
        ///Command Data
        enum class CmdVal : unsigned {
            v000=0x00000000,     ///<Transmit DATA[7:0].
            v001=0x00000001,     ///<Receive (DATA[7:0] + 1) bytes.
            v010=0x00000002,     ///<Generate STOP condition.
            v011=0x00000003,     ///<Receive and discard (DATA[7:0] + 1) bytes.
            v100=0x00000004,     ///<Generate (repeated) START and transmit address in DATA[7:0].
            v101=0x00000005,     ///<Generate (repeated) START and transmit address in DATA[7:0]. This transfer expects a NACK to be returned.
            v110=0x00000006,     ///<Generate (repeated) START and transmit address in DATA[7:0] using high speed mode.
            v111=0x00000007,     ///<Generate (repeated) START and transmit address in DATA[7:0] using high speed mode. This transfer expects a NACK to be returned.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CmdVal> cmd{}; 
        namespace CmdValC{
            constexpr Register::FieldValue<decltype(cmd)::Type,CmdVal::v000> v000{};
            constexpr Register::FieldValue<decltype(cmd)::Type,CmdVal::v001> v001{};
            constexpr Register::FieldValue<decltype(cmd)::Type,CmdVal::v010> v010{};
            constexpr Register::FieldValue<decltype(cmd)::Type,CmdVal::v011> v011{};
            constexpr Register::FieldValue<decltype(cmd)::Type,CmdVal::v100> v100{};
            constexpr Register::FieldValue<decltype(cmd)::Type,CmdVal::v101> v101{};
            constexpr Register::FieldValue<decltype(cmd)::Type,CmdVal::v110> v110{};
            constexpr Register::FieldValue<decltype(cmd)::Type,CmdVal::v111> v111{};
        }
    }
    namespace Lpi2c1Mrdr{    ///<Master Receive Data Register
        using Addr = Register::Address<0x40067070,0xffffbf00,0x00000000,unsigned>;
        ///Receive Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> data{}; 
        ///RX Empty
        enum class RxemptyVal : unsigned {
            v0=0x00000000,     ///<Receive FIFO is not empty.
            v1=0x00000001,     ///<Receive FIFO is empty.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,14),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RxemptyVal> rxempty{}; 
        namespace RxemptyValC{
            constexpr Register::FieldValue<decltype(rxempty)::Type,RxemptyVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rxempty)::Type,RxemptyVal::v1> v1{};
        }
    }
    namespace Lpi2c1Scr{    ///<Slave Control Register
        using Addr = Register::Address<0x40067110,0xffffffcc,0x00000000,unsigned>;
        ///Slave Enable
        enum class SenVal : unsigned {
            v0=0x00000000,     ///<Slave mode is disabled.
            v1=0x00000001,     ///<Slave mode is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,SenVal> sen{}; 
        namespace SenValC{
            constexpr Register::FieldValue<decltype(sen)::Type,SenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sen)::Type,SenVal::v1> v1{};
        }
        ///Software Reset
        enum class RstVal : unsigned {
            v0=0x00000000,     ///<Slave logic is not reset.
            v1=0x00000001,     ///<Slave logic is reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,RstVal> rst{}; 
        namespace RstValC{
            constexpr Register::FieldValue<decltype(rst)::Type,RstVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rst)::Type,RstVal::v1> v1{};
        }
        ///Filter Enable
        enum class FiltenVal : unsigned {
            v0=0x00000000,     ///<Disable digital filter and output delay counter for slave mode.
            v1=0x00000001,     ///<Enable digital filter and output delay counter for slave mode.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,4),Register::ReadWriteAccess,FiltenVal> filten{}; 
        namespace FiltenValC{
            constexpr Register::FieldValue<decltype(filten)::Type,FiltenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(filten)::Type,FiltenVal::v1> v1{};
        }
        ///Filter Doze Enable
        enum class FiltdzVal : unsigned {
            v0=0x00000000,     ///<Filter remains enabled in Doze mode.
            v1=0x00000001,     ///<Filter is disabled in Doze mode.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,FiltdzVal> filtdz{}; 
        namespace FiltdzValC{
            constexpr Register::FieldValue<decltype(filtdz)::Type,FiltdzVal::v0> v0{};
            constexpr Register::FieldValue<decltype(filtdz)::Type,FiltdzVal::v1> v1{};
        }
    }
    namespace Lpi2c1Ssr{    ///<Slave Status Register
        using Addr = Register::Address<0x40067114,0xfcff00f0,0x00000000,unsigned>;
        ///Transmit Data Flag
        enum class TdfVal : unsigned {
            v0=0x00000000,     ///<Transmit data not requested.
            v1=0x00000001,     ///<Transmit data is requested.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,TdfVal> tdf{}; 
        namespace TdfValC{
            constexpr Register::FieldValue<decltype(tdf)::Type,TdfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tdf)::Type,TdfVal::v1> v1{};
        }
        ///Receive Data Flag
        enum class RdfVal : unsigned {
            v0=0x00000000,     ///<Receive Data is not ready.
            v1=0x00000001,     ///<Receive data is ready.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RdfVal> rdf{}; 
        namespace RdfValC{
            constexpr Register::FieldValue<decltype(rdf)::Type,RdfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rdf)::Type,RdfVal::v1> v1{};
        }
        ///Address Valid Flag
        enum class AvfVal : unsigned {
            v0=0x00000000,     ///<Address Status Register is not valid.
            v1=0x00000001,     ///<Address Status Register is valid.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,AvfVal> avf{}; 
        namespace AvfValC{
            constexpr Register::FieldValue<decltype(avf)::Type,AvfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(avf)::Type,AvfVal::v1> v1{};
        }
        ///Transmit ACK Flag
        enum class TafVal : unsigned {
            v0=0x00000000,     ///<Transmit ACK/NACK is not required.
            v1=0x00000001,     ///<Transmit ACK/NACK is required.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,TafVal> taf{}; 
        namespace TafValC{
            constexpr Register::FieldValue<decltype(taf)::Type,TafVal::v0> v0{};
            constexpr Register::FieldValue<decltype(taf)::Type,TafVal::v1> v1{};
        }
        ///Repeated Start Flag
        enum class RsfVal : unsigned {
            v0=0x00000000,     ///<Slave has not detected a Repeated START condition.
            v1=0x00000001,     ///<Slave has detected a Repeated START condition.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::ReadWriteAccess,RsfVal> rsf{}; 
        namespace RsfValC{
            constexpr Register::FieldValue<decltype(rsf)::Type,RsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rsf)::Type,RsfVal::v1> v1{};
        }
        ///STOP Detect Flag
        enum class SdfVal : unsigned {
            v0=0x00000000,     ///<Slave has not detected a STOP condition.
            v1=0x00000001,     ///<Slave has detected a STOP condition.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,9),Register::ReadWriteAccess,SdfVal> sdf{}; 
        namespace SdfValC{
            constexpr Register::FieldValue<decltype(sdf)::Type,SdfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sdf)::Type,SdfVal::v1> v1{};
        }
        ///Bit Error Flag
        enum class BefVal : unsigned {
            v0=0x00000000,     ///<Slave has not detected a bit error.
            v1=0x00000001,     ///<Slave has detected a bit error.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,10),Register::ReadWriteAccess,BefVal> bef{}; 
        namespace BefValC{
            constexpr Register::FieldValue<decltype(bef)::Type,BefVal::v0> v0{};
            constexpr Register::FieldValue<decltype(bef)::Type,BefVal::v1> v1{};
        }
        ///FIFO Error Flag
        enum class FefVal : unsigned {
            v0=0x00000000,     ///<FIFO underflow or overflow not detected.
            v1=0x00000001,     ///<FIFO underflow or overflow detected.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,FefVal> fef{}; 
        namespace FefValC{
            constexpr Register::FieldValue<decltype(fef)::Type,FefVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fef)::Type,FefVal::v1> v1{};
        }
        ///Address Match 0 Flag
        enum class Am0fVal : unsigned {
            v0=0x00000000,     ///<Have not received ADDR0 matching address.
            v1=0x00000001,     ///<Have received ADDR0 matching address.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,12),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,Am0fVal> am0f{}; 
        namespace Am0fValC{
            constexpr Register::FieldValue<decltype(am0f)::Type,Am0fVal::v0> v0{};
            constexpr Register::FieldValue<decltype(am0f)::Type,Am0fVal::v1> v1{};
        }
        ///Address Match 1 Flag
        enum class Am1fVal : unsigned {
            v0=0x00000000,     ///<Have not received ADDR1 or ADDR0/ADDR1 range matching address.
            v1=0x00000001,     ///<Have received ADDR1 or ADDR0/ADDR1 range matching address.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,13),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,Am1fVal> am1f{}; 
        namespace Am1fValC{
            constexpr Register::FieldValue<decltype(am1f)::Type,Am1fVal::v0> v0{};
            constexpr Register::FieldValue<decltype(am1f)::Type,Am1fVal::v1> v1{};
        }
        ///General Call Flag
        enum class GcfVal : unsigned {
            v0=0x00000000,     ///<Slave has not detected the General Call Address or General Call Address disabled.
            v1=0x00000001,     ///<Slave has detected the General Call Address.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,14),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,GcfVal> gcf{}; 
        namespace GcfValC{
            constexpr Register::FieldValue<decltype(gcf)::Type,GcfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(gcf)::Type,GcfVal::v1> v1{};
        }
        ///SMBus Alert Response Flag
        enum class SarfVal : unsigned {
            v0=0x00000000,     ///<SMBus Alert Response disabled or not detected.
            v1=0x00000001,     ///<SMBus Alert Response enabled and detected.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,SarfVal> sarf{}; 
        namespace SarfValC{
            constexpr Register::FieldValue<decltype(sarf)::Type,SarfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sarf)::Type,SarfVal::v1> v1{};
        }
        ///Slave Busy Flag
        enum class SbfVal : unsigned {
            v0=0x00000000,     ///<I2C Slave is idle.
            v1=0x00000001,     ///<I2C Slave is busy.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,SbfVal> sbf{}; 
        namespace SbfValC{
            constexpr Register::FieldValue<decltype(sbf)::Type,SbfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sbf)::Type,SbfVal::v1> v1{};
        }
        ///Bus Busy Flag
        enum class BbfVal : unsigned {
            v0=0x00000000,     ///<I2C Bus is idle.
            v1=0x00000001,     ///<I2C Bus is busy.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,BbfVal> bbf{}; 
        namespace BbfValC{
            constexpr Register::FieldValue<decltype(bbf)::Type,BbfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(bbf)::Type,BbfVal::v1> v1{};
        }
    }
    namespace Lpi2c1Sier{    ///<Slave Interrupt Enable Register
        using Addr = Register::Address<0x40067118,0xffff00f0,0x00000000,unsigned>;
        ///Transmit Data Interrupt Enable
        enum class TdieVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,TdieVal> tdie{}; 
        namespace TdieValC{
            constexpr Register::FieldValue<decltype(tdie)::Type,TdieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tdie)::Type,TdieVal::v1> v1{};
        }
        ///Receive Data Interrupt Enable
        enum class RdieVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,RdieVal> rdie{}; 
        namespace RdieValC{
            constexpr Register::FieldValue<decltype(rdie)::Type,RdieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rdie)::Type,RdieVal::v1> v1{};
        }
        ///Address Valid Interrupt Enable
        enum class AvieVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,AvieVal> avie{}; 
        namespace AvieValC{
            constexpr Register::FieldValue<decltype(avie)::Type,AvieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(avie)::Type,AvieVal::v1> v1{};
        }
        ///Transmit ACK Interrupt Enable
        enum class TaieVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,TaieVal> taie{}; 
        namespace TaieValC{
            constexpr Register::FieldValue<decltype(taie)::Type,TaieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(taie)::Type,TaieVal::v1> v1{};
        }
        ///Repeated Start Interrupt Enable
        enum class RsieVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::ReadWriteAccess,RsieVal> rsie{}; 
        namespace RsieValC{
            constexpr Register::FieldValue<decltype(rsie)::Type,RsieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rsie)::Type,RsieVal::v1> v1{};
        }
        ///STOP Detect Interrupt Enable
        enum class SdieVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,9),Register::ReadWriteAccess,SdieVal> sdie{}; 
        namespace SdieValC{
            constexpr Register::FieldValue<decltype(sdie)::Type,SdieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sdie)::Type,SdieVal::v1> v1{};
        }
        ///Bit Error Interrupt Enable
        enum class BeieVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,10),Register::ReadWriteAccess,BeieVal> beie{}; 
        namespace BeieValC{
            constexpr Register::FieldValue<decltype(beie)::Type,BeieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(beie)::Type,BeieVal::v1> v1{};
        }
        ///FIFO Error Interrupt Enable
        enum class FeieVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,FeieVal> feie{}; 
        namespace FeieValC{
            constexpr Register::FieldValue<decltype(feie)::Type,FeieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(feie)::Type,FeieVal::v1> v1{};
        }
        ///Address Match 0 Interrupt Enable
        enum class Am0ieVal : unsigned {
            v0=0x00000000,     ///<Interrupt enabled.
            v1=0x00000001,     ///<Interrupt disabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,12),Register::ReadWriteAccess,Am0ieVal> am0ie{}; 
        namespace Am0ieValC{
            constexpr Register::FieldValue<decltype(am0ie)::Type,Am0ieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(am0ie)::Type,Am0ieVal::v1> v1{};
        }
        ///Address Match 1 Interrupt Enable
        enum class Am1fVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,13),Register::ReadWriteAccess,Am1fVal> am1f{}; 
        namespace Am1fValC{
            constexpr Register::FieldValue<decltype(am1f)::Type,Am1fVal::v0> v0{};
            constexpr Register::FieldValue<decltype(am1f)::Type,Am1fVal::v1> v1{};
        }
        ///General Call Interrupt Enable
        enum class GcieVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,14),Register::ReadWriteAccess,GcieVal> gcie{}; 
        namespace GcieValC{
            constexpr Register::FieldValue<decltype(gcie)::Type,GcieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(gcie)::Type,GcieVal::v1> v1{};
        }
        ///SMBus Alert Response Interrupt Enable
        enum class SarieVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,SarieVal> sarie{}; 
        namespace SarieValC{
            constexpr Register::FieldValue<decltype(sarie)::Type,SarieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sarie)::Type,SarieVal::v1> v1{};
        }
    }
    namespace Lpi2c1Sder{    ///<Slave DMA Enable Register
        using Addr = Register::Address<0x4006711c,0xfffffff8,0x00000000,unsigned>;
        ///Transmit Data DMA Enable
        enum class TddeVal : unsigned {
            v0=0x00000000,     ///<DMA request disabled.
            v1=0x00000001,     ///<DMA request enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,TddeVal> tdde{}; 
        namespace TddeValC{
            constexpr Register::FieldValue<decltype(tdde)::Type,TddeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tdde)::Type,TddeVal::v1> v1{};
        }
        ///Receive Data DMA Enable
        enum class RddeVal : unsigned {
            v0=0x00000000,     ///<DMA request disabled.
            v1=0x00000001,     ///<DMA request enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,RddeVal> rdde{}; 
        namespace RddeValC{
            constexpr Register::FieldValue<decltype(rdde)::Type,RddeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rdde)::Type,RddeVal::v1> v1{};
        }
        ///Address Valid DMA Enable
        enum class AvdeVal : unsigned {
            v0=0x00000000,     ///<DMA request disabled.
            v1=0x00000001,     ///<DMA request enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,AvdeVal> avde{}; 
        namespace AvdeValC{
            constexpr Register::FieldValue<decltype(avde)::Type,AvdeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(avde)::Type,AvdeVal::v1> v1{};
        }
    }
    namespace Lpi2c1Scfgr1{    ///<Slave Configuration Register 1
        using Addr = Register::Address<0x40067124,0xfff8c0f0,0x00000000,unsigned>;
        ///Address SCL Stall
        enum class AdrstallVal : unsigned {
            v0=0x00000000,     ///<Clock stretching disabled.
            v1=0x00000001,     ///<Clock stretching enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,AdrstallVal> adrstall{}; 
        namespace AdrstallValC{
            constexpr Register::FieldValue<decltype(adrstall)::Type,AdrstallVal::v0> v0{};
            constexpr Register::FieldValue<decltype(adrstall)::Type,AdrstallVal::v1> v1{};
        }
        ///RX SCL Stall
        enum class RxstallVal : unsigned {
            v0=0x00000000,     ///<Clock stretching disabled.
            v1=0x00000001,     ///<Clock stretching enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,RxstallVal> rxstall{}; 
        namespace RxstallValC{
            constexpr Register::FieldValue<decltype(rxstall)::Type,RxstallVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rxstall)::Type,RxstallVal::v1> v1{};
        }
        ///TX Data SCL Stall
        enum class TxdstallVal : unsigned {
            v0=0x00000000,     ///<Clock stretching disabled.
            v1=0x00000001,     ///<Clock stretching enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,TxdstallVal> txdstall{}; 
        namespace TxdstallValC{
            constexpr Register::FieldValue<decltype(txdstall)::Type,TxdstallVal::v0> v0{};
            constexpr Register::FieldValue<decltype(txdstall)::Type,TxdstallVal::v1> v1{};
        }
        ///ACK SCL Stall
        enum class AckstallVal : unsigned {
            v0=0x00000000,     ///<Clock stretching disabled.
            v1=0x00000001,     ///<Clock stretching enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,AckstallVal> ackstall{}; 
        namespace AckstallValC{
            constexpr Register::FieldValue<decltype(ackstall)::Type,AckstallVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ackstall)::Type,AckstallVal::v1> v1{};
        }
        ///General Call Enable
        enum class GcenVal : unsigned {
            v0=0x00000000,     ///<General Call address is disabled.
            v1=0x00000001,     ///<General call address is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::ReadWriteAccess,GcenVal> gcen{}; 
        namespace GcenValC{
            constexpr Register::FieldValue<decltype(gcen)::Type,GcenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(gcen)::Type,GcenVal::v1> v1{};
        }
        ///SMBus Alert Enable
        enum class SaenVal : unsigned {
            v0=0x00000000,     ///<Disables match on SMBus Alert.
            v1=0x00000001,     ///<Enables match on SMBus Alert.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,9),Register::ReadWriteAccess,SaenVal> saen{}; 
        namespace SaenValC{
            constexpr Register::FieldValue<decltype(saen)::Type,SaenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(saen)::Type,SaenVal::v1> v1{};
        }
        ///Transmit Flag Configuration
        enum class TxcfgVal : unsigned {
            v0=0x00000000,     ///<Transmit Data Flag will only assert during a slave-transmit transfer when the transmit data register is empty.
            v1=0x00000001,     ///<Transmit Data Flag will assert whenever the transmit data register is empty.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,10),Register::ReadWriteAccess,TxcfgVal> txcfg{}; 
        namespace TxcfgValC{
            constexpr Register::FieldValue<decltype(txcfg)::Type,TxcfgVal::v0> v0{};
            constexpr Register::FieldValue<decltype(txcfg)::Type,TxcfgVal::v1> v1{};
        }
        ///Receive Data Configuration
        enum class RxcfgVal : unsigned {
            v0=0x00000000,     ///<Reading the receive data register will return receive data and clear the receive data flag.
            v1=0x00000001,     ///<Reading the receive data register when the address valid flag is set will return the address status register and clear the address valid flag. Reading the receive data register when the address valid flag is clear will return receive data and clear the receive data flag.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,RxcfgVal> rxcfg{}; 
        namespace RxcfgValC{
            constexpr Register::FieldValue<decltype(rxcfg)::Type,RxcfgVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rxcfg)::Type,RxcfgVal::v1> v1{};
        }
        ///Ignore NACK
        enum class IgnackVal : unsigned {
            v0=0x00000000,     ///<Slave will end transfer when NACK detected.
            v1=0x00000001,     ///<Slave will not end transfer when NACK detected.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,12),Register::ReadWriteAccess,IgnackVal> ignack{}; 
        namespace IgnackValC{
            constexpr Register::FieldValue<decltype(ignack)::Type,IgnackVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ignack)::Type,IgnackVal::v1> v1{};
        }
        ///High Speed Mode Enable
        enum class HsmenVal : unsigned {
            v0=0x00000000,     ///<Disables detection of Hs-mode master code.
            v1=0x00000001,     ///<Enables detection of Hs-mode master code.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,13),Register::ReadWriteAccess,HsmenVal> hsmen{}; 
        namespace HsmenValC{
            constexpr Register::FieldValue<decltype(hsmen)::Type,HsmenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(hsmen)::Type,HsmenVal::v1> v1{};
        }
        ///Address Configuration
        enum class AddrcfgVal : unsigned {
            v000=0x00000000,     ///<Address match 0 (7-bit).
            v001=0x00000001,     ///<Address match 0 (10-bit).
            v010=0x00000002,     ///<Address match 0 (7-bit) or Address match 1 (7-bit).
            v011=0x00000003,     ///<Address match 0 (10-bit) or Address match 1 (10-bit).
            v100=0x00000004,     ///<Address match 0 (7-bit) or Address match 1 (10-bit).
            v101=0x00000005,     ///<Address match 0 (10-bit) or Address match 1 (7-bit).
            v110=0x00000006,     ///<From Address match 0 (7-bit) to Address match 1 (7-bit).
            v111=0x00000007,     ///<From Address match 0 (10-bit) to Address match 1 (10-bit).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(18,16),Register::ReadWriteAccess,AddrcfgVal> addrcfg{}; 
        namespace AddrcfgValC{
            constexpr Register::FieldValue<decltype(addrcfg)::Type,AddrcfgVal::v000> v000{};
            constexpr Register::FieldValue<decltype(addrcfg)::Type,AddrcfgVal::v001> v001{};
            constexpr Register::FieldValue<decltype(addrcfg)::Type,AddrcfgVal::v010> v010{};
            constexpr Register::FieldValue<decltype(addrcfg)::Type,AddrcfgVal::v011> v011{};
            constexpr Register::FieldValue<decltype(addrcfg)::Type,AddrcfgVal::v100> v100{};
            constexpr Register::FieldValue<decltype(addrcfg)::Type,AddrcfgVal::v101> v101{};
            constexpr Register::FieldValue<decltype(addrcfg)::Type,AddrcfgVal::v110> v110{};
            constexpr Register::FieldValue<decltype(addrcfg)::Type,AddrcfgVal::v111> v111{};
        }
    }
    namespace Lpi2c1Scfgr2{    ///<Slave Configuration Register 2
        using Addr = Register::Address<0x40067128,0xf0f0c0f0,0x00000000,unsigned>;
        ///Clock Hold Time
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::ReadWriteAccess,unsigned> clkhold{}; 
        ///Data Valid Delay
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,8),Register::ReadWriteAccess,unsigned> datavd{}; 
        ///Glitch Filter SCL
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,unsigned> filtscl{}; 
        ///Glitch Filter SDA
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,24),Register::ReadWriteAccess,unsigned> filtsda{}; 
    }
    namespace Lpi2c1Samr{    ///<Slave Address Match Register
        using Addr = Register::Address<0x40067140,0xf801f801,0x00000000,unsigned>;
        ///Address 0 Value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,1),Register::ReadWriteAccess,unsigned> addr0{}; 
        ///Address 1 Value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,17),Register::ReadWriteAccess,unsigned> addr1{}; 
    }
    namespace Lpi2c1Sasr{    ///<Slave Address Status Register
        using Addr = Register::Address<0x40067150,0xffffb800,0x00000000,unsigned>;
        ///Received Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> raddr{}; 
        ///Address Not Valid
        enum class AnvVal : unsigned {
            v0=0x00000000,     ///<RADDR is valid.
            v1=0x00000001,     ///<RADDR is not valid.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,14),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,AnvVal> anv{}; 
        namespace AnvValC{
            constexpr Register::FieldValue<decltype(anv)::Type,AnvVal::v0> v0{};
            constexpr Register::FieldValue<decltype(anv)::Type,AnvVal::v1> v1{};
        }
    }
    namespace Lpi2c1Star{    ///<Slave Transmit ACK Register
        using Addr = Register::Address<0x40067154,0xfffffffe,0x00000000,unsigned>;
        ///Transmit NACK
        enum class TxnackVal : unsigned {
            v0=0x00000000,     ///<Transmit ACK for received word.
            v1=0x00000001,     ///<Transmit NACK for received word.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,TxnackVal> txnack{}; 
        namespace TxnackValC{
            constexpr Register::FieldValue<decltype(txnack)::Type,TxnackVal::v0> v0{};
            constexpr Register::FieldValue<decltype(txnack)::Type,TxnackVal::v1> v1{};
        }
    }
    namespace Lpi2c1Stdr{    ///<Slave Transmit Data Register
        using Addr = Register::Address<0x40067160,0xffffff00,0x00000000,unsigned>;
        ///Transmit Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> data{}; 
    }
    namespace Lpi2c1Srdr{    ///<Slave Receive Data Register
        using Addr = Register::Address<0x40067170,0xffff3f00,0x00000000,unsigned>;
        ///Receive Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> data{}; 
        ///RX Empty
        enum class RxemptyVal : unsigned {
            v0=0x00000000,     ///<The Receive Data Register is not empty.
            v1=0x00000001,     ///<The Receive Data Register is empty.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,14),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RxemptyVal> rxempty{}; 
        namespace RxemptyValC{
            constexpr Register::FieldValue<decltype(rxempty)::Type,RxemptyVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rxempty)::Type,RxemptyVal::v1> v1{};
        }
        ///Start Of Frame
        enum class SofVal : unsigned {
            v0=0x00000000,     ///<Indicates this is not the first data word since a (repeated) START or STOP condition.
            v1=0x00000001,     ///<Indicates this is the first data word since a (repeated) START or STOP condition.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,SofVal> sof{}; 
        namespace SofValC{
            constexpr Register::FieldValue<decltype(sof)::Type,SofVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sof)::Type,SofVal::v1> v1{};
        }
    }
}
