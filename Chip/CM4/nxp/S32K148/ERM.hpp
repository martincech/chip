#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//ERM
    namespace ErmCr0{    ///<ERM Configuration Register 0
        using Addr = Register::Address<0x40018000,0x33ffffff,0x00000000,unsigned>;
        ///ENCIE1
        enum class Encie1Val : unsigned {
            v0=0x00000000,     ///<Interrupt notification of Memory 1 non-correctable error events is disabled.
            v1=0x00000001,     ///<Interrupt notification of Memory 1 non-correctable error events is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,Encie1Val> encie1{}; 
        namespace Encie1ValC{
            constexpr Register::FieldValue<decltype(encie1)::Type,Encie1Val::v0> v0{};
            constexpr Register::FieldValue<decltype(encie1)::Type,Encie1Val::v1> v1{};
        }
        ///ESCIE1
        enum class Escie1Val : unsigned {
            v0=0x00000000,     ///<Interrupt notification of Memory 1 single-bit correction events is disabled.
            v1=0x00000001,     ///<Interrupt notification of Memory 1 single-bit correction events is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,Escie1Val> escie1{}; 
        namespace Escie1ValC{
            constexpr Register::FieldValue<decltype(escie1)::Type,Escie1Val::v0> v0{};
            constexpr Register::FieldValue<decltype(escie1)::Type,Escie1Val::v1> v1{};
        }
        ///ENCIE0
        enum class Encie0Val : unsigned {
            v0=0x00000000,     ///<Interrupt notification of Memory 0 non-correctable error events is disabled.
            v1=0x00000001,     ///<Interrupt notification of Memory 0 non-correctable error events is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,Encie0Val> encie0{}; 
        namespace Encie0ValC{
            constexpr Register::FieldValue<decltype(encie0)::Type,Encie0Val::v0> v0{};
            constexpr Register::FieldValue<decltype(encie0)::Type,Encie0Val::v1> v1{};
        }
        ///ESCIE0
        enum class Escie0Val : unsigned {
            v0=0x00000000,     ///<Interrupt notification of Memory 0 single-bit correction events is disabled.
            v1=0x00000001,     ///<Interrupt notification of Memory 0 single-bit correction events is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,Escie0Val> escie0{}; 
        namespace Escie0ValC{
            constexpr Register::FieldValue<decltype(escie0)::Type,Escie0Val::v0> v0{};
            constexpr Register::FieldValue<decltype(escie0)::Type,Escie0Val::v1> v1{};
        }
    }
    namespace ErmSr0{    ///<ERM Status Register 0
        using Addr = Register::Address<0x40018010,0x33ffffff,0x00000000,unsigned>;
        ///NCE1
        enum class Nce1Val : unsigned {
            v0=0x00000000,     ///<No non-correctable error event on Memory 1 detected
            v1=0x00000001,     ///<Non-correctable error event on Memory 1 detected
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,Nce1Val> nce1{}; 
        namespace Nce1ValC{
            constexpr Register::FieldValue<decltype(nce1)::Type,Nce1Val::v0> v0{};
            constexpr Register::FieldValue<decltype(nce1)::Type,Nce1Val::v1> v1{};
        }
        ///SBC1
        enum class Sbc1Val : unsigned {
            v0=0x00000000,     ///<No single-bit correction event on Memory 1 detected
            v1=0x00000001,     ///<Single-bit correction event on Memory 1 detected
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,Sbc1Val> sbc1{}; 
        namespace Sbc1ValC{
            constexpr Register::FieldValue<decltype(sbc1)::Type,Sbc1Val::v0> v0{};
            constexpr Register::FieldValue<decltype(sbc1)::Type,Sbc1Val::v1> v1{};
        }
        ///NCE0
        enum class Nce0Val : unsigned {
            v0=0x00000000,     ///<No non-correctable error event on Memory 0 detected
            v1=0x00000001,     ///<Non-correctable error event on Memory 0 detected
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,Nce0Val> nce0{}; 
        namespace Nce0ValC{
            constexpr Register::FieldValue<decltype(nce0)::Type,Nce0Val::v0> v0{};
            constexpr Register::FieldValue<decltype(nce0)::Type,Nce0Val::v1> v1{};
        }
        ///SBC0
        enum class Sbc0Val : unsigned {
            v0=0x00000000,     ///<No single-bit correction event on Memory 0 detected
            v1=0x00000001,     ///<Single-bit correction event on Memory 0 detected
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,Sbc0Val> sbc0{}; 
        namespace Sbc0ValC{
            constexpr Register::FieldValue<decltype(sbc0)::Type,Sbc0Val::v0> v0{};
            constexpr Register::FieldValue<decltype(sbc0)::Type,Sbc0Val::v1> v1{};
        }
    }
    namespace ErmEar0{    ///<ERM Memory n Error Address Register
        using Addr = Register::Address<0x40018100,0x00000000,0x00000000,unsigned>;
        ///EAR
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> ear{}; 
    }
    namespace ErmEar1{    ///<ERM Memory n Error Address Register
        using Addr = Register::Address<0x40018110,0x00000000,0x00000000,unsigned>;
        ///EAR
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> ear{}; 
    }
}
