#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//Analog-to-Digital Converter
    namespace Adc1Sc1a{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x40027000,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1b{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x40027004,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1c{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x40027008,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1d{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x4002700c,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1e{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x40027010,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1f{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x40027014,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1g{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x40027018,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1h{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x4002701c,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1i{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x40027020,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1j{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x40027024,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1k{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x40027028,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1l{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x4002702c,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1m{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x40027030,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1n{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x40027034,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1o{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x40027038,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1p{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x4002703c,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Cfg1{    ///<ADC Configuration Register 1
        using Addr = Register::Address<0x40027040,0xfffffe90,0x00000000,unsigned>;
        ///Input Clock Select
        enum class AdiclkVal : unsigned {
            v00=0x00000000,     ///<Alternate clock 1 (ADC_ALTCLK1)
            v01=0x00000001,     ///<Alternate clock 2 (ADC_ALTCLK2)
            v10=0x00000002,     ///<Alternate clock 3 (ADC_ALTCLK3)
            v11=0x00000003,     ///<Alternate clock 4 (ADC_ALTCLK4)
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,0),Register::ReadWriteAccess,AdiclkVal> adiclk{}; 
        namespace AdiclkValC{
            constexpr Register::FieldValue<decltype(adiclk)::Type,AdiclkVal::v00> v00{};
            constexpr Register::FieldValue<decltype(adiclk)::Type,AdiclkVal::v01> v01{};
            constexpr Register::FieldValue<decltype(adiclk)::Type,AdiclkVal::v10> v10{};
            constexpr Register::FieldValue<decltype(adiclk)::Type,AdiclkVal::v11> v11{};
        }
        ///Conversion mode selection
        enum class ModeVal : unsigned {
            v00=0x00000000,     ///<8-bit conversion.
            v01=0x00000001,     ///<12-bit conversion.
            v10=0x00000002,     ///<10-bit conversion.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,2),Register::ReadWriteAccess,ModeVal> mode{}; 
        namespace ModeValC{
            constexpr Register::FieldValue<decltype(mode)::Type,ModeVal::v00> v00{};
            constexpr Register::FieldValue<decltype(mode)::Type,ModeVal::v01> v01{};
            constexpr Register::FieldValue<decltype(mode)::Type,ModeVal::v10> v10{};
        }
        ///Clock Divide Select
        enum class AdivVal : unsigned {
            v00=0x00000000,     ///<The divide ratio is 1 and the clock rate is input clock.
            v01=0x00000001,     ///<The divide ratio is 2 and the clock rate is (input clock)/2.
            v10=0x00000002,     ///<The divide ratio is 4 and the clock rate is (input clock)/4.
            v11=0x00000003,     ///<The divide ratio is 8 and the clock rate is (input clock)/8.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,5),Register::ReadWriteAccess,AdivVal> adiv{}; 
        namespace AdivValC{
            constexpr Register::FieldValue<decltype(adiv)::Type,AdivVal::v00> v00{};
            constexpr Register::FieldValue<decltype(adiv)::Type,AdivVal::v01> v01{};
            constexpr Register::FieldValue<decltype(adiv)::Type,AdivVal::v10> v10{};
            constexpr Register::FieldValue<decltype(adiv)::Type,AdivVal::v11> v11{};
        }
        ///Clear Latch Trigger in Trigger Handler Block
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> clrltrg{}; 
    }
    namespace Adc1Cfg2{    ///<ADC Configuration Register 2
        using Addr = Register::Address<0x40027044,0xffffff00,0x00000000,unsigned>;
        ///Sample Time Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> smplts{}; 
    }
    namespace Adc1Ra{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x40027048,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Rb{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x4002704c,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Rc{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x40027050,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Rd{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x40027054,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Re{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x40027058,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Rf{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x4002705c,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Rg{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x40027060,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Rh{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x40027064,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Ri{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x40027068,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Rj{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x4002706c,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Rk{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x40027070,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Rl{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x40027074,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Rm{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x40027078,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Rn{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x4002707c,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Ro{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x40027080,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Rp{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x40027084,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Sc2{    ///<Status and Control Register 2
        using Addr = Register::Address<0x40027090,0xf0f09f00,0x00000000,unsigned>;
        ///Voltage Reference Selection
        enum class RefselVal : unsigned {
            def=0x00000000,     ///<Default voltage reference pin pair, that is, external pins VREFH and VREFL
            alternate=0x00000001,     ///<Alternate reference voltage, that is, VALTH. This voltage may be additional external pin or internal source depending on the MCU configuration. See the chip configuration information for details specific to this MCU.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,0),Register::ReadWriteAccess,RefselVal> refsel{}; 
        namespace RefselValC{
            constexpr Register::FieldValue<decltype(refsel)::Type,RefselVal::def> def{};
            constexpr Register::FieldValue<decltype(refsel)::Type,RefselVal::alternate> alternate{};
        }
        ///DMA Enable
        enum class DmaenVal : unsigned {
            disabled=0x00000000,     ///<DMA is disabled.
            enabled=0x00000001,     ///<DMA is enabled and will assert the ADC DMA request during an ADC conversion complete event , which is indicated when any SC1n[COCO] flag is asserted.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,DmaenVal> dmaen{}; 
        namespace DmaenValC{
            constexpr Register::FieldValue<decltype(dmaen)::Type,DmaenVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(dmaen)::Type,DmaenVal::enabled> enabled{};
        }
        ///Compare Function Range Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,unsigned> acren{}; 
        ///Compare Function Greater Than Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,4),Register::ReadWriteAccess,unsigned> acfgt{}; 
        ///Compare Function Enable
        enum class AcfeVal : unsigned {
            disabled=0x00000000,     ///<Compare function disabled.
            enabled=0x00000001,     ///<Compare function enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,AcfeVal> acfe{}; 
        namespace AcfeValC{
            constexpr Register::FieldValue<decltype(acfe)::Type,AcfeVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(acfe)::Type,AcfeVal::enabled> enabled{};
        }
        ///Conversion Trigger Select
        enum class AdtrgVal : unsigned {
            v0=0x00000000,     ///<Software trigger selected.
            v1=0x00000001,     ///<Hardware trigger selected.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AdtrgVal> adtrg{}; 
        namespace AdtrgValC{
            constexpr Register::FieldValue<decltype(adtrg)::Type,AdtrgVal::v0> v0{};
            constexpr Register::FieldValue<decltype(adtrg)::Type,AdtrgVal::v1> v1{};
        }
        ///Conversion Active
        enum class AdactVal : unsigned {
            v0=0x00000000,     ///<Conversion not in progress.
            v1=0x00000001,     ///<Conversion in progress.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,AdactVal> adact{}; 
        namespace AdactValC{
            constexpr Register::FieldValue<decltype(adact)::Type,AdactVal::v0> v0{};
            constexpr Register::FieldValue<decltype(adact)::Type,AdactVal::v1> v1{};
        }
        ///Trigger Process Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,13),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> trgprnum{}; 
        ///Trigger Status
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> trgstlat{}; 
        ///Error in Multiplexed Trigger Request
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> trgsterr{}; 
    }
    namespace Adc1Sc3{    ///<Status and Control Register 3
        using Addr = Register::Address<0x40027094,0xffffff70,0x00000000,unsigned>;
        ///Hardware Average Select
        enum class AvgsVal : unsigned {
            v00=0x00000000,     ///<4 samples averaged.
            v01=0x00000001,     ///<8 samples averaged.
            v10=0x00000002,     ///<16 samples averaged.
            v11=0x00000003,     ///<32 samples averaged.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,0),Register::ReadWriteAccess,AvgsVal> avgs{}; 
        namespace AvgsValC{
            constexpr Register::FieldValue<decltype(avgs)::Type,AvgsVal::v00> v00{};
            constexpr Register::FieldValue<decltype(avgs)::Type,AvgsVal::v01> v01{};
            constexpr Register::FieldValue<decltype(avgs)::Type,AvgsVal::v10> v10{};
            constexpr Register::FieldValue<decltype(avgs)::Type,AvgsVal::v11> v11{};
        }
        ///Hardware Average Enable
        enum class AvgeVal : unsigned {
            v0=0x00000000,     ///<Hardware average function disabled.
            v1=0x00000001,     ///<Hardware average function enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,AvgeVal> avge{}; 
        namespace AvgeValC{
            constexpr Register::FieldValue<decltype(avge)::Type,AvgeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(avge)::Type,AvgeVal::v1> v1{};
        }
        ///Continuous Conversion Enable
        enum class AdcoVal : unsigned {
            v0=0x00000000,     ///<One conversion will be performed (or one set of conversions, if AVGE is set) after a conversion is initiated.
            v1=0x00000001,     ///<Continuous conversions will be performed (or continuous sets of conversions, if AVGE is set) after a conversion is initiated.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,AdcoVal> adco{}; 
        namespace AdcoValC{
            constexpr Register::FieldValue<decltype(adco)::Type,AdcoVal::v0> v0{};
            constexpr Register::FieldValue<decltype(adco)::Type,AdcoVal::v1> v1{};
        }
        ///Calibration
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::ReadWriteAccess,unsigned> cal{}; 
    }
    namespace Adc1BaseOfs{    ///<BASE Offset Register
        using Addr = Register::Address<0x40027098,0xffffff00,0x00000000,unsigned>;
        ///Base Offset Error Correction Value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> baOfs{}; 
    }
    namespace Adc1Ofs{    ///<ADC Offset Correction Register
        using Addr = Register::Address<0x4002709c,0xffff0000,0x00000000,unsigned>;
        ///Offset Error Correction Value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::ReadWriteAccess,unsigned> ofs{}; 
    }
    namespace Adc1UsrOfs{    ///<USER Offset Correction Register
        using Addr = Register::Address<0x400270a0,0xffffff00,0x00000000,unsigned>;
        ///USER Offset Error Correction Value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> usrOfs{}; 
    }
    namespace Adc1Xofs{    ///<ADC X Offset Correction Register
        using Addr = Register::Address<0x400270a4,0xffffffc0,0x00000000,unsigned>;
        ///X offset error correction value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,unsigned> xofs{}; 
    }
    namespace Adc1Yofs{    ///<ADC Y Offset Correction Register
        using Addr = Register::Address<0x400270a8,0xffffff00,0x00000000,unsigned>;
        ///Y offset error correction value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> yofs{}; 
    }
    namespace Adc1G{    ///<ADC Gain Register
        using Addr = Register::Address<0x400270ac,0xfffff800,0x00000000,unsigned>;
        ///Gain error adjustment factor for the overall conversion
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,0),Register::ReadWriteAccess,unsigned> g{}; 
    }
    namespace Adc1Ug{    ///<ADC User Gain Register
        using Addr = Register::Address<0x400270b0,0xfffffc00,0x00000000,unsigned>;
        ///User gain error correction value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,0),Register::ReadWriteAccess,unsigned> ug{}; 
    }
    namespace Adc1Clps{    ///<ADC General Calibration Value Register S
        using Addr = Register::Address<0x400270b4,0xffffff80,0x00000000,unsigned>;
        ///Calibration Value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> clps{}; 
    }
    namespace Adc1Clp3{    ///<ADC Plus-Side General Calibration Value Register 3
        using Addr = Register::Address<0x400270b8,0xfffffc00,0x00000000,unsigned>;
        ///Calibration Value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,0),Register::ReadWriteAccess,unsigned> clp3{}; 
    }
    namespace Adc1Clp2{    ///<ADC Plus-Side General Calibration Value Register 2
        using Addr = Register::Address<0x400270bc,0xfffffc00,0x00000000,unsigned>;
        ///Calibration Value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,0),Register::ReadWriteAccess,unsigned> clp2{}; 
    }
    namespace Adc1Clp1{    ///<ADC Plus-Side General Calibration Value Register 1
        using Addr = Register::Address<0x400270c0,0xfffffe00,0x00000000,unsigned>;
        ///Calibration Value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,0),Register::ReadWriteAccess,unsigned> clp1{}; 
    }
    namespace Adc1Clp0{    ///<ADC Plus-Side General Calibration Value Register 0
        using Addr = Register::Address<0x400270c4,0xffffff00,0x00000000,unsigned>;
        ///Calibration Value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> clp0{}; 
    }
    namespace Adc1Clpx{    ///<ADC Plus-Side General Calibration Value Register X
        using Addr = Register::Address<0x400270c8,0xffffff80,0x00000000,unsigned>;
        ///Calibration Value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> clpx{}; 
    }
    namespace Adc1Clp9{    ///<ADC Plus-Side General Calibration Value Register 9
        using Addr = Register::Address<0x400270cc,0xffffff80,0x00000000,unsigned>;
        ///Calibration Value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> clp9{}; 
    }
    namespace Adc1ClpsOfs{    ///<ADC General Calibration Offset Value Register S
        using Addr = Register::Address<0x400270d0,0xfffffff0,0x00000000,unsigned>;
        ///CLPS Offset
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::ReadWriteAccess,unsigned> clpsOfs{}; 
    }
    namespace Adc1Clp3Ofs{    ///<ADC Plus-Side General Calibration Offset Value Register 3
        using Addr = Register::Address<0x400270d4,0xfffffff0,0x00000000,unsigned>;
        ///CLP3 Offset
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::ReadWriteAccess,unsigned> clp3Ofs{}; 
    }
    namespace Adc1Clp2Ofs{    ///<ADC Plus-Side General Calibration Offset Value Register 2
        using Addr = Register::Address<0x400270d8,0xfffffff0,0x00000000,unsigned>;
        ///CLP2 Offset
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::ReadWriteAccess,unsigned> clp2Ofs{}; 
    }
    namespace Adc1Clp1Ofs{    ///<ADC Plus-Side General Calibration Offset Value Register 1
        using Addr = Register::Address<0x400270dc,0xfffffff0,0x00000000,unsigned>;
        ///CLP1 Offset
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::ReadWriteAccess,unsigned> clp1Ofs{}; 
    }
    namespace Adc1Clp0Ofs{    ///<ADC Plus-Side General Calibration Offset Value Register 0
        using Addr = Register::Address<0x400270e0,0xfffffff0,0x00000000,unsigned>;
        ///CLP0 Offset
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::ReadWriteAccess,unsigned> clp0Ofs{}; 
    }
    namespace Adc1ClpxOfs{    ///<ADC Plus-Side General Calibration Offset Value Register X
        using Addr = Register::Address<0x400270e4,0xfffff000,0x00000000,unsigned>;
        ///CLPX Offset
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::ReadWriteAccess,unsigned> clpxOfs{}; 
    }
    namespace Adc1Clp9Ofs{    ///<ADC Plus-Side General Calibration Offset Value Register 9
        using Addr = Register::Address<0x400270e8,0xfffff000,0x00000000,unsigned>;
        ///CLP9 Offset
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::ReadWriteAccess,unsigned> clp9Ofs{}; 
    }
    namespace Adc1Sc1q{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x40027148,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1r{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x4002714c,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1s{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x40027150,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1t{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x40027154,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1u{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x40027158,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1v{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x4002715c,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1w{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x40027160,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1x{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x40027164,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1y{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x40027168,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1z{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x4002716c,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            v000000=0x00000000,     ///<Exernal intput channel 0 is selected.
            v000001=0x00000001,     ///<Exernal channel 1 is selected as input.
            v000010=0x00000002,     ///<Exernal channel 2 is selected as input.
            v000011=0x00000003,     ///<Exernal channel 3 is selected as input.
            v000100=0x00000004,     ///<Exernal channel 4 is selected as input.
            v000101=0x00000005,     ///<Exernal channel 5 is selected as input.
            v000110=0x00000006,     ///<Exernal channel 6 is selected as input.
            v000111=0x00000007,     ///<Exernal channel 7 is selected as input.
            v001000=0x00000008,     ///<Exernal channel 8 is selected as input.
            v01001=0x00000009,     ///<Exernal channel 9 is selected as input.
            v001010=0x0000000a,     ///<Exernal channel 10 is selected as input.
            v001011=0x0000000b,     ///<Exernal channel 11 is selected as input.
            v001100=0x0000000c,     ///<Exernal channel 12 is selected as input.
            v001101=0x0000000d,     ///<Exernal channel 13 is selected as input.
            v001110=0x0000000e,     ///<Exernal channel 14 is selected as input.
            v001111=0x0000000f,     ///<Exernal channel 15 is selected as input.
            v010101=0x00000015,     ///<Internal channel 0 is selected as input.
            v010110=0x00000016,     ///<Internal channel 1 is selected as input.
            v010111=0x00000017,     ///<Internal channel 2 is selected as input.
            v011010=0x0000001a,     ///<Temp Sensor
            v011011=0x0000001b,     ///<Band Gap
            v011100=0x0000001c,     ///<Internal channel 3 is selected as input.
            v011101=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011110=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            v100000=0x00000020,     ///<Internal channel 16 is selected as input.
            v100001=0x00000021,     ///<Exernal channel 17 is selected as input.
            v100010=0x00000022,     ///<Exernal channel 18 is selected as input.
            v100011=0x00000023,     ///<Exernal channel 19 is selected as input.
            v100100=0x00000024,     ///<Exernal channel 20 is selected as input.
            v100101=0x00000025,     ///<Exernal channel 21 is selected as input.
            v100110=0x00000026,     ///<Exernal channel 22 is selected as input.
            v100111=0x00000027,     ///<Exernal channel 23 is selected as input.
            v101000=0x00000028,     ///<Exernal channel 24 is selected as input.
            v101001=0x00000029,     ///<Exernal channel 25 is selected as input.
            v101010=0x0000002a,     ///<Exernal channel 26 is selected as input.
            v101011=0x0000002b,     ///<Exernal channel 27 is selected as input.
            v101100=0x0000002c,     ///<Exernal channel 28 is selected as input.
            v101101=0x0000002d,     ///<Exernal channel 29 is selected as input.
            v101110=0x0000002e,     ///<Exernal channel 30 is selected as input.
            v101111=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v000000> v000000{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v000001> v000001{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v000010> v000010{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v000011> v000011{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v000100> v000100{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v000101> v000101{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v000110> v000110{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v000111> v000111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v001000> v001000{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v01001> v01001{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v001010> v001010{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v001011> v001011{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v001100> v001100{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v001101> v001101{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v001110> v001110{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v001111> v001111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v010101> v010101{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v010110> v010110{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v010111> v010111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011010> v011010{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011011> v011011{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011100> v011100{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011101> v011101{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011110> v011110{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v100000> v100000{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v100001> v100001{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v100010> v100010{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v100011> v100011{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v100100> v100100{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v100101> v100101{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v100110> v100110{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v100111> v100111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v101000> v101000{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v101001> v101001{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v101010> v101010{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v101011> v101011{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v101100> v101100{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v101101> v101101{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v101110> v101110{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v101111> v101111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            v0=0x00000000,     ///<Conversion complete interrupt is disabled.
            v1=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::v0> v0{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::v1> v1{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            v0=0x00000000,     ///<Conversion is not completed.
            v1=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::v0> v0{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::v1> v1{};
        }
    }
    namespace Adc1Sc1aa{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x40027170,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1ab{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x40027174,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1ac{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x40027178,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1ad{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x4002717c,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1ae{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x40027180,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Sc1af{    ///<ADC Status and Control Register 1
        using Addr = Register::Address<0x40027184,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Rq{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x400271c8,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Rr{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x400271cc,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Rs{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x400271d0,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Rt{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x400271d4,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Ru{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x400271d8,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Rv{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x400271dc,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Rw{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x400271e0,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Rx{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x400271e4,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Ry{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x400271e8,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Rz{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x400271ec,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Raa{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x400271f0,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Rab{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x400271f4,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Rac{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x400271f8,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Rad{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x400271fc,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Rae{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x40027200,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Raf{    ///<ADC Data Result Registers
        using Addr = Register::Address<0x40027204,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Cv1{    ///<Compare Value Registers
        using Addr = Register::Address<0x40027088,0xffff0000,0x00000000,unsigned>;
        ///Compare Value.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::ReadWriteAccess,unsigned> cv{}; 
    }
    namespace Adc1Cv2{    ///<Compare Value Registers
        using Addr = Register::Address<0x4002708c,0xffff0000,0x00000000,unsigned>;
        ///Compare Value.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::ReadWriteAccess,unsigned> cv{}; 
    }
    namespace Adc1Asc1a{    ///<ADC Status and Control Register 1 (alias)
        using Addr = Register::Address<0x40027108,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Asc1b{    ///<ADC Status and Control Register 1 (alias)
        using Addr = Register::Address<0x4002710c,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Asc1c{    ///<ADC Status and Control Register 1 (alias)
        using Addr = Register::Address<0x40027110,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Asc1d{    ///<ADC Status and Control Register 1 (alias)
        using Addr = Register::Address<0x40027114,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Asc1e{    ///<ADC Status and Control Register 1 (alias)
        using Addr = Register::Address<0x40027118,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Asc1f{    ///<ADC Status and Control Register 1 (alias)
        using Addr = Register::Address<0x4002711c,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Asc1g{    ///<ADC Status and Control Register 1 (alias)
        using Addr = Register::Address<0x40027120,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Asc1h{    ///<ADC Status and Control Register 1 (alias)
        using Addr = Register::Address<0x40027124,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Asc1i{    ///<ADC Status and Control Register 1 (alias)
        using Addr = Register::Address<0x40027128,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Asc1j{    ///<ADC Status and Control Register 1 (alias)
        using Addr = Register::Address<0x4002712c,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Asc1k{    ///<ADC Status and Control Register 1 (alias)
        using Addr = Register::Address<0x40027130,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Asc1l{    ///<ADC Status and Control Register 1 (alias)
        using Addr = Register::Address<0x40027134,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Asc1m{    ///<ADC Status and Control Register 1 (alias)
        using Addr = Register::Address<0x40027138,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Asc1n{    ///<ADC Status and Control Register 1 (alias)
        using Addr = Register::Address<0x4002713c,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Asc1o{    ///<ADC Status and Control Register 1 (alias)
        using Addr = Register::Address<0x40027140,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Asc1p{    ///<ADC Status and Control Register 1 (alias)
        using Addr = Register::Address<0x40027144,0xffffff00,0x00000000,unsigned>;
        ///Input channel select
        enum class AdchVal : unsigned {
            extchannel0=0x00000000,     ///<Exernal intput channel 0 is selected.
            extchannel1=0x00000001,     ///<Exernal channel 1 is selected as input.
            extchannel2=0x00000002,     ///<Exernal channel 2 is selected as input.
            extchannel3=0x00000003,     ///<Exernal channel 3 is selected as input.
            extchannel4=0x00000004,     ///<Exernal channel 4 is selected as input.
            extchannel5=0x00000005,     ///<Exernal channel 5 is selected as input.
            extchannel6=0x00000006,     ///<Exernal channel 6 is selected as input.
            extchannel7=0x00000007,     ///<Exernal channel 7 is selected as input.
            extchannel8=0x00000008,     ///<Exernal channel 8 is selected as input.
            extchannel9=0x00000009,     ///<Exernal channel 9 is selected as input.
            extchannel10=0x0000000a,     ///<Exernal channel 10 is selected as input.
            extchannel11=0x0000000b,     ///<Exernal channel 11 is selected as input.
            extchannel12=0x0000000c,     ///<Exernal channel 12 is selected as input.
            extchannel13=0x0000000d,     ///<Exernal channel 13 is selected as input.
            extchannel14=0x0000000e,     ///<Exernal channel 14 is selected as input.
            extchannel15=0x0000000f,     ///<Exernal channel 15 is selected as input.
            intchannel0=0x00000015,     ///<Internal channel 0 is selected as input.
            intchannel1=0x00000016,     ///<Internal channel 1 is selected as input.
            intchannel2=0x00000017,     ///<Internal channel 2 is selected as input.
            tempsensor=0x0000001a,     ///<Temp Sensor
            bandgap=0x0000001b,     ///<Band Gap
            intchannel3=0x0000001c,     ///<Internal channel 3 is selected as input.
            vrefsh=0x0000001d,     ///<VREFSH is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            vrefsl=0x0000001e,     ///<VREFSL is selected as input. Voltage reference selected is determined by SC2[REFSEL].
            v011111=0x0000001f,     ///<Module is disabled
            extchannel16=0x00000020,     ///<Internal channel 16 is selected as input.
            extchannel17=0x00000021,     ///<Exernal channel 17 is selected as input.
            extchannel18=0x00000022,     ///<Exernal channel 18 is selected as input.
            extchannel19=0x00000023,     ///<Exernal channel 19 is selected as input.
            extchannel20=0x00000024,     ///<Exernal channel 20 is selected as input.
            extchannel21=0x00000025,     ///<Exernal channel 21 is selected as input.
            extchannel22=0x00000026,     ///<Exernal channel 22 is selected as input.
            extchannel23=0x00000027,     ///<Exernal channel 23 is selected as input.
            extchannel24=0x00000028,     ///<Exernal channel 24 is selected as input.
            extchannel25=0x00000029,     ///<Exernal channel 25 is selected as input.
            extchannel26=0x0000002a,     ///<Exernal channel 26 is selected as input.
            extchannel27=0x0000002b,     ///<Exernal channel 27 is selected as input.
            extchannel28=0x0000002c,     ///<Exernal channel 28 is selected as input.
            extchannel29=0x0000002d,     ///<Exernal channel 29 is selected as input.
            extchannel30=0x0000002e,     ///<Exernal channel 30 is selected as input.
            extchannel31=0x0000002f,     ///<Exernal channel 31 is selected as input.
            v11xxxx=0x00000030,     ///<Module is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::ReadWriteAccess,AdchVal> adch{}; 
        namespace AdchValC{
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel0> extchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel1> extchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel2> extchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel3> extchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel4> extchannel4{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel5> extchannel5{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel6> extchannel6{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel7> extchannel7{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel8> extchannel8{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel9> extchannel9{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel10> extchannel10{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel11> extchannel11{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel12> extchannel12{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel13> extchannel13{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel14> extchannel14{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel15> extchannel15{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel0> intchannel0{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel1> intchannel1{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel2> intchannel2{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::tempsensor> tempsensor{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::bandgap> bandgap{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::intchannel3> intchannel3{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsh> vrefsh{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::vrefsl> vrefsl{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v011111> v011111{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel16> extchannel16{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel17> extchannel17{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel18> extchannel18{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel19> extchannel19{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel20> extchannel20{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel21> extchannel21{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel22> extchannel22{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel23> extchannel23{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel24> extchannel24{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel25> extchannel25{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel26> extchannel26{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel27> extchannel27{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel28> extchannel28{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel29> extchannel29{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel30> extchannel30{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::extchannel31> extchannel31{};
            constexpr Register::FieldValue<decltype(adch)::Type,AdchVal::v11xxxx> v11xxxx{};
        }
        ///Interrupt Enable
        enum class AienVal : unsigned {
            disabled=0x00000000,     ///<Conversion complete interrupt is disabled.
            enabled=0x00000001,     ///<Conversion complete interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,AienVal> aien{}; 
        namespace AienValC{
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(aien)::Type,AienVal::enabled> enabled{};
        }
        ///Conversion Complete Flag
        enum class CocoVal : unsigned {
            not_completed=0x00000000,     ///<Conversion is not completed.
            completed=0x00000001,     ///<Conversion is completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CocoVal> coco{}; 
        namespace CocoValC{
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::not_completed> not_completed{};
            constexpr Register::FieldValue<decltype(coco)::Type,CocoVal::completed> completed{};
        }
    }
    namespace Adc1Ara{    ///<ADC Data Result Registers (alias)
        using Addr = Register::Address<0x40027188,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Arb{    ///<ADC Data Result Registers (alias)
        using Addr = Register::Address<0x4002718c,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Arc{    ///<ADC Data Result Registers (alias)
        using Addr = Register::Address<0x40027190,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Ard{    ///<ADC Data Result Registers (alias)
        using Addr = Register::Address<0x40027194,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Are{    ///<ADC Data Result Registers (alias)
        using Addr = Register::Address<0x40027198,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Arf{    ///<ADC Data Result Registers (alias)
        using Addr = Register::Address<0x4002719c,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Arg{    ///<ADC Data Result Registers (alias)
        using Addr = Register::Address<0x400271a0,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Arh{    ///<ADC Data Result Registers (alias)
        using Addr = Register::Address<0x400271a4,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Ari{    ///<ADC Data Result Registers (alias)
        using Addr = Register::Address<0x400271a8,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Arj{    ///<ADC Data Result Registers (alias)
        using Addr = Register::Address<0x400271ac,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Ark{    ///<ADC Data Result Registers (alias)
        using Addr = Register::Address<0x400271b0,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Arl{    ///<ADC Data Result Registers (alias)
        using Addr = Register::Address<0x400271b4,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Arm{    ///<ADC Data Result Registers (alias)
        using Addr = Register::Address<0x400271b8,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Arn{    ///<ADC Data Result Registers (alias)
        using Addr = Register::Address<0x400271bc,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Aro{    ///<ADC Data Result Registers (alias)
        using Addr = Register::Address<0x400271c0,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
    namespace Adc1Arp{    ///<ADC Data Result Registers (alias)
        using Addr = Register::Address<0x400271c4,0xfffff000,0x00000000,unsigned>;
        ///Data result
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> d{}; 
    }
}
