#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//Core Platform Miscellaneous Control Module
    namespace McmPlasc{    ///<Crossbar Switch (AXBS) Slave Configuration
        using Addr = Register::Address<0xe0080008,0xffffff00,0x00000000,unsigned short>;
        ///Each bit in the ASC field indicates whether there is a corresponding connection to the crossbar switch's slave input port.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> asc{}; 
    }
    namespace McmPlamc{    ///<Crossbar Switch (AXBS) Master Configuration
        using Addr = Register::Address<0xe008000a,0xffffff00,0x00000000,unsigned short>;
        ///Each bit in the AMC field indicates whether there is a corresponding connection to the AXBS master input port.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> amc{}; 
    }
    namespace McmCpcr{    ///<Core Platform Control Register
        using Addr = Register::Address<0xe008000c,0x88fffda0,0x00000000,unsigned>;
        ///AXBS Halt State Machine Status
        enum class HltfsmstVal : unsigned {
            v00=0x00000000,     ///<Waiting for request
            v01=0x00000001,     ///<Waiting for platform idle
            v11=0x00000003,     ///<Platform stalled
            v10=0x00000002,     ///<Unused state
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,HltfsmstVal> hltFsmSt{}; 
        namespace HltfsmstValC{
            constexpr Register::FieldValue<decltype(hltFsmSt)::Type,HltfsmstVal::v00> v00{};
            constexpr Register::FieldValue<decltype(hltFsmSt)::Type,HltfsmstVal::v01> v01{};
            constexpr Register::FieldValue<decltype(hltFsmSt)::Type,HltfsmstVal::v11> v11{};
            constexpr Register::FieldValue<decltype(hltFsmSt)::Type,HltfsmstVal::v10> v10{};
        }
        ///AXBS Halt Request
        enum class AxbshltreqVal : unsigned {
            v0=0x00000000,     ///<AXBS is not receiving halt request
            v1=0x00000001,     ///<AXBS is receiving halt request
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,AxbshltreqVal> axbsHltReq{}; 
        namespace AxbshltreqValC{
            constexpr Register::FieldValue<decltype(axbsHltReq)::Type,AxbshltreqVal::v0> v0{};
            constexpr Register::FieldValue<decltype(axbsHltReq)::Type,AxbshltreqVal::v1> v1{};
        }
        ///AXBS Halted
        enum class AxbshltdVal : unsigned {
            v0=0x00000000,     ///<AXBS is not currently halted
            v1=0x00000001,     ///<AXBS is currently halted
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,AxbshltdVal> axbsHltd{}; 
        namespace AxbshltdValC{
            constexpr Register::FieldValue<decltype(axbsHltd)::Type,AxbshltdVal::v0> v0{};
            constexpr Register::FieldValue<decltype(axbsHltd)::Type,AxbshltdVal::v1> v1{};
        }
        ///Flash Memory Controller Program Flash Idle
        enum class FmcpfidleVal : unsigned {
            v0=0x00000000,     ///<FMC program flash is not idle
            v1=0x00000001,     ///<FMC program flash is currently idle
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,4),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,FmcpfidleVal> fmcPfIdle{}; 
        namespace FmcpfidleValC{
            constexpr Register::FieldValue<decltype(fmcPfIdle)::Type,FmcpfidleVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fmcPfIdle)::Type,FmcpfidleVal::v1> v1{};
        }
        ///Peripheral Bridge Idle
        enum class PbridgeidleVal : unsigned {
            v0=0x00000000,     ///<PBRIDGE is not idle
            v1=0x00000001,     ///<PBRIDGE is currently idle
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PbridgeidleVal> pbridgeIdle{}; 
        namespace PbridgeidleValC{
            constexpr Register::FieldValue<decltype(pbridgeIdle)::Type,PbridgeidleVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pbridgeIdle)::Type,PbridgeidleVal::v1> v1{};
        }
        ///Crossbar Round-robin Arbitration Enable
        enum class CbrrVal : unsigned {
            v0=0x00000000,     ///<Fixed-priority arbitration
            v1=0x00000001,     ///<Round-robin arbitration
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,9),Register::ReadWriteAccess,CbrrVal> cbrr{}; 
        namespace CbrrValC{
            constexpr Register::FieldValue<decltype(cbrr)::Type,CbrrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cbrr)::Type,CbrrVal::v1> v1{};
        }
        ///SRAM_U Arbitration Priority
        enum class SramuapVal : unsigned {
            v00=0x00000000,     ///<Round robin
            v01=0x00000001,     ///<Special round robin (favors SRAM backdoor accesses over the processor)
            v10=0x00000002,     ///<Fixed priority. Processor has highest, backdoor has lowest
            v11=0x00000003,     ///<Fixed priority. Backdoor has highest, processor has lowest
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,SramuapVal> sramuap{}; 
        namespace SramuapValC{
            constexpr Register::FieldValue<decltype(sramuap)::Type,SramuapVal::v00> v00{};
            constexpr Register::FieldValue<decltype(sramuap)::Type,SramuapVal::v01> v01{};
            constexpr Register::FieldValue<decltype(sramuap)::Type,SramuapVal::v10> v10{};
            constexpr Register::FieldValue<decltype(sramuap)::Type,SramuapVal::v11> v11{};
        }
        ///SRAM_U Write Protect
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,unsigned> sramuwp{}; 
        ///SRAM_L Arbitration Priority
        enum class SramlapVal : unsigned {
            v00=0x00000000,     ///<Round robin
            v01=0x00000001,     ///<Special round robin (favors SRAM backdoor accesses over the processor)
            v10=0x00000002,     ///<Fixed priority. Processor has highest, backdoor has lowest
            v11=0x00000003,     ///<Fixed priority. Backdoor has highest, processor has lowest
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,28),Register::ReadWriteAccess,SramlapVal> sramlap{}; 
        namespace SramlapValC{
            constexpr Register::FieldValue<decltype(sramlap)::Type,SramlapVal::v00> v00{};
            constexpr Register::FieldValue<decltype(sramlap)::Type,SramlapVal::v01> v01{};
            constexpr Register::FieldValue<decltype(sramlap)::Type,SramlapVal::v10> v10{};
            constexpr Register::FieldValue<decltype(sramlap)::Type,SramlapVal::v11> v11{};
        }
        ///SRAM_L Write Protect
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,unsigned> sramlwp{}; 
    }
    namespace McmIscr{    ///<Interrupt Status and Control Register
        using Addr = Register::Address<0xe0080010,0x60ff60ff,0x00000000,unsigned>;
        ///FPU Invalid Operation Interrupt Status
        enum class FiocVal : unsigned {
            v0=0x00000000,     ///<No interrupt
            v1=0x00000001,     ///<Interrupt occurred
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,FiocVal> fioc{}; 
        namespace FiocValC{
            constexpr Register::FieldValue<decltype(fioc)::Type,FiocVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fioc)::Type,FiocVal::v1> v1{};
        }
        ///FPU Divide-by-Zero Interrupt Status
        enum class FdzcVal : unsigned {
            v0=0x00000000,     ///<No interrupt
            v1=0x00000001,     ///<Interrupt occurred
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,9),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,FdzcVal> fdzc{}; 
        namespace FdzcValC{
            constexpr Register::FieldValue<decltype(fdzc)::Type,FdzcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fdzc)::Type,FdzcVal::v1> v1{};
        }
        ///FPU Overflow Interrupt Status
        enum class FofcVal : unsigned {
            v0=0x00000000,     ///<No interrupt
            v1=0x00000001,     ///<Interrupt occurred
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,10),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,FofcVal> fofc{}; 
        namespace FofcValC{
            constexpr Register::FieldValue<decltype(fofc)::Type,FofcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fofc)::Type,FofcVal::v1> v1{};
        }
        ///FPU Underflow Interrupt Status
        enum class FufcVal : unsigned {
            v0=0x00000000,     ///<No interrupt
            v1=0x00000001,     ///<Interrupt occurred
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,FufcVal> fufc{}; 
        namespace FufcValC{
            constexpr Register::FieldValue<decltype(fufc)::Type,FufcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fufc)::Type,FufcVal::v1> v1{};
        }
        ///FPU Inexact Interrupt Status
        enum class FixcVal : unsigned {
            v0=0x00000000,     ///<No interrupt
            v1=0x00000001,     ///<Interrupt occurred
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,12),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,FixcVal> fixc{}; 
        namespace FixcValC{
            constexpr Register::FieldValue<decltype(fixc)::Type,FixcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fixc)::Type,FixcVal::v1> v1{};
        }
        ///FPU Input Denormal Interrupt Status
        enum class FidcVal : unsigned {
            v0=0x00000000,     ///<No interrupt
            v1=0x00000001,     ///<Interrupt occurred
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,FidcVal> fidc{}; 
        namespace FidcValC{
            constexpr Register::FieldValue<decltype(fidc)::Type,FidcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fidc)::Type,FidcVal::v1> v1{};
        }
        ///FPU Invalid Operation Interrupt Enable
        enum class FioceVal : unsigned {
            v0=0x00000000,     ///<Disable interrupt
            v1=0x00000001,     ///<Enable interrupt
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,FioceVal> fioce{}; 
        namespace FioceValC{
            constexpr Register::FieldValue<decltype(fioce)::Type,FioceVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fioce)::Type,FioceVal::v1> v1{};
        }
        ///FPU Divide-by-Zero Interrupt Enable
        enum class FdzceVal : unsigned {
            v0=0x00000000,     ///<Disable interrupt
            v1=0x00000001,     ///<Enable interrupt
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,FdzceVal> fdzce{}; 
        namespace FdzceValC{
            constexpr Register::FieldValue<decltype(fdzce)::Type,FdzceVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fdzce)::Type,FdzceVal::v1> v1{};
        }
        ///FPU Overflow Interrupt Enable
        enum class FofceVal : unsigned {
            v0=0x00000000,     ///<Disable interrupt
            v1=0x00000001,     ///<Enable interrupt
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,FofceVal> fofce{}; 
        namespace FofceValC{
            constexpr Register::FieldValue<decltype(fofce)::Type,FofceVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fofce)::Type,FofceVal::v1> v1{};
        }
        ///FPU Underflow Interrupt Enable
        enum class FufceVal : unsigned {
            v0=0x00000000,     ///<Disable interrupt
            v1=0x00000001,     ///<Enable interrupt
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,FufceVal> fufce{}; 
        namespace FufceValC{
            constexpr Register::FieldValue<decltype(fufce)::Type,FufceVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fufce)::Type,FufceVal::v1> v1{};
        }
        ///FPU Inexact Interrupt Enable
        enum class FixceVal : unsigned {
            v0=0x00000000,     ///<Disable interrupt
            v1=0x00000001,     ///<Enable interrupt
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,FixceVal> fixce{}; 
        namespace FixceValC{
            constexpr Register::FieldValue<decltype(fixce)::Type,FixceVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fixce)::Type,FixceVal::v1> v1{};
        }
        ///FPU Input Denormal Interrupt Enable
        enum class FidceVal : unsigned {
            v0=0x00000000,     ///<Disable interrupt
            v1=0x00000001,     ///<Enable interrupt
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,FidceVal> fidce{}; 
        namespace FidceValC{
            constexpr Register::FieldValue<decltype(fidce)::Type,FidceVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fidce)::Type,FidceVal::v1> v1{};
        }
    }
    namespace McmPid{    ///<Process ID Register
        using Addr = Register::Address<0xe0080030,0xffffff00,0x00000000,unsigned>;
        ///M0_PID and M1_PID for MPU
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> pid{}; 
    }
    namespace McmCpo{    ///<Compute Operation Control Register
        using Addr = Register::Address<0xe0080040,0xfffffff8,0x00000000,unsigned>;
        ///Compute Operation Request
        enum class CporeqVal : unsigned {
            v0=0x00000000,     ///<Request is cleared.
            v1=0x00000001,     ///<Request Compute Operation.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,CporeqVal> cporeq{}; 
        namespace CporeqValC{
            constexpr Register::FieldValue<decltype(cporeq)::Type,CporeqVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cporeq)::Type,CporeqVal::v1> v1{};
        }
        ///Compute Operation Acknowledge
        enum class CpoackVal : unsigned {
            v0=0x00000000,     ///<Compute operation entry has not completed or compute operation exit has completed.
            v1=0x00000001,     ///<Compute operation entry has completed or compute operation exit has not completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CpoackVal> cpoack{}; 
        namespace CpoackValC{
            constexpr Register::FieldValue<decltype(cpoack)::Type,CpoackVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cpoack)::Type,CpoackVal::v1> v1{};
        }
        ///Compute Operation Wakeup On Interrupt
        enum class CpowoiVal : unsigned {
            v0=0x00000000,     ///<No effect.
            v1=0x00000001,     ///<When set, the CPOREQ is cleared on any interrupt or exception vector fetch.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,CpowoiVal> cpowoi{}; 
        namespace CpowoiValC{
            constexpr Register::FieldValue<decltype(cpowoi)::Type,CpowoiVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cpowoi)::Type,CpowoiVal::v1> v1{};
        }
    }
    namespace McmLmdr0{    ///<Local Memory Descriptor Register
        using Addr = Register::Address<0xe0080400,0x60001ff0,0x00000000,unsigned>;
        ///Control Field 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::ReadWriteAccess,unsigned> cf0{}; 
        ///Memory Type
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,13),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> mt{}; 
        ///LOCK
        enum class LockVal : unsigned {
            v0=0x00000000,     ///<Writes to the LMDRn[7:0] are allowed.
            v1=0x00000001,     ///<Writes to the LMDRn[7:0] are ignored.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,LockVal> lock{}; 
        namespace LockValC{
            constexpr Register::FieldValue<decltype(lock)::Type,LockVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lock)::Type,LockVal::v1> v1{};
        }
        ///LMEM Data Path Width. This field defines the width of the local memory.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,17),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dpw{}; 
        ///Level 1 Cache Ways
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,20),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> wy{}; 
        ///LMEM Size
        enum class LmszVal : unsigned {
            v0000=0x00000000,     ///<no LMEMn (0 KB)
            v0001=0x00000001,     ///<1 KB LMEMn
            v0010=0x00000002,     ///<2 KB LMEMn
            v0011=0x00000003,     ///<4 KB LMEMn
            v0100=0x00000004,     ///<8 KB LMEMn
            v0101=0x00000005,     ///<16 KB LMEMn
            v0110=0x00000006,     ///<32 KB LMEMn
            v0111=0x00000007,     ///<64 KB LMEMn
            v1000=0x00000008,     ///<128 KB LMEMn
            v1001=0x00000009,     ///<256 KB LMEMn
            v1010=0x0000000a,     ///<512 KB LMEMn
            v1011=0x0000000b,     ///<1024 KB LMEMn
            v1100=0x0000000c,     ///<2048 KB LMEMn
            v1101=0x0000000d,     ///<4096 KB LMEMn
            v1110=0x0000000e,     ///<8192 KB LMEMn
            v1111=0x0000000f,     ///<16384 KB LMEMn
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,LmszVal> lmsz{}; 
        namespace LmszValC{
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v0100> v0100{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v0101> v0101{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v0110> v0110{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v0111> v0111{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v1100> v1100{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v1101> v1101{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v1110> v1110{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v1111> v1111{};
        }
        ///LMEM Size Hole
        enum class LmszhVal : unsigned {
            v0=0x00000000,     ///<LMEMn is a power-of-2 capacity.
            v1=0x00000001,     ///<LMEMn is not a power-of-2, with a capacity is 0.75 * LMSZ.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,LmszhVal> lmszh{}; 
        namespace LmszhValC{
            constexpr Register::FieldValue<decltype(lmszh)::Type,LmszhVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lmszh)::Type,LmszhVal::v1> v1{};
        }
        ///Local Memory Valid
        enum class VVal : unsigned {
            v0=0x00000000,     ///<LMEMn is not present.
            v1=0x00000001,     ///<LMEMn is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,VVal> v{}; 
        namespace VValC{
            constexpr Register::FieldValue<decltype(v)::Type,VVal::v0> v0{};
            constexpr Register::FieldValue<decltype(v)::Type,VVal::v1> v1{};
        }
    }
    namespace McmLmdr1{    ///<Local Memory Descriptor Register
        using Addr = Register::Address<0xe0080404,0x60001ff0,0x00000000,unsigned>;
        ///Control Field 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::ReadWriteAccess,unsigned> cf0{}; 
        ///Memory Type
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,13),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> mt{}; 
        ///LOCK
        enum class LockVal : unsigned {
            v0=0x00000000,     ///<Writes to the LMDRn[7:0] are allowed.
            v1=0x00000001,     ///<Writes to the LMDRn[7:0] are ignored.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,LockVal> lock{}; 
        namespace LockValC{
            constexpr Register::FieldValue<decltype(lock)::Type,LockVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lock)::Type,LockVal::v1> v1{};
        }
        ///LMEM Data Path Width. This field defines the width of the local memory.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,17),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dpw{}; 
        ///Level 1 Cache Ways
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,20),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> wy{}; 
        ///LMEM Size
        enum class LmszVal : unsigned {
            v0000=0x00000000,     ///<no LMEMn (0 KB)
            v0001=0x00000001,     ///<1 KB LMEMn
            v0010=0x00000002,     ///<2 KB LMEMn
            v0011=0x00000003,     ///<4 KB LMEMn
            v0100=0x00000004,     ///<8 KB LMEMn
            v0101=0x00000005,     ///<16 KB LMEMn
            v0110=0x00000006,     ///<32 KB LMEMn
            v0111=0x00000007,     ///<64 KB LMEMn
            v1000=0x00000008,     ///<128 KB LMEMn
            v1001=0x00000009,     ///<256 KB LMEMn
            v1010=0x0000000a,     ///<512 KB LMEMn
            v1011=0x0000000b,     ///<1024 KB LMEMn
            v1100=0x0000000c,     ///<2048 KB LMEMn
            v1101=0x0000000d,     ///<4096 KB LMEMn
            v1110=0x0000000e,     ///<8192 KB LMEMn
            v1111=0x0000000f,     ///<16384 KB LMEMn
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,LmszVal> lmsz{}; 
        namespace LmszValC{
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v0100> v0100{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v0101> v0101{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v0110> v0110{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v0111> v0111{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v1100> v1100{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v1101> v1101{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v1110> v1110{};
            constexpr Register::FieldValue<decltype(lmsz)::Type,LmszVal::v1111> v1111{};
        }
        ///LMEM Size Hole
        enum class LmszhVal : unsigned {
            v0=0x00000000,     ///<LMEMn is a power-of-2 capacity.
            v1=0x00000001,     ///<LMEMn is not a power-of-2, with a capacity is 0.75 * LMSZ.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,LmszhVal> lmszh{}; 
        namespace LmszhValC{
            constexpr Register::FieldValue<decltype(lmszh)::Type,LmszhVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lmszh)::Type,LmszhVal::v1> v1{};
        }
        ///Local Memory Valid
        enum class VVal : unsigned {
            v0=0x00000000,     ///<LMEMn is not present.
            v1=0x00000001,     ///<LMEMn is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,VVal> v{}; 
        namespace VValC{
            constexpr Register::FieldValue<decltype(v)::Type,VVal::v0> v0{};
            constexpr Register::FieldValue<decltype(v)::Type,VVal::v1> v1{};
        }
    }
    namespace McmLmdr2{    ///<Local Memory Descriptor Register2
        using Addr = Register::Address<0xe0080408,0x60001f0f,0x00000000,unsigned>;
        ///Control Field 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,4),Register::ReadWriteAccess,unsigned> cf1{}; 
        ///Memory Type
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,13),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> mt{}; 
        ///LOCK
        enum class LockVal : unsigned {
            v0=0x00000000,     ///<Writes to the LMDRn[7:0] are allowed.
            v1=0x00000001,     ///<Writes to the LMDRn[7:0] are ignored.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,LockVal> lock{}; 
        namespace LockValC{
            constexpr Register::FieldValue<decltype(lock)::Type,LockVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lock)::Type,LockVal::v1> v1{};
        }
        ///LMEM Data Path Width. This field defines the width of the local memory.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,17),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dpw{}; 
        ///Level 1 Cache Ways
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,20),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> wy{}; 
        ///LMEM Size
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> lmsz{}; 
        ///LMEM Size Hole
        enum class LmszhVal : unsigned {
            v0=0x00000000,     ///<LMEMn is a power-of-2 capacity.
            v1=0x00000001,     ///<LMEMn is not a power-of-2, with a capacity is 0.75 * LMSZ.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,LmszhVal> lmszh{}; 
        namespace LmszhValC{
            constexpr Register::FieldValue<decltype(lmszh)::Type,LmszhVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lmszh)::Type,LmszhVal::v1> v1{};
        }
        ///Local Memory Valid
        enum class VVal : unsigned {
            v0=0x00000000,     ///<LMEMn is not present.
            v1=0x00000001,     ///<LMEMn is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,VVal> v{}; 
        namespace VValC{
            constexpr Register::FieldValue<decltype(v)::Type,VVal::v0> v0{};
            constexpr Register::FieldValue<decltype(v)::Type,VVal::v1> v1{};
        }
    }
    namespace McmLmpecr{    ///<LMEM Parity and ECC Control Register
        using Addr = Register::Address<0xe0080480,0xffeffefe,0x00000000,unsigned>;
        ///Enable RAM ECC Noncorrectable Reporting
        enum class ErncrVal : unsigned {
            v0=0x00000000,     ///<Reporting disabled
            v1=0x00000001,     ///<Reporting enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,ErncrVal> erncr{}; 
        namespace ErncrValC{
            constexpr Register::FieldValue<decltype(erncr)::Type,ErncrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(erncr)::Type,ErncrVal::v1> v1{};
        }
        ///Enable RAM ECC 1 Bit Reporting
        enum class Er1brVal : unsigned {
            v0=0x00000000,     ///<Reporting disabled
            v1=0x00000001,     ///<Reporting enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::ReadWriteAccess,Er1brVal> er1br{}; 
        namespace Er1brValC{
            constexpr Register::FieldValue<decltype(er1br)::Type,Er1brVal::v0> v0{};
            constexpr Register::FieldValue<decltype(er1br)::Type,Er1brVal::v1> v1{};
        }
        ///Enable Cache Parity Reporting
        enum class EcprVal : unsigned {
            v0=0x00000000,     ///<Reporting disabled
            v1=0x00000001,     ///<Reporting enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,20),Register::ReadWriteAccess,EcprVal> ecpr{}; 
        namespace EcprValC{
            constexpr Register::FieldValue<decltype(ecpr)::Type,EcprVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ecpr)::Type,EcprVal::v1> v1{};
        }
    }
    namespace McmLmpeir{    ///<LMEM Parity and ECC Interrupt Register
        using Addr = Register::Address<0xe0080488,0x60000000,0x00000000,unsigned>;
        ///ENCn = ECC Noncorrectable Error n
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> enc{}; 
        ///E1Bn = ECC 1-bit Error n
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> e1b{}; 
        ///Cache Parity Error
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> pe{}; 
        ///Parity or ECC Error Location
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> peeloc{}; 
        ///Valid Bit
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> v{}; 
    }
    namespace McmLmfar{    ///<LMEM Fault Address Register
        using Addr = Register::Address<0xe0080490,0x00000000,0x00000000,unsigned>;
        ///ECC Fault Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> efadd{}; 
    }
    namespace McmLmfatr{    ///<LMEM Fault Attribute Register
        using Addr = Register::Address<0xe0080494,0x7fff0000,0x00000000,unsigned>;
        ///Parity/ECC Fault Protection
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> pefprt{}; 
        ///Parity/ECC Fault Master Size
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,4),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> pefsize{}; 
        ///Parity/ECC Fault Write
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> pefw{}; 
        ///Parity/ECC Fault Master Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> pefmst{}; 
        ///Overrun
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> ovr{}; 
    }
    namespace McmLmfdhr{    ///<LMEM Fault Data High Register
        using Addr = Register::Address<0xe00804a0,0x00000000,0x00000000,unsigned>;
        ///Parity or ECC Fault Data High
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> pefdh{}; 
    }
    namespace McmLmfdlr{    ///<LMEM Fault Data Low Register
        using Addr = Register::Address<0xe00804a4,0x00000000,0x00000000,unsigned>;
        ///Parity or ECC Fault Data Low
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> pefdl{}; 
    }
}
