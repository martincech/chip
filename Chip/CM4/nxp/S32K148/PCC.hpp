#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//PCC
    namespace PccPccFtfc{    ///<PCC FTFC Register
        using Addr = Register::Address<0x40065080,0x3fffffff,0x00000000,unsigned>;
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccDmamux{    ///<PCC DMAMUX Register
        using Addr = Register::Address<0x40065084,0x3fffffff,0x00000000,unsigned>;
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccFlexcan0{    ///<PCC FlexCAN0 Register
        using Addr = Register::Address<0x40065090,0x3fffffff,0x00000000,unsigned>;
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccFlexcan1{    ///<PCC FlexCAN1 Register
        using Addr = Register::Address<0x40065094,0x3fffffff,0x00000000,unsigned>;
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccFtm3{    ///<PCC FTM3 Register
        using Addr = Register::Address<0x40065098,0x38ffffff,0x00000000,unsigned>;
        ///Peripheral Clock Source Select
        enum class PcsVal : unsigned {
            v000=0x00000000,     ///<Clock is off. An external clock can be enabled for this peripheral.
            v001=0x00000001,     ///<Clock option 1
            v010=0x00000002,     ///<Clock option 2
            v011=0x00000003,     ///<Clock option 3
            v100=0x00000004,     ///<Clock option 4
            v101=0x00000005,     ///<Clock option 5
            v110=0x00000006,     ///<Clock option 6
            v111=0x00000007,     ///<Clock option 7
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,24),Register::ReadWriteAccess,PcsVal> pcs{}; 
        namespace PcsValC{
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v000> v000{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v001> v001{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v010> v010{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v011> v011{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v100> v100{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v101> v101{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v110> v110{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v111> v111{};
        }
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccAdc1{    ///<PCC ADC1 Register
        using Addr = Register::Address<0x4006509c,0x38ffffff,0x00000000,unsigned>;
        ///Peripheral Clock Source Select
        enum class PcsVal : unsigned {
            v000=0x00000000,     ///<Clock is off.
            v001=0x00000001,     ///<Clock option 1
            v010=0x00000002,     ///<Clock option 2
            v011=0x00000003,     ///<Clock option 3
            v100=0x00000004,     ///<Clock option 4
            v101=0x00000005,     ///<Clock option 5
            v110=0x00000006,     ///<Clock option 6
            v111=0x00000007,     ///<Clock option 7
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,24),Register::ReadWriteAccess,PcsVal> pcs{}; 
        namespace PcsValC{
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v000> v000{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v001> v001{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v010> v010{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v011> v011{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v100> v100{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v101> v101{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v110> v110{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v111> v111{};
        }
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccFlexcan2{    ///<PCC FlexCAN2 Register
        using Addr = Register::Address<0x400650ac,0x3fffffff,0x00000000,unsigned>;
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccLpspi0{    ///<PCC LPSPI0 Register
        using Addr = Register::Address<0x400650b0,0x38ffffff,0x00000000,unsigned>;
        ///Peripheral Clock Source Select
        enum class PcsVal : unsigned {
            v000=0x00000000,     ///<Clock is off.
            v001=0x00000001,     ///<Clock option 1
            v010=0x00000002,     ///<Clock option 2
            v011=0x00000003,     ///<Clock option 3
            v100=0x00000004,     ///<Clock option 4
            v101=0x00000005,     ///<Clock option 5
            v110=0x00000006,     ///<Clock option 6
            v111=0x00000007,     ///<Clock option 7
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,24),Register::ReadWriteAccess,PcsVal> pcs{}; 
        namespace PcsValC{
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v000> v000{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v001> v001{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v010> v010{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v011> v011{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v100> v100{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v101> v101{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v110> v110{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v111> v111{};
        }
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccLpspi1{    ///<PCC LPSPI1 Register
        using Addr = Register::Address<0x400650b4,0x38ffffff,0x00000000,unsigned>;
        ///Peripheral Clock Source Select
        enum class PcsVal : unsigned {
            v000=0x00000000,     ///<Clock is off.
            v001=0x00000001,     ///<Clock option 1
            v010=0x00000002,     ///<Clock option 2
            v011=0x00000003,     ///<Clock option 3
            v100=0x00000004,     ///<Clock option 4
            v101=0x00000005,     ///<Clock option 5
            v110=0x00000006,     ///<Clock option 6
            v111=0x00000007,     ///<Clock option 7
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,24),Register::ReadWriteAccess,PcsVal> pcs{}; 
        namespace PcsValC{
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v000> v000{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v001> v001{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v010> v010{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v011> v011{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v100> v100{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v101> v101{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v110> v110{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v111> v111{};
        }
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccLpspi2{    ///<PCC LPSPI2 Register
        using Addr = Register::Address<0x400650b8,0x38ffffff,0x00000000,unsigned>;
        ///Peripheral Clock Source Select
        enum class PcsVal : unsigned {
            v000=0x00000000,     ///<Clock is off.
            v001=0x00000001,     ///<Clock option 1
            v010=0x00000002,     ///<Clock option 2
            v011=0x00000003,     ///<Clock option 3
            v100=0x00000004,     ///<Clock option 4
            v101=0x00000005,     ///<Clock option 5
            v110=0x00000006,     ///<Clock option 6
            v111=0x00000007,     ///<Clock option 7
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,24),Register::ReadWriteAccess,PcsVal> pcs{}; 
        namespace PcsValC{
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v000> v000{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v001> v001{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v010> v010{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v011> v011{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v100> v100{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v101> v101{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v110> v110{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v111> v111{};
        }
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccPdb1{    ///<PCC PDB1 Register
        using Addr = Register::Address<0x400650c4,0x3fffffff,0x00000000,unsigned>;
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccCrc{    ///<PCC CRC Register
        using Addr = Register::Address<0x400650c8,0x3fffffff,0x00000000,unsigned>;
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccPdb0{    ///<PCC PDB0 Register
        using Addr = Register::Address<0x400650d8,0x3fffffff,0x00000000,unsigned>;
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccLpit{    ///<PCC LPIT Register
        using Addr = Register::Address<0x400650dc,0x38ffffff,0x00000000,unsigned>;
        ///Peripheral Clock Source Select
        enum class PcsVal : unsigned {
            v000=0x00000000,     ///<Clock is off.
            v001=0x00000001,     ///<Clock option 1
            v010=0x00000002,     ///<Clock option 2
            v011=0x00000003,     ///<Clock option 3
            v100=0x00000004,     ///<Clock option 4
            v101=0x00000005,     ///<Clock option 5
            v110=0x00000006,     ///<Clock option 6
            v111=0x00000007,     ///<Clock option 7
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,24),Register::ReadWriteAccess,PcsVal> pcs{}; 
        namespace PcsValC{
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v000> v000{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v001> v001{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v010> v010{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v011> v011{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v100> v100{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v101> v101{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v110> v110{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v111> v111{};
        }
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccFtm0{    ///<PCC FTM0 Register
        using Addr = Register::Address<0x400650e0,0x38ffffff,0x00000000,unsigned>;
        ///Peripheral Clock Source Select
        enum class PcsVal : unsigned {
            v000=0x00000000,     ///<Clock is off. An external clock can be enabled for this peripheral.
            v001=0x00000001,     ///<Clock option 1
            v010=0x00000002,     ///<Clock option 2
            v011=0x00000003,     ///<Clock option 3
            v100=0x00000004,     ///<Clock option 4
            v101=0x00000005,     ///<Clock option 5
            v110=0x00000006,     ///<Clock option 6
            v111=0x00000007,     ///<Clock option 7
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,24),Register::ReadWriteAccess,PcsVal> pcs{}; 
        namespace PcsValC{
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v000> v000{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v001> v001{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v010> v010{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v011> v011{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v100> v100{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v101> v101{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v110> v110{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v111> v111{};
        }
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccFtm1{    ///<PCC FTM1 Register
        using Addr = Register::Address<0x400650e4,0x38ffffff,0x00000000,unsigned>;
        ///Peripheral Clock Source Select
        enum class PcsVal : unsigned {
            v000=0x00000000,     ///<Clock is off. An external clock can be enabled for this peripheral.
            v001=0x00000001,     ///<Clock option 1
            v010=0x00000002,     ///<Clock option 2
            v011=0x00000003,     ///<Clock option 3
            v100=0x00000004,     ///<Clock option 4
            v101=0x00000005,     ///<Clock option 5
            v110=0x00000006,     ///<Clock option 6
            v111=0x00000007,     ///<Clock option 7
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,24),Register::ReadWriteAccess,PcsVal> pcs{}; 
        namespace PcsValC{
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v000> v000{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v001> v001{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v010> v010{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v011> v011{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v100> v100{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v101> v101{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v110> v110{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v111> v111{};
        }
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccFtm2{    ///<PCC FTM2 Register
        using Addr = Register::Address<0x400650e8,0x38ffffff,0x00000000,unsigned>;
        ///Peripheral Clock Source Select
        enum class PcsVal : unsigned {
            v000=0x00000000,     ///<Clock is off. An external clock can be enabled for this peripheral.
            v001=0x00000001,     ///<Clock option 1
            v010=0x00000002,     ///<Clock option 2
            v011=0x00000003,     ///<Clock option 3
            v100=0x00000004,     ///<Clock option 4
            v101=0x00000005,     ///<Clock option 5
            v110=0x00000006,     ///<Clock option 6
            v111=0x00000007,     ///<Clock option 7
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,24),Register::ReadWriteAccess,PcsVal> pcs{}; 
        namespace PcsValC{
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v000> v000{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v001> v001{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v010> v010{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v011> v011{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v100> v100{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v101> v101{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v110> v110{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v111> v111{};
        }
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccAdc0{    ///<PCC ADC0 Register
        using Addr = Register::Address<0x400650ec,0x38ffffff,0x00000000,unsigned>;
        ///Peripheral Clock Source Select
        enum class PcsVal : unsigned {
            v000=0x00000000,     ///<Clock is off.
            v001=0x00000001,     ///<Clock option 1
            v010=0x00000002,     ///<Clock option 2
            v011=0x00000003,     ///<Clock option 3
            v100=0x00000004,     ///<Clock option 4
            v101=0x00000005,     ///<Clock option 5
            v110=0x00000006,     ///<Clock option 6
            v111=0x00000007,     ///<Clock option 7
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,24),Register::ReadWriteAccess,PcsVal> pcs{}; 
        namespace PcsValC{
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v000> v000{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v001> v001{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v010> v010{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v011> v011{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v100> v100{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v101> v101{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v110> v110{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v111> v111{};
        }
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccRtc{    ///<PCC RTC Register
        using Addr = Register::Address<0x400650f4,0x3fffffff,0x00000000,unsigned>;
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccLptmr0{    ///<PCC LPTMR0 Register
        using Addr = Register::Address<0x40065100,0x38fffff0,0x00000000,unsigned>;
        ///Peripheral Clock Divider Select
        enum class PcdVal : unsigned {
            v000=0x00000000,     ///<Divide by 1.
            v001=0x00000001,     ///<Divide by 2.
            v010=0x00000002,     ///<Divide by 3.
            v011=0x00000003,     ///<Divide by 4.
            v100=0x00000004,     ///<Divide by 5.
            v101=0x00000005,     ///<Divide by 6.
            v110=0x00000006,     ///<Divide by 7.
            v111=0x00000007,     ///<Divide by 8.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,PcdVal> pcd{}; 
        namespace PcdValC{
            constexpr Register::FieldValue<decltype(pcd)::Type,PcdVal::v000> v000{};
            constexpr Register::FieldValue<decltype(pcd)::Type,PcdVal::v001> v001{};
            constexpr Register::FieldValue<decltype(pcd)::Type,PcdVal::v010> v010{};
            constexpr Register::FieldValue<decltype(pcd)::Type,PcdVal::v011> v011{};
            constexpr Register::FieldValue<decltype(pcd)::Type,PcdVal::v100> v100{};
            constexpr Register::FieldValue<decltype(pcd)::Type,PcdVal::v101> v101{};
            constexpr Register::FieldValue<decltype(pcd)::Type,PcdVal::v110> v110{};
            constexpr Register::FieldValue<decltype(pcd)::Type,PcdVal::v111> v111{};
        }
        ///Peripheral Clock Divider Fraction
        enum class FracVal : unsigned {
            v0=0x00000000,     ///<Fractional value is 0.
            v1=0x00000001,     ///<Fractional value is 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,FracVal> frac{}; 
        namespace FracValC{
            constexpr Register::FieldValue<decltype(frac)::Type,FracVal::v0> v0{};
            constexpr Register::FieldValue<decltype(frac)::Type,FracVal::v1> v1{};
        }
        ///Peripheral Clock Source Select
        enum class PcsVal : unsigned {
            v000=0x00000000,     ///<Clock is off.
            v001=0x00000001,     ///<Clock option 1
            v010=0x00000002,     ///<Clock option 2
            v011=0x00000003,     ///<Clock option 3
            v100=0x00000004,     ///<Clock option 4
            v101=0x00000005,     ///<Clock option 5
            v110=0x00000006,     ///<Clock option 6
            v111=0x00000007,     ///<Clock option 7
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,24),Register::ReadWriteAccess,PcsVal> pcs{}; 
        namespace PcsValC{
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v000> v000{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v001> v001{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v010> v010{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v011> v011{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v100> v100{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v101> v101{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v110> v110{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v111> v111{};
        }
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccPorta{    ///<PCC PORTA Register
        using Addr = Register::Address<0x40065124,0x3fffffff,0x00000000,unsigned>;
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccPortb{    ///<PCC PORTB Register
        using Addr = Register::Address<0x40065128,0x3fffffff,0x00000000,unsigned>;
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccPortc{    ///<PCC PORTC Register
        using Addr = Register::Address<0x4006512c,0x3fffffff,0x00000000,unsigned>;
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccPortd{    ///<PCC PORTD Register
        using Addr = Register::Address<0x40065130,0x3fffffff,0x00000000,unsigned>;
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccPorte{    ///<PCC PORTE Register
        using Addr = Register::Address<0x40065134,0x3fffffff,0x00000000,unsigned>;
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccSai0{    ///<PCC SAI0 Register
        using Addr = Register::Address<0x40065150,0x3fffffff,0x00000000,unsigned>;
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccSai1{    ///<PCC SAI1 Register
        using Addr = Register::Address<0x40065154,0x3fffffff,0x00000000,unsigned>;
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccFlexio{    ///<PCC FlexIO Register
        using Addr = Register::Address<0x40065168,0x38ffffff,0x00000000,unsigned>;
        ///Peripheral Clock Source Select
        enum class PcsVal : unsigned {
            v000=0x00000000,     ///<Clock is off.
            v001=0x00000001,     ///<Clock option 1
            v010=0x00000002,     ///<Clock option 2
            v011=0x00000003,     ///<Clock option 3
            v100=0x00000004,     ///<Clock option 4
            v101=0x00000005,     ///<Clock option 5
            v110=0x00000006,     ///<Clock option 6
            v111=0x00000007,     ///<Clock option 7
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,24),Register::ReadWriteAccess,PcsVal> pcs{}; 
        namespace PcsValC{
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v000> v000{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v001> v001{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v010> v010{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v011> v011{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v100> v100{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v101> v101{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v110> v110{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v111> v111{};
        }
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccEwm{    ///<PCC EWM Register
        using Addr = Register::Address<0x40065184,0x3fffffff,0x00000000,unsigned>;
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccLpi2c0{    ///<PCC LPI2C0 Register
        using Addr = Register::Address<0x40065198,0x38ffffff,0x00000000,unsigned>;
        ///Peripheral Clock Source Select
        enum class PcsVal : unsigned {
            v000=0x00000000,     ///<Clock is off.
            v001=0x00000001,     ///<Clock option 1
            v010=0x00000002,     ///<Clock option 2
            v011=0x00000003,     ///<Clock option 3
            v100=0x00000004,     ///<Clock option 4
            v101=0x00000005,     ///<Clock option 5
            v110=0x00000006,     ///<Clock option 6
            v111=0x00000007,     ///<Clock option 7
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,24),Register::ReadWriteAccess,PcsVal> pcs{}; 
        namespace PcsValC{
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v000> v000{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v001> v001{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v010> v010{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v011> v011{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v100> v100{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v101> v101{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v110> v110{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v111> v111{};
        }
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccLpi2c1{    ///<PCC LPI2C1 Register
        using Addr = Register::Address<0x4006519c,0x38ffffff,0x00000000,unsigned>;
        ///Peripheral Clock Source Select
        enum class PcsVal : unsigned {
            v000=0x00000000,     ///<Clock is off.
            v001=0x00000001,     ///<Clock option 1
            v010=0x00000002,     ///<Clock option 2
            v011=0x00000003,     ///<Clock option 3
            v100=0x00000004,     ///<Clock option 4
            v101=0x00000005,     ///<Clock option 5
            v110=0x00000006,     ///<Clock option 6
            v111=0x00000007,     ///<Clock option 7
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,24),Register::ReadWriteAccess,PcsVal> pcs{}; 
        namespace PcsValC{
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v000> v000{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v001> v001{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v010> v010{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v011> v011{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v100> v100{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v101> v101{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v110> v110{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v111> v111{};
        }
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccLpuart0{    ///<PCC LPUART0 Register
        using Addr = Register::Address<0x400651a8,0x38ffffff,0x00000000,unsigned>;
        ///Peripheral Clock Source Select
        enum class PcsVal : unsigned {
            v000=0x00000000,     ///<Clock is off.
            v001=0x00000001,     ///<Clock option 1
            v010=0x00000002,     ///<Clock option 2
            v011=0x00000003,     ///<Clock option 3
            v100=0x00000004,     ///<Clock option 4
            v101=0x00000005,     ///<Clock option 5
            v110=0x00000006,     ///<Clock option 6
            v111=0x00000007,     ///<Clock option 7
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,24),Register::ReadWriteAccess,PcsVal> pcs{}; 
        namespace PcsValC{
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v000> v000{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v001> v001{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v010> v010{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v011> v011{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v100> v100{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v101> v101{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v110> v110{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v111> v111{};
        }
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccLpuart1{    ///<PCC LPUART1 Register
        using Addr = Register::Address<0x400651ac,0x38ffffff,0x00000000,unsigned>;
        ///Peripheral Clock Source Select
        enum class PcsVal : unsigned {
            v000=0x00000000,     ///<Clock is off.
            v001=0x00000001,     ///<Clock option 1
            v010=0x00000002,     ///<Clock option 2
            v011=0x00000003,     ///<Clock option 3
            v100=0x00000004,     ///<Clock option 4
            v101=0x00000005,     ///<Clock option 5
            v110=0x00000006,     ///<Clock option 6
            v111=0x00000007,     ///<Clock option 7
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,24),Register::ReadWriteAccess,PcsVal> pcs{}; 
        namespace PcsValC{
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v000> v000{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v001> v001{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v010> v010{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v011> v011{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v100> v100{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v101> v101{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v110> v110{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v111> v111{};
        }
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccLpuart2{    ///<PCC LPUART2 Register
        using Addr = Register::Address<0x400651b0,0x38ffffff,0x00000000,unsigned>;
        ///Peripheral Clock Source Select
        enum class PcsVal : unsigned {
            v000=0x00000000,     ///<Clock is off.
            v001=0x00000001,     ///<Clock option 1
            v010=0x00000002,     ///<Clock option 2
            v011=0x00000003,     ///<Clock option 3
            v100=0x00000004,     ///<Clock option 4
            v101=0x00000005,     ///<Clock option 5
            v110=0x00000006,     ///<Clock option 6
            v111=0x00000007,     ///<Clock option 7
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,24),Register::ReadWriteAccess,PcsVal> pcs{}; 
        namespace PcsValC{
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v000> v000{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v001> v001{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v010> v010{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v011> v011{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v100> v100{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v101> v101{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v110> v110{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v111> v111{};
        }
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccFtm4{    ///<PCC FTM4 Register
        using Addr = Register::Address<0x400651b8,0x38ffffff,0x00000000,unsigned>;
        ///Peripheral Clock Source Select
        enum class PcsVal : unsigned {
            v000=0x00000000,     ///<Clock is off. An external clock can be enabled for this peripheral.
            v001=0x00000001,     ///<Clock option 1
            v010=0x00000002,     ///<Clock option 2
            v011=0x00000003,     ///<Clock option 3
            v100=0x00000004,     ///<Clock option 4
            v101=0x00000005,     ///<Clock option 5
            v110=0x00000006,     ///<Clock option 6
            v111=0x00000007,     ///<Clock option 7
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,24),Register::ReadWriteAccess,PcsVal> pcs{}; 
        namespace PcsValC{
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v000> v000{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v001> v001{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v010> v010{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v011> v011{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v100> v100{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v101> v101{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v110> v110{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v111> v111{};
        }
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccFtm5{    ///<PCC FTM5 Register
        using Addr = Register::Address<0x400651bc,0x38ffffff,0x00000000,unsigned>;
        ///Peripheral Clock Source Select
        enum class PcsVal : unsigned {
            v000=0x00000000,     ///<Clock is off. An external clock can be enabled for this peripheral.
            v001=0x00000001,     ///<Clock option 1
            v010=0x00000002,     ///<Clock option 2
            v011=0x00000003,     ///<Clock option 3
            v100=0x00000004,     ///<Clock option 4
            v101=0x00000005,     ///<Clock option 5
            v110=0x00000006,     ///<Clock option 6
            v111=0x00000007,     ///<Clock option 7
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,24),Register::ReadWriteAccess,PcsVal> pcs{}; 
        namespace PcsValC{
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v000> v000{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v001> v001{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v010> v010{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v011> v011{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v100> v100{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v101> v101{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v110> v110{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v111> v111{};
        }
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccFtm6{    ///<PCC FTM6 Register
        using Addr = Register::Address<0x400651c0,0x38ffffff,0x00000000,unsigned>;
        ///Peripheral Clock Source Select
        enum class PcsVal : unsigned {
            v000=0x00000000,     ///<Clock is off. An external clock can be enabled for this peripheral.
            v001=0x00000001,     ///<Clock option 1
            v010=0x00000002,     ///<Clock option 2
            v011=0x00000003,     ///<Clock option 3
            v100=0x00000004,     ///<Clock option 4
            v101=0x00000005,     ///<Clock option 5
            v110=0x00000006,     ///<Clock option 6
            v111=0x00000007,     ///<Clock option 7
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,24),Register::ReadWriteAccess,PcsVal> pcs{}; 
        namespace PcsValC{
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v000> v000{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v001> v001{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v010> v010{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v011> v011{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v100> v100{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v101> v101{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v110> v110{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v111> v111{};
        }
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccFtm7{    ///<PCC FTM7 Register
        using Addr = Register::Address<0x400651c4,0x38ffffff,0x00000000,unsigned>;
        ///Peripheral Clock Source Select
        enum class PcsVal : unsigned {
            v000=0x00000000,     ///<Clock is off. An external clock can be enabled for this peripheral.
            v001=0x00000001,     ///<Clock option 1
            v010=0x00000002,     ///<Clock option 2
            v011=0x00000003,     ///<Clock option 3
            v100=0x00000004,     ///<Clock option 4
            v101=0x00000005,     ///<Clock option 5
            v110=0x00000006,     ///<Clock option 6
            v111=0x00000007,     ///<Clock option 7
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,24),Register::ReadWriteAccess,PcsVal> pcs{}; 
        namespace PcsValC{
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v000> v000{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v001> v001{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v010> v010{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v011> v011{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v100> v100{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v101> v101{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v110> v110{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v111> v111{};
        }
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccCmp0{    ///<PCC CMP0 Register
        using Addr = Register::Address<0x400651cc,0x3fffffff,0x00000000,unsigned>;
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccQspi{    ///<PCC QSPI Register
        using Addr = Register::Address<0x400651d8,0x3fffffff,0x00000000,unsigned>;
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
    namespace PccPccEnet{    ///<PCC ENET Register
        using Addr = Register::Address<0x400651e4,0x38fffff0,0x00000000,unsigned>;
        ///Peripheral Clock Divider Select
        enum class PcdVal : unsigned {
            v000=0x00000000,     ///<Divide by 1.
            v001=0x00000001,     ///<Divide by 2.
            v010=0x00000002,     ///<Divide by 3.
            v011=0x00000003,     ///<Divide by 4.
            v100=0x00000004,     ///<Divide by 5.
            v101=0x00000005,     ///<Divide by 6.
            v110=0x00000006,     ///<Divide by 7.
            v111=0x00000007,     ///<Divide by 8.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,PcdVal> pcd{}; 
        namespace PcdValC{
            constexpr Register::FieldValue<decltype(pcd)::Type,PcdVal::v000> v000{};
            constexpr Register::FieldValue<decltype(pcd)::Type,PcdVal::v001> v001{};
            constexpr Register::FieldValue<decltype(pcd)::Type,PcdVal::v010> v010{};
            constexpr Register::FieldValue<decltype(pcd)::Type,PcdVal::v011> v011{};
            constexpr Register::FieldValue<decltype(pcd)::Type,PcdVal::v100> v100{};
            constexpr Register::FieldValue<decltype(pcd)::Type,PcdVal::v101> v101{};
            constexpr Register::FieldValue<decltype(pcd)::Type,PcdVal::v110> v110{};
            constexpr Register::FieldValue<decltype(pcd)::Type,PcdVal::v111> v111{};
        }
        ///Peripheral Clock Divider Fraction
        enum class FracVal : unsigned {
            v0=0x00000000,     ///<Fractional value is 0.
            v1=0x00000001,     ///<Fractional value is 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,FracVal> frac{}; 
        namespace FracValC{
            constexpr Register::FieldValue<decltype(frac)::Type,FracVal::v0> v0{};
            constexpr Register::FieldValue<decltype(frac)::Type,FracVal::v1> v1{};
        }
        ///Peripheral Clock Source Select
        enum class PcsVal : unsigned {
            v000=0x00000000,     ///<Clock is off.
            v001=0x00000001,     ///<Clock option 1
            v010=0x00000002,     ///<Clock option 2
            v011=0x00000003,     ///<Clock option 3
            v100=0x00000004,     ///<Clock option 4
            v101=0x00000005,     ///<Clock option 5
            v110=0x00000006,     ///<Clock option 6
            v111=0x00000007,     ///<Clock option 7
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,24),Register::ReadWriteAccess,PcsVal> pcs{}; 
        namespace PcsValC{
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v000> v000{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v001> v001{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v010> v010{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v011> v011{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v100> v100{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v101> v101{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v110> v110{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v111> v111{};
        }
        ///Clock Gate Control
        enum class CgcVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled. The current clock selection and divider options are locked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CgcVal> cgc{}; 
        namespace CgcValC{
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgc)::Type,CgcVal::v1> v1{};
        }
        ///Present
        enum class PrVal : unsigned {
            v0=0x00000000,     ///<Peripheral is not present.
            v1=0x00000001,     ///<Peripheral is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PrVal> pr{}; 
        namespace PrValC{
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pr)::Type,PrVal::v1> v1{};
        }
    }
}
