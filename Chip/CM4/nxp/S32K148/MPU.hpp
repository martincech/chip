#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//Memory protection unit
    namespace MpuCesr{    ///<Control/Error Status Register
        using Addr = Register::Address<0x4000d000,0x07f000fe,0x00000000,unsigned>;
        ///Valid
        enum class VldVal : unsigned {
            v0=0x00000000,     ///<MPU is disabled. All accesses from all bus masters are allowed.
            v1=0x00000001,     ///<MPU is enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,VldVal> vld{}; 
        namespace VldValC{
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v0> v0{};
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v1> v1{};
        }
        ///Number Of Region Descriptors
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> nrgd{}; 
        ///Number Of Slave Ports
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,12),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> nsp{}; 
        ///Hardware Revision Level
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> hrl{}; 
        ///Slave Port 4 Error
        enum class Sperr4Val : unsigned {
            v0=0x00000000,     ///<No error has occurred for slave port 4.
            v1=0x00000001,     ///<An error has occurred for slave port 4.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,Sperr4Val> sperr4{}; 
        namespace Sperr4ValC{
            constexpr Register::FieldValue<decltype(sperr4)::Type,Sperr4Val::v0> v0{};
            constexpr Register::FieldValue<decltype(sperr4)::Type,Sperr4Val::v1> v1{};
        }
        ///Slave Port 3 Error
        enum class Sperr3Val : unsigned {
            v0=0x00000000,     ///<No error has occurred for slave port 3.
            v1=0x00000001,     ///<An error has occurred for slave port 3.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,Sperr3Val> sperr3{}; 
        namespace Sperr3ValC{
            constexpr Register::FieldValue<decltype(sperr3)::Type,Sperr3Val::v0> v0{};
            constexpr Register::FieldValue<decltype(sperr3)::Type,Sperr3Val::v1> v1{};
        }
        ///Slave Port 2 Error
        enum class Sperr2Val : unsigned {
            v0=0x00000000,     ///<No error has occurred for slave port 2.
            v1=0x00000001,     ///<An error has occurred for slave port 2.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,Sperr2Val> sperr2{}; 
        namespace Sperr2ValC{
            constexpr Register::FieldValue<decltype(sperr2)::Type,Sperr2Val::v0> v0{};
            constexpr Register::FieldValue<decltype(sperr2)::Type,Sperr2Val::v1> v1{};
        }
        ///Slave Port 1 Error
        enum class Sperr1Val : unsigned {
            v0=0x00000000,     ///<No error has occurred for slave port 1.
            v1=0x00000001,     ///<An error has occurred for slave port 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,Sperr1Val> sperr1{}; 
        namespace Sperr1ValC{
            constexpr Register::FieldValue<decltype(sperr1)::Type,Sperr1Val::v0> v0{};
            constexpr Register::FieldValue<decltype(sperr1)::Type,Sperr1Val::v1> v1{};
        }
        ///Slave Port 0 Error
        enum class Sperr0Val : unsigned {
            v0=0x00000000,     ///<No error has occurred for slave port 0.
            v1=0x00000001,     ///<An error has occurred for slave port 0.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,Sperr0Val> sperr0{}; 
        namespace Sperr0ValC{
            constexpr Register::FieldValue<decltype(sperr0)::Type,Sperr0Val::v0> v0{};
            constexpr Register::FieldValue<decltype(sperr0)::Type,Sperr0Val::v1> v1{};
        }
    }
    namespace MpuEar0{    ///<Error Address Register, slave port 0
        using Addr = Register::Address<0x4000d010,0x00000000,0x00000000,unsigned>;
        ///Error Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> eaddr{}; 
    }
    namespace MpuEdr0{    ///<Error Detail Register, slave port 0
        using Addr = Register::Address<0x4000d014,0x00000000,0x00000000,unsigned>;
        ///Error Read/Write
        enum class ErwVal : unsigned {
            v0=0x00000000,     ///<Read
            v1=0x00000001,     ///<Write
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,ErwVal> erw{}; 
        namespace ErwValC{
            constexpr Register::FieldValue<decltype(erw)::Type,ErwVal::v0> v0{};
            constexpr Register::FieldValue<decltype(erw)::Type,ErwVal::v1> v1{};
        }
        ///Error Attributes
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,1),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> eattr{}; 
        ///Error Master Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,4),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> emn{}; 
        ///Error Process Identification
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> epid{}; 
        ///Error Access Control Detail
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> eacd{}; 
    }
    namespace MpuEar1{    ///<Error Address Register, slave port 1
        using Addr = Register::Address<0x4000d018,0x00000000,0x00000000,unsigned>;
        ///Error Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> eaddr{}; 
    }
    namespace MpuEdr1{    ///<Error Detail Register, slave port 1
        using Addr = Register::Address<0x4000d01c,0x00000000,0x00000000,unsigned>;
        ///Error Read/Write
        enum class ErwVal : unsigned {
            v0=0x00000000,     ///<Read
            v1=0x00000001,     ///<Write
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,ErwVal> erw{}; 
        namespace ErwValC{
            constexpr Register::FieldValue<decltype(erw)::Type,ErwVal::v0> v0{};
            constexpr Register::FieldValue<decltype(erw)::Type,ErwVal::v1> v1{};
        }
        ///Error Attributes
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,1),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> eattr{}; 
        ///Error Master Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,4),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> emn{}; 
        ///Error Process Identification
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> epid{}; 
        ///Error Access Control Detail
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> eacd{}; 
    }
    namespace MpuEar2{    ///<Error Address Register, slave port 2
        using Addr = Register::Address<0x4000d020,0x00000000,0x00000000,unsigned>;
        ///Error Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> eaddr{}; 
    }
    namespace MpuEdr2{    ///<Error Detail Register, slave port 2
        using Addr = Register::Address<0x4000d024,0x00000000,0x00000000,unsigned>;
        ///Error Read/Write
        enum class ErwVal : unsigned {
            v0=0x00000000,     ///<Read
            v1=0x00000001,     ///<Write
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,ErwVal> erw{}; 
        namespace ErwValC{
            constexpr Register::FieldValue<decltype(erw)::Type,ErwVal::v0> v0{};
            constexpr Register::FieldValue<decltype(erw)::Type,ErwVal::v1> v1{};
        }
        ///Error Attributes
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,1),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> eattr{}; 
        ///Error Master Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,4),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> emn{}; 
        ///Error Process Identification
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> epid{}; 
        ///Error Access Control Detail
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> eacd{}; 
    }
    namespace MpuEar3{    ///<Error Address Register, slave port 3
        using Addr = Register::Address<0x4000d028,0x00000000,0x00000000,unsigned>;
        ///Error Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> eaddr{}; 
    }
    namespace MpuEdr3{    ///<Error Detail Register, slave port 3
        using Addr = Register::Address<0x4000d02c,0x00000000,0x00000000,unsigned>;
        ///Error Read/Write
        enum class ErwVal : unsigned {
            v0=0x00000000,     ///<Read
            v1=0x00000001,     ///<Write
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,ErwVal> erw{}; 
        namespace ErwValC{
            constexpr Register::FieldValue<decltype(erw)::Type,ErwVal::v0> v0{};
            constexpr Register::FieldValue<decltype(erw)::Type,ErwVal::v1> v1{};
        }
        ///Error Attributes
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,1),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> eattr{}; 
        ///Error Master Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,4),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> emn{}; 
        ///Error Process Identification
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> epid{}; 
        ///Error Access Control Detail
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> eacd{}; 
    }
    namespace MpuEar4{    ///<Error Address Register, slave port 4
        using Addr = Register::Address<0x4000d030,0x00000000,0x00000000,unsigned>;
        ///Error Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> eaddr{}; 
    }
    namespace MpuEdr4{    ///<Error Detail Register, slave port 4
        using Addr = Register::Address<0x4000d034,0x00000000,0x00000000,unsigned>;
        ///Error Read/Write
        enum class ErwVal : unsigned {
            v0=0x00000000,     ///<Read
            v1=0x00000001,     ///<Write
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,ErwVal> erw{}; 
        namespace ErwValC{
            constexpr Register::FieldValue<decltype(erw)::Type,ErwVal::v0> v0{};
            constexpr Register::FieldValue<decltype(erw)::Type,ErwVal::v1> v1{};
        }
        ///Error Attributes
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,1),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> eattr{}; 
        ///Error Master Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,4),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> emn{}; 
        ///Error Process Identification
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> epid{}; 
        ///Error Access Control Detail
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> eacd{}; 
    }
    namespace MpuRgd0Word0{    ///<Region Descriptor 0, Word 0
        using Addr = Register::Address<0x4000d400,0x0000001f,0x00000000,unsigned>;
        ///Start Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> srtaddr{}; 
    }
    namespace MpuRgd0Word1{    ///<Region Descriptor 0, Word 1
        using Addr = Register::Address<0x4000d404,0x0000001f,0x00000000,unsigned>;
        ///End Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> endaddr{}; 
    }
    namespace MpuRgd0Word2{    ///<Region Descriptor 0, Word 2
        using Addr = Register::Address<0x4000d408,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgd0Word3{    ///<Region Descriptor 0, Word 3
        using Addr = Register::Address<0x4000d40c,0x0000fffe,0x00000000,unsigned>;
        ///Valid
        enum class VldVal : unsigned {
            v0=0x00000000,     ///<Region descriptor is invalid
            v1=0x00000001,     ///<Region descriptor is valid
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,VldVal> vld{}; 
        namespace VldValC{
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v0> v0{};
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v1> v1{};
        }
        ///Process Identifier Mask
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> pidmask{}; 
        ///Process Identifier
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> pid{}; 
    }
    namespace MpuRgd1Word0{    ///<Region Descriptor 1, Word 0
        using Addr = Register::Address<0x4000d410,0x0000001f,0x00000000,unsigned>;
        ///Start Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> srtaddr{}; 
    }
    namespace MpuRgd1Word1{    ///<Region Descriptor 1, Word 1
        using Addr = Register::Address<0x4000d414,0x0000001f,0x00000000,unsigned>;
        ///End Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> endaddr{}; 
    }
    namespace MpuRgd1Word2{    ///<Region Descriptor 1, Word 2
        using Addr = Register::Address<0x4000d418,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgd1Word3{    ///<Region Descriptor 1, Word 3
        using Addr = Register::Address<0x4000d41c,0x0000fffe,0x00000000,unsigned>;
        ///Valid
        enum class VldVal : unsigned {
            v0=0x00000000,     ///<Region descriptor is invalid
            v1=0x00000001,     ///<Region descriptor is valid
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,VldVal> vld{}; 
        namespace VldValC{
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v0> v0{};
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v1> v1{};
        }
        ///Process Identifier Mask
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> pidmask{}; 
        ///Process Identifier
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> pid{}; 
    }
    namespace MpuRgd2Word0{    ///<Region Descriptor 2, Word 0
        using Addr = Register::Address<0x4000d420,0x0000001f,0x00000000,unsigned>;
        ///Start Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> srtaddr{}; 
    }
    namespace MpuRgd2Word1{    ///<Region Descriptor 2, Word 1
        using Addr = Register::Address<0x4000d424,0x0000001f,0x00000000,unsigned>;
        ///End Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> endaddr{}; 
    }
    namespace MpuRgd2Word2{    ///<Region Descriptor 2, Word 2
        using Addr = Register::Address<0x4000d428,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgd2Word3{    ///<Region Descriptor 2, Word 3
        using Addr = Register::Address<0x4000d42c,0x0000fffe,0x00000000,unsigned>;
        ///Valid
        enum class VldVal : unsigned {
            v0=0x00000000,     ///<Region descriptor is invalid
            v1=0x00000001,     ///<Region descriptor is valid
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,VldVal> vld{}; 
        namespace VldValC{
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v0> v0{};
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v1> v1{};
        }
        ///Process Identifier Mask
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> pidmask{}; 
        ///Process Identifier
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> pid{}; 
    }
    namespace MpuRgd3Word0{    ///<Region Descriptor 3, Word 0
        using Addr = Register::Address<0x4000d430,0x0000001f,0x00000000,unsigned>;
        ///Start Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> srtaddr{}; 
    }
    namespace MpuRgd3Word1{    ///<Region Descriptor 3, Word 1
        using Addr = Register::Address<0x4000d434,0x0000001f,0x00000000,unsigned>;
        ///End Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> endaddr{}; 
    }
    namespace MpuRgd3Word2{    ///<Region Descriptor 3, Word 2
        using Addr = Register::Address<0x4000d438,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgd3Word3{    ///<Region Descriptor 3, Word 3
        using Addr = Register::Address<0x4000d43c,0x0000fffe,0x00000000,unsigned>;
        ///Valid
        enum class VldVal : unsigned {
            v0=0x00000000,     ///<Region descriptor is invalid
            v1=0x00000001,     ///<Region descriptor is valid
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,VldVal> vld{}; 
        namespace VldValC{
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v0> v0{};
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v1> v1{};
        }
        ///Process Identifier Mask
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> pidmask{}; 
        ///Process Identifier
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> pid{}; 
    }
    namespace MpuRgd4Word0{    ///<Region Descriptor 4, Word 0
        using Addr = Register::Address<0x4000d440,0x0000001f,0x00000000,unsigned>;
        ///Start Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> srtaddr{}; 
    }
    namespace MpuRgd4Word1{    ///<Region Descriptor 4, Word 1
        using Addr = Register::Address<0x4000d444,0x0000001f,0x00000000,unsigned>;
        ///End Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> endaddr{}; 
    }
    namespace MpuRgd4Word2{    ///<Region Descriptor 4, Word 2
        using Addr = Register::Address<0x4000d448,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgd4Word3{    ///<Region Descriptor 4, Word 3
        using Addr = Register::Address<0x4000d44c,0x0000fffe,0x00000000,unsigned>;
        ///Valid
        enum class VldVal : unsigned {
            v0=0x00000000,     ///<Region descriptor is invalid
            v1=0x00000001,     ///<Region descriptor is valid
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,VldVal> vld{}; 
        namespace VldValC{
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v0> v0{};
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v1> v1{};
        }
        ///Process Identifier Mask
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> pidmask{}; 
        ///Process Identifier
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> pid{}; 
    }
    namespace MpuRgd5Word0{    ///<Region Descriptor 5, Word 0
        using Addr = Register::Address<0x4000d450,0x0000001f,0x00000000,unsigned>;
        ///Start Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> srtaddr{}; 
    }
    namespace MpuRgd5Word1{    ///<Region Descriptor 5, Word 1
        using Addr = Register::Address<0x4000d454,0x0000001f,0x00000000,unsigned>;
        ///End Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> endaddr{}; 
    }
    namespace MpuRgd5Word2{    ///<Region Descriptor 5, Word 2
        using Addr = Register::Address<0x4000d458,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgd5Word3{    ///<Region Descriptor 5, Word 3
        using Addr = Register::Address<0x4000d45c,0x0000fffe,0x00000000,unsigned>;
        ///Valid
        enum class VldVal : unsigned {
            v0=0x00000000,     ///<Region descriptor is invalid
            v1=0x00000001,     ///<Region descriptor is valid
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,VldVal> vld{}; 
        namespace VldValC{
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v0> v0{};
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v1> v1{};
        }
        ///Process Identifier Mask
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> pidmask{}; 
        ///Process Identifier
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> pid{}; 
    }
    namespace MpuRgd6Word0{    ///<Region Descriptor 6, Word 0
        using Addr = Register::Address<0x4000d460,0x0000001f,0x00000000,unsigned>;
        ///Start Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> srtaddr{}; 
    }
    namespace MpuRgd6Word1{    ///<Region Descriptor 6, Word 1
        using Addr = Register::Address<0x4000d464,0x0000001f,0x00000000,unsigned>;
        ///End Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> endaddr{}; 
    }
    namespace MpuRgd6Word2{    ///<Region Descriptor 6, Word 2
        using Addr = Register::Address<0x4000d468,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgd6Word3{    ///<Region Descriptor 6, Word 3
        using Addr = Register::Address<0x4000d46c,0x0000fffe,0x00000000,unsigned>;
        ///Valid
        enum class VldVal : unsigned {
            v0=0x00000000,     ///<Region descriptor is invalid
            v1=0x00000001,     ///<Region descriptor is valid
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,VldVal> vld{}; 
        namespace VldValC{
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v0> v0{};
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v1> v1{};
        }
        ///Process Identifier Mask
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> pidmask{}; 
        ///Process Identifier
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> pid{}; 
    }
    namespace MpuRgd7Word0{    ///<Region Descriptor 7, Word 0
        using Addr = Register::Address<0x4000d470,0x0000001f,0x00000000,unsigned>;
        ///Start Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> srtaddr{}; 
    }
    namespace MpuRgd7Word1{    ///<Region Descriptor 7, Word 1
        using Addr = Register::Address<0x4000d474,0x0000001f,0x00000000,unsigned>;
        ///End Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> endaddr{}; 
    }
    namespace MpuRgd7Word2{    ///<Region Descriptor 7, Word 2
        using Addr = Register::Address<0x4000d478,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgd7Word3{    ///<Region Descriptor 7, Word 3
        using Addr = Register::Address<0x4000d47c,0x0000fffe,0x00000000,unsigned>;
        ///Valid
        enum class VldVal : unsigned {
            v0=0x00000000,     ///<Region descriptor is invalid
            v1=0x00000001,     ///<Region descriptor is valid
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,VldVal> vld{}; 
        namespace VldValC{
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v0> v0{};
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v1> v1{};
        }
        ///Process Identifier Mask
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> pidmask{}; 
        ///Process Identifier
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> pid{}; 
    }
    namespace MpuRgd8Word0{    ///<Region Descriptor 8, Word 0
        using Addr = Register::Address<0x4000d480,0x0000001f,0x00000000,unsigned>;
        ///Start Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> srtaddr{}; 
    }
    namespace MpuRgd8Word1{    ///<Region Descriptor 8, Word 1
        using Addr = Register::Address<0x4000d484,0x0000001f,0x00000000,unsigned>;
        ///End Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> endaddr{}; 
    }
    namespace MpuRgd8Word2{    ///<Region Descriptor 8, Word 2
        using Addr = Register::Address<0x4000d488,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgd8Word3{    ///<Region Descriptor 8, Word 3
        using Addr = Register::Address<0x4000d48c,0x0000fffe,0x00000000,unsigned>;
        ///Valid
        enum class VldVal : unsigned {
            v0=0x00000000,     ///<Region descriptor is invalid
            v1=0x00000001,     ///<Region descriptor is valid
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,VldVal> vld{}; 
        namespace VldValC{
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v0> v0{};
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v1> v1{};
        }
        ///Process Identifier Mask
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> pidmask{}; 
        ///Process Identifier
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> pid{}; 
    }
    namespace MpuRgd9Word0{    ///<Region Descriptor 9, Word 0
        using Addr = Register::Address<0x4000d490,0x0000001f,0x00000000,unsigned>;
        ///Start Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> srtaddr{}; 
    }
    namespace MpuRgd9Word1{    ///<Region Descriptor 9, Word 1
        using Addr = Register::Address<0x4000d494,0x0000001f,0x00000000,unsigned>;
        ///End Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> endaddr{}; 
    }
    namespace MpuRgd9Word2{    ///<Region Descriptor 9, Word 2
        using Addr = Register::Address<0x4000d498,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgd9Word3{    ///<Region Descriptor 9, Word 3
        using Addr = Register::Address<0x4000d49c,0x0000fffe,0x00000000,unsigned>;
        ///Valid
        enum class VldVal : unsigned {
            v0=0x00000000,     ///<Region descriptor is invalid
            v1=0x00000001,     ///<Region descriptor is valid
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,VldVal> vld{}; 
        namespace VldValC{
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v0> v0{};
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v1> v1{};
        }
        ///Process Identifier Mask
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> pidmask{}; 
        ///Process Identifier
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> pid{}; 
    }
    namespace MpuRgd10Word0{    ///<Region Descriptor 10, Word 0
        using Addr = Register::Address<0x4000d4a0,0x0000001f,0x00000000,unsigned>;
        ///Start Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> srtaddr{}; 
    }
    namespace MpuRgd10Word1{    ///<Region Descriptor 10, Word 1
        using Addr = Register::Address<0x4000d4a4,0x0000001f,0x00000000,unsigned>;
        ///End Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> endaddr{}; 
    }
    namespace MpuRgd10Word2{    ///<Region Descriptor 10, Word 2
        using Addr = Register::Address<0x4000d4a8,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgd10Word3{    ///<Region Descriptor 10, Word 3
        using Addr = Register::Address<0x4000d4ac,0x0000fffe,0x00000000,unsigned>;
        ///Valid
        enum class VldVal : unsigned {
            v0=0x00000000,     ///<Region descriptor is invalid
            v1=0x00000001,     ///<Region descriptor is valid
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,VldVal> vld{}; 
        namespace VldValC{
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v0> v0{};
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v1> v1{};
        }
        ///Process Identifier Mask
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> pidmask{}; 
        ///Process Identifier
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> pid{}; 
    }
    namespace MpuRgd11Word0{    ///<Region Descriptor 11, Word 0
        using Addr = Register::Address<0x4000d4b0,0x0000001f,0x00000000,unsigned>;
        ///Start Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> srtaddr{}; 
    }
    namespace MpuRgd11Word1{    ///<Region Descriptor 11, Word 1
        using Addr = Register::Address<0x4000d4b4,0x0000001f,0x00000000,unsigned>;
        ///End Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> endaddr{}; 
    }
    namespace MpuRgd11Word2{    ///<Region Descriptor 11, Word 2
        using Addr = Register::Address<0x4000d4b8,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgd11Word3{    ///<Region Descriptor 11, Word 3
        using Addr = Register::Address<0x4000d4bc,0x0000fffe,0x00000000,unsigned>;
        ///Valid
        enum class VldVal : unsigned {
            v0=0x00000000,     ///<Region descriptor is invalid
            v1=0x00000001,     ///<Region descriptor is valid
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,VldVal> vld{}; 
        namespace VldValC{
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v0> v0{};
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v1> v1{};
        }
        ///Process Identifier Mask
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> pidmask{}; 
        ///Process Identifier
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> pid{}; 
    }
    namespace MpuRgd12Word0{    ///<Region Descriptor 12, Word 0
        using Addr = Register::Address<0x4000d4c0,0x0000001f,0x00000000,unsigned>;
        ///Start Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> srtaddr{}; 
    }
    namespace MpuRgd12Word1{    ///<Region Descriptor 12, Word 1
        using Addr = Register::Address<0x4000d4c4,0x0000001f,0x00000000,unsigned>;
        ///End Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> endaddr{}; 
    }
    namespace MpuRgd12Word2{    ///<Region Descriptor 12, Word 2
        using Addr = Register::Address<0x4000d4c8,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgd12Word3{    ///<Region Descriptor 12, Word 3
        using Addr = Register::Address<0x4000d4cc,0x0000fffe,0x00000000,unsigned>;
        ///Valid
        enum class VldVal : unsigned {
            v0=0x00000000,     ///<Region descriptor is invalid
            v1=0x00000001,     ///<Region descriptor is valid
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,VldVal> vld{}; 
        namespace VldValC{
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v0> v0{};
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v1> v1{};
        }
        ///Process Identifier Mask
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> pidmask{}; 
        ///Process Identifier
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> pid{}; 
    }
    namespace MpuRgd13Word0{    ///<Region Descriptor 13, Word 0
        using Addr = Register::Address<0x4000d4d0,0x0000001f,0x00000000,unsigned>;
        ///Start Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> srtaddr{}; 
    }
    namespace MpuRgd13Word1{    ///<Region Descriptor 13, Word 1
        using Addr = Register::Address<0x4000d4d4,0x0000001f,0x00000000,unsigned>;
        ///End Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> endaddr{}; 
    }
    namespace MpuRgd13Word2{    ///<Region Descriptor 13, Word 2
        using Addr = Register::Address<0x4000d4d8,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgd13Word3{    ///<Region Descriptor 13, Word 3
        using Addr = Register::Address<0x4000d4dc,0x0000fffe,0x00000000,unsigned>;
        ///Valid
        enum class VldVal : unsigned {
            v0=0x00000000,     ///<Region descriptor is invalid
            v1=0x00000001,     ///<Region descriptor is valid
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,VldVal> vld{}; 
        namespace VldValC{
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v0> v0{};
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v1> v1{};
        }
        ///Process Identifier Mask
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> pidmask{}; 
        ///Process Identifier
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> pid{}; 
    }
    namespace MpuRgd14Word0{    ///<Region Descriptor 14, Word 0
        using Addr = Register::Address<0x4000d4e0,0x0000001f,0x00000000,unsigned>;
        ///Start Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> srtaddr{}; 
    }
    namespace MpuRgd14Word1{    ///<Region Descriptor 14, Word 1
        using Addr = Register::Address<0x4000d4e4,0x0000001f,0x00000000,unsigned>;
        ///End Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> endaddr{}; 
    }
    namespace MpuRgd14Word2{    ///<Region Descriptor 14, Word 2
        using Addr = Register::Address<0x4000d4e8,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgd14Word3{    ///<Region Descriptor 14, Word 3
        using Addr = Register::Address<0x4000d4ec,0x0000fffe,0x00000000,unsigned>;
        ///Valid
        enum class VldVal : unsigned {
            v0=0x00000000,     ///<Region descriptor is invalid
            v1=0x00000001,     ///<Region descriptor is valid
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,VldVal> vld{}; 
        namespace VldValC{
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v0> v0{};
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v1> v1{};
        }
        ///Process Identifier Mask
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> pidmask{}; 
        ///Process Identifier
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> pid{}; 
    }
    namespace MpuRgd15Word0{    ///<Region Descriptor 15, Word 0
        using Addr = Register::Address<0x4000d4f0,0x0000001f,0x00000000,unsigned>;
        ///Start Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> srtaddr{}; 
    }
    namespace MpuRgd15Word1{    ///<Region Descriptor 15, Word 1
        using Addr = Register::Address<0x4000d4f4,0x0000001f,0x00000000,unsigned>;
        ///End Address
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,5),Register::ReadWriteAccess,unsigned> endaddr{}; 
    }
    namespace MpuRgd15Word2{    ///<Region Descriptor 15, Word 2
        using Addr = Register::Address<0x4000d4f8,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgd15Word3{    ///<Region Descriptor 15, Word 3
        using Addr = Register::Address<0x4000d4fc,0x0000fffe,0x00000000,unsigned>;
        ///Valid
        enum class VldVal : unsigned {
            v0=0x00000000,     ///<Region descriptor is invalid
            v1=0x00000001,     ///<Region descriptor is valid
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,VldVal> vld{}; 
        namespace VldValC{
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v0> v0{};
            constexpr Register::FieldValue<decltype(vld)::Type,VldVal::v1> v1{};
        }
        ///Process Identifier Mask
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> pidmask{}; 
        ///Process Identifier
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> pid{}; 
    }
    namespace MpuRgdaac0{    ///<Region Descriptor Alternate Access Control 0
        using Addr = Register::Address<0x4000d800,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier Enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier Enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgdaac1{    ///<Region Descriptor Alternate Access Control 1
        using Addr = Register::Address<0x4000d804,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier Enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier Enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgdaac2{    ///<Region Descriptor Alternate Access Control 2
        using Addr = Register::Address<0x4000d808,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier Enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier Enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgdaac3{    ///<Region Descriptor Alternate Access Control 3
        using Addr = Register::Address<0x4000d80c,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier Enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier Enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgdaac4{    ///<Region Descriptor Alternate Access Control 4
        using Addr = Register::Address<0x4000d810,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier Enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier Enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgdaac5{    ///<Region Descriptor Alternate Access Control 5
        using Addr = Register::Address<0x4000d814,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier Enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier Enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgdaac6{    ///<Region Descriptor Alternate Access Control 6
        using Addr = Register::Address<0x4000d818,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier Enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier Enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgdaac7{    ///<Region Descriptor Alternate Access Control 7
        using Addr = Register::Address<0x4000d81c,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier Enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier Enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgdaac8{    ///<Region Descriptor Alternate Access Control 8
        using Addr = Register::Address<0x4000d820,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier Enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier Enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgdaac9{    ///<Region Descriptor Alternate Access Control 9
        using Addr = Register::Address<0x4000d824,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier Enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier Enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgdaac10{    ///<Region Descriptor Alternate Access Control 10
        using Addr = Register::Address<0x4000d828,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier Enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier Enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgdaac11{    ///<Region Descriptor Alternate Access Control 11
        using Addr = Register::Address<0x4000d82c,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier Enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier Enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgdaac12{    ///<Region Descriptor Alternate Access Control 12
        using Addr = Register::Address<0x4000d830,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier Enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier Enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgdaac13{    ///<Region Descriptor Alternate Access Control 13
        using Addr = Register::Address<0x4000d834,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier Enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier Enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgdaac14{    ///<Region Descriptor Alternate Access Control 14
        using Addr = Register::Address<0x4000d838,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier Enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier Enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
    namespace MpuRgdaac15{    ///<Region Descriptor Alternate Access Control 15
        using Addr = Register::Address<0x4000d83c,0x00820000,0x00000000,unsigned>;
        ///Bus Master 0 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> m0um{}; 
        ///Bus Master 0 Supervisor Mode Access Control
        enum class M0smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M0UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,M0smVal> m0sm{}; 
        namespace M0smValC{
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m0sm)::Type,M0smVal::v11> v11{};
        }
        ///Bus Master 0 Process Identifier Enable
        enum class M0peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,M0peVal> m0pe{}; 
        namespace M0peValC{
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m0pe)::Type,M0peVal::v1> v1{};
        }
        ///Bus Master 1 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,6),Register::ReadWriteAccess,unsigned> m1um{}; 
        ///Bus Master 1 Supervisor Mode Access Control
        enum class M1smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M1UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,9),Register::ReadWriteAccess,M1smVal> m1sm{}; 
        namespace M1smValC{
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m1sm)::Type,M1smVal::v11> v11{};
        }
        ///Bus Master 1 Process Identifier Enable
        enum class M1peVal : unsigned {
            v0=0x00000000,     ///<Do not include the process identifier in the evaluation
            v1=0x00000001,     ///<Include the process identifier and mask (RGDn.RGDAAC) in the region hit evaluation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M1peVal> m1pe{}; 
        namespace M1peValC{
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m1pe)::Type,M1peVal::v1> v1{};
        }
        ///Bus Master 2 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> m2um{}; 
        ///Bus Master 2 Supervisor Mode Access Control
        enum class M2smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M2UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,15),Register::ReadWriteAccess,M2smVal> m2sm{}; 
        namespace M2smValC{
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m2sm)::Type,M2smVal::v11> v11{};
        }
        ///Bus Master 3 User Mode Access Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,18),Register::ReadWriteAccess,unsigned> m3um{}; 
        ///Bus Master 3 Supervisor Mode Access Control
        enum class M3smVal : unsigned {
            v0=0x00000000,     ///<r/w/x; read, write and execute allowed
            v1=0x00000001,     ///<r/x; read and execute allowed, but no write
            v10=0x00000002,     ///<r/w; read and write allowed, but no execute
            v11=0x00000003,     ///<Same as User mode defined in M3UM
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,21),Register::ReadWriteAccess,M3smVal> m3sm{}; 
        namespace M3smValC{
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v1> v1{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v10> v10{};
            constexpr Register::FieldValue<decltype(m3sm)::Type,M3smVal::v11> v11{};
        }
        ///Bus Master 4 Write Enable
        enum class M4weVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 4 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,M4weVal> m4we{}; 
        namespace M4weValC{
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4we)::Type,M4weVal::v1> v1{};
        }
        ///Bus Master 4 Read Enable
        enum class M4reVal : unsigned {
            v0=0x00000000,     ///<Bus master 4 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 4 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,M4reVal> m4re{}; 
        namespace M4reValC{
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m4re)::Type,M4reVal::v1> v1{};
        }
        ///Bus Master 5 Write Enable
        enum class M5weVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 5 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,M5weVal> m5we{}; 
        namespace M5weValC{
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5we)::Type,M5weVal::v1> v1{};
        }
        ///Bus Master 5 Read Enable
        enum class M5reVal : unsigned {
            v0=0x00000000,     ///<Bus master 5 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 5 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,M5reVal> m5re{}; 
        namespace M5reValC{
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m5re)::Type,M5reVal::v1> v1{};
        }
        ///Bus Master 6 Write Enable
        enum class M6weVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 6 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,M6weVal> m6we{}; 
        namespace M6weValC{
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6we)::Type,M6weVal::v1> v1{};
        }
        ///Bus Master 6 Read Enable
        enum class M6reVal : unsigned {
            v0=0x00000000,     ///<Bus master 6 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 6 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M6reVal> m6re{}; 
        namespace M6reValC{
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m6re)::Type,M6reVal::v1> v1{};
        }
        ///Bus Master 7 Write Enable
        enum class M7weVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 writes terminate with an access error and the write is not performed
            v1=0x00000001,     ///<Bus master 7 writes allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,M7weVal> m7we{}; 
        namespace M7weValC{
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7we)::Type,M7weVal::v1> v1{};
        }
        ///Bus Master 7 Read Enable
        enum class M7reVal : unsigned {
            v0=0x00000000,     ///<Bus master 7 reads terminate with an access error and the read is not performed
            v1=0x00000001,     ///<Bus master 7 reads allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,M7reVal> m7re{}; 
        namespace M7reValC{
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m7re)::Type,M7reVal::v1> v1{};
        }
    }
}
