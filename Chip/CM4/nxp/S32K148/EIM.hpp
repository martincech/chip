#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//Error Injection Module
    namespace EimEimcr{    ///<Error Injection Module Configuration Register
        using Addr = Register::Address<0x40019000,0xfffffffe,0x00000000,unsigned>;
        ///Global Error Injection Enable
        enum class GeienVal : unsigned {
            v0=0x00000000,     ///<Disabled
            v1=0x00000001,     ///<Enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,GeienVal> geien{}; 
        namespace GeienValC{
            constexpr Register::FieldValue<decltype(geien)::Type,GeienVal::v0> v0{};
            constexpr Register::FieldValue<decltype(geien)::Type,GeienVal::v1> v1{};
        }
    }
    namespace EimEichen{    ///<Error Injection Channel Enable register
        using Addr = Register::Address<0x40019004,0x3fffffff,0x00000000,unsigned>;
        ///Error Injection Channel 1 Enable
        enum class Eich1enVal : unsigned {
            v0=0x00000000,     ///<Error injection is disabled on Error Injection Channel 1
            v1=0x00000001,     ///<Error injection is enabled on Error Injection Channel 1
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,Eich1enVal> eich1en{}; 
        namespace Eich1enValC{
            constexpr Register::FieldValue<decltype(eich1en)::Type,Eich1enVal::v0> v0{};
            constexpr Register::FieldValue<decltype(eich1en)::Type,Eich1enVal::v1> v1{};
        }
        ///Error Injection Channel 0 Enable
        enum class Eich0enVal : unsigned {
            v0=0x00000000,     ///<Error injection is disabled on Error Injection Channel 0
            v1=0x00000001,     ///<Error injection is enabled on Error Injection Channel 0
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,Eich0enVal> eich0en{}; 
        namespace Eich0enValC{
            constexpr Register::FieldValue<decltype(eich0en)::Type,Eich0enVal::v0> v0{};
            constexpr Register::FieldValue<decltype(eich0en)::Type,Eich0enVal::v1> v1{};
        }
    }
    namespace EimEichd0Word0{    ///<Error Injection Channel Descriptor n, Word0
        using Addr = Register::Address<0x40019100,0x01ffffff,0x00000000,unsigned>;
        ///Checkbit Mask
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,25),Register::ReadWriteAccess,unsigned> chkbitMask{}; 
    }
    namespace EimEichd0Word1{    ///<Error Injection Channel Descriptor n, Word1
        using Addr = Register::Address<0x40019104,0x00000000,0x00000000,unsigned>;
        ///Data Mask Bytes 0-3
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> b03dataMask{}; 
    }
    namespace EimEichd1Word0{    ///<Error Injection Channel Descriptor n, Word0
        using Addr = Register::Address<0x40019200,0x01ffffff,0x00000000,unsigned>;
        ///Checkbit Mask
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,25),Register::ReadWriteAccess,unsigned> chkbitMask{}; 
    }
    namespace EimEichd1Word1{    ///<Error Injection Channel Descriptor n, Word1
        using Addr = Register::Address<0x40019204,0x00000000,0x00000000,unsigned>;
        ///Data Mask Bytes 0-3
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> b03dataMask{}; 
    }
}
