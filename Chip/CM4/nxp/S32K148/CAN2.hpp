#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//Flex Controller Area Network module
    namespace Can2Mcr{    ///<Module Configuration Register
        using Addr = Register::Address<0x4002b000,0x044c0480,0x00000000,unsigned>;
        ///Number Of The Last Message Buffer
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> maxmb{}; 
        ///ID Acceptance Mode
        enum class IdamVal : unsigned {
            v00=0x00000000,     ///<Format A: One full ID (standard and extended) per ID Filter Table element.
            v01=0x00000001,     ///<Format B: Two full standard IDs or two partial 14-bit (standard and extended) IDs per ID Filter Table element.
            v10=0x00000002,     ///<Format C: Four partial 8-bit Standard IDs per ID Filter Table element.
            v11=0x00000003,     ///<Format D: All frames rejected.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,IdamVal> idam{}; 
        namespace IdamValC{
            constexpr Register::FieldValue<decltype(idam)::Type,IdamVal::v00> v00{};
            constexpr Register::FieldValue<decltype(idam)::Type,IdamVal::v01> v01{};
            constexpr Register::FieldValue<decltype(idam)::Type,IdamVal::v10> v10{};
            constexpr Register::FieldValue<decltype(idam)::Type,IdamVal::v11> v11{};
        }
        ///CAN FD operation enable
        enum class FdenVal : unsigned {
            v1=0x00000001,     ///<CAN FD is enabled. FlexCAN is able to receive and transmit messages in both CAN FD and CAN 2.0 formats.
            v0=0x00000000,     ///<CAN FD is disabled. FlexCAN is able to receive and transmit messages in CAN 2.0 format.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,FdenVal> fden{}; 
        namespace FdenValC{
            constexpr Register::FieldValue<decltype(fden)::Type,FdenVal::v1> v1{};
            constexpr Register::FieldValue<decltype(fden)::Type,FdenVal::v0> v0{};
        }
        ///Abort Enable
        enum class AenVal : unsigned {
            v0=0x00000000,     ///<Abort disabled.
            v1=0x00000001,     ///<Abort enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,12),Register::ReadWriteAccess,AenVal> aen{}; 
        namespace AenValC{
            constexpr Register::FieldValue<decltype(aen)::Type,AenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(aen)::Type,AenVal::v1> v1{};
        }
        ///Local Priority Enable
        enum class LprioenVal : unsigned {
            v0=0x00000000,     ///<Local Priority disabled.
            v1=0x00000001,     ///<Local Priority enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,13),Register::ReadWriteAccess,LprioenVal> lprioen{}; 
        namespace LprioenValC{
            constexpr Register::FieldValue<decltype(lprioen)::Type,LprioenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lprioen)::Type,LprioenVal::v1> v1{};
        }
        ///Pretended Networking Enable
        enum class PnetenVal : unsigned {
            v0=0x00000000,     ///<Pretended Networking mode is disabled.
            v1=0x00000001,     ///<Pretended Networking mode is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,14),Register::ReadWriteAccess,PnetenVal> pnetEn{}; 
        namespace PnetenValC{
            constexpr Register::FieldValue<decltype(pnetEn)::Type,PnetenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pnetEn)::Type,PnetenVal::v1> v1{};
        }
        ///DMA Enable
        enum class DmaVal : unsigned {
            v0=0x00000000,     ///<DMA feature for RX FIFO disabled.
            v1=0x00000001,     ///<DMA feature for RX FIFO enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,DmaVal> dma{}; 
        namespace DmaValC{
            constexpr Register::FieldValue<decltype(dma)::Type,DmaVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dma)::Type,DmaVal::v1> v1{};
        }
        ///Individual Rx Masking And Queue Enable
        enum class IrmqVal : unsigned {
            v0=0x00000000,     ///<Individual Rx masking and queue feature are disabled. For backward compatibility with legacy applications, the reading of C/S word locks the MB even if it is EMPTY.
            v1=0x00000001,     ///<Individual Rx masking and queue feature are enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,IrmqVal> irmq{}; 
        namespace IrmqValC{
            constexpr Register::FieldValue<decltype(irmq)::Type,IrmqVal::v0> v0{};
            constexpr Register::FieldValue<decltype(irmq)::Type,IrmqVal::v1> v1{};
        }
        ///Self Reception Disable
        enum class SrxdisVal : unsigned {
            v0=0x00000000,     ///<Self reception enabled.
            v1=0x00000001,     ///<Self reception disabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,17),Register::ReadWriteAccess,SrxdisVal> srxdis{}; 
        namespace SrxdisValC{
            constexpr Register::FieldValue<decltype(srxdis)::Type,SrxdisVal::v0> v0{};
            constexpr Register::FieldValue<decltype(srxdis)::Type,SrxdisVal::v1> v1{};
        }
        ///Low-Power Mode Acknowledge
        enum class LpmackVal : unsigned {
            v0=0x00000000,     ///<FlexCAN is not in a low-power mode.
            v1=0x00000001,     ///<FlexCAN is in a low-power mode.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,20),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,LpmackVal> lpmack{}; 
        namespace LpmackValC{
            constexpr Register::FieldValue<decltype(lpmack)::Type,LpmackVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lpmack)::Type,LpmackVal::v1> v1{};
        }
        ///Warning Interrupt Enable
        enum class WrnenVal : unsigned {
            v0=0x00000000,     ///<TWRNINT and RWRNINT bits are zero, independent of the values in the error counters.
            v1=0x00000001,     ///<TWRNINT and RWRNINT bits are set when the respective error counter transitions from less than 96 to greater than or equal to 96.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(21,21),Register::ReadWriteAccess,WrnenVal> wrnen{}; 
        namespace WrnenValC{
            constexpr Register::FieldValue<decltype(wrnen)::Type,WrnenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(wrnen)::Type,WrnenVal::v1> v1{};
        }
        ///Supervisor Mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,23),Register::ReadWriteAccess,unsigned> supv{}; 
        ///Freeze Mode Acknowledge
        enum class FrzackVal : unsigned {
            v0=0x00000000,     ///<FlexCAN not in Freeze mode, prescaler running.
            v1=0x00000001,     ///<FlexCAN in Freeze mode, prescaler stopped.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,FrzackVal> frzack{}; 
        namespace FrzackValC{
            constexpr Register::FieldValue<decltype(frzack)::Type,FrzackVal::v0> v0{};
            constexpr Register::FieldValue<decltype(frzack)::Type,FrzackVal::v1> v1{};
        }
        ///Soft Reset
        enum class SoftrstVal : unsigned {
            v0=0x00000000,     ///<No reset request.
            v1=0x00000001,     ///<Resets the registers affected by soft reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,SoftrstVal> softrst{}; 
        namespace SoftrstValC{
            constexpr Register::FieldValue<decltype(softrst)::Type,SoftrstVal::v0> v0{};
            constexpr Register::FieldValue<decltype(softrst)::Type,SoftrstVal::v1> v1{};
        }
        ///FlexCAN Not Ready
        enum class NotrdyVal : unsigned {
            v0=0x00000000,     ///<FlexCAN module is either in Normal mode, Listen-Only mode or Loop-Back mode.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,NotrdyVal> notrdy{}; 
        namespace NotrdyValC{
            constexpr Register::FieldValue<decltype(notrdy)::Type,NotrdyVal::v0> v0{};
        }
        ///Halt FlexCAN
        enum class HaltVal : unsigned {
            v0=0x00000000,     ///<No Freeze mode request.
            v1=0x00000001,     ///<Enters Freeze mode if the FRZ bit is asserted.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,HaltVal> halt{}; 
        namespace HaltValC{
            constexpr Register::FieldValue<decltype(halt)::Type,HaltVal::v0> v0{};
            constexpr Register::FieldValue<decltype(halt)::Type,HaltVal::v1> v1{};
        }
        ///Rx FIFO Enable
        enum class RfenVal : unsigned {
            v0=0x00000000,     ///<Rx FIFO not enabled.
            v1=0x00000001,     ///<Rx FIFO enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,RfenVal> rfen{}; 
        namespace RfenValC{
            constexpr Register::FieldValue<decltype(rfen)::Type,RfenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rfen)::Type,RfenVal::v1> v1{};
        }
        ///Freeze Enable
        enum class FrzVal : unsigned {
            v0=0x00000000,     ///<Not enabled to enter Freeze mode.
            v1=0x00000001,     ///<Enabled to enter Freeze mode.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,FrzVal> frz{}; 
        namespace FrzValC{
            constexpr Register::FieldValue<decltype(frz)::Type,FrzVal::v0> v0{};
            constexpr Register::FieldValue<decltype(frz)::Type,FrzVal::v1> v1{};
        }
        ///Module Disable
        enum class MdisVal : unsigned {
            v0=0x00000000,     ///<Enable the FlexCAN module.
            v1=0x00000001,     ///<Disable the FlexCAN module.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,MdisVal> mdis{}; 
        namespace MdisValC{
            constexpr Register::FieldValue<decltype(mdis)::Type,MdisVal::v0> v0{};
            constexpr Register::FieldValue<decltype(mdis)::Type,MdisVal::v1> v1{};
        }
    }
    namespace Can2Ctrl1{    ///<Control 1 register
        using Addr = Register::Address<0x4002b004,0x00000300,0x00000000,unsigned>;
        ///Propagation Segment
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> propseg{}; 
        ///Listen-Only Mode
        enum class LomVal : unsigned {
            v0=0x00000000,     ///<Listen-Only mode is deactivated.
            v1=0x00000001,     ///<FlexCAN module operates in Listen-Only mode.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,LomVal> lom{}; 
        namespace LomValC{
            constexpr Register::FieldValue<decltype(lom)::Type,LomVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lom)::Type,LomVal::v1> v1{};
        }
        ///Lowest Buffer Transmitted First
        enum class LbufVal : unsigned {
            v0=0x00000000,     ///<Buffer with highest priority is transmitted first.
            v1=0x00000001,     ///<Lowest number buffer is transmitted first.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,4),Register::ReadWriteAccess,LbufVal> lbuf{}; 
        namespace LbufValC{
            constexpr Register::FieldValue<decltype(lbuf)::Type,LbufVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lbuf)::Type,LbufVal::v1> v1{};
        }
        ///Timer Sync
        enum class TsynVal : unsigned {
            v0=0x00000000,     ///<Timer Sync feature disabled
            v1=0x00000001,     ///<Timer Sync feature enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,TsynVal> tsyn{}; 
        namespace TsynValC{
            constexpr Register::FieldValue<decltype(tsyn)::Type,TsynVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tsyn)::Type,TsynVal::v1> v1{};
        }
        ///Bus Off Recovery
        enum class BoffrecVal : unsigned {
            v0=0x00000000,     ///<Automatic recovering from Bus Off state enabled.
            v1=0x00000001,     ///<Automatic recovering from Bus Off state disabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,BoffrecVal> boffrec{}; 
        namespace BoffrecValC{
            constexpr Register::FieldValue<decltype(boffrec)::Type,BoffrecVal::v0> v0{};
            constexpr Register::FieldValue<decltype(boffrec)::Type,BoffrecVal::v1> v1{};
        }
        ///CAN Bit Sampling
        enum class SmpVal : unsigned {
            v0=0x00000000,     ///<Just one sample is used to determine the bit value.
            v1=0x00000001,     ///<Three samples are used to determine the value of the received bit: the regular one (sample point) and 2 preceding samples; a majority rule is used.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::ReadWriteAccess,SmpVal> smp{}; 
        namespace SmpValC{
            constexpr Register::FieldValue<decltype(smp)::Type,SmpVal::v0> v0{};
            constexpr Register::FieldValue<decltype(smp)::Type,SmpVal::v1> v1{};
        }
        ///Rx Warning Interrupt Mask
        enum class RwrnmskVal : unsigned {
            v0=0x00000000,     ///<Rx Warning Interrupt disabled.
            v1=0x00000001,     ///<Rx Warning Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,10),Register::ReadWriteAccess,RwrnmskVal> rwrnmsk{}; 
        namespace RwrnmskValC{
            constexpr Register::FieldValue<decltype(rwrnmsk)::Type,RwrnmskVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rwrnmsk)::Type,RwrnmskVal::v1> v1{};
        }
        ///Tx Warning Interrupt Mask
        enum class TwrnmskVal : unsigned {
            v0=0x00000000,     ///<Tx Warning Interrupt disabled.
            v1=0x00000001,     ///<Tx Warning Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,TwrnmskVal> twrnmsk{}; 
        namespace TwrnmskValC{
            constexpr Register::FieldValue<decltype(twrnmsk)::Type,TwrnmskVal::v0> v0{};
            constexpr Register::FieldValue<decltype(twrnmsk)::Type,TwrnmskVal::v1> v1{};
        }
        ///Loop Back Mode
        enum class LpbVal : unsigned {
            v0=0x00000000,     ///<Loop Back disabled.
            v1=0x00000001,     ///<Loop Back enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,12),Register::ReadWriteAccess,LpbVal> lpb{}; 
        namespace LpbValC{
            constexpr Register::FieldValue<decltype(lpb)::Type,LpbVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lpb)::Type,LpbVal::v1> v1{};
        }
        ///CAN Engine Clock Source
        enum class ClksrcVal : unsigned {
            v0=0x00000000,     ///<The CAN engine clock source is the oscillator clock. Under this condition, the oscillator clock frequency must be lower than the bus clock.
            v1=0x00000001,     ///<The CAN engine clock source is the peripheral clock.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,13),Register::ReadWriteAccess,ClksrcVal> clksrc{}; 
        namespace ClksrcValC{
            constexpr Register::FieldValue<decltype(clksrc)::Type,ClksrcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(clksrc)::Type,ClksrcVal::v1> v1{};
        }
        ///Error Interrupt Mask
        enum class ErrmskVal : unsigned {
            v0=0x00000000,     ///<Error interrupt disabled.
            v1=0x00000001,     ///<Error interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,14),Register::ReadWriteAccess,ErrmskVal> errmsk{}; 
        namespace ErrmskValC{
            constexpr Register::FieldValue<decltype(errmsk)::Type,ErrmskVal::v0> v0{};
            constexpr Register::FieldValue<decltype(errmsk)::Type,ErrmskVal::v1> v1{};
        }
        ///Bus Off Interrupt Mask
        enum class BoffmskVal : unsigned {
            v0=0x00000000,     ///<Bus Off interrupt disabled.
            v1=0x00000001,     ///<Bus Off interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,BoffmskVal> boffmsk{}; 
        namespace BoffmskValC{
            constexpr Register::FieldValue<decltype(boffmsk)::Type,BoffmskVal::v0> v0{};
            constexpr Register::FieldValue<decltype(boffmsk)::Type,BoffmskVal::v1> v1{};
        }
        ///Phase Segment 2
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(18,16),Register::ReadWriteAccess,unsigned> pseg2{}; 
        ///Phase Segment 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(21,19),Register::ReadWriteAccess,unsigned> pseg1{}; 
        ///Resync Jump Width
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,22),Register::ReadWriteAccess,unsigned> rjw{}; 
        ///Prescaler Division Factor
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> presdiv{}; 
    }
    namespace Can2Timer{    ///<Free Running Timer
        using Addr = Register::Address<0x4002b008,0xffff0000,0x00000000,unsigned>;
        ///Timer Value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::ReadWriteAccess,unsigned> timer{}; 
    }
    namespace Can2Rxmgmask{    ///<Rx Mailboxes Global Mask Register
        using Addr = Register::Address<0x4002b010,0x00000000,0x00000000,unsigned>;
        ///Rx Mailboxes Global Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mg{}; 
    }
    namespace Can2Rx14mask{    ///<Rx 14 Mask register
        using Addr = Register::Address<0x4002b014,0x00000000,0x00000000,unsigned>;
        ///Rx Buffer 14 Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> rx14m{}; 
    }
    namespace Can2Rx15mask{    ///<Rx 15 Mask register
        using Addr = Register::Address<0x4002b018,0x00000000,0x00000000,unsigned>;
        ///Rx Buffer 15 Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> rx15m{}; 
    }
    namespace Can2Ecr{    ///<Error Counter
        using Addr = Register::Address<0x4002b01c,0x00000000,0x00000000,unsigned>;
        ///Transmit Error Counter
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> txerrcnt{}; 
        ///Receive Error Counter
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> rxerrcnt{}; 
        ///Transmit Error Counter for fast bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> txerrcntFast{}; 
        ///Receive Error Counter for fast bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> rxerrcntFast{}; 
    }
    namespace Can2Esr1{    ///<Error and Status 1 register
        using Addr = Register::Address<0x4002b020,0x23c00001,0x00000000,unsigned>;
        ///Error Interrupt
        enum class ErrintVal : unsigned {
            v0=0x00000000,     ///<No such occurrence.
            v1=0x00000001,     ///<Indicates setting of any Error Bit in the Error and Status Register.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,ErrintVal> errint{}; 
        namespace ErrintValC{
            constexpr Register::FieldValue<decltype(errint)::Type,ErrintVal::v0> v0{};
            constexpr Register::FieldValue<decltype(errint)::Type,ErrintVal::v1> v1{};
        }
        ///Bus Off Interrupt
        enum class BoffintVal : unsigned {
            v0=0x00000000,     ///<No such occurrence.
            v1=0x00000001,     ///<FlexCAN module entered Bus Off state.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,BoffintVal> boffint{}; 
        namespace BoffintValC{
            constexpr Register::FieldValue<decltype(boffint)::Type,BoffintVal::v0> v0{};
            constexpr Register::FieldValue<decltype(boffint)::Type,BoffintVal::v1> v1{};
        }
        ///FlexCAN In Reception
        enum class RxVal : unsigned {
            v0=0x00000000,     ///<FlexCAN is not receiving a message.
            v1=0x00000001,     ///<FlexCAN is receiving a message.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RxVal> rx{}; 
        namespace RxValC{
            constexpr Register::FieldValue<decltype(rx)::Type,RxVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rx)::Type,RxVal::v1> v1{};
        }
        ///Fault Confinement State
        enum class FltconfVal : unsigned {
            v00=0x00000000,     ///<Error Active
            v01=0x00000001,     ///<Error Passive
            v1x=0x00000002,     ///<Bus Off
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,4),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,FltconfVal> fltconf{}; 
        namespace FltconfValC{
            constexpr Register::FieldValue<decltype(fltconf)::Type,FltconfVal::v00> v00{};
            constexpr Register::FieldValue<decltype(fltconf)::Type,FltconfVal::v01> v01{};
            constexpr Register::FieldValue<decltype(fltconf)::Type,FltconfVal::v1x> v1x{};
        }
        ///FlexCAN In Transmission
        enum class TxVal : unsigned {
            v0=0x00000000,     ///<FlexCAN is not transmitting a message.
            v1=0x00000001,     ///<FlexCAN is transmitting a message.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,TxVal> tx{}; 
        namespace TxValC{
            constexpr Register::FieldValue<decltype(tx)::Type,TxVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tx)::Type,TxVal::v1> v1{};
        }
        ///IDLE
        enum class IdleVal : unsigned {
            v0=0x00000000,     ///<No such occurrence.
            v1=0x00000001,     ///<CAN bus is now IDLE.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,IdleVal> idle{}; 
        namespace IdleValC{
            constexpr Register::FieldValue<decltype(idle)::Type,IdleVal::v0> v0{};
            constexpr Register::FieldValue<decltype(idle)::Type,IdleVal::v1> v1{};
        }
        ///Rx Error Warning
        enum class RxwrnVal : unsigned {
            v0=0x00000000,     ///<No such occurrence.
            v1=0x00000001,     ///<RXERRCNT is greater than or equal to 96.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RxwrnVal> rxwrn{}; 
        namespace RxwrnValC{
            constexpr Register::FieldValue<decltype(rxwrn)::Type,RxwrnVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rxwrn)::Type,RxwrnVal::v1> v1{};
        }
        ///TX Error Warning
        enum class TxwrnVal : unsigned {
            v0=0x00000000,     ///<No such occurrence.
            v1=0x00000001,     ///<TXERRCNT is greater than or equal to 96.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,9),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,TxwrnVal> txwrn{}; 
        namespace TxwrnValC{
            constexpr Register::FieldValue<decltype(txwrn)::Type,TxwrnVal::v0> v0{};
            constexpr Register::FieldValue<decltype(txwrn)::Type,TxwrnVal::v1> v1{};
        }
        ///Stuffing Error
        enum class StferrVal : unsigned {
            v0=0x00000000,     ///<No such occurrence.
            v1=0x00000001,     ///<A Stuffing Error occurred since last read of this register.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,10),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,StferrVal> stferr{}; 
        namespace StferrValC{
            constexpr Register::FieldValue<decltype(stferr)::Type,StferrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(stferr)::Type,StferrVal::v1> v1{};
        }
        ///Form Error
        enum class FrmerrVal : unsigned {
            v0=0x00000000,     ///<No such occurrence.
            v1=0x00000001,     ///<A Form Error occurred since last read of this register.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,FrmerrVal> frmerr{}; 
        namespace FrmerrValC{
            constexpr Register::FieldValue<decltype(frmerr)::Type,FrmerrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(frmerr)::Type,FrmerrVal::v1> v1{};
        }
        ///Cyclic Redundancy Check Error
        enum class CrcerrVal : unsigned {
            v0=0x00000000,     ///<No such occurrence.
            v1=0x00000001,     ///<A CRC error occurred since last read of this register.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,12),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CrcerrVal> crcerr{}; 
        namespace CrcerrValC{
            constexpr Register::FieldValue<decltype(crcerr)::Type,CrcerrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(crcerr)::Type,CrcerrVal::v1> v1{};
        }
        ///Acknowledge Error
        enum class AckerrVal : unsigned {
            v0=0x00000000,     ///<No such occurrence.
            v1=0x00000001,     ///<An ACK error occurred since last read of this register.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,13),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,AckerrVal> ackerr{}; 
        namespace AckerrValC{
            constexpr Register::FieldValue<decltype(ackerr)::Type,AckerrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ackerr)::Type,AckerrVal::v1> v1{};
        }
        ///Bit0 Error
        enum class Bit0errVal : unsigned {
            v0=0x00000000,     ///<No such occurrence.
            v1=0x00000001,     ///<At least one bit sent as dominant is received as recessive.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,14),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,Bit0errVal> bit0err{}; 
        namespace Bit0errValC{
            constexpr Register::FieldValue<decltype(bit0err)::Type,Bit0errVal::v0> v0{};
            constexpr Register::FieldValue<decltype(bit0err)::Type,Bit0errVal::v1> v1{};
        }
        ///Bit1 Error
        enum class Bit1errVal : unsigned {
            v0=0x00000000,     ///<No such occurrence.
            v1=0x00000001,     ///<At least one bit sent as recessive is received as dominant.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,Bit1errVal> bit1err{}; 
        namespace Bit1errValC{
            constexpr Register::FieldValue<decltype(bit1err)::Type,Bit1errVal::v0> v0{};
            constexpr Register::FieldValue<decltype(bit1err)::Type,Bit1errVal::v1> v1{};
        }
        ///Rx Warning Interrupt Flag
        enum class RwrnintVal : unsigned {
            v0=0x00000000,     ///<No such occurrence.
            v1=0x00000001,     ///<The Rx error counter transitioned from less than 96 to greater than or equal to 96.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,RwrnintVal> rwrnint{}; 
        namespace RwrnintValC{
            constexpr Register::FieldValue<decltype(rwrnint)::Type,RwrnintVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rwrnint)::Type,RwrnintVal::v1> v1{};
        }
        ///Tx Warning Interrupt Flag
        enum class TwrnintVal : unsigned {
            v0=0x00000000,     ///<No such occurrence.
            v1=0x00000001,     ///<The Tx error counter transitioned from less than 96 to greater than or equal to 96.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,17),Register::ReadWriteAccess,TwrnintVal> twrnint{}; 
        namespace TwrnintValC{
            constexpr Register::FieldValue<decltype(twrnint)::Type,TwrnintVal::v0> v0{};
            constexpr Register::FieldValue<decltype(twrnint)::Type,TwrnintVal::v1> v1{};
        }
        ///CAN Synchronization Status
        enum class SynchVal : unsigned {
            v0=0x00000000,     ///<FlexCAN is not synchronized to the CAN bus.
            v1=0x00000001,     ///<FlexCAN is synchronized to the CAN bus.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(18,18),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,SynchVal> synch{}; 
        namespace SynchValC{
            constexpr Register::FieldValue<decltype(synch)::Type,SynchVal::v0> v0{};
            constexpr Register::FieldValue<decltype(synch)::Type,SynchVal::v1> v1{};
        }
        ///Bus Off Done Interrupt
        enum class BoffdoneintVal : unsigned {
            v0=0x00000000,     ///<No such occurrence.
            v1=0x00000001,     ///<FlexCAN module has completed Bus Off process.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,19),Register::ReadWriteAccess,BoffdoneintVal> boffdoneint{}; 
        namespace BoffdoneintValC{
            constexpr Register::FieldValue<decltype(boffdoneint)::Type,BoffdoneintVal::v0> v0{};
            constexpr Register::FieldValue<decltype(boffdoneint)::Type,BoffdoneintVal::v1> v1{};
        }
        ///Error Interrupt for errors detected in the Data Phase of CAN FD frames with the BRS bit set
        enum class ErrintfastVal : unsigned {
            v0=0x00000000,     ///<No such occurrence.
            v1=0x00000001,     ///<Indicates setting of any Error Bit detected in the Data Phase of CAN FD frames with the BRS bit set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,20),Register::ReadWriteAccess,ErrintfastVal> errintFast{}; 
        namespace ErrintfastValC{
            constexpr Register::FieldValue<decltype(errintFast)::Type,ErrintfastVal::v0> v0{};
            constexpr Register::FieldValue<decltype(errintFast)::Type,ErrintfastVal::v1> v1{};
        }
        ///Error Overrun bit
        enum class ErrovrVal : unsigned {
            v0=0x00000000,     ///<Overrun has not occurred.
            v1=0x00000001,     ///<Overrun has occurred.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(21,21),Register::ReadWriteAccess,ErrovrVal> errovr{}; 
        namespace ErrovrValC{
            constexpr Register::FieldValue<decltype(errovr)::Type,ErrovrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(errovr)::Type,ErrovrVal::v1> v1{};
        }
        ///Stuffing Error in the Data Phase of CAN FD frames with the BRS bit set
        enum class StferrfastVal : unsigned {
            v0=0x00000000,     ///<No such occurrence.
            v1=0x00000001,     ///<A Stuffing Error occurred since last read of this register.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,StferrfastVal> stferrFast{}; 
        namespace StferrfastValC{
            constexpr Register::FieldValue<decltype(stferrFast)::Type,StferrfastVal::v0> v0{};
            constexpr Register::FieldValue<decltype(stferrFast)::Type,StferrfastVal::v1> v1{};
        }
        ///Form Error in the Data Phase of CAN FD frames with the BRS bit set
        enum class FrmerrfastVal : unsigned {
            v0=0x00000000,     ///<No such occurrence.
            v1=0x00000001,     ///<A Form Error occurred since last read of this register.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,FrmerrfastVal> frmerrFast{}; 
        namespace FrmerrfastValC{
            constexpr Register::FieldValue<decltype(frmerrFast)::Type,FrmerrfastVal::v0> v0{};
            constexpr Register::FieldValue<decltype(frmerrFast)::Type,FrmerrfastVal::v1> v1{};
        }
        ///Cyclic Redundancy Check Error in the CRC field of CAN FD frames with the BRS bit set
        enum class CrcerrfastVal : unsigned {
            v0=0x00000000,     ///<No such occurrence.
            v1=0x00000001,     ///<A CRC error occurred since last read of this register.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CrcerrfastVal> crcerrFast{}; 
        namespace CrcerrfastValC{
            constexpr Register::FieldValue<decltype(crcerrFast)::Type,CrcerrfastVal::v0> v0{};
            constexpr Register::FieldValue<decltype(crcerrFast)::Type,CrcerrfastVal::v1> v1{};
        }
        ///Bit0 Error in the Data Phase of CAN FD frames with the BRS bit set
        enum class Bit0errfastVal : unsigned {
            v0=0x00000000,     ///<No such occurrence.
            v1=0x00000001,     ///<At least one bit sent as dominant is received as recessive.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,Bit0errfastVal> bit0errFast{}; 
        namespace Bit0errfastValC{
            constexpr Register::FieldValue<decltype(bit0errFast)::Type,Bit0errfastVal::v0> v0{};
            constexpr Register::FieldValue<decltype(bit0errFast)::Type,Bit0errfastVal::v1> v1{};
        }
        ///Bit1 Error in the Data Phase of CAN FD frames with the BRS bit set
        enum class Bit1errfastVal : unsigned {
            v0=0x00000000,     ///<No such occurrence.
            v1=0x00000001,     ///<At least one bit sent as recessive is received as dominant.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,Bit1errfastVal> bit1errFast{}; 
        namespace Bit1errfastValC{
            constexpr Register::FieldValue<decltype(bit1errFast)::Type,Bit1errfastVal::v0> v0{};
            constexpr Register::FieldValue<decltype(bit1errFast)::Type,Bit1errfastVal::v1> v1{};
        }
    }
    namespace Can2Imask1{    ///<Interrupt Masks 1 register
        using Addr = Register::Address<0x4002b028,0x00000000,0x00000000,unsigned>;
        ///Buffer MB i Mask
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> buf31to0m{}; 
    }
    namespace Can2Iflag1{    ///<Interrupt Flags 1 register
        using Addr = Register::Address<0x4002b030,0x00000000,0x00000000,unsigned>;
        ///Buffer MB0 Interrupt Or Clear FIFO bit
        enum class Buf0iVal : unsigned {
            v0=0x00000000,     ///<The corresponding buffer has no occurrence of successfully completed transmission or reception when MCR[RFEN]=0.
            v1=0x00000001,     ///<The corresponding buffer has successfully completed transmission or reception when MCR[RFEN]=0.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,Buf0iVal> buf0i{}; 
        namespace Buf0iValC{
            constexpr Register::FieldValue<decltype(buf0i)::Type,Buf0iVal::v0> v0{};
            constexpr Register::FieldValue<decltype(buf0i)::Type,Buf0iVal::v1> v1{};
        }
        ///Buffer MB i Interrupt Or "reserved"
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,1),Register::ReadWriteAccess,unsigned> buf4to1i{}; 
        ///Buffer MB5 Interrupt Or "Frames available in Rx FIFO"
        enum class Buf5iVal : unsigned {
            v0=0x00000000,     ///<No occurrence of MB5 completing transmission/reception when MCR[RFEN]=0, or of frame(s) available in the FIFO, when MCR[RFEN]=1
            v1=0x00000001,     ///<MB5 completed transmission/reception when MCR[RFEN]=0, or frame(s) available in the Rx FIFO when MCR[RFEN]=1. It generates a DMA request in case of MCR[RFEN] and MCR[DMA] are enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,Buf5iVal> buf5i{}; 
        namespace Buf5iValC{
            constexpr Register::FieldValue<decltype(buf5i)::Type,Buf5iVal::v0> v0{};
            constexpr Register::FieldValue<decltype(buf5i)::Type,Buf5iVal::v1> v1{};
        }
        ///Buffer MB6 Interrupt Or "Rx FIFO Warning"
        enum class Buf6iVal : unsigned {
            v0=0x00000000,     ///<No occurrence of MB6 completing transmission/reception when MCR[RFEN]=0, or of Rx FIFO almost full when MCR[RFEN]=1
            v1=0x00000001,     ///<MB6 completed transmission/reception when MCR[RFEN]=0, or Rx FIFO almost full when MCR[RFEN]=1
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,Buf6iVal> buf6i{}; 
        namespace Buf6iValC{
            constexpr Register::FieldValue<decltype(buf6i)::Type,Buf6iVal::v0> v0{};
            constexpr Register::FieldValue<decltype(buf6i)::Type,Buf6iVal::v1> v1{};
        }
        ///Buffer MB7 Interrupt Or "Rx FIFO Overflow"
        enum class Buf7iVal : unsigned {
            v0=0x00000000,     ///<No occurrence of MB7 completing transmission/reception when MCR[RFEN]=0, or of Rx FIFO overflow when MCR[RFEN]=1
            v1=0x00000001,     ///<MB7 completed transmission/reception when MCR[RFEN]=0, or Rx FIFO overflow when MCR[RFEN]=1
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::ReadWriteAccess,Buf7iVal> buf7i{}; 
        namespace Buf7iValC{
            constexpr Register::FieldValue<decltype(buf7i)::Type,Buf7iVal::v0> v0{};
            constexpr Register::FieldValue<decltype(buf7i)::Type,Buf7iVal::v1> v1{};
        }
        ///Buffer MBi Interrupt
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,8),Register::ReadWriteAccess,unsigned> buf31to8i{}; 
    }
    namespace Can2Ctrl2{    ///<Control 2 register
        using Addr = Register::Address<0x4002b034,0x300027ff,0x00000000,unsigned>;
        ///Edge Filter Disable
        enum class EdfltdisVal : unsigned {
            v0=0x00000000,     ///<Edge Filter is enabled.
            v1=0x00000001,     ///<Edge Filter is disabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,EdfltdisVal> edfltdis{}; 
        namespace EdfltdisValC{
            constexpr Register::FieldValue<decltype(edfltdis)::Type,EdfltdisVal::v0> v0{};
            constexpr Register::FieldValue<decltype(edfltdis)::Type,EdfltdisVal::v1> v1{};
        }
        ///ISO CAN FD Enable
        enum class IsocanfdenVal : unsigned {
            v0=0x00000000,     ///<FlexCAN operates using the non-ISO CAN FD protocol.
            v1=0x00000001,     ///<FlexCAN operates using the ISO CAN FD protocol (ISO 11898-1).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,12),Register::ReadWriteAccess,IsocanfdenVal> isocanfden{}; 
        namespace IsocanfdenValC{
            constexpr Register::FieldValue<decltype(isocanfden)::Type,IsocanfdenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isocanfden)::Type,IsocanfdenVal::v1> v1{};
        }
        ///Protocol Exception Enable
        enum class PrexcenVal : unsigned {
            v0=0x00000000,     ///<Protocol Exception is disabled.
            v1=0x00000001,     ///<Protocol Exception is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,14),Register::ReadWriteAccess,PrexcenVal> prexcen{}; 
        namespace PrexcenValC{
            constexpr Register::FieldValue<decltype(prexcen)::Type,PrexcenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(prexcen)::Type,PrexcenVal::v1> v1{};
        }
        ///Timer Source
        enum class TimersrcVal : unsigned {
            v0=0x00000000,     ///<The Free Running Timer is clocked by the CAN bit clock, which defines the baud rate on the CAN bus.
            v1=0x00000001,     ///<The Free Running Timer is clocked by an external time tick. The period can be either adjusted to be equal to the baud rate on the CAN bus, or a different value as required. See the device specific section for details about the external time tick.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,TimersrcVal> timerSrc{}; 
        namespace TimersrcValC{
            constexpr Register::FieldValue<decltype(timerSrc)::Type,TimersrcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(timerSrc)::Type,TimersrcVal::v1> v1{};
        }
        ///Entire Frame Arbitration Field Comparison Enable For Rx Mailboxes
        enum class EacenVal : unsigned {
            v0=0x00000000,     ///<Rx Mailbox filter's IDE bit is always compared and RTR is never compared despite mask bits.
            v1=0x00000001,     ///<Enables the comparison of both Rx Mailbox filter's IDE and RTR bit with their corresponding bits within the incoming frame. Mask bits do apply.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,EacenVal> eacen{}; 
        namespace EacenValC{
            constexpr Register::FieldValue<decltype(eacen)::Type,EacenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(eacen)::Type,EacenVal::v1> v1{};
        }
        ///Remote Request Storing
        enum class RrsVal : unsigned {
            v0=0x00000000,     ///<Remote Response Frame is generated.
            v1=0x00000001,     ///<Remote Request Frame is stored.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,17),Register::ReadWriteAccess,RrsVal> rrs{}; 
        namespace RrsValC{
            constexpr Register::FieldValue<decltype(rrs)::Type,RrsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rrs)::Type,RrsVal::v1> v1{};
        }
        ///Mailboxes Reception Priority
        enum class MrpVal : unsigned {
            v0=0x00000000,     ///<Matching starts from Rx FIFO and continues on Mailboxes.
            v1=0x00000001,     ///<Matching starts from Mailboxes and continues on Rx FIFO.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(18,18),Register::ReadWriteAccess,MrpVal> mrp{}; 
        namespace MrpValC{
            constexpr Register::FieldValue<decltype(mrp)::Type,MrpVal::v0> v0{};
            constexpr Register::FieldValue<decltype(mrp)::Type,MrpVal::v1> v1{};
        }
        ///Tx Arbitration Start Delay
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,19),Register::ReadWriteAccess,unsigned> tasd{}; 
        ///Number Of Rx FIFO Filters
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,24),Register::ReadWriteAccess,unsigned> rffn{}; 
        ///Bus Off Done Interrupt Mask
        enum class BoffdonemskVal : unsigned {
            v0=0x00000000,     ///<Bus Off Done interrupt disabled.
            v1=0x00000001,     ///<Bus Off Done interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,BoffdonemskVal> boffdonemsk{}; 
        namespace BoffdonemskValC{
            constexpr Register::FieldValue<decltype(boffdonemsk)::Type,BoffdonemskVal::v0> v0{};
            constexpr Register::FieldValue<decltype(boffdonemsk)::Type,BoffdonemskVal::v1> v1{};
        }
        ///Error Interrupt Mask for errors detected in the Data Phase of fast CAN FD frames
        enum class ErrmskfastVal : unsigned {
            v0=0x00000000,     ///<ERRINT_FAST Error interrupt disabled.
            v1=0x00000001,     ///<ERRINT_FAST Error interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,ErrmskfastVal> errmskFast{}; 
        namespace ErrmskfastValC{
            constexpr Register::FieldValue<decltype(errmskFast)::Type,ErrmskfastVal::v0> v0{};
            constexpr Register::FieldValue<decltype(errmskFast)::Type,ErrmskfastVal::v1> v1{};
        }
    }
    namespace Can2Esr2{    ///<Error and Status 2 register
        using Addr = Register::Address<0x4002b038,0xff809fff,0x00000000,unsigned>;
        ///Inactive Mailbox
        enum class ImbVal : unsigned {
            v0=0x00000000,     ///<If ESR2[VPS] is asserted, the ESR2[LPTM] is not an inactive Mailbox.
            v1=0x00000001,     ///<If ESR2[VPS] is asserted, there is at least one inactive Mailbox. LPTM content is the number of the first one.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,13),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,ImbVal> imb{}; 
        namespace ImbValC{
            constexpr Register::FieldValue<decltype(imb)::Type,ImbVal::v0> v0{};
            constexpr Register::FieldValue<decltype(imb)::Type,ImbVal::v1> v1{};
        }
        ///Valid Priority Status
        enum class VpsVal : unsigned {
            v0=0x00000000,     ///<Contents of IMB and LPTM are invalid.
            v1=0x00000001,     ///<Contents of IMB and LPTM are valid.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,14),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,VpsVal> vps{}; 
        namespace VpsValC{
            constexpr Register::FieldValue<decltype(vps)::Type,VpsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(vps)::Type,VpsVal::v1> v1{};
        }
        ///Lowest Priority Tx Mailbox
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> lptm{}; 
    }
    namespace Can2Crcr{    ///<CRC Register
        using Addr = Register::Address<0x4002b044,0xff808000,0x00000000,unsigned>;
        ///Transmitted CRC value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> txcrc{}; 
        ///CRC Mailbox
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> mbcrc{}; 
    }
    namespace Can2Rxfgmask{    ///<Rx FIFO Global Mask register
        using Addr = Register::Address<0x4002b048,0x00000000,0x00000000,unsigned>;
        ///Rx FIFO Global Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> fgm{}; 
    }
    namespace Can2Rxfir{    ///<Rx FIFO Information Register
        using Addr = Register::Address<0x4002b04c,0xfffffe00,0x00000000,unsigned>;
        ///Identifier Acceptance Filter Hit Indicator
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> idhit{}; 
    }
    namespace Can2Cbt{    ///<CAN Bit Timing Register
        using Addr = Register::Address<0x4002b050,0x00000000,0x00000000,unsigned>;
        ///Extended Phase Segment 2
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,0),Register::ReadWriteAccess,unsigned> epseg2{}; 
        ///Extended Phase Segment 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,5),Register::ReadWriteAccess,unsigned> epseg1{}; 
        ///Extended Propagation Segment
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> epropseg{}; 
        ///Extended Resync Jump Width
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,16),Register::ReadWriteAccess,unsigned> erjw{}; 
        ///Extended Prescaler Division Factor
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,21),Register::ReadWriteAccess,unsigned> epresdiv{}; 
        ///Bit Timing Format Enable
        enum class BtfVal : unsigned {
            v0=0x00000000,     ///<Extended bit time definitions disabled.
            v1=0x00000001,     ///<Extended bit time definitions enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,BtfVal> btf{}; 
        namespace BtfValC{
            constexpr Register::FieldValue<decltype(btf)::Type,BtfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(btf)::Type,BtfVal::v1> v1{};
        }
    }
    namespace Can2Embeddedram0{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b080,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram1{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b084,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram2{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b088,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram3{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b08c,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram4{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b090,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram5{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b094,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram6{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b098,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram7{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b09c,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram8{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b0a0,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram9{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b0a4,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram10{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b0a8,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram11{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b0ac,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram12{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b0b0,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram13{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b0b4,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram14{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b0b8,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram15{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b0bc,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram16{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b0c0,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram17{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b0c4,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram18{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b0c8,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram19{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b0cc,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram20{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b0d0,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram21{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b0d4,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram22{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b0d8,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram23{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b0dc,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram24{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b0e0,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram25{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b0e4,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram26{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b0e8,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram27{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b0ec,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram28{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b0f0,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram29{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b0f4,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram30{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b0f8,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram31{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b0fc,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram32{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b100,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram33{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b104,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram34{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b108,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram35{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b10c,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram36{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b110,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram37{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b114,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram38{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b118,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram39{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b11c,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram40{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b120,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram41{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b124,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram42{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b128,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram43{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b12c,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram44{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b130,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram45{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b134,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram46{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b138,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram47{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b13c,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram48{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b140,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram49{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b144,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram50{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b148,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram51{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b14c,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram52{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b150,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram53{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b154,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram54{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b158,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram55{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b15c,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram56{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b160,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram57{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b164,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram58{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b168,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram59{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b16c,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram60{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b170,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram61{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b174,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram62{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b178,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram63{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b17c,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram64{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b180,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram65{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b184,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram66{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b188,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram67{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b18c,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram68{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b190,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram69{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b194,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram70{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b198,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram71{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b19c,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram72{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b1a0,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram73{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b1a4,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram74{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b1a8,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram75{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b1ac,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram76{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b1b0,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram77{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b1b4,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram78{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b1b8,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram79{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b1bc,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram80{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b1c0,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram81{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b1c4,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram82{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b1c8,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram83{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b1cc,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram84{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b1d0,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram85{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b1d4,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram86{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b1d8,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram87{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b1dc,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram88{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b1e0,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram89{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b1e4,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram90{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b1e8,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram91{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b1ec,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram92{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b1f0,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram93{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b1f4,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram94{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b1f8,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram95{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b1fc,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram96{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b200,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram97{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b204,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram98{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b208,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram99{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b20c,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram100{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b210,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram101{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b214,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram102{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b218,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram103{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b21c,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram104{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b220,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram105{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b224,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram106{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b228,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram107{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b22c,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram108{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b230,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram109{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b234,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram110{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b238,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram111{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b23c,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram112{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b240,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram113{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b244,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram114{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b248,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram115{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b24c,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram116{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b250,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram117{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b254,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram118{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b258,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram119{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b25c,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram120{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b260,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram121{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b264,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram122{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b268,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram123{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b26c,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram124{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b270,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram125{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b274,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram126{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b278,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Embeddedram127{    ///<Embedded RAM
        using Addr = Register::Address<0x4002b27c,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Rximr0{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b880,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr1{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b884,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr2{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b888,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr3{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b88c,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr4{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b890,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr5{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b894,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr6{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b898,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr7{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b89c,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr8{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b8a0,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr9{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b8a4,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr10{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b8a8,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr11{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b8ac,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr12{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b8b0,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr13{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b8b4,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr14{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b8b8,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr15{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b8bc,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr16{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b8c0,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr17{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b8c4,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr18{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b8c8,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr19{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b8cc,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr20{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b8d0,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr21{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b8d4,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr22{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b8d8,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr23{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b8dc,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr24{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b8e0,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr25{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b8e4,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr26{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b8e8,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr27{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b8ec,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr28{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b8f0,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr29{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b8f4,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr30{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b8f8,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Rximr31{    ///<Rx Individual Mask Registers
        using Addr = Register::Address<0x4002b8fc,0x00000000,0x00000000,unsigned>;
        ///Individual Mask Bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> mi{}; 
    }
    namespace Can2Ctrl1Pn{    ///<Pretended Networking Control 1 Register
        using Addr = Register::Address<0x4002bb00,0xfffc00c0,0x00000000,unsigned>;
        ///Filtering Combination Selection
        enum class FcsVal : unsigned {
            v00=0x00000000,     ///<Message ID filtering only
            v01=0x00000001,     ///<Message ID filtering and payload filtering
            v10=0x00000002,     ///<Message ID filtering occurring a specified number of times.
            v11=0x00000003,     ///<Message ID filtering and payload filtering a specified number of times
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,0),Register::ReadWriteAccess,FcsVal> fcs{}; 
        namespace FcsValC{
            constexpr Register::FieldValue<decltype(fcs)::Type,FcsVal::v00> v00{};
            constexpr Register::FieldValue<decltype(fcs)::Type,FcsVal::v01> v01{};
            constexpr Register::FieldValue<decltype(fcs)::Type,FcsVal::v10> v10{};
            constexpr Register::FieldValue<decltype(fcs)::Type,FcsVal::v11> v11{};
        }
        ///ID Filtering Selection
        enum class IdfsVal : unsigned {
            v00=0x00000000,     ///<Match upon a ID contents against an exact target value
            v01=0x00000001,     ///<Match upon a ID value greater than or equal to a specified target value
            v10=0x00000002,     ///<Match upon a ID value smaller than or equal to a specified target value
            v11=0x00000003,     ///<Match upon a ID value inside a range, greater than or equal to a specified lower limit and smaller than or equal a specified upper limit
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,2),Register::ReadWriteAccess,IdfsVal> idfs{}; 
        namespace IdfsValC{
            constexpr Register::FieldValue<decltype(idfs)::Type,IdfsVal::v00> v00{};
            constexpr Register::FieldValue<decltype(idfs)::Type,IdfsVal::v01> v01{};
            constexpr Register::FieldValue<decltype(idfs)::Type,IdfsVal::v10> v10{};
            constexpr Register::FieldValue<decltype(idfs)::Type,IdfsVal::v11> v11{};
        }
        ///Payload Filtering Selection
        enum class PlfsVal : unsigned {
            v00=0x00000000,     ///<Match upon a payload contents against an exact target value
            v01=0x00000001,     ///<Match upon a payload value greater than or equal to a specified target value
            v10=0x00000002,     ///<Match upon a payload value smaller than or equal to a specified target value
            v11=0x00000003,     ///<Match upon a payload value inside a range, greater than or equal to a specified lower limit and smaller than or equal a specified upper limit
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,4),Register::ReadWriteAccess,PlfsVal> plfs{}; 
        namespace PlfsValC{
            constexpr Register::FieldValue<decltype(plfs)::Type,PlfsVal::v00> v00{};
            constexpr Register::FieldValue<decltype(plfs)::Type,PlfsVal::v01> v01{};
            constexpr Register::FieldValue<decltype(plfs)::Type,PlfsVal::v10> v10{};
            constexpr Register::FieldValue<decltype(plfs)::Type,PlfsVal::v11> v11{};
        }
        ///Number of Messages Matching the Same Filtering Criteria
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> nmatch{}; 
        ///Wake Up by Match Flag Mask Bit
        enum class WumfmskVal : unsigned {
            v0=0x00000000,     ///<Wake up match event is disabled
            v1=0x00000001,     ///<Wake up match event is enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,WumfmskVal> wumfMsk{}; 
        namespace WumfmskValC{
            constexpr Register::FieldValue<decltype(wumfMsk)::Type,WumfmskVal::v0> v0{};
            constexpr Register::FieldValue<decltype(wumfMsk)::Type,WumfmskVal::v1> v1{};
        }
        ///Wake Up by Timeout Flag Mask Bit
        enum class WtofmskVal : unsigned {
            v0=0x00000000,     ///<Timeout wake up event is disabled
            v1=0x00000001,     ///<Timeout wake up event is enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,17),Register::ReadWriteAccess,WtofmskVal> wtofMsk{}; 
        namespace WtofmskValC{
            constexpr Register::FieldValue<decltype(wtofMsk)::Type,WtofmskVal::v0> v0{};
            constexpr Register::FieldValue<decltype(wtofMsk)::Type,WtofmskVal::v1> v1{};
        }
    }
    namespace Can2Ctrl2Pn{    ///<Pretended Networking Control 2 Register
        using Addr = Register::Address<0x4002bb04,0xffff0000,0x00000000,unsigned>;
        ///Timeout for No Message Matching the Filtering Criteria
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::ReadWriteAccess,unsigned> matchto{}; 
    }
    namespace Can2WuMtc{    ///<Pretended Networking Wake Up Match Register
        using Addr = Register::Address<0x4002bb08,0xfffc00ff,0x00000000,unsigned>;
        ///Number of Matches while in Pretended Networking
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> mcounter{}; 
        ///Wake Up by Match Flag Bit
        enum class WumfVal : unsigned {
            v0=0x00000000,     ///<No wake up by match event detected
            v1=0x00000001,     ///<Wake up by match event detected
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,WumfVal> wumf{}; 
        namespace WumfValC{
            constexpr Register::FieldValue<decltype(wumf)::Type,WumfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(wumf)::Type,WumfVal::v1> v1{};
        }
        ///Wake Up by Timeout Flag Bit
        enum class WtofVal : unsigned {
            v0=0x00000000,     ///<No wake up by timeout event detected
            v1=0x00000001,     ///<Wake up by timeout event detected
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,17),Register::ReadWriteAccess,WtofVal> wtof{}; 
        namespace WtofValC{
            constexpr Register::FieldValue<decltype(wtof)::Type,WtofVal::v0> v0{};
            constexpr Register::FieldValue<decltype(wtof)::Type,WtofVal::v1> v1{};
        }
    }
    namespace Can2FltId1{    ///<Pretended Networking ID Filter 1 Register
        using Addr = Register::Address<0x4002bb0c,0x80000000,0x00000000,unsigned>;
        ///ID Filter 1 for Pretended Networking filtering
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,0),Register::ReadWriteAccess,unsigned> fltId1{}; 
        ///Remote Transmission Request Filter
        enum class FltrtrVal : unsigned {
            v0=0x00000000,     ///<Reject remote frame (accept data frame)
            v1=0x00000001,     ///<Accept remote frame
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,FltrtrVal> fltRtr{}; 
        namespace FltrtrValC{
            constexpr Register::FieldValue<decltype(fltRtr)::Type,FltrtrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fltRtr)::Type,FltrtrVal::v1> v1{};
        }
        ///ID Extended Filter
        enum class FltideVal : unsigned {
            v0=0x00000000,     ///<Accept standard frame format
            v1=0x00000001,     ///<Accept extended frame format
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,FltideVal> fltIde{}; 
        namespace FltideValC{
            constexpr Register::FieldValue<decltype(fltIde)::Type,FltideVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fltIde)::Type,FltideVal::v1> v1{};
        }
    }
    namespace Can2FltDlc{    ///<Pretended Networking DLC Filter Register
        using Addr = Register::Address<0x4002bb10,0xfff0fff0,0x00000000,unsigned>;
        ///Upper Limit for Length of Data Bytes Filter
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::ReadWriteAccess,unsigned> fltDlcHi{}; 
        ///Lower Limit for Length of Data Bytes Filter
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,unsigned> fltDlcLo{}; 
    }
    namespace Can2Pl1Lo{    ///<Pretended Networking Payload Low Filter 1 Register
        using Addr = Register::Address<0x4002bb14,0x00000000,0x00000000,unsigned>;
        ///Payload Filter 1 low order bits for Pretended Networking payload filtering corresponding to the data byte 3.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Payload Filter 1 low order bits for Pretended Networking payload filtering corresponding to the data byte 2.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Payload Filter 1 low order bits for Pretended Networking payload filtering corresponding to the data byte 1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Payload Filter 1 low order bits for Pretended Networking payload filtering corresponding to the data byte 0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Pl1Hi{    ///<Pretended Networking Payload High Filter 1 Register
        using Addr = Register::Address<0x4002bb18,0x00000000,0x00000000,unsigned>;
        ///Payload Filter 1 high order bits for Pretended Networking payload filtering corresponding to the data byte 7.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte7{}; 
        ///Payload Filter 1 high order bits for Pretended Networking payload filtering corresponding to the data byte 6.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte6{}; 
        ///Payload Filter 1 high order bits for Pretended Networking payload filtering corresponding to the data byte 5.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte5{}; 
        ///Payload Filter 1 high order bits for Pretended Networking payload filtering corresponding to the data byte 4.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte4{}; 
    }
    namespace Can2FltId2Idmask{    ///<Pretended Networking ID Filter 2 Register / ID Mask Register
        using Addr = Register::Address<0x4002bb1c,0x80000000,0x00000000,unsigned>;
        ///ID Filter 2 for Pretended Networking Filtering / ID Mask Bits for Pretended Networking ID Filtering
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,0),Register::ReadWriteAccess,unsigned> fltId2Idmask{}; 
        ///Remote Transmission Request Mask Bit
        enum class RtrmskVal : unsigned {
            v0=0x00000000,     ///<The corresponding bit in the filter is "don't care"
            v1=0x00000001,     ///<The corresponding bit in the filter is checked
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,RtrmskVal> rtrMsk{}; 
        namespace RtrmskValC{
            constexpr Register::FieldValue<decltype(rtrMsk)::Type,RtrmskVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rtrMsk)::Type,RtrmskVal::v1> v1{};
        }
        ///ID Extended Mask Bit
        enum class IdemskVal : unsigned {
            v0=0x00000000,     ///<The corresponding bit in the filter is "don't care"
            v1=0x00000001,     ///<The corresponding bit in the filter is checked
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,IdemskVal> ideMsk{}; 
        namespace IdemskValC{
            constexpr Register::FieldValue<decltype(ideMsk)::Type,IdemskVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ideMsk)::Type,IdemskVal::v1> v1{};
        }
    }
    namespace Can2Pl2PlmaskLo{    ///<Pretended Networking Payload Low Filter 2 Register / Payload Low Mask Register
        using Addr = Register::Address<0x4002bb20,0x00000000,0x00000000,unsigned>;
        ///Payload Filter 2 low order bits / Payload Mask low order bits for Pretended Networking payload filtering corresponding to the data byte 3.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte3{}; 
        ///Payload Filter 2 low order bits / Payload Mask low order bits for Pretended Networking payload filtering corresponding to the data byte 2.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte2{}; 
        ///Payload Filter 2 low order bits / Payload Mask low order bits for Pretended Networking payload filtering corresponding to the data byte 1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte1{}; 
        ///Payload Filter 2 low order bits / Payload Mask low order bits for Pretended Networking payload filtering corresponding to the data byte 0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte0{}; 
    }
    namespace Can2Pl2PlmaskHi{    ///<Pretended Networking Payload High Filter 2 low order bits / Payload High Mask Register
        using Addr = Register::Address<0x4002bb24,0x00000000,0x00000000,unsigned>;
        ///Payload Filter 2 high order bits / Payload Mask high order bits for Pretended Networking payload filtering corresponding to the data byte 7.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> dataByte7{}; 
        ///Payload Filter 2 high order bits / Payload Mask high order bits for Pretended Networking payload filtering corresponding to the data byte 6.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dataByte6{}; 
        ///Payload Filter 2 high order bits / Payload Mask high order bits for Pretended Networking payload filtering corresponding to the data byte 5.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> dataByte5{}; 
        ///Payload Filter 2 high order bits / Payload Mask high order bits for Pretended Networking payload filtering corresponding to the data byte 4.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> dataByte4{}; 
    }
    namespace Can2Wmb0Cs{    ///<Wake Up Message Buffer Register for C/S
        using Addr = Register::Address<0x4002bb40,0xff80ffff,0x00000000,unsigned>;
        ///Length of Data in Bytes
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dlc{}; 
        ///Remote Transmission Request Bit
        enum class RtrVal : unsigned {
            v0=0x00000000,     ///<Frame is data one (not remote)
            v1=0x00000001,     ///<Frame is a remote one
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,20),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RtrVal> rtr{}; 
        namespace RtrValC{
            constexpr Register::FieldValue<decltype(rtr)::Type,RtrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rtr)::Type,RtrVal::v1> v1{};
        }
        ///ID Extended Bit
        enum class IdeVal : unsigned {
            v0=0x00000000,     ///<Frame format is standard
            v1=0x00000001,     ///<Frame format is extended
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(21,21),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,IdeVal> ide{}; 
        namespace IdeValC{
            constexpr Register::FieldValue<decltype(ide)::Type,IdeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ide)::Type,IdeVal::v1> v1{};
        }
        ///Substitute Remote Request
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,22),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> srr{}; 
    }
    namespace Can2Wmb0Id{    ///<Wake Up Message Buffer Register for ID
        using Addr = Register::Address<0x4002bb44,0xe0000000,0x00000000,unsigned>;
        ///Received ID under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> id{}; 
    }
    namespace Can2Wmb0D03{    ///<Wake Up Message Buffer Register for Data 0-3
        using Addr = Register::Address<0x4002bb48,0x00000000,0x00000000,unsigned>;
        ///Received payload corresponding to the data byte 3 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte3{}; 
        ///Received payload corresponding to the data byte 2 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte2{}; 
        ///Received payload corresponding to the data byte 1 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte1{}; 
        ///Received payload corresponding to the data byte 0 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte0{}; 
    }
    namespace Can2Wmb0D47{    ///<Wake Up Message Buffer Register Data 4-7
        using Addr = Register::Address<0x4002bb4c,0x00000000,0x00000000,unsigned>;
        ///Received payload corresponding to the data byte 7 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte7{}; 
        ///Received payload corresponding to the data byte 6 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte6{}; 
        ///Received payload corresponding to the data byte 5 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte5{}; 
        ///Received payload corresponding to the data byte 4 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte4{}; 
    }
    namespace Can2Wmb1Cs{    ///<Wake Up Message Buffer Register for C/S
        using Addr = Register::Address<0x4002bb50,0xff80ffff,0x00000000,unsigned>;
        ///Length of Data in Bytes
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dlc{}; 
        ///Remote Transmission Request Bit
        enum class RtrVal : unsigned {
            v0=0x00000000,     ///<Frame is data one (not remote)
            v1=0x00000001,     ///<Frame is a remote one
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,20),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RtrVal> rtr{}; 
        namespace RtrValC{
            constexpr Register::FieldValue<decltype(rtr)::Type,RtrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rtr)::Type,RtrVal::v1> v1{};
        }
        ///ID Extended Bit
        enum class IdeVal : unsigned {
            v0=0x00000000,     ///<Frame format is standard
            v1=0x00000001,     ///<Frame format is extended
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(21,21),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,IdeVal> ide{}; 
        namespace IdeValC{
            constexpr Register::FieldValue<decltype(ide)::Type,IdeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ide)::Type,IdeVal::v1> v1{};
        }
        ///Substitute Remote Request
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,22),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> srr{}; 
    }
    namespace Can2Wmb1Id{    ///<Wake Up Message Buffer Register for ID
        using Addr = Register::Address<0x4002bb54,0xe0000000,0x00000000,unsigned>;
        ///Received ID under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> id{}; 
    }
    namespace Can2Wmb1D03{    ///<Wake Up Message Buffer Register for Data 0-3
        using Addr = Register::Address<0x4002bb58,0x00000000,0x00000000,unsigned>;
        ///Received payload corresponding to the data byte 3 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte3{}; 
        ///Received payload corresponding to the data byte 2 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte2{}; 
        ///Received payload corresponding to the data byte 1 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte1{}; 
        ///Received payload corresponding to the data byte 0 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte0{}; 
    }
    namespace Can2Wmb1D47{    ///<Wake Up Message Buffer Register Data 4-7
        using Addr = Register::Address<0x4002bb5c,0x00000000,0x00000000,unsigned>;
        ///Received payload corresponding to the data byte 7 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte7{}; 
        ///Received payload corresponding to the data byte 6 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte6{}; 
        ///Received payload corresponding to the data byte 5 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte5{}; 
        ///Received payload corresponding to the data byte 4 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte4{}; 
    }
    namespace Can2Wmb2Cs{    ///<Wake Up Message Buffer Register for C/S
        using Addr = Register::Address<0x4002bb60,0xff80ffff,0x00000000,unsigned>;
        ///Length of Data in Bytes
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dlc{}; 
        ///Remote Transmission Request Bit
        enum class RtrVal : unsigned {
            v0=0x00000000,     ///<Frame is data one (not remote)
            v1=0x00000001,     ///<Frame is a remote one
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,20),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RtrVal> rtr{}; 
        namespace RtrValC{
            constexpr Register::FieldValue<decltype(rtr)::Type,RtrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rtr)::Type,RtrVal::v1> v1{};
        }
        ///ID Extended Bit
        enum class IdeVal : unsigned {
            v0=0x00000000,     ///<Frame format is standard
            v1=0x00000001,     ///<Frame format is extended
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(21,21),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,IdeVal> ide{}; 
        namespace IdeValC{
            constexpr Register::FieldValue<decltype(ide)::Type,IdeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ide)::Type,IdeVal::v1> v1{};
        }
        ///Substitute Remote Request
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,22),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> srr{}; 
    }
    namespace Can2Wmb2Id{    ///<Wake Up Message Buffer Register for ID
        using Addr = Register::Address<0x4002bb64,0xe0000000,0x00000000,unsigned>;
        ///Received ID under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> id{}; 
    }
    namespace Can2Wmb2D03{    ///<Wake Up Message Buffer Register for Data 0-3
        using Addr = Register::Address<0x4002bb68,0x00000000,0x00000000,unsigned>;
        ///Received payload corresponding to the data byte 3 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte3{}; 
        ///Received payload corresponding to the data byte 2 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte2{}; 
        ///Received payload corresponding to the data byte 1 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte1{}; 
        ///Received payload corresponding to the data byte 0 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte0{}; 
    }
    namespace Can2Wmb2D47{    ///<Wake Up Message Buffer Register Data 4-7
        using Addr = Register::Address<0x4002bb6c,0x00000000,0x00000000,unsigned>;
        ///Received payload corresponding to the data byte 7 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte7{}; 
        ///Received payload corresponding to the data byte 6 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte6{}; 
        ///Received payload corresponding to the data byte 5 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte5{}; 
        ///Received payload corresponding to the data byte 4 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte4{}; 
    }
    namespace Can2Wmb3Cs{    ///<Wake Up Message Buffer Register for C/S
        using Addr = Register::Address<0x4002bb70,0xff80ffff,0x00000000,unsigned>;
        ///Length of Data in Bytes
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dlc{}; 
        ///Remote Transmission Request Bit
        enum class RtrVal : unsigned {
            v0=0x00000000,     ///<Frame is data one (not remote)
            v1=0x00000001,     ///<Frame is a remote one
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,20),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RtrVal> rtr{}; 
        namespace RtrValC{
            constexpr Register::FieldValue<decltype(rtr)::Type,RtrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rtr)::Type,RtrVal::v1> v1{};
        }
        ///ID Extended Bit
        enum class IdeVal : unsigned {
            v0=0x00000000,     ///<Frame format is standard
            v1=0x00000001,     ///<Frame format is extended
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(21,21),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,IdeVal> ide{}; 
        namespace IdeValC{
            constexpr Register::FieldValue<decltype(ide)::Type,IdeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ide)::Type,IdeVal::v1> v1{};
        }
        ///Substitute Remote Request
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,22),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> srr{}; 
    }
    namespace Can2Wmb3Id{    ///<Wake Up Message Buffer Register for ID
        using Addr = Register::Address<0x4002bb74,0xe0000000,0x00000000,unsigned>;
        ///Received ID under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> id{}; 
    }
    namespace Can2Wmb3D03{    ///<Wake Up Message Buffer Register for Data 0-3
        using Addr = Register::Address<0x4002bb78,0x00000000,0x00000000,unsigned>;
        ///Received payload corresponding to the data byte 3 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte3{}; 
        ///Received payload corresponding to the data byte 2 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte2{}; 
        ///Received payload corresponding to the data byte 1 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte1{}; 
        ///Received payload corresponding to the data byte 0 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte0{}; 
    }
    namespace Can2Wmb3D47{    ///<Wake Up Message Buffer Register Data 4-7
        using Addr = Register::Address<0x4002bb7c,0x00000000,0x00000000,unsigned>;
        ///Received payload corresponding to the data byte 7 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte7{}; 
        ///Received payload corresponding to the data byte 6 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte6{}; 
        ///Received payload corresponding to the data byte 5 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte5{}; 
        ///Received payload corresponding to the data byte 4 under Pretended Networking mode
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataByte4{}; 
    }
    namespace Can2Fdctrl{    ///<CAN FD Control Register
        using Addr = Register::Address<0x4002bc00,0x7ffc20c0,0x00000000,unsigned>;
        ///Transceiver Delay Compensation Value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> tdcval{}; 
        ///Transceiver Delay Compensation Offset
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,8),Register::ReadWriteAccess,unsigned> tdcoff{}; 
        ///Transceiver Delay Compensation Fail
        enum class TdcfailVal : unsigned {
            v0=0x00000000,     ///<Measured loop delay is in range.
            v1=0x00000001,     ///<Measured loop delay is out of range.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,14),Register::ReadWriteAccess,TdcfailVal> tdcfail{}; 
        namespace TdcfailValC{
            constexpr Register::FieldValue<decltype(tdcfail)::Type,TdcfailVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tdcfail)::Type,TdcfailVal::v1> v1{};
        }
        ///Transceiver Delay Compensation Enable
        enum class TdcenVal : unsigned {
            v0=0x00000000,     ///<TDC is disabled
            v1=0x00000001,     ///<TDC is enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,TdcenVal> tdcen{}; 
        namespace TdcenValC{
            constexpr Register::FieldValue<decltype(tdcen)::Type,TdcenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tdcen)::Type,TdcenVal::v1> v1{};
        }
        ///Message Buffer Data Size for Region 0
        enum class Mbdsr0Val : unsigned {
            v00=0x00000000,     ///<Selects 8 bytes per Message Buffer.
            v01=0x00000001,     ///<Selects 16 bytes per Message Buffer.
            v10=0x00000002,     ///<Selects 32 bytes per Message Buffer.
            v11=0x00000003,     ///<Selects 64 bytes per Message Buffer.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,16),Register::ReadWriteAccess,Mbdsr0Val> mbdsr0{}; 
        namespace Mbdsr0ValC{
            constexpr Register::FieldValue<decltype(mbdsr0)::Type,Mbdsr0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(mbdsr0)::Type,Mbdsr0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(mbdsr0)::Type,Mbdsr0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(mbdsr0)::Type,Mbdsr0Val::v11> v11{};
        }
        ///Bit Rate Switch Enable
        enum class FdrateVal : unsigned {
            v0=0x00000000,     ///<Transmit a frame in nominal rate. The BRS bit in the Tx MB has no effect.
            v1=0x00000001,     ///<Transmit a frame with bit rate switching if the BRS bit in the Tx MB is recessive.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,FdrateVal> fdrate{}; 
        namespace FdrateValC{
            constexpr Register::FieldValue<decltype(fdrate)::Type,FdrateVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fdrate)::Type,FdrateVal::v1> v1{};
        }
    }
    namespace Can2Fdcbt{    ///<CAN FD Bit Timing Register
        using Addr = Register::Address<0x4002bc04,0xc0088318,0x00000000,unsigned>;
        ///Fast Phase Segment 2
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> fpseg2{}; 
        ///Fast Phase Segment 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,5),Register::ReadWriteAccess,unsigned> fpseg1{}; 
        ///Fast Propagation Segment
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,10),Register::ReadWriteAccess,unsigned> fpropseg{}; 
        ///Fast Resync Jump Width
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(18,16),Register::ReadWriteAccess,unsigned> frjw{}; 
        ///Fast Prescaler Division Factor
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,20),Register::ReadWriteAccess,unsigned> fpresdiv{}; 
    }
    namespace Can2Fdcrc{    ///<CAN FD CRC Register
        using Addr = Register::Address<0x4002bc08,0x80e00000,0x00000000,unsigned>;
        ///Extended Transmitted CRC value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> fdTxcrc{}; 
        ///CRC Mailbox Number for FD_TXCRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> fdMbcrc{}; 
    }
}
