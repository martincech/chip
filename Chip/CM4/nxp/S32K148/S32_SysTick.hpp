#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//System timer
    namespace S32SystickCsr{    ///<SysTick Control and Status Register
        using Addr = Register::Address<0xe000e010,0xfffefff8,0x00000000,unsigned>;
        ///Enables the counter
        enum class EnableVal : unsigned {
            v0=0x00000000,     ///<counter disabled
            v1=0x00000001,     ///<counter enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,EnableVal> enable{}; 
        namespace EnableValC{
            constexpr Register::FieldValue<decltype(enable)::Type,EnableVal::v0> v0{};
            constexpr Register::FieldValue<decltype(enable)::Type,EnableVal::v1> v1{};
        }
        ///Enables SysTick exception request
        enum class TickintVal : unsigned {
            v0=0x00000000,     ///<counting down to 0 does not assert the SysTick exception request
            v1=0x00000001,     ///<counting down to 0 asserts the SysTick exception request
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,TickintVal> tickint{}; 
        namespace TickintValC{
            constexpr Register::FieldValue<decltype(tickint)::Type,TickintVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tickint)::Type,TickintVal::v1> v1{};
        }
        ///Indicates the clock source
        enum class ClksourceVal : unsigned {
            v0=0x00000000,     ///<external clock
            v1=0x00000001,     ///<processor clock
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,ClksourceVal> clksource{}; 
        namespace ClksourceValC{
            constexpr Register::FieldValue<decltype(clksource)::Type,ClksourceVal::v0> v0{};
            constexpr Register::FieldValue<decltype(clksource)::Type,ClksourceVal::v1> v1{};
        }
        ///Returns 1 if timer counted to 0 since last time this was read
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,unsigned> countflag{}; 
    }
    namespace S32SystickRvr{    ///<SysTick Reload Value Register
        using Addr = Register::Address<0xe000e014,0xff000000,0x00000000,unsigned>;
        ///Value to load into the SysTick Current Value Register when the counter reaches 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,0),Register::ReadWriteAccess,unsigned> reload{}; 
    }
    namespace S32SystickCvr{    ///<SysTick Current Value Register
        using Addr = Register::Address<0xe000e018,0xff000000,0x00000000,unsigned>;
        ///Current value at the time the register is accessed
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,0),Register::ReadWriteAccess,unsigned> current{}; 
    }
    namespace S32SystickCalib{    ///<SysTick Calibration Value Register
        using Addr = Register::Address<0xe000e01c,0x3f000000,0x00000000,unsigned>;
        ///Reload value to use for 10ms timing
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> tenms{}; 
        ///Indicates whether the TENMS value is exact
        enum class SkewVal : unsigned {
            v0=0x00000000,     ///<TENMS value is exact
            v1=0x00000001,     ///<TENMS value is inexact, or not given
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,SkewVal> skew{}; 
        namespace SkewValC{
            constexpr Register::FieldValue<decltype(skew)::Type,SkewVal::v0> v0{};
            constexpr Register::FieldValue<decltype(skew)::Type,SkewVal::v1> v1{};
        }
        ///Indicates whether the device provides a reference clock to the processor
        enum class NorefVal : unsigned {
            v0=0x00000000,     ///<The reference clock is provided
            v1=0x00000001,     ///<The reference clock is not provided
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,NorefVal> noref{}; 
        namespace NorefValC{
            constexpr Register::FieldValue<decltype(noref)::Type,NorefVal::v0> v0{};
            constexpr Register::FieldValue<decltype(noref)::Type,NorefVal::v1> v1{};
        }
    }
}
