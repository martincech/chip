#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//System Control Registers
    namespace S32ScbActlr{    ///<Auxiliary Control Register,
        using Addr = Register::Address<0xe000e008,0xfffffcf8,0x00000000,unsigned>;
        ///Disables interruption of multi-cycle instructions.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,unsigned> dismcycint{}; 
        ///Disables write buffer use during default memory map accesses.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,unsigned> disdefwbuf{}; 
        ///Disables folding of IT instructions.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,unsigned> disfold{}; 
        ///SBZP / Disables automatic update of CONTROL.FPCA.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::ReadWriteAccess,unsigned> disfpca{}; 
        ///Disables floating point instructions completing out of order with respect to integer instructions.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,9),Register::ReadWriteAccess,unsigned> disoofp{}; 
    }
    namespace S32ScbCpuid{    ///<CPUID Base Register
        using Addr = Register::Address<0xe000ed00,0x000f0000,0x00000000,unsigned>;
        ///Indicates patch release: 0x0 = Patch 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> revision{}; 
        ///Indicates part number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,4),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> partno{}; 
        ///Indicates processor revision: 0x2 = Revision 2
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,20),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> variant{}; 
        ///Implementer code
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> implementer{}; 
    }
    namespace S32ScbIcsr{    ///<Interrupt Control and State Register
        using Addr = Register::Address<0xe000ed04,0x613c0600,0x00000000,unsigned>;
        ///Active exception number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> vectactive{}; 
        ///Indicates whether there are preempted active exceptions
        enum class RettobaseVal : unsigned {
            v0=0x00000000,     ///<there are preempted active exceptions to execute
            v1=0x00000001,     ///<there are no active exceptions, or the currently-executing exception is the only active exception
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RettobaseVal> rettobase{}; 
        namespace RettobaseValC{
            constexpr Register::FieldValue<decltype(rettobase)::Type,RettobaseVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rettobase)::Type,RettobaseVal::v1> v1{};
        }
        ///Exception number of the highest priority pending enabled exception
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,12),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> vectpending{}; 
        ///Interrupt pending flag, excluding NMI and Faults
        enum class IsrpendingVal : unsigned {
            v0=0x00000000,     ///<interrupt not pending
            v1=0x00000001,     ///<interrupt pending
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,22),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,IsrpendingVal> isrpending{}; 
        namespace IsrpendingValC{
            constexpr Register::FieldValue<decltype(isrpending)::Type,IsrpendingVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isrpending)::Type,IsrpendingVal::v1> v1{};
        }
        ///no description available
        enum class IsrpreemptVal : unsigned {
            v0=0x00000000,     ///<Will not service
            v1=0x00000001,     ///<Will service a pending exception
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,23),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,IsrpreemptVal> isrpreempt{}; 
        namespace IsrpreemptValC{
            constexpr Register::FieldValue<decltype(isrpreempt)::Type,IsrpreemptVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isrpreempt)::Type,IsrpreemptVal::v1> v1{};
        }
        ///SysTick exception clear-pending bit
        enum class PendstclrVal : unsigned {
            v0=0x00000000,     ///<no effect
            v1=0x00000001,     ///<removes the pending state from the SysTick exception
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PendstclrVal> pendstclr{}; 
        namespace PendstclrValC{
            constexpr Register::FieldValue<decltype(pendstclr)::Type,PendstclrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pendstclr)::Type,PendstclrVal::v1> v1{};
        }
        ///SysTick exception set-pending bit
        enum class PendstsetVal : unsigned {
            v0=0x00000000,     ///<write: no effect; read: SysTick exception is not pending
            v1=0x00000001,     ///<write: changes SysTick exception state to pending; read: SysTick exception is pending
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,PendstsetVal> pendstset{}; 
        namespace PendstsetValC{
            constexpr Register::FieldValue<decltype(pendstset)::Type,PendstsetVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pendstset)::Type,PendstsetVal::v1> v1{};
        }
        ///PendSV clear-pending bit
        enum class PendsvclrVal : unsigned {
            v0=0x00000000,     ///<no effect
            v1=0x00000001,     ///<removes the pending state from the PendSV exception
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PendsvclrVal> pendsvclr{}; 
        namespace PendsvclrValC{
            constexpr Register::FieldValue<decltype(pendsvclr)::Type,PendsvclrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pendsvclr)::Type,PendsvclrVal::v1> v1{};
        }
        ///PendSV set-pending bit
        enum class PendsvsetVal : unsigned {
            v0=0x00000000,     ///<write: no effect; read: PendSV exception is not pending
            v1=0x00000001,     ///<write: changes PendSV exception state to pending; read: PendSV exception is pending
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,PendsvsetVal> pendsvset{}; 
        namespace PendsvsetValC{
            constexpr Register::FieldValue<decltype(pendsvset)::Type,PendsvsetVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pendsvset)::Type,PendsvsetVal::v1> v1{};
        }
        ///NMI set-pending bit
        enum class NmipendsetVal : unsigned {
            v0=0x00000000,     ///<write: no effect; read: NMI exception is not pending
            v1=0x00000001,     ///<write: changes NMI exception state to pending; read: NMI exception is pending
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,NmipendsetVal> nmipendset{}; 
        namespace NmipendsetValC{
            constexpr Register::FieldValue<decltype(nmipendset)::Type,NmipendsetVal::v0> v0{};
            constexpr Register::FieldValue<decltype(nmipendset)::Type,NmipendsetVal::v1> v1{};
        }
    }
    namespace S32ScbVtor{    ///<Vector Table Offset Register
        using Addr = Register::Address<0xe000ed08,0x0000007f,0x00000000,unsigned>;
        ///Vector table base offset
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,7),Register::ReadWriteAccess,unsigned> tbloff{}; 
    }
    namespace S32ScbAircr{    ///<Application Interrupt and Reset Control Register
        using Addr = Register::Address<0xe000ed0c,0x000078f8,0x00000000,unsigned>;
        ///Reserved for Debug use. This bit reads as 0. When writing to the register you must write 0 to this bit, otherwise behavior is Unpredictable.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> vectreset{}; 
        ///Reserved for Debug use. This bit reads as 0. When writing to the register you must write 0 to this bit, otherwise behavior is Unpredictable.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> vectclractive{}; 
        ///System reset request bit is implementation defined
        enum class SysresetreqVal : unsigned {
            v0=0x00000000,     ///<no system reset request
            v1=0x00000001,     ///<asserts a signal to the outer system that requests a reset
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,SysresetreqVal> sysresetreq{}; 
        namespace SysresetreqValC{
            constexpr Register::FieldValue<decltype(sysresetreq)::Type,SysresetreqVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sysresetreq)::Type,SysresetreqVal::v1> v1{};
        }
        ///Interrupt priority grouping field. This field determines the split of group priority from subpriority.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,unsigned> prigroup{}; 
        ///Data endianness bit is implementation defined
        enum class EndiannessVal : unsigned {
            v0=0x00000000,     ///<Little-endian
            v1=0x00000001,     ///<Big-endian
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,EndiannessVal> endianness{}; 
        namespace EndiannessValC{
            constexpr Register::FieldValue<decltype(endianness)::Type,EndiannessVal::v0> v0{};
            constexpr Register::FieldValue<decltype(endianness)::Type,EndiannessVal::v1> v1{};
        }
        ///Register key
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,16),Register::ReadWriteAccess,unsigned> vectkey{}; 
    }
    namespace S32ScbScr{    ///<System Control Register
        using Addr = Register::Address<0xe000ed10,0xffffffe9,0x00000000,unsigned>;
        ///Indicates sleep-on-exit when returning from Handler mode to Thread mode
        enum class SleeponexitVal : unsigned {
            v0=0x00000000,     ///<o not sleep when returning to Thread mode
            v1=0x00000001,     ///<enter sleep, or deep sleep, on return from an ISR
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,SleeponexitVal> sleeponexit{}; 
        namespace SleeponexitValC{
            constexpr Register::FieldValue<decltype(sleeponexit)::Type,SleeponexitVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sleeponexit)::Type,SleeponexitVal::v1> v1{};
        }
        ///Controls whether the processor uses sleep or deep sleep as its low power mode
        enum class SleepdeepVal : unsigned {
            v0=0x00000000,     ///<sleep
            v1=0x00000001,     ///<deep sleep
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,SleepdeepVal> sleepdeep{}; 
        namespace SleepdeepValC{
            constexpr Register::FieldValue<decltype(sleepdeep)::Type,SleepdeepVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sleepdeep)::Type,SleepdeepVal::v1> v1{};
        }
        ///Send Event on Pending bit
        enum class SevonpendVal : unsigned {
            v0=0x00000000,     ///<only enabled interrupts or events can wakeup the processor, disabled interrupts are excluded
            v1=0x00000001,     ///<enabled events and all interrupts, including disabled interrupts, can wakeup the processor
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,4),Register::ReadWriteAccess,SevonpendVal> sevonpend{}; 
        namespace SevonpendValC{
            constexpr Register::FieldValue<decltype(sevonpend)::Type,SevonpendVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sevonpend)::Type,SevonpendVal::v1> v1{};
        }
    }
    namespace S32ScbCcr{    ///<Configuration and Control Register
        using Addr = Register::Address<0xe000ed14,0xfffffce4,0x00000000,unsigned>;
        ///no description available
        enum class NonbasethrdenaVal : unsigned {
            v0=0x00000000,     ///<processor can enter Thread mode only when no exception is active
            v1=0x00000001,     ///<processor can enter Thread mode from any level under the control of an EXC_RETURN value
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,NonbasethrdenaVal> nonbasethrdena{}; 
        namespace NonbasethrdenaValC{
            constexpr Register::FieldValue<decltype(nonbasethrdena)::Type,NonbasethrdenaVal::v0> v0{};
            constexpr Register::FieldValue<decltype(nonbasethrdena)::Type,NonbasethrdenaVal::v1> v1{};
        }
        ///Enables unprivileged software access to the STIR
        enum class UsersetmpendVal : unsigned {
            v0=0x00000000,     ///<disable
            v1=0x00000001,     ///<enable
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,UsersetmpendVal> usersetmpend{}; 
        namespace UsersetmpendValC{
            constexpr Register::FieldValue<decltype(usersetmpend)::Type,UsersetmpendVal::v0> v0{};
            constexpr Register::FieldValue<decltype(usersetmpend)::Type,UsersetmpendVal::v1> v1{};
        }
        ///Enables unaligned access traps
        enum class UnaligntrpVal : unsigned {
            v0=0x00000000,     ///<do not trap unaligned halfword and word accesses
            v1=0x00000001,     ///<trap unaligned halfword and word accesses
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,UnaligntrpVal> unalignTrp{}; 
        namespace UnaligntrpValC{
            constexpr Register::FieldValue<decltype(unalignTrp)::Type,UnaligntrpVal::v0> v0{};
            constexpr Register::FieldValue<decltype(unalignTrp)::Type,UnaligntrpVal::v1> v1{};
        }
        ///Enables faulting or halting when the processor executes an SDIV or UDIV instruction with a divisor of 0
        enum class Div0trpVal : unsigned {
            v0=0x00000000,     ///<do not trap divide by 0
            v1=0x00000001,     ///<trap divide by 0
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,4),Register::ReadWriteAccess,Div0trpVal> div0Trp{}; 
        namespace Div0trpValC{
            constexpr Register::FieldValue<decltype(div0Trp)::Type,Div0trpVal::v0> v0{};
            constexpr Register::FieldValue<decltype(div0Trp)::Type,Div0trpVal::v1> v1{};
        }
        ///Enables handlers with priority -1 or -2 to ignore data BusFaults caused by load and store instructions.
        enum class BfhfnmignVal : unsigned {
            v0=0x00000000,     ///<data bus faults caused by load and store instructions cause a lock-up
            v1=0x00000001,     ///<handlers running at priority -1 and -2 ignore data bus faults caused by load and store instructions
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::ReadWriteAccess,BfhfnmignVal> bfhfnmign{}; 
        namespace BfhfnmignValC{
            constexpr Register::FieldValue<decltype(bfhfnmign)::Type,BfhfnmignVal::v0> v0{};
            constexpr Register::FieldValue<decltype(bfhfnmign)::Type,BfhfnmignVal::v1> v1{};
        }
        ///Indicates stack alignment on exception entry
        enum class StkalignVal : unsigned {
            v0=0x00000000,     ///<4-byte aligned
            v1=0x00000001,     ///<8-byte aligned
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,9),Register::ReadWriteAccess,StkalignVal> stkalign{}; 
        namespace StkalignValC{
            constexpr Register::FieldValue<decltype(stkalign)::Type,StkalignVal::v0> v0{};
            constexpr Register::FieldValue<decltype(stkalign)::Type,StkalignVal::v1> v1{};
        }
    }
    namespace S32ScbShpr1{    ///<System Handler Priority Register 1
        using Addr = Register::Address<0xe000ed18,0xff000000,0x00000000,unsigned>;
        ///Priority of system handler 4, MemManage
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> pri4{}; 
        ///Priority of system handler 5, BusFault
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> pri5{}; 
        ///Priority of system handler 6, UsageFault
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> pri6{}; 
    }
    namespace S32ScbShpr2{    ///<System Handler Priority Register 2
        using Addr = Register::Address<0xe000ed1c,0x00ffffff,0x00000000,unsigned>;
        ///Priority of system handler 11, SVCall
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> pri11{}; 
    }
    namespace S32ScbShpr3{    ///<System Handler Priority Register 3
        using Addr = Register::Address<0xe000ed20,0x0000ff00,0x00000000,unsigned>;
        ///Priority of system handler 12, DebugMonitor
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> pri12{}; 
        ///Priority of system handler 14, PendSV
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> pri14{}; 
        ///Priority of system handler 15, SysTick exception
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> pri15{}; 
    }
    namespace S32ScbShcsr{    ///<System Handler Control and State Register
        using Addr = Register::Address<0xe000ed24,0xfff80274,0x00000000,unsigned>;
        ///MemManage exception active bit, reads as 1 if exception is active
        enum class MemfaultactVal : unsigned {
            v0=0x00000000,     ///<exception is not active
            v1=0x00000001,     ///<exception is active
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,MemfaultactVal> memfaultact{}; 
        namespace MemfaultactValC{
            constexpr Register::FieldValue<decltype(memfaultact)::Type,MemfaultactVal::v0> v0{};
            constexpr Register::FieldValue<decltype(memfaultact)::Type,MemfaultactVal::v1> v1{};
        }
        ///BusFault exception active bit, reads as 1 if exception is active
        enum class BusfaultactVal : unsigned {
            v0=0x00000000,     ///<exception is not active
            v1=0x00000001,     ///<exception is active
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,BusfaultactVal> busfaultact{}; 
        namespace BusfaultactValC{
            constexpr Register::FieldValue<decltype(busfaultact)::Type,BusfaultactVal::v0> v0{};
            constexpr Register::FieldValue<decltype(busfaultact)::Type,BusfaultactVal::v1> v1{};
        }
        ///UsageFault exception active bit, reads as 1 if exception is active
        enum class UsgfaultactVal : unsigned {
            v0=0x00000000,     ///<exception is not active
            v1=0x00000001,     ///<exception is active
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,UsgfaultactVal> usgfaultact{}; 
        namespace UsgfaultactValC{
            constexpr Register::FieldValue<decltype(usgfaultact)::Type,UsgfaultactVal::v0> v0{};
            constexpr Register::FieldValue<decltype(usgfaultact)::Type,UsgfaultactVal::v1> v1{};
        }
        ///SVCall active bit, reads as 1 if SVC call is active
        enum class SvcallactVal : unsigned {
            v0=0x00000000,     ///<exception is not active
            v1=0x00000001,     ///<exception is active
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::ReadWriteAccess,SvcallactVal> svcallact{}; 
        namespace SvcallactValC{
            constexpr Register::FieldValue<decltype(svcallact)::Type,SvcallactVal::v0> v0{};
            constexpr Register::FieldValue<decltype(svcallact)::Type,SvcallactVal::v1> v1{};
        }
        ///Debug monitor active bit, reads as 1 if Debug monitor is active
        enum class MonitoractVal : unsigned {
            v0=0x00000000,     ///<exception is not active
            v1=0x00000001,     ///<exception is active
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::ReadWriteAccess,MonitoractVal> monitoract{}; 
        namespace MonitoractValC{
            constexpr Register::FieldValue<decltype(monitoract)::Type,MonitoractVal::v0> v0{};
            constexpr Register::FieldValue<decltype(monitoract)::Type,MonitoractVal::v1> v1{};
        }
        ///PendSV exception active bit, reads as 1 if exception is active
        enum class PendsvactVal : unsigned {
            v0=0x00000000,     ///<exception is not active
            v1=0x00000001,     ///<exception is active
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,10),Register::ReadWriteAccess,PendsvactVal> pendsvact{}; 
        namespace PendsvactValC{
            constexpr Register::FieldValue<decltype(pendsvact)::Type,PendsvactVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pendsvact)::Type,PendsvactVal::v1> v1{};
        }
        ///SysTick exception active bit, reads as 1 if exception is active
        enum class SystickactVal : unsigned {
            v0=0x00000000,     ///<exception is not active
            v1=0x00000001,     ///<exception is active
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,SystickactVal> systickact{}; 
        namespace SystickactValC{
            constexpr Register::FieldValue<decltype(systickact)::Type,SystickactVal::v0> v0{};
            constexpr Register::FieldValue<decltype(systickact)::Type,SystickactVal::v1> v1{};
        }
        ///UsageFault exception pending bit, reads as 1 if exception is pending
        enum class UsgfaultpendedVal : unsigned {
            v0=0x00000000,     ///<exception is not pending
            v1=0x00000001,     ///<exception is pending
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,12),Register::ReadWriteAccess,UsgfaultpendedVal> usgfaultpended{}; 
        namespace UsgfaultpendedValC{
            constexpr Register::FieldValue<decltype(usgfaultpended)::Type,UsgfaultpendedVal::v0> v0{};
            constexpr Register::FieldValue<decltype(usgfaultpended)::Type,UsgfaultpendedVal::v1> v1{};
        }
        ///MemManage exception pending bit, reads as 1 if exception is pending
        enum class MemfaultpendedVal : unsigned {
            v0=0x00000000,     ///<exception is not pending
            v1=0x00000001,     ///<exception is pending
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,13),Register::ReadWriteAccess,MemfaultpendedVal> memfaultpended{}; 
        namespace MemfaultpendedValC{
            constexpr Register::FieldValue<decltype(memfaultpended)::Type,MemfaultpendedVal::v0> v0{};
            constexpr Register::FieldValue<decltype(memfaultpended)::Type,MemfaultpendedVal::v1> v1{};
        }
        ///BusFault exception pending bit, reads as 1 if exception is pending
        enum class BusfaultpendedVal : unsigned {
            v0=0x00000000,     ///<exception is not pending
            v1=0x00000001,     ///<exception is pending
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,14),Register::ReadWriteAccess,BusfaultpendedVal> busfaultpended{}; 
        namespace BusfaultpendedValC{
            constexpr Register::FieldValue<decltype(busfaultpended)::Type,BusfaultpendedVal::v0> v0{};
            constexpr Register::FieldValue<decltype(busfaultpended)::Type,BusfaultpendedVal::v1> v1{};
        }
        ///SVCall pending bit, reads as 1 if exception is pending
        enum class SvcallpendedVal : unsigned {
            v0=0x00000000,     ///<exception is not pending
            v1=0x00000001,     ///<exception is pending
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,SvcallpendedVal> svcallpended{}; 
        namespace SvcallpendedValC{
            constexpr Register::FieldValue<decltype(svcallpended)::Type,SvcallpendedVal::v0> v0{};
            constexpr Register::FieldValue<decltype(svcallpended)::Type,SvcallpendedVal::v1> v1{};
        }
        ///MemManage enable bit, set to 1 to enable
        enum class MemfaultenaVal : unsigned {
            v0=0x00000000,     ///<disable the exception
            v1=0x00000001,     ///<enable the exception
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,MemfaultenaVal> memfaultena{}; 
        namespace MemfaultenaValC{
            constexpr Register::FieldValue<decltype(memfaultena)::Type,MemfaultenaVal::v0> v0{};
            constexpr Register::FieldValue<decltype(memfaultena)::Type,MemfaultenaVal::v1> v1{};
        }
        ///BusFault enable bit, set to 1 to enable
        enum class BusfaultenaVal : unsigned {
            v0=0x00000000,     ///<disable the exception
            v1=0x00000001,     ///<enable the exception
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,17),Register::ReadWriteAccess,BusfaultenaVal> busfaultena{}; 
        namespace BusfaultenaValC{
            constexpr Register::FieldValue<decltype(busfaultena)::Type,BusfaultenaVal::v0> v0{};
            constexpr Register::FieldValue<decltype(busfaultena)::Type,BusfaultenaVal::v1> v1{};
        }
        ///UsageFault enable bit, set to 1 to enable
        enum class UsgfaultenaVal : unsigned {
            v0=0x00000000,     ///<disable the exception
            v1=0x00000001,     ///<enable the exception
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(18,18),Register::ReadWriteAccess,UsgfaultenaVal> usgfaultena{}; 
        namespace UsgfaultenaValC{
            constexpr Register::FieldValue<decltype(usgfaultena)::Type,UsgfaultenaVal::v0> v0{};
            constexpr Register::FieldValue<decltype(usgfaultena)::Type,UsgfaultenaVal::v1> v1{};
        }
    }
    namespace S32ScbCfsr{    ///<Configurable Fault Status Registers
        using Addr = Register::Address<0xe000ed28,0xfcf04044,0x00000000,unsigned>;
        ///Instruction access violation flag
        enum class IaccviolVal : unsigned {
            v0=0x00000000,     ///<no instruction access violation fault
            v1=0x00000001,     ///<the processor attempted an instruction fetch from a location that does not permit execution
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,IaccviolVal> iaccviol{}; 
        namespace IaccviolValC{
            constexpr Register::FieldValue<decltype(iaccviol)::Type,IaccviolVal::v0> v0{};
            constexpr Register::FieldValue<decltype(iaccviol)::Type,IaccviolVal::v1> v1{};
        }
        ///Data access violation flag
        enum class DaccviolVal : unsigned {
            v0=0x00000000,     ///<no data access violation fault
            v1=0x00000001,     ///<the processor attempted a load or store at a location that does not permit the operation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,DaccviolVal> daccviol{}; 
        namespace DaccviolValC{
            constexpr Register::FieldValue<decltype(daccviol)::Type,DaccviolVal::v0> v0{};
            constexpr Register::FieldValue<decltype(daccviol)::Type,DaccviolVal::v1> v1{};
        }
        ///MemManage fault on unstacking for a return from exception
        enum class MunstkerrVal : unsigned {
            v0=0x00000000,     ///<no unstacking fault
            v1=0x00000001,     ///<unstack for an exception return has caused one or more access violations
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,MunstkerrVal> munstkerr{}; 
        namespace MunstkerrValC{
            constexpr Register::FieldValue<decltype(munstkerr)::Type,MunstkerrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(munstkerr)::Type,MunstkerrVal::v1> v1{};
        }
        ///MemManage fault on stacking for exception entry
        enum class MstkerrVal : unsigned {
            v0=0x00000000,     ///<no stacking fault
            v1=0x00000001,     ///<stacking for an exception entry has caused one or more access violations
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,4),Register::ReadWriteAccess,MstkerrVal> mstkerr{}; 
        namespace MstkerrValC{
            constexpr Register::FieldValue<decltype(mstkerr)::Type,MstkerrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(mstkerr)::Type,MstkerrVal::v1> v1{};
        }
        ///no description available
        enum class MlsperrVal : unsigned {
            v0=0x00000000,     ///<No MemManage fault occurred during floating-point lazy state preservation
            v1=0x00000001,     ///<A MemManage fault occurred during floating-point lazy state preservation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,MlsperrVal> mlsperr{}; 
        namespace MlsperrValC{
            constexpr Register::FieldValue<decltype(mlsperr)::Type,MlsperrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(mlsperr)::Type,MlsperrVal::v1> v1{};
        }
        ///MemManage Fault Address Register (MMFAR) valid flag
        enum class MmarvalidVal : unsigned {
            v0=0x00000000,     ///<value in MMAR is not a valid fault address
            v1=0x00000001,     ///<MMAR holds a valid fault address
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::ReadWriteAccess,MmarvalidVal> mmarvalid{}; 
        namespace MmarvalidValC{
            constexpr Register::FieldValue<decltype(mmarvalid)::Type,MmarvalidVal::v0> v0{};
            constexpr Register::FieldValue<decltype(mmarvalid)::Type,MmarvalidVal::v1> v1{};
        }
        ///Instruction bus error
        enum class IbuserrVal : unsigned {
            v0=0x00000000,     ///<no instruction bus error
            v1=0x00000001,     ///<instruction bus error
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::ReadWriteAccess,IbuserrVal> ibuserr{}; 
        namespace IbuserrValC{
            constexpr Register::FieldValue<decltype(ibuserr)::Type,IbuserrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ibuserr)::Type,IbuserrVal::v1> v1{};
        }
        ///Precise data bus error
        enum class PreciserrVal : unsigned {
            v0=0x00000000,     ///<no precise data bus error
            v1=0x00000001,     ///<a data bus error has occurred, and the PC value stacked for the exception return points to the instruction that caused the fault
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,9),Register::ReadWriteAccess,PreciserrVal> preciserr{}; 
        namespace PreciserrValC{
            constexpr Register::FieldValue<decltype(preciserr)::Type,PreciserrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(preciserr)::Type,PreciserrVal::v1> v1{};
        }
        ///Imprecise data bus error
        enum class ImpreciserrVal : unsigned {
            v0=0x00000000,     ///<no imprecise data bus error
            v1=0x00000001,     ///<a data bus error has occurred, but the return address in the stack frame is not related to the instruction that caused the error
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,10),Register::ReadWriteAccess,ImpreciserrVal> impreciserr{}; 
        namespace ImpreciserrValC{
            constexpr Register::FieldValue<decltype(impreciserr)::Type,ImpreciserrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(impreciserr)::Type,ImpreciserrVal::v1> v1{};
        }
        ///BusFault on unstacking for a return from exception
        enum class UnstkerrVal : unsigned {
            v0=0x00000000,     ///<no unstacking fault
            v1=0x00000001,     ///<unstack for an exception return has caused one or more BusFaults
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,UnstkerrVal> unstkerr{}; 
        namespace UnstkerrValC{
            constexpr Register::FieldValue<decltype(unstkerr)::Type,UnstkerrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(unstkerr)::Type,UnstkerrVal::v1> v1{};
        }
        ///BusFault on stacking for exception entry
        enum class StkerrVal : unsigned {
            v0=0x00000000,     ///<no stacking fault
            v1=0x00000001,     ///<stacking for an exception entry has caused one or more BusFaults
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,12),Register::ReadWriteAccess,StkerrVal> stkerr{}; 
        namespace StkerrValC{
            constexpr Register::FieldValue<decltype(stkerr)::Type,StkerrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(stkerr)::Type,StkerrVal::v1> v1{};
        }
        ///no description available
        enum class LsperrVal : unsigned {
            v0=0x00000000,     ///<No bus fault occurred during floating-point lazy state preservation
            v1=0x00000001,     ///<A bus fault occurred during floating-point lazy state preservation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,13),Register::ReadWriteAccess,LsperrVal> lsperr{}; 
        namespace LsperrValC{
            constexpr Register::FieldValue<decltype(lsperr)::Type,LsperrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lsperr)::Type,LsperrVal::v1> v1{};
        }
        ///BusFault Address Register (BFAR) valid flag
        enum class BfarvalidVal : unsigned {
            v0=0x00000000,     ///<value in BFAR is not a valid fault address
            v1=0x00000001,     ///<BFAR holds a valid fault address
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,BfarvalidVal> bfarvalid{}; 
        namespace BfarvalidValC{
            constexpr Register::FieldValue<decltype(bfarvalid)::Type,BfarvalidVal::v0> v0{};
            constexpr Register::FieldValue<decltype(bfarvalid)::Type,BfarvalidVal::v1> v1{};
        }
        ///Undefined instruction UsageFault
        enum class UndefinstrVal : unsigned {
            v0=0x00000000,     ///<no undefined instruction UsageFault
            v1=0x00000001,     ///<the processor has attempted to execute an undefined instruction
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,UndefinstrVal> undefinstr{}; 
        namespace UndefinstrValC{
            constexpr Register::FieldValue<decltype(undefinstr)::Type,UndefinstrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(undefinstr)::Type,UndefinstrVal::v1> v1{};
        }
        ///Invalid state UsageFault
        enum class InvstateVal : unsigned {
            v0=0x00000000,     ///<no invalid state UsageFault
            v1=0x00000001,     ///<the processor has attempted to execute an instruction that makes illegal use of the EPSR
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,17),Register::ReadWriteAccess,InvstateVal> invstate{}; 
        namespace InvstateValC{
            constexpr Register::FieldValue<decltype(invstate)::Type,InvstateVal::v0> v0{};
            constexpr Register::FieldValue<decltype(invstate)::Type,InvstateVal::v1> v1{};
        }
        ///Invalid PC load UsageFault, caused by an invalid PC load by EXC_RETURN
        enum class InvpcVal : unsigned {
            v0=0x00000000,     ///<no invalid PC load UsageFault
            v1=0x00000001,     ///<the processor has attempted an illegal load of EXC_RETURN to the PC
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(18,18),Register::ReadWriteAccess,InvpcVal> invpc{}; 
        namespace InvpcValC{
            constexpr Register::FieldValue<decltype(invpc)::Type,InvpcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(invpc)::Type,InvpcVal::v1> v1{};
        }
        ///No coprocessor UsageFault. The processor does not support coprocessor instructions
        enum class NocpVal : unsigned {
            v0=0x00000000,     ///<no UsageFault caused by attempting to access a coprocessor
            v1=0x00000001,     ///<the processor has attempted to access a coprocessor
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,19),Register::ReadWriteAccess,NocpVal> nocp{}; 
        namespace NocpValC{
            constexpr Register::FieldValue<decltype(nocp)::Type,NocpVal::v0> v0{};
            constexpr Register::FieldValue<decltype(nocp)::Type,NocpVal::v1> v1{};
        }
        ///Unaligned access UsageFault
        enum class UnalignedVal : unsigned {
            v0=0x00000000,     ///<no unaligned access fault, or unaligned access trapping not enabled
            v1=0x00000001,     ///<the processor has made an unaligned memory access
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,UnalignedVal> unaligned{}; 
        namespace UnalignedValC{
            constexpr Register::FieldValue<decltype(unaligned)::Type,UnalignedVal::v0> v0{};
            constexpr Register::FieldValue<decltype(unaligned)::Type,UnalignedVal::v1> v1{};
        }
        ///Divide by zero UsageFault
        enum class DivbyzeroVal : unsigned {
            v0=0x00000000,     ///<no divide by zero fault, or divide by zero trapping not enabled
            v1=0x00000001,     ///<the processor has executed an SDIV or UDIV instruction with a divisor of 0
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,DivbyzeroVal> divbyzero{}; 
        namespace DivbyzeroValC{
            constexpr Register::FieldValue<decltype(divbyzero)::Type,DivbyzeroVal::v0> v0{};
            constexpr Register::FieldValue<decltype(divbyzero)::Type,DivbyzeroVal::v1> v1{};
        }
    }
    namespace S32ScbHfsr{    ///<HardFault Status register
        using Addr = Register::Address<0xe000ed2c,0x3ffffffd,0x00000000,unsigned>;
        ///Indicates a BusFault on a vector table read during exception processing
        enum class VecttblVal : unsigned {
            v0=0x00000000,     ///<no BusFault on vector table read
            v1=0x00000001,     ///<BusFault on vector table read
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,VecttblVal> vecttbl{}; 
        namespace VecttblValC{
            constexpr Register::FieldValue<decltype(vecttbl)::Type,VecttblVal::v0> v0{};
            constexpr Register::FieldValue<decltype(vecttbl)::Type,VecttblVal::v1> v1{};
        }
        ///Indicates a forced hard fault, generated by escalation of a fault with configurable priority that cannot be handles, either because of priority or because it is disabled
        enum class ForcedVal : unsigned {
            v0=0x00000000,     ///<no forced HardFault
            v1=0x00000001,     ///<forced HardFault
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,ForcedVal> forced{}; 
        namespace ForcedValC{
            constexpr Register::FieldValue<decltype(forced)::Type,ForcedVal::v0> v0{};
            constexpr Register::FieldValue<decltype(forced)::Type,ForcedVal::v1> v1{};
        }
        ///Reserved for Debug use. When writing to the register you must write 0 to this bit, otherwise behavior is Unpredictable.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,unsigned> debugevt{}; 
    }
    namespace S32ScbDfsr{    ///<Debug Fault Status Register
        using Addr = Register::Address<0xe000ed30,0xffffffe0,0x00000000,unsigned>;
        ///no description available
        enum class HaltedVal : unsigned {
            v0=0x00000000,     ///<No active halt request debug event
            v1=0x00000001,     ///<Halt request debug event active
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,HaltedVal> halted{}; 
        namespace HaltedValC{
            constexpr Register::FieldValue<decltype(halted)::Type,HaltedVal::v0> v0{};
            constexpr Register::FieldValue<decltype(halted)::Type,HaltedVal::v1> v1{};
        }
        ///no description available
        enum class BkptVal : unsigned {
            v0=0x00000000,     ///<No current breakpoint debug event
            v1=0x00000001,     ///<At least one current breakpoint debug event
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,BkptVal> bkpt{}; 
        namespace BkptValC{
            constexpr Register::FieldValue<decltype(bkpt)::Type,BkptVal::v0> v0{};
            constexpr Register::FieldValue<decltype(bkpt)::Type,BkptVal::v1> v1{};
        }
        ///no description available
        enum class DwttrapVal : unsigned {
            v0=0x00000000,     ///<No current debug events generated by the DWT
            v1=0x00000001,     ///<At least one current debug event generated by the DWT
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,DwttrapVal> dwttrap{}; 
        namespace DwttrapValC{
            constexpr Register::FieldValue<decltype(dwttrap)::Type,DwttrapVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dwttrap)::Type,DwttrapVal::v1> v1{};
        }
        ///no description available
        enum class VcatchVal : unsigned {
            v0=0x00000000,     ///<No Vector catch triggered
            v1=0x00000001,     ///<Vector catch triggered
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,VcatchVal> vcatch{}; 
        namespace VcatchValC{
            constexpr Register::FieldValue<decltype(vcatch)::Type,VcatchVal::v0> v0{};
            constexpr Register::FieldValue<decltype(vcatch)::Type,VcatchVal::v1> v1{};
        }
        ///no description available
        enum class ExternalVal : unsigned {
            v0=0x00000000,     ///<No EDBGRQ debug event
            v1=0x00000001,     ///<EDBGRQ debug event
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,4),Register::ReadWriteAccess,ExternalVal> external{}; 
        namespace ExternalValC{
            constexpr Register::FieldValue<decltype(external)::Type,ExternalVal::v0> v0{};
            constexpr Register::FieldValue<decltype(external)::Type,ExternalVal::v1> v1{};
        }
    }
    namespace S32ScbMmfar{    ///<MemManage Address Register
        using Addr = Register::Address<0xe000ed34,0x00000000,0x00000000,unsigned>;
        ///Address of MemManage fault location
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> address{}; 
    }
    namespace S32ScbBfar{    ///<BusFault Address Register
        using Addr = Register::Address<0xe000ed38,0x00000000,0x00000000,unsigned>;
        ///Address of the BusFault location
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> address{}; 
    }
    namespace S32ScbAfsr{    ///<Auxiliary Fault Status Register
        using Addr = Register::Address<0xe000ed3c,0x00000000,0x00000000,unsigned>;
        ///Latched version of the AUXFAULT inputs
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> auxfault{}; 
    }
    namespace S32ScbCpacr{    ///<Coprocessor Access Control Register
        using Addr = Register::Address<0xe000ed88,0xff0fffff,0x00000000,unsigned>;
        ///Access privileges for coprocessor 10.
        enum class Cp10Val : unsigned {
            v00=0x00000000,     ///<Access denied. Any attempted access generates a NOCP UsageFault
            v01=0x00000001,     ///<Privileged access only. An unprivileged access generates a NOCP fault.
            v10=0x00000002,     ///<Reserved. The result of any access is UNPREDICTABLE.
            v11=0x00000003,     ///<Full access.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(21,20),Register::ReadWriteAccess,Cp10Val> cp10{}; 
        namespace Cp10ValC{
            constexpr Register::FieldValue<decltype(cp10)::Type,Cp10Val::v00> v00{};
            constexpr Register::FieldValue<decltype(cp10)::Type,Cp10Val::v01> v01{};
            constexpr Register::FieldValue<decltype(cp10)::Type,Cp10Val::v10> v10{};
            constexpr Register::FieldValue<decltype(cp10)::Type,Cp10Val::v11> v11{};
        }
        ///Access privileges for coprocessor 11.
        enum class Cp11Val : unsigned {
            v00=0x00000000,     ///<Access denied. Any attempted access generates a NOCP UsageFault
            v01=0x00000001,     ///<Privileged access only. An unprivileged access generates a NOCP fault.
            v10=0x00000002,     ///<Reserved. The result of any access is UNPREDICTABLE.
            v11=0x00000003,     ///<Full access.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,22),Register::ReadWriteAccess,Cp11Val> cp11{}; 
        namespace Cp11ValC{
            constexpr Register::FieldValue<decltype(cp11)::Type,Cp11Val::v00> v00{};
            constexpr Register::FieldValue<decltype(cp11)::Type,Cp11Val::v01> v01{};
            constexpr Register::FieldValue<decltype(cp11)::Type,Cp11Val::v10> v10{};
            constexpr Register::FieldValue<decltype(cp11)::Type,Cp11Val::v11> v11{};
        }
    }
    namespace S32ScbFpccr{    ///<Floating-point Context Control Register
        using Addr = Register::Address<0xe000ef34,0x3ffffe84,0x00000000,unsigned>;
        ///Lazy state preservation.
        enum class LspactVal : unsigned {
            v0=0x00000000,     ///<Lazy state preservation is not active.
            v1=0x00000001,     ///<Lazy state preservation is active. floating-point stack frame has been allocated but saving state to it has been deferred.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,LspactVal> lspact{}; 
        namespace LspactValC{
            constexpr Register::FieldValue<decltype(lspact)::Type,LspactVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lspact)::Type,LspactVal::v1> v1{};
        }
        ///Privilege level when the floating-point stack frame was allocated.
        enum class UserVal : unsigned {
            v0=0x00000000,     ///<Privilege level was not user when the floating-point stack frame was allocated.
            v1=0x00000001,     ///<Privilege level was user when the floating-point stack frame was allocated.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,UserVal> user{}; 
        namespace UserValC{
            constexpr Register::FieldValue<decltype(user)::Type,UserVal::v0> v0{};
            constexpr Register::FieldValue<decltype(user)::Type,UserVal::v1> v1{};
        }
        ///Mode when the floating-point stack frame was allocated.
        enum class ThreadVal : unsigned {
            v0=0x00000000,     ///<Mode was not Thread Mode when the floating-point stack frame was allocated.
            v1=0x00000001,     ///<Mode was Thread Mode when the floating-point stack frame was allocated.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,ThreadVal> thread{}; 
        namespace ThreadValC{
            constexpr Register::FieldValue<decltype(thread)::Type,ThreadVal::v0> v0{};
            constexpr Register::FieldValue<decltype(thread)::Type,ThreadVal::v1> v1{};
        }
        ///Permission to set the HardFault handler to the pending state when the floating-point stack frame was allocated.
        enum class HfrdyVal : unsigned {
            v0=0x00000000,     ///<Priority did not permit setting the HardFault handler to the pending state when the floating-point stack frame was allocated.
            v1=0x00000001,     ///<Priority permitted setting the HardFault handler to the pending state when the floating-point stack frame was allocated.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,4),Register::ReadWriteAccess,HfrdyVal> hfrdy{}; 
        namespace HfrdyValC{
            constexpr Register::FieldValue<decltype(hfrdy)::Type,HfrdyVal::v0> v0{};
            constexpr Register::FieldValue<decltype(hfrdy)::Type,HfrdyVal::v1> v1{};
        }
        ///Permission to set the MemManage handler to the pending state when the floating-point stack frame was allocated.
        enum class MmrdyVal : unsigned {
            v0=0x00000000,     ///<MemManage is disabled or priority did not permit setting the MemManage handler to the pending state when the floating-point stack frame was allocated.
            v1=0x00000001,     ///<MemManage is enabled and priority permitted setting the MemManage handler to the pending state when the floating-point stack frame was allocated.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,MmrdyVal> mmrdy{}; 
        namespace MmrdyValC{
            constexpr Register::FieldValue<decltype(mmrdy)::Type,MmrdyVal::v0> v0{};
            constexpr Register::FieldValue<decltype(mmrdy)::Type,MmrdyVal::v1> v1{};
        }
        ///Permission to set the BusFault handler to the pending state when the floating-point stack frame was allocated.
        enum class BfrdyVal : unsigned {
            v0=0x00000000,     ///<BusFault is disabled or priority did not permit setting the BusFault handler to the pending state when the floating-point stack frame was allocated.
            v1=0x00000001,     ///<BusFault is disabled or priority did not permit setting the BusFault handler to the pending state when the floating-point stack frame was allocated.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,BfrdyVal> bfrdy{}; 
        namespace BfrdyValC{
            constexpr Register::FieldValue<decltype(bfrdy)::Type,BfrdyVal::v0> v0{};
            constexpr Register::FieldValue<decltype(bfrdy)::Type,BfrdyVal::v1> v1{};
        }
        ///Permission to set the MON_PEND when the floating-point stack frame was allocated.
        enum class MonrdyVal : unsigned {
            v0=0x00000000,     ///<DebugMonitor is disabled or priority did not permit setting MON_PEND when the floating-point stack frame was allocated.
            v1=0x00000001,     ///<DebugMonitor is enabled and priority permits setting MON_PEND when the floating-point stack frame was allocated.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::ReadWriteAccess,MonrdyVal> monrdy{}; 
        namespace MonrdyValC{
            constexpr Register::FieldValue<decltype(monrdy)::Type,MonrdyVal::v0> v0{};
            constexpr Register::FieldValue<decltype(monrdy)::Type,MonrdyVal::v1> v1{};
        }
        ///Lazy state preservation for floating-point context.
        enum class LspenVal : unsigned {
            v0=0x00000000,     ///<Disable automatic lazy state preservation for floating-point context.
            v1=0x00000001,     ///<Enable automatic lazy state preservation for floating-point context.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,LspenVal> lspen{}; 
        namespace LspenValC{
            constexpr Register::FieldValue<decltype(lspen)::Type,LspenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lspen)::Type,LspenVal::v1> v1{};
        }
        ///Enables CONTROL2 setting on execution of a floating-point instruction. This results in automatic hardware state preservation and restoration, for floating-point context, on exception entry and exit.
        enum class AspenVal : unsigned {
            v0=0x00000000,     ///<Disable CONTROL2 setting on execution of a floating-point instruction.
            v1=0x00000001,     ///<Enable CONTROL2 setting on execution of a floating-point instruction.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,AspenVal> aspen{}; 
        namespace AspenValC{
            constexpr Register::FieldValue<decltype(aspen)::Type,AspenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(aspen)::Type,AspenVal::v1> v1{};
        }
    }
    namespace S32ScbFpcar{    ///<Floating-point Context Address Register
        using Addr = Register::Address<0xe000ef38,0x00000007,0x00000000,unsigned>;
        ///The location of the unpopulated floating-point register space allocated on an exception stack frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,3),Register::ReadWriteAccess,unsigned> address{}; 
    }
    namespace S32ScbFpdscr{    ///<Floating-point Default Status Control Register
        using Addr = Register::Address<0xe000ef3c,0xf83fffff,0x00000000,unsigned>;
        ///Default value for FPSCR.RMode (Rounding Mode control field).
        enum class RmodeVal : unsigned {
            v00=0x00000000,     ///<Round to Nearest (RN) mode
            v01=0x00000001,     ///<Round towards Plus Infinity (RP) mode.
            v10=0x00000002,     ///<Round towards Minus Infinity (RM) mode.
            v11=0x00000003,     ///<Round towards Zero (RZ) mode.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,22),Register::ReadWriteAccess,RmodeVal> rmode{}; 
        namespace RmodeValC{
            constexpr Register::FieldValue<decltype(rmode)::Type,RmodeVal::v00> v00{};
            constexpr Register::FieldValue<decltype(rmode)::Type,RmodeVal::v01> v01{};
            constexpr Register::FieldValue<decltype(rmode)::Type,RmodeVal::v10> v10{};
            constexpr Register::FieldValue<decltype(rmode)::Type,RmodeVal::v11> v11{};
        }
        ///Default value for FPSCR.FZ (Flush-to-zero mode control bit).
        enum class FzVal : unsigned {
            v0=0x00000000,     ///<Flush-to-zero mode disabled. Behavior of the floating-point system is fully compliant with the IEEE 754 standard.
            v1=0x00000001,     ///<Flush-to-zero mode enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,FzVal> fz{}; 
        namespace FzValC{
            constexpr Register::FieldValue<decltype(fz)::Type,FzVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fz)::Type,FzVal::v1> v1{};
        }
        ///Default value for FPSCR.DN (Default NaN mode control bit).
        enum class DnVal : unsigned {
            v0=0x00000000,     ///<NaN operands propagate through to the output of a floating-point operation.
            v1=0x00000001,     ///<Any operation involving one or more NaNs returns the Default NaN.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,DnVal> dn{}; 
        namespace DnValC{
            constexpr Register::FieldValue<decltype(dn)::Type,DnVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dn)::Type,DnVal::v1> v1{};
        }
        ///Default value for FPSCR.AHP (Alternative half-precision control bit).
        enum class AhpVal : unsigned {
            v0=0x00000000,     ///<IEEE half-precision format selected.
            v1=0x00000001,     ///<Alternative half-precision format selected.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,AhpVal> ahp{}; 
        namespace AhpValC{
            constexpr Register::FieldValue<decltype(ahp)::Type,AhpVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ahp)::Type,AhpVal::v1> v1{};
        }
    }
}
