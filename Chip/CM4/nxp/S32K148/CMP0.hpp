#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//High-Speed Comparator (CMP), Voltage Reference (VREF) Digital-to-Analog Converter (DAC), and Analog Mux (ANMUX)
    namespace Cmp0C0{    ///<CMP Control Register 0
        using Addr = Register::Address<0x40073000,0xa0002088,0x00000000,unsigned>;
        ///Comparator hard block hysteresis control. See chip data sheet to get the actual hystersis value with each level
        enum class HystctrVal : unsigned {
            v00=0x00000000,     ///<The hard block output has level 0 hysteresis internally.
            v01=0x00000001,     ///<The hard block output has level 1 hysteresis internally.
            v10=0x00000002,     ///<The hard block output has level 2 hysteresis internally.
            v11=0x00000003,     ///<The hard block output has level 3 hysteresis internally.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,0),Register::ReadWriteAccess,HystctrVal> hystctr{}; 
        namespace HystctrValC{
            constexpr Register::FieldValue<decltype(hystctr)::Type,HystctrVal::v00> v00{};
            constexpr Register::FieldValue<decltype(hystctr)::Type,HystctrVal::v01> v01{};
            constexpr Register::FieldValue<decltype(hystctr)::Type,HystctrVal::v10> v10{};
            constexpr Register::FieldValue<decltype(hystctr)::Type,HystctrVal::v11> v11{};
        }
        ///Comparator hard block offset control. See chip data sheet to get the actual offset value with each level
        enum class OffsetVal : unsigned {
            v0=0x00000000,     ///<The comparator hard block output has level 0 offset internally.
            v1=0x00000001,     ///<The comparator hard block output has level 1 offset internally.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,OffsetVal> offset{}; 
        namespace OffsetValC{
            constexpr Register::FieldValue<decltype(offset)::Type,OffsetVal::v0> v0{};
            constexpr Register::FieldValue<decltype(offset)::Type,OffsetVal::v1> v1{};
        }
        ///Filter Sample Count
        enum class FiltercntVal : unsigned {
            v000=0x00000000,     ///<Filter is disabled. If SE = 1, then COUT is a logic zero (this is not a legal state, and is not recommended). If SE = 0, COUT = COUTA.
            v001=0x00000001,     ///<1 consecutive sample must agree (comparator output is simply sampled).
            v010=0x00000002,     ///<2 consecutive samples must agree.
            v011=0x00000003,     ///<3 consecutive samples must agree.
            v100=0x00000004,     ///<4 consecutive samples must agree.
            v101=0x00000005,     ///<5 consecutive samples must agree.
            v110=0x00000006,     ///<6 consecutive samples must agree.
            v111=0x00000007,     ///<7 consecutive samples must agree.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,4),Register::ReadWriteAccess,FiltercntVal> filterCnt{}; 
        namespace FiltercntValC{
            constexpr Register::FieldValue<decltype(filterCnt)::Type,FiltercntVal::v000> v000{};
            constexpr Register::FieldValue<decltype(filterCnt)::Type,FiltercntVal::v001> v001{};
            constexpr Register::FieldValue<decltype(filterCnt)::Type,FiltercntVal::v010> v010{};
            constexpr Register::FieldValue<decltype(filterCnt)::Type,FiltercntVal::v011> v011{};
            constexpr Register::FieldValue<decltype(filterCnt)::Type,FiltercntVal::v100> v100{};
            constexpr Register::FieldValue<decltype(filterCnt)::Type,FiltercntVal::v101> v101{};
            constexpr Register::FieldValue<decltype(filterCnt)::Type,FiltercntVal::v110> v110{};
            constexpr Register::FieldValue<decltype(filterCnt)::Type,FiltercntVal::v111> v111{};
        }
        ///Comparator Module Enable
        enum class EnVal : unsigned {
            v0=0x00000000,     ///<Analog Comparator is disabled.
            v1=0x00000001,     ///<Analog Comparator is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::ReadWriteAccess,EnVal> en{}; 
        namespace EnValC{
            constexpr Register::FieldValue<decltype(en)::Type,EnVal::v0> v0{};
            constexpr Register::FieldValue<decltype(en)::Type,EnVal::v1> v1{};
        }
        ///Comparator Output Pin Enable
        enum class OpeVal : unsigned {
            v0=0x00000000,     ///<When OPE is 0, the comparator output (after window/filter settings dependent on software configuration) is not available to a packaged pin.
            v1=0x00000001,     ///<When OPE is 1, and if the software has configured the comparator to own a packaged pin, the comparator is available in a packaged pin.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,9),Register::ReadWriteAccess,OpeVal> ope{}; 
        namespace OpeValC{
            constexpr Register::FieldValue<decltype(ope)::Type,OpeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ope)::Type,OpeVal::v1> v1{};
        }
        ///Comparator Output Select
        enum class CosVal : unsigned {
            v0=0x00000000,     ///<Set CMPO to equal COUT (filtered comparator output).
            v1=0x00000001,     ///<Set CMPO to equal COUTA (unfiltered comparator output).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,10),Register::ReadWriteAccess,CosVal> cos{}; 
        namespace CosValC{
            constexpr Register::FieldValue<decltype(cos)::Type,CosVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cos)::Type,CosVal::v1> v1{};
        }
        ///Comparator invert
        enum class InvtVal : unsigned {
            v0=0x00000000,     ///<Does not invert the comparator output.
            v1=0x00000001,     ///<Inverts the comparator output.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,InvtVal> invt{}; 
        namespace InvtValC{
            constexpr Register::FieldValue<decltype(invt)::Type,InvtVal::v0> v0{};
            constexpr Register::FieldValue<decltype(invt)::Type,InvtVal::v1> v1{};
        }
        ///Power Mode Select
        enum class PmodeVal : unsigned {
            v0=0x00000000,     ///<Low Speed (LS) comparison mode is selected.
            v1=0x00000001,     ///<High Speed (HS) comparison mode is selected, in VLPx mode, or Stop mode switched to Low Speed (LS) mode.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,12),Register::ReadWriteAccess,PmodeVal> pmode{}; 
        namespace PmodeValC{
            constexpr Register::FieldValue<decltype(pmode)::Type,PmodeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pmode)::Type,PmodeVal::v1> v1{};
        }
        ///Windowing Enable
        enum class WeVal : unsigned {
            v0=0x00000000,     ///<Windowing mode is not selected.
            v1=0x00000001,     ///<Windowing mode is selected.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,14),Register::ReadWriteAccess,WeVal> we{}; 
        namespace WeValC{
            constexpr Register::FieldValue<decltype(we)::Type,WeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(we)::Type,WeVal::v1> v1{};
        }
        ///Sample Enable
        enum class SeVal : unsigned {
            v0=0x00000000,     ///<Sampling mode is not selected.
            v1=0x00000001,     ///<Sampling mode is selected.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,SeVal> se{}; 
        namespace SeValC{
            constexpr Register::FieldValue<decltype(se)::Type,SeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(se)::Type,SeVal::v1> v1{};
        }
        ///Filter Sample Period
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> fpr{}; 
        ///Analog Comparator Output
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> cout{}; 
        ///Analog Comparator Flag Falling
        enum class CffVal : unsigned {
            v0=0x00000000,     ///<A falling edge has not been detected on COUT.
            v1=0x00000001,     ///<A falling edge on COUT has occurred.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,CffVal> cff{}; 
        namespace CffValC{
            constexpr Register::FieldValue<decltype(cff)::Type,CffVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cff)::Type,CffVal::v1> v1{};
        }
        ///Analog Comparator Flag Rising
        enum class CfrVal : unsigned {
            v0=0x00000000,     ///<A rising edge has not been detected on COUT.
            v1=0x00000001,     ///<A rising edge on COUT has occurred.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,CfrVal> cfr{}; 
        namespace CfrValC{
            constexpr Register::FieldValue<decltype(cfr)::Type,CfrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cfr)::Type,CfrVal::v1> v1{};
        }
        ///Comparator Interrupt Enable Falling
        enum class IefVal : unsigned {
            v0=0x00000000,     ///<Interrupt is disabled.
            v1=0x00000001,     ///<Interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,IefVal> ief{}; 
        namespace IefValC{
            constexpr Register::FieldValue<decltype(ief)::Type,IefVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ief)::Type,IefVal::v1> v1{};
        }
        ///Comparator Interrupt Enable Rising
        enum class IerVal : unsigned {
            v0=0x00000000,     ///<Interrupt is disabled.
            v1=0x00000001,     ///<Interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,IerVal> ier{}; 
        namespace IerValC{
            constexpr Register::FieldValue<decltype(ier)::Type,IerVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ier)::Type,IerVal::v1> v1{};
        }
        ///DMA Enable
        enum class DmaenVal : unsigned {
            v0=0x00000000,     ///<DMA is disabled.
            v1=0x00000001,     ///<DMA is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,DmaenVal> dmaen{}; 
        namespace DmaenValC{
            constexpr Register::FieldValue<decltype(dmaen)::Type,DmaenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dmaen)::Type,DmaenVal::v1> v1{};
        }
    }
    namespace Cmp0C1{    ///<CMP Control Register 1
        using Addr = Register::Address<0x40073004,0xe4000000,0x00000000,unsigned>;
        ///DAC Output Voltage Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> vosel{}; 
        ///Minus Input MUX Control
        enum class MselVal : unsigned {
            v000=0x00000000,     ///<IN0
            v001=0x00000001,     ///<IN1
            v010=0x00000002,     ///<IN2
            v011=0x00000003,     ///<IN3
            v100=0x00000004,     ///<IN4
            v101=0x00000005,     ///<IN5
            v110=0x00000006,     ///<IN6
            v111=0x00000007,     ///<IN7
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MselVal> msel{}; 
        namespace MselValC{
            constexpr Register::FieldValue<decltype(msel)::Type,MselVal::v000> v000{};
            constexpr Register::FieldValue<decltype(msel)::Type,MselVal::v001> v001{};
            constexpr Register::FieldValue<decltype(msel)::Type,MselVal::v010> v010{};
            constexpr Register::FieldValue<decltype(msel)::Type,MselVal::v011> v011{};
            constexpr Register::FieldValue<decltype(msel)::Type,MselVal::v100> v100{};
            constexpr Register::FieldValue<decltype(msel)::Type,MselVal::v101> v101{};
            constexpr Register::FieldValue<decltype(msel)::Type,MselVal::v110> v110{};
            constexpr Register::FieldValue<decltype(msel)::Type,MselVal::v111> v111{};
        }
        ///Plus Input MUX Control
        enum class PselVal : unsigned {
            v000=0x00000000,     ///<IN0
            v001=0x00000001,     ///<IN1
            v010=0x00000002,     ///<IN2
            v011=0x00000003,     ///<IN3
            v100=0x00000004,     ///<IN4
            v101=0x00000005,     ///<IN5
            v110=0x00000006,     ///<IN6
            v111=0x00000007,     ///<IN7
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,11),Register::ReadWriteAccess,PselVal> psel{}; 
        namespace PselValC{
            constexpr Register::FieldValue<decltype(psel)::Type,PselVal::v000> v000{};
            constexpr Register::FieldValue<decltype(psel)::Type,PselVal::v001> v001{};
            constexpr Register::FieldValue<decltype(psel)::Type,PselVal::v010> v010{};
            constexpr Register::FieldValue<decltype(psel)::Type,PselVal::v011> v011{};
            constexpr Register::FieldValue<decltype(psel)::Type,PselVal::v100> v100{};
            constexpr Register::FieldValue<decltype(psel)::Type,PselVal::v101> v101{};
            constexpr Register::FieldValue<decltype(psel)::Type,PselVal::v110> v110{};
            constexpr Register::FieldValue<decltype(psel)::Type,PselVal::v111> v111{};
        }
        ///Supply Voltage Reference Source Select
        enum class VrselVal : unsigned {
            v0=0x00000000,     ///<Vin1 is selected as resistor ladder network supply reference Vin.
            v1=0x00000001,     ///<Vin2 is selected as resistor ladder network supply reference Vin.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,14),Register::ReadWriteAccess,VrselVal> vrsel{}; 
        namespace VrselValC{
            constexpr Register::FieldValue<decltype(vrsel)::Type,VrselVal::v0> v0{};
            constexpr Register::FieldValue<decltype(vrsel)::Type,VrselVal::v1> v1{};
        }
        ///DAC Enable
        enum class DacenVal : unsigned {
            v0=0x00000000,     ///<DAC is disabled.
            v1=0x00000001,     ///<DAC is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,DacenVal> dacen{}; 
        namespace DacenValC{
            constexpr Register::FieldValue<decltype(dacen)::Type,DacenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dacen)::Type,DacenVal::v1> v1{};
        }
        ///Channel 0 input enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,unsigned> chn0{}; 
        ///Channel 1 input enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,17),Register::ReadWriteAccess,unsigned> chn1{}; 
        ///Channel 2 input enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(18,18),Register::ReadWriteAccess,unsigned> chn2{}; 
        ///Channel 3 input enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,19),Register::ReadWriteAccess,unsigned> chn3{}; 
        ///Channel 4 input enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,20),Register::ReadWriteAccess,unsigned> chn4{}; 
        ///Channel 5 input enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(21,21),Register::ReadWriteAccess,unsigned> chn5{}; 
        ///Channel 6 input enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,22),Register::ReadWriteAccess,unsigned> chn6{}; 
        ///Channel 7 input enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,23),Register::ReadWriteAccess,unsigned> chn7{}; 
        ///Selection of the input to the negative port of the comparator
        enum class InnselVal : unsigned {
            v00=0x00000000,     ///<IN0, from the 8-bit DAC output
            v01=0x00000001,     ///<IN1, from the analog 8-1 mux
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,InnselVal> innsel{}; 
        namespace InnselValC{
            constexpr Register::FieldValue<decltype(innsel)::Type,InnselVal::v00> v00{};
            constexpr Register::FieldValue<decltype(innsel)::Type,InnselVal::v01> v01{};
        }
        ///Selection of the input to the positive port of the comparator
        enum class InpselVal : unsigned {
            v00=0x00000000,     ///<IN0, from the 8-bit DAC output
            v01=0x00000001,     ///<IN1, from the analog 8-1 mux
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,27),Register::ReadWriteAccess,InpselVal> inpsel{}; 
        namespace InpselValC{
            constexpr Register::FieldValue<decltype(inpsel)::Type,InpselVal::v00> v00{};
            constexpr Register::FieldValue<decltype(inpsel)::Type,InpselVal::v01> v01{};
        }
    }
    namespace Cmp0C2{    ///<CMP Control Register 2
        using Addr = Register::Address<0x40073008,0x11000000,0x00000000,unsigned>;
        ///The result of the input comparison for channel n
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> acon{}; 
        ///Comparator and DAC initialization delay modulus.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,8),Register::ReadWriteAccess,unsigned> initmod{}; 
        ///Number of sample clocks
        enum class NsamVal : unsigned {
            v00=0x00000000,     ///<The comparison result is sampled as soon as the active channel is scanned in one round-robin clock.
            v01=0x00000001,     ///<The sampling takes place 1 round-robin clock cycle after the next cycle of the round-robin clock.
            v10=0x00000002,     ///<The sampling takes place 2 round-robin clock cycles after the next cycle of the round-robin clock.
            v11=0x00000003,     ///<The sampling takes place 3 round-robin clock cycles after the next cycle of the round-robin clock.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,14),Register::ReadWriteAccess,NsamVal> nsam{}; 
        namespace NsamValC{
            constexpr Register::FieldValue<decltype(nsam)::Type,NsamVal::v00> v00{};
            constexpr Register::FieldValue<decltype(nsam)::Type,NsamVal::v01> v01{};
            constexpr Register::FieldValue<decltype(nsam)::Type,NsamVal::v10> v10{};
            constexpr Register::FieldValue<decltype(nsam)::Type,NsamVal::v11> v11{};
        }
        ///Channel 0 input changed flag
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,unsigned> ch0f{}; 
        ///Channel 1 input changed flag
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,17),Register::ReadWriteAccess,unsigned> ch1f{}; 
        ///Channel 2 input changed flag
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(18,18),Register::ReadWriteAccess,unsigned> ch2f{}; 
        ///Channel 3 input changed flag
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,19),Register::ReadWriteAccess,unsigned> ch3f{}; 
        ///Channel 4 input changed flag
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,20),Register::ReadWriteAccess,unsigned> ch4f{}; 
        ///Channel 5 input changed flag
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(21,21),Register::ReadWriteAccess,unsigned> ch5f{}; 
        ///Channel 6 input changed flag
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,22),Register::ReadWriteAccess,unsigned> ch6f{}; 
        ///Channel 7 input changed flag
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,23),Register::ReadWriteAccess,unsigned> ch7f{}; 
        ///Fixed channel selection
        enum class FxmxchVal : unsigned {
            v000=0x00000000,     ///<Channel 0 is selected as the fixed reference input for the fixed mux port.
            v001=0x00000001,     ///<Channel 1 is selected as the fixed reference input for the fixed mux port.
            v010=0x00000002,     ///<Channel 2 is selected as the fixed reference input for the fixed mux port.
            v011=0x00000003,     ///<Channel 3 is selected as the fixed reference input for the fixed mux port.
            v100=0x00000004,     ///<Channel 4 is selected as the fixed reference input for the fixed mux port.
            v101=0x00000005,     ///<Channel 5 is selected as the fixed reference input for the fixed mux port.
            v110=0x00000006,     ///<Channel 6 is selected as the fixed reference input for the fixed mux port.
            v111=0x00000007,     ///<Channel 7 is selected as the fixed reference input for the fixed mux port.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,25),Register::ReadWriteAccess,FxmxchVal> fxmxch{}; 
        namespace FxmxchValC{
            constexpr Register::FieldValue<decltype(fxmxch)::Type,FxmxchVal::v000> v000{};
            constexpr Register::FieldValue<decltype(fxmxch)::Type,FxmxchVal::v001> v001{};
            constexpr Register::FieldValue<decltype(fxmxch)::Type,FxmxchVal::v010> v010{};
            constexpr Register::FieldValue<decltype(fxmxch)::Type,FxmxchVal::v011> v011{};
            constexpr Register::FieldValue<decltype(fxmxch)::Type,FxmxchVal::v100> v100{};
            constexpr Register::FieldValue<decltype(fxmxch)::Type,FxmxchVal::v101> v101{};
            constexpr Register::FieldValue<decltype(fxmxch)::Type,FxmxchVal::v110> v110{};
            constexpr Register::FieldValue<decltype(fxmxch)::Type,FxmxchVal::v111> v111{};
        }
        ///Fixed MUX Port
        enum class FxmpVal : unsigned {
            v0=0x00000000,     ///<The Plus port is fixed. Only the inputs to the Minus port are swept in each round.
            v1=0x00000001,     ///<The Minus port is fixed. Only the inputs to the Plus port are swept in each round.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,FxmpVal> fxmp{}; 
        namespace FxmpValC{
            constexpr Register::FieldValue<decltype(fxmp)::Type,FxmpVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fxmp)::Type,FxmpVal::v1> v1{};
        }
        ///Round-Robin interrupt enable
        enum class RrieVal : unsigned {
            v0=0x00000000,     ///<The round-robin interrupt is disabled.
            v1=0x00000001,     ///<The round-robin interrupt is enabled when a comparison result changes from the last sample.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,RrieVal> rrie{}; 
        namespace RrieValC{
            constexpr Register::FieldValue<decltype(rrie)::Type,RrieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rrie)::Type,RrieVal::v1> v1{};
        }
        ///Round-Robin Enable
        enum class RreVal : unsigned {
            v0=0x00000000,     ///<Round-robin operation is disabled.
            v1=0x00000001,     ///<Round-robin operation is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,RreVal> rre{}; 
        namespace RreValC{
            constexpr Register::FieldValue<decltype(rre)::Type,RreVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rre)::Type,RreVal::v1> v1{};
        }
    }
}
