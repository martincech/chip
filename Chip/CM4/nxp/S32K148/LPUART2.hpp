#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//Universal Asynchronous Receiver/Transmitter
    namespace Lpuart2Verid{    ///<Version ID Register
        using Addr = Register::Address<0x4006c000,0x00000000,0x00000000,unsigned>;
        ///Feature Identification Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> feature{}; 
        ///Minor Version Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> minor{}; 
        ///Major Version Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> major{}; 
    }
    namespace Lpuart2Param{    ///<Parameter Register
        using Addr = Register::Address<0x4006c004,0xffff0000,0x00000000,unsigned>;
        ///Transmit FIFO Size
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> txfifo{}; 
        ///Receive FIFO Size
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxfifo{}; 
    }
    namespace Lpuart2Global{    ///<LPUART Global Register
        using Addr = Register::Address<0x4006c008,0xfffffffd,0x00000000,unsigned>;
        ///Software Reset
        enum class RstVal : unsigned {
            v0=0x00000000,     ///<Module is not reset.
            v1=0x00000001,     ///<Module is reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,RstVal> rst{}; 
        namespace RstValC{
            constexpr Register::FieldValue<decltype(rst)::Type,RstVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rst)::Type,RstVal::v1> v1{};
        }
    }
    namespace Lpuart2Pincfg{    ///<LPUART Pin Configuration Register
        using Addr = Register::Address<0x4006c00c,0xfffffffc,0x00000000,unsigned>;
        ///Trigger Select
        enum class TrgselVal : unsigned {
            v00=0x00000000,     ///<Input trigger is disabled.
            v01=0x00000001,     ///<Input trigger is used instead of RXD pin input.
            v10=0x00000002,     ///<Input trigger is used instead of CTS_B pin input.
            v11=0x00000003,     ///<Input trigger is used to modulate the TXD pin output. The TXD pin output (after TXINV configuration) is ANDed with the input trigger.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,0),Register::ReadWriteAccess,TrgselVal> trgsel{}; 
        namespace TrgselValC{
            constexpr Register::FieldValue<decltype(trgsel)::Type,TrgselVal::v00> v00{};
            constexpr Register::FieldValue<decltype(trgsel)::Type,TrgselVal::v01> v01{};
            constexpr Register::FieldValue<decltype(trgsel)::Type,TrgselVal::v10> v10{};
            constexpr Register::FieldValue<decltype(trgsel)::Type,TrgselVal::v11> v11{};
        }
    }
    namespace Lpuart2Baud{    ///<LPUART Baud Rate Register
        using Addr = Register::Address<0x4006c010,0x00400000,0x00000000,unsigned>;
        ///Baud Rate Modulo Divisor.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,0),Register::ReadWriteAccess,unsigned> sbr{}; 
        ///Stop Bit Number Select
        enum class SbnsVal : unsigned {
            v0=0x00000000,     ///<One stop bit.
            v1=0x00000001,     ///<Two stop bits.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,13),Register::ReadWriteAccess,SbnsVal> sbns{}; 
        namespace SbnsValC{
            constexpr Register::FieldValue<decltype(sbns)::Type,SbnsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sbns)::Type,SbnsVal::v1> v1{};
        }
        ///RX Input Active Edge Interrupt Enable
        enum class RxedgieVal : unsigned {
            v0=0x00000000,     ///<Hardware interrupts from LPUART_STAT[RXEDGIF] disabled.
            v1=0x00000001,     ///<Hardware interrupt requested when LPUART_STAT[RXEDGIF] flag is 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,14),Register::ReadWriteAccess,RxedgieVal> rxedgie{}; 
        namespace RxedgieValC{
            constexpr Register::FieldValue<decltype(rxedgie)::Type,RxedgieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rxedgie)::Type,RxedgieVal::v1> v1{};
        }
        ///LIN Break Detect Interrupt Enable
        enum class LbkdieVal : unsigned {
            v0=0x00000000,     ///<Hardware interrupts from LPUART_STAT[LBKDIF] disabled (use polling).
            v1=0x00000001,     ///<Hardware interrupt requested when LPUART_STAT[LBKDIF] flag is 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LbkdieVal> lbkdie{}; 
        namespace LbkdieValC{
            constexpr Register::FieldValue<decltype(lbkdie)::Type,LbkdieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lbkdie)::Type,LbkdieVal::v1> v1{};
        }
        ///Resynchronization Disable
        enum class ResyncdisVal : unsigned {
            v0=0x00000000,     ///<Resynchronization during received data word is supported
            v1=0x00000001,     ///<Resynchronization during received data word is disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,ResyncdisVal> resyncdis{}; 
        namespace ResyncdisValC{
            constexpr Register::FieldValue<decltype(resyncdis)::Type,ResyncdisVal::v0> v0{};
            constexpr Register::FieldValue<decltype(resyncdis)::Type,ResyncdisVal::v1> v1{};
        }
        ///Both Edge Sampling
        enum class BothedgeVal : unsigned {
            v0=0x00000000,     ///<Receiver samples input data using the rising edge of the baud rate clock.
            v1=0x00000001,     ///<Receiver samples input data using the rising and falling edge of the baud rate clock.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,17),Register::ReadWriteAccess,BothedgeVal> bothedge{}; 
        namespace BothedgeValC{
            constexpr Register::FieldValue<decltype(bothedge)::Type,BothedgeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(bothedge)::Type,BothedgeVal::v1> v1{};
        }
        ///Match Configuration
        enum class MatcfgVal : unsigned {
            v00=0x00000000,     ///<Address Match Wakeup
            v01=0x00000001,     ///<Idle Match Wakeup
            v10=0x00000002,     ///<Match On and Match Off
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,18),Register::ReadWriteAccess,MatcfgVal> matcfg{}; 
        namespace MatcfgValC{
            constexpr Register::FieldValue<decltype(matcfg)::Type,MatcfgVal::v00> v00{};
            constexpr Register::FieldValue<decltype(matcfg)::Type,MatcfgVal::v01> v01{};
            constexpr Register::FieldValue<decltype(matcfg)::Type,MatcfgVal::v10> v10{};
        }
        ///Receiver Idle DMA Enable
        enum class RidmaeVal : unsigned {
            v0=0x00000000,     ///<DMA request disabled.
            v1=0x00000001,     ///<DMA request enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,20),Register::ReadWriteAccess,RidmaeVal> ridmae{}; 
        namespace RidmaeValC{
            constexpr Register::FieldValue<decltype(ridmae)::Type,RidmaeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ridmae)::Type,RidmaeVal::v1> v1{};
        }
        ///Receiver Full DMA Enable
        enum class RdmaeVal : unsigned {
            v0=0x00000000,     ///<DMA request disabled.
            v1=0x00000001,     ///<DMA request enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(21,21),Register::ReadWriteAccess,RdmaeVal> rdmae{}; 
        namespace RdmaeValC{
            constexpr Register::FieldValue<decltype(rdmae)::Type,RdmaeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rdmae)::Type,RdmaeVal::v1> v1{};
        }
        ///Transmitter DMA Enable
        enum class TdmaeVal : unsigned {
            v0=0x00000000,     ///<DMA request disabled.
            v1=0x00000001,     ///<DMA request enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,23),Register::ReadWriteAccess,TdmaeVal> tdmae{}; 
        namespace TdmaeValC{
            constexpr Register::FieldValue<decltype(tdmae)::Type,TdmaeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tdmae)::Type,TdmaeVal::v1> v1{};
        }
        ///Oversampling Ratio
        enum class OsrVal : unsigned {
            v00000=0x00000000,     ///<Writing 0 to this field will result in an oversampling ratio of 16
            v00011=0x00000003,     ///<Oversampling ratio of 4, requires BOTHEDGE to be set.
            v00100=0x00000004,     ///<Oversampling ratio of 5, requires BOTHEDGE to be set.
            v00101=0x00000005,     ///<Oversampling ratio of 6, requires BOTHEDGE to be set.
            v00110=0x00000006,     ///<Oversampling ratio of 7, requires BOTHEDGE to be set.
            v00111=0x00000007,     ///<Oversampling ratio of 8.
            v01000=0x00000008,     ///<Oversampling ratio of 9.
            v01001=0x00000009,     ///<Oversampling ratio of 10.
            v01010=0x0000000a,     ///<Oversampling ratio of 11.
            v01011=0x0000000b,     ///<Oversampling ratio of 12.
            v01100=0x0000000c,     ///<Oversampling ratio of 13.
            v01101=0x0000000d,     ///<Oversampling ratio of 14.
            v01110=0x0000000e,     ///<Oversampling ratio of 15.
            v01111=0x0000000f,     ///<Oversampling ratio of 16.
            v10000=0x00000010,     ///<Oversampling ratio of 17.
            v10001=0x00000011,     ///<Oversampling ratio of 18.
            v10010=0x00000012,     ///<Oversampling ratio of 19.
            v10011=0x00000013,     ///<Oversampling ratio of 20.
            v10100=0x00000014,     ///<Oversampling ratio of 21.
            v10101=0x00000015,     ///<Oversampling ratio of 22.
            v10110=0x00000016,     ///<Oversampling ratio of 23.
            v10111=0x00000017,     ///<Oversampling ratio of 24.
            v11000=0x00000018,     ///<Oversampling ratio of 25.
            v11001=0x00000019,     ///<Oversampling ratio of 26.
            v11010=0x0000001a,     ///<Oversampling ratio of 27.
            v11011=0x0000001b,     ///<Oversampling ratio of 28.
            v11100=0x0000001c,     ///<Oversampling ratio of 29.
            v11101=0x0000001d,     ///<Oversampling ratio of 30.
            v11110=0x0000001e,     ///<Oversampling ratio of 31.
            v11111=0x0000001f,     ///<Oversampling ratio of 32.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,24),Register::ReadWriteAccess,OsrVal> osr{}; 
        namespace OsrValC{
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v00000> v00000{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v00011> v00011{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v00100> v00100{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v00101> v00101{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v00110> v00110{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v00111> v00111{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v01000> v01000{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v01001> v01001{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v01010> v01010{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v01011> v01011{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v01100> v01100{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v01101> v01101{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v01110> v01110{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v01111> v01111{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v10000> v10000{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v10001> v10001{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v10010> v10010{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v10011> v10011{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v10100> v10100{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v10101> v10101{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v10110> v10110{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v10111> v10111{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v11000> v11000{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v11001> v11001{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v11010> v11010{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v11011> v11011{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v11100> v11100{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v11101> v11101{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v11110> v11110{};
            constexpr Register::FieldValue<decltype(osr)::Type,OsrVal::v11111> v11111{};
        }
        ///10-bit Mode select
        enum class M10Val : unsigned {
            v0=0x00000000,     ///<Receiver and transmitter use 7-bit to 9-bit data characters.
            v1=0x00000001,     ///<Receiver and transmitter use 10-bit data characters.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,M10Val> m10{}; 
        namespace M10ValC{
            constexpr Register::FieldValue<decltype(m10)::Type,M10Val::v0> v0{};
            constexpr Register::FieldValue<decltype(m10)::Type,M10Val::v1> v1{};
        }
        ///Match Address Mode Enable 2
        enum class Maen2Val : unsigned {
            v0=0x00000000,     ///<Normal operation.
            v1=0x00000001,     ///<Enables automatic address matching or data matching mode for MATCH[MA2].
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,Maen2Val> maen2{}; 
        namespace Maen2ValC{
            constexpr Register::FieldValue<decltype(maen2)::Type,Maen2Val::v0> v0{};
            constexpr Register::FieldValue<decltype(maen2)::Type,Maen2Val::v1> v1{};
        }
        ///Match Address Mode Enable 1
        enum class Maen1Val : unsigned {
            v0=0x00000000,     ///<Normal operation.
            v1=0x00000001,     ///<Enables automatic address matching or data matching mode for MATCH[MA1].
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,Maen1Val> maen1{}; 
        namespace Maen1ValC{
            constexpr Register::FieldValue<decltype(maen1)::Type,Maen1Val::v0> v0{};
            constexpr Register::FieldValue<decltype(maen1)::Type,Maen1Val::v1> v1{};
        }
    }
    namespace Lpuart2Stat{    ///<LPUART Status Register
        using Addr = Register::Address<0x4006c014,0x00003fff,0x00000000,unsigned>;
        ///Match 2 Flag
        enum class Ma2fVal : unsigned {
            v0=0x00000000,     ///<Received data is not equal to MA2
            v1=0x00000001,     ///<Received data is equal to MA2
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,14),Register::ReadWriteAccess,Ma2fVal> ma2f{}; 
        namespace Ma2fValC{
            constexpr Register::FieldValue<decltype(ma2f)::Type,Ma2fVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ma2f)::Type,Ma2fVal::v1> v1{};
        }
        ///Match 1 Flag
        enum class Ma1fVal : unsigned {
            v0=0x00000000,     ///<Received data is not equal to MA1
            v1=0x00000001,     ///<Received data is equal to MA1
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,Ma1fVal> ma1f{}; 
        namespace Ma1fValC{
            constexpr Register::FieldValue<decltype(ma1f)::Type,Ma1fVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ma1f)::Type,Ma1fVal::v1> v1{};
        }
        ///Parity Error Flag
        enum class PfVal : unsigned {
            v0=0x00000000,     ///<No parity error.
            v1=0x00000001,     ///<Parity error.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,PfVal> pf{}; 
        namespace PfValC{
            constexpr Register::FieldValue<decltype(pf)::Type,PfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pf)::Type,PfVal::v1> v1{};
        }
        ///Framing Error Flag
        enum class FeVal : unsigned {
            v0=0x00000000,     ///<No framing error detected. This does not guarantee the framing is correct.
            v1=0x00000001,     ///<Framing error.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,17),Register::ReadWriteAccess,FeVal> fe{}; 
        namespace FeValC{
            constexpr Register::FieldValue<decltype(fe)::Type,FeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fe)::Type,FeVal::v1> v1{};
        }
        ///Noise Flag
        enum class NfVal : unsigned {
            v0=0x00000000,     ///<No noise detected.
            v1=0x00000001,     ///<Noise detected in the received character in LPUART_DATA.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(18,18),Register::ReadWriteAccess,NfVal> nf{}; 
        namespace NfValC{
            constexpr Register::FieldValue<decltype(nf)::Type,NfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(nf)::Type,NfVal::v1> v1{};
        }
        ///Receiver Overrun Flag
        enum class Or_Val : unsigned {
            v0=0x00000000,     ///<No overrun.
            v1=0x00000001,     ///<Receive overrun (new LPUART data lost).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,19),Register::ReadWriteAccess,Or_Val> or_{}; 
        namespace Or_ValC{
            constexpr Register::FieldValue<decltype(or_)::Type,Or_Val::v0> v0{};
            constexpr Register::FieldValue<decltype(or_)::Type,Or_Val::v1> v1{};
        }
        ///Idle Line Flag
        enum class IdleVal : unsigned {
            v0=0x00000000,     ///<No idle line detected.
            v1=0x00000001,     ///<Idle line was detected.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,20),Register::ReadWriteAccess,IdleVal> idle{}; 
        namespace IdleValC{
            constexpr Register::FieldValue<decltype(idle)::Type,IdleVal::v0> v0{};
            constexpr Register::FieldValue<decltype(idle)::Type,IdleVal::v1> v1{};
        }
        ///Receive Data Register Full Flag
        enum class RdrfVal : unsigned {
            v0=0x00000000,     ///<Receive data buffer empty.
            v1=0x00000001,     ///<Receive data buffer full.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(21,21),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RdrfVal> rdrf{}; 
        namespace RdrfValC{
            constexpr Register::FieldValue<decltype(rdrf)::Type,RdrfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rdrf)::Type,RdrfVal::v1> v1{};
        }
        ///Transmission Complete Flag
        enum class TcVal : unsigned {
            v0=0x00000000,     ///<Transmitter active (sending data, a preamble, or a break).
            v1=0x00000001,     ///<Transmitter idle (transmission activity complete).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,22),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,TcVal> tc{}; 
        namespace TcValC{
            constexpr Register::FieldValue<decltype(tc)::Type,TcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tc)::Type,TcVal::v1> v1{};
        }
        ///Transmit Data Register Empty Flag
        enum class TdreVal : unsigned {
            v0=0x00000000,     ///<Transmit data buffer full.
            v1=0x00000001,     ///<Transmit data buffer empty.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,23),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,TdreVal> tdre{}; 
        namespace TdreValC{
            constexpr Register::FieldValue<decltype(tdre)::Type,TdreVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tdre)::Type,TdreVal::v1> v1{};
        }
        ///Receiver Active Flag
        enum class RafVal : unsigned {
            v0=0x00000000,     ///<LPUART receiver idle waiting for a start bit.
            v1=0x00000001,     ///<LPUART receiver active (RXD input not idle).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RafVal> raf{}; 
        namespace RafValC{
            constexpr Register::FieldValue<decltype(raf)::Type,RafVal::v0> v0{};
            constexpr Register::FieldValue<decltype(raf)::Type,RafVal::v1> v1{};
        }
        ///LIN Break Detection Enable
        enum class LbkdeVal : unsigned {
            v0=0x00000000,     ///<LIN break detect is disabled, normal break character can be detected.
            v1=0x00000001,     ///<LIN break detect is enabled. LIN break character is detected at length of 11 bit times (if M = 0) or 12 (if M = 1) or 13 (M10 = 1).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,LbkdeVal> lbkde{}; 
        namespace LbkdeValC{
            constexpr Register::FieldValue<decltype(lbkde)::Type,LbkdeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lbkde)::Type,LbkdeVal::v1> v1{};
        }
        ///Break Character Generation Length
        enum class Brk13Val : unsigned {
            v0=0x00000000,     ///<Break character is transmitted with length of 9 to 13 bit times.
            v1=0x00000001,     ///<Break character is transmitted with length of 12 to 15 bit times.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,Brk13Val> brk13{}; 
        namespace Brk13ValC{
            constexpr Register::FieldValue<decltype(brk13)::Type,Brk13Val::v0> v0{};
            constexpr Register::FieldValue<decltype(brk13)::Type,Brk13Val::v1> v1{};
        }
        ///Receive Wake Up Idle Detect
        enum class RwuidVal : unsigned {
            v0=0x00000000,     ///<During receive standby state (RWU = 1), the IDLE bit does not get set upon detection of an idle character. During address match wakeup, the IDLE bit does not set when an address does not match.
            v1=0x00000001,     ///<During receive standby state (RWU = 1), the IDLE bit gets set upon detection of an idle character. During address match wakeup, the IDLE bit does set when an address does not match.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,RwuidVal> rwuid{}; 
        namespace RwuidValC{
            constexpr Register::FieldValue<decltype(rwuid)::Type,RwuidVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rwuid)::Type,RwuidVal::v1> v1{};
        }
        ///Receive Data Inversion
        enum class RxinvVal : unsigned {
            v0=0x00000000,     ///<Receive data not inverted.
            v1=0x00000001,     ///<Receive data inverted.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,RxinvVal> rxinv{}; 
        namespace RxinvValC{
            constexpr Register::FieldValue<decltype(rxinv)::Type,RxinvVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rxinv)::Type,RxinvVal::v1> v1{};
        }
        ///MSB First
        enum class MsbfVal : unsigned {
            v0=0x00000000,     ///<LSB (bit0) is the first bit that is transmitted following the start bit. Further, the first bit received after the start bit is identified as bit0.
            v1=0x00000001,     ///<MSB (bit9, bit8, bit7 or bit6) is the first bit that is transmitted following the start bit depending on the setting of CTRL[M], CTRL[PE] and BAUD[M10]. Further, the first bit received after the start bit is identified as bit9, bit8, bit7 or bit6 depending on the setting of CTRL[M] and CTRL[PE].
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,MsbfVal> msbf{}; 
        namespace MsbfValC{
            constexpr Register::FieldValue<decltype(msbf)::Type,MsbfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(msbf)::Type,MsbfVal::v1> v1{};
        }
        ///RXD Pin Active Edge Interrupt Flag
        enum class RxedgifVal : unsigned {
            v0=0x00000000,     ///<No active edge on the receive pin has occurred.
            v1=0x00000001,     ///<An active edge on the receive pin has occurred.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,RxedgifVal> rxedgif{}; 
        namespace RxedgifValC{
            constexpr Register::FieldValue<decltype(rxedgif)::Type,RxedgifVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rxedgif)::Type,RxedgifVal::v1> v1{};
        }
        ///LIN Break Detect Interrupt Flag
        enum class LbkdifVal : unsigned {
            v0=0x00000000,     ///<No LIN break character has been detected.
            v1=0x00000001,     ///<LIN break character has been detected.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,LbkdifVal> lbkdif{}; 
        namespace LbkdifValC{
            constexpr Register::FieldValue<decltype(lbkdif)::Type,LbkdifVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lbkdif)::Type,LbkdifVal::v1> v1{};
        }
    }
    namespace Lpuart2Ctrl{    ///<LPUART Control Register
        using Addr = Register::Address<0x4006c018,0x00003000,0x00000000,unsigned>;
        ///Parity Type
        enum class PtVal : unsigned {
            v0=0x00000000,     ///<Even parity.
            v1=0x00000001,     ///<Odd parity.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PtVal> pt{}; 
        namespace PtValC{
            constexpr Register::FieldValue<decltype(pt)::Type,PtVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pt)::Type,PtVal::v1> v1{};
        }
        ///Parity Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<No hardware parity generation or checking.
            v1=0x00000001,     ///<Parity enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Idle Line Type Select
        enum class IltVal : unsigned {
            v0=0x00000000,     ///<Idle character bit count starts after start bit.
            v1=0x00000001,     ///<Idle character bit count starts after stop bit.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,IltVal> ilt{}; 
        namespace IltValC{
            constexpr Register::FieldValue<decltype(ilt)::Type,IltVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ilt)::Type,IltVal::v1> v1{};
        }
        ///Receiver Wakeup Method Select
        enum class WakeVal : unsigned {
            v0=0x00000000,     ///<Configures RWU for idle-line wakeup.
            v1=0x00000001,     ///<Configures RWU with address-mark wakeup.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,WakeVal> wake{}; 
        namespace WakeValC{
            constexpr Register::FieldValue<decltype(wake)::Type,WakeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(wake)::Type,WakeVal::v1> v1{};
        }
        ///9-Bit or 8-Bit Mode Select
        enum class MVal : unsigned {
            v0=0x00000000,     ///<Receiver and transmitter use 8-bit data characters.
            v1=0x00000001,     ///<Receiver and transmitter use 9-bit data characters.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,4),Register::ReadWriteAccess,MVal> m{}; 
        namespace MValC{
            constexpr Register::FieldValue<decltype(m)::Type,MVal::v0> v0{};
            constexpr Register::FieldValue<decltype(m)::Type,MVal::v1> v1{};
        }
        ///Receiver Source Select
        enum class RsrcVal : unsigned {
            v0=0x00000000,     ///<Provided LOOPS is set, RSRC is cleared, selects internal loop back mode and the LPUART does not use the RXD pin.
            v1=0x00000001,     ///<Single-wire LPUART mode where the TXD pin is connected to the transmitter output and receiver input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,RsrcVal> rsrc{}; 
        namespace RsrcValC{
            constexpr Register::FieldValue<decltype(rsrc)::Type,RsrcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rsrc)::Type,RsrcVal::v1> v1{};
        }
        ///Doze Enable
        enum class DozeenVal : unsigned {
            v0=0x00000000,     ///<LPUART is enabled in Doze mode.
            v1=0x00000001,     ///<LPUART is disabled in Doze mode.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,DozeenVal> dozeen{}; 
        namespace DozeenValC{
            constexpr Register::FieldValue<decltype(dozeen)::Type,DozeenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dozeen)::Type,DozeenVal::v1> v1{};
        }
        ///Loop Mode Select
        enum class LoopsVal : unsigned {
            v0=0x00000000,     ///<Normal operation - RXD and TXD use separate pins.
            v1=0x00000001,     ///<Loop mode or single-wire mode where transmitter outputs are internally connected to receiver input (see RSRC bit).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::ReadWriteAccess,LoopsVal> loops{}; 
        namespace LoopsValC{
            constexpr Register::FieldValue<decltype(loops)::Type,LoopsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(loops)::Type,LoopsVal::v1> v1{};
        }
        ///Idle Configuration
        enum class IdlecfgVal : unsigned {
            v000=0x00000000,     ///<1 idle character
            v001=0x00000001,     ///<2 idle characters
            v010=0x00000002,     ///<4 idle characters
            v011=0x00000003,     ///<8 idle characters
            v100=0x00000004,     ///<16 idle characters
            v101=0x00000005,     ///<32 idle characters
            v110=0x00000006,     ///<64 idle characters
            v111=0x00000007,     ///<128 idle characters
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,IdlecfgVal> idlecfg{}; 
        namespace IdlecfgValC{
            constexpr Register::FieldValue<decltype(idlecfg)::Type,IdlecfgVal::v000> v000{};
            constexpr Register::FieldValue<decltype(idlecfg)::Type,IdlecfgVal::v001> v001{};
            constexpr Register::FieldValue<decltype(idlecfg)::Type,IdlecfgVal::v010> v010{};
            constexpr Register::FieldValue<decltype(idlecfg)::Type,IdlecfgVal::v011> v011{};
            constexpr Register::FieldValue<decltype(idlecfg)::Type,IdlecfgVal::v100> v100{};
            constexpr Register::FieldValue<decltype(idlecfg)::Type,IdlecfgVal::v101> v101{};
            constexpr Register::FieldValue<decltype(idlecfg)::Type,IdlecfgVal::v110> v110{};
            constexpr Register::FieldValue<decltype(idlecfg)::Type,IdlecfgVal::v111> v111{};
        }
        ///7-Bit Mode Select
        enum class M7Val : unsigned {
            v0=0x00000000,     ///<Receiver and transmitter use 8-bit to 10-bit data characters.
            v1=0x00000001,     ///<Receiver and transmitter use 7-bit data characters.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,M7Val> m7{}; 
        namespace M7ValC{
            constexpr Register::FieldValue<decltype(m7)::Type,M7Val::v0> v0{};
            constexpr Register::FieldValue<decltype(m7)::Type,M7Val::v1> v1{};
        }
        ///Match 2 Interrupt Enable
        enum class Ma2ieVal : unsigned {
            v0=0x00000000,     ///<MA2F interrupt disabled
            v1=0x00000001,     ///<MA2F interrupt enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,14),Register::ReadWriteAccess,Ma2ieVal> ma2ie{}; 
        namespace Ma2ieValC{
            constexpr Register::FieldValue<decltype(ma2ie)::Type,Ma2ieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ma2ie)::Type,Ma2ieVal::v1> v1{};
        }
        ///Match 1 Interrupt Enable
        enum class Ma1ieVal : unsigned {
            v0=0x00000000,     ///<MA1F interrupt disabled
            v1=0x00000001,     ///<MA1F interrupt enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,Ma1ieVal> ma1ie{}; 
        namespace Ma1ieValC{
            constexpr Register::FieldValue<decltype(ma1ie)::Type,Ma1ieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ma1ie)::Type,Ma1ieVal::v1> v1{};
        }
        ///Send Break
        enum class SbkVal : unsigned {
            v0=0x00000000,     ///<Normal transmitter operation.
            v1=0x00000001,     ///<Queue break character(s) to be sent.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,SbkVal> sbk{}; 
        namespace SbkValC{
            constexpr Register::FieldValue<decltype(sbk)::Type,SbkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sbk)::Type,SbkVal::v1> v1{};
        }
        ///Receiver Wakeup Control
        enum class RwuVal : unsigned {
            v0=0x00000000,     ///<Normal receiver operation.
            v1=0x00000001,     ///<LPUART receiver in standby waiting for wakeup condition.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,17),Register::ReadWriteAccess,RwuVal> rwu{}; 
        namespace RwuValC{
            constexpr Register::FieldValue<decltype(rwu)::Type,RwuVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rwu)::Type,RwuVal::v1> v1{};
        }
        ///Receiver Enable
        enum class ReVal : unsigned {
            v0=0x00000000,     ///<Receiver disabled.
            v1=0x00000001,     ///<Receiver enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(18,18),Register::ReadWriteAccess,ReVal> re{}; 
        namespace ReValC{
            constexpr Register::FieldValue<decltype(re)::Type,ReVal::v0> v0{};
            constexpr Register::FieldValue<decltype(re)::Type,ReVal::v1> v1{};
        }
        ///Transmitter Enable
        enum class TeVal : unsigned {
            v0=0x00000000,     ///<Transmitter disabled.
            v1=0x00000001,     ///<Transmitter enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,19),Register::ReadWriteAccess,TeVal> te{}; 
        namespace TeValC{
            constexpr Register::FieldValue<decltype(te)::Type,TeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(te)::Type,TeVal::v1> v1{};
        }
        ///Idle Line Interrupt Enable
        enum class IlieVal : unsigned {
            v0=0x00000000,     ///<Hardware interrupts from IDLE disabled; use polling.
            v1=0x00000001,     ///<Hardware interrupt requested when IDLE flag is 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,20),Register::ReadWriteAccess,IlieVal> ilie{}; 
        namespace IlieValC{
            constexpr Register::FieldValue<decltype(ilie)::Type,IlieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ilie)::Type,IlieVal::v1> v1{};
        }
        ///Receiver Interrupt Enable
        enum class RieVal : unsigned {
            v0=0x00000000,     ///<Hardware interrupts from RDRF disabled; use polling.
            v1=0x00000001,     ///<Hardware interrupt requested when RDRF flag is 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(21,21),Register::ReadWriteAccess,RieVal> rie{}; 
        namespace RieValC{
            constexpr Register::FieldValue<decltype(rie)::Type,RieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rie)::Type,RieVal::v1> v1{};
        }
        ///Transmission Complete Interrupt Enable for
        enum class TcieVal : unsigned {
            v0=0x00000000,     ///<Hardware interrupts from TC disabled; use polling.
            v1=0x00000001,     ///<Hardware interrupt requested when TC flag is 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,22),Register::ReadWriteAccess,TcieVal> tcie{}; 
        namespace TcieValC{
            constexpr Register::FieldValue<decltype(tcie)::Type,TcieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tcie)::Type,TcieVal::v1> v1{};
        }
        ///Transmit Interrupt Enable
        enum class TieVal : unsigned {
            v0=0x00000000,     ///<Hardware interrupts from TDRE disabled; use polling.
            v1=0x00000001,     ///<Hardware interrupt requested when TDRE flag is 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,23),Register::ReadWriteAccess,TieVal> tie{}; 
        namespace TieValC{
            constexpr Register::FieldValue<decltype(tie)::Type,TieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tie)::Type,TieVal::v1> v1{};
        }
        ///Parity Error Interrupt Enable
        enum class PeieVal : unsigned {
            v0=0x00000000,     ///<PF interrupts disabled; use polling).
            v1=0x00000001,     ///<Hardware interrupt requested when PF is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,PeieVal> peie{}; 
        namespace PeieValC{
            constexpr Register::FieldValue<decltype(peie)::Type,PeieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(peie)::Type,PeieVal::v1> v1{};
        }
        ///Framing Error Interrupt Enable
        enum class FeieVal : unsigned {
            v0=0x00000000,     ///<FE interrupts disabled; use polling.
            v1=0x00000001,     ///<Hardware interrupt requested when FE is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,FeieVal> feie{}; 
        namespace FeieValC{
            constexpr Register::FieldValue<decltype(feie)::Type,FeieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(feie)::Type,FeieVal::v1> v1{};
        }
        ///Noise Error Interrupt Enable
        enum class NeieVal : unsigned {
            v0=0x00000000,     ///<NF interrupts disabled; use polling.
            v1=0x00000001,     ///<Hardware interrupt requested when NF is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,NeieVal> neie{}; 
        namespace NeieValC{
            constexpr Register::FieldValue<decltype(neie)::Type,NeieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(neie)::Type,NeieVal::v1> v1{};
        }
        ///Overrun Interrupt Enable
        enum class OrieVal : unsigned {
            v0=0x00000000,     ///<OR interrupts disabled; use polling.
            v1=0x00000001,     ///<Hardware interrupt requested when OR is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,OrieVal> orie{}; 
        namespace OrieValC{
            constexpr Register::FieldValue<decltype(orie)::Type,OrieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(orie)::Type,OrieVal::v1> v1{};
        }
        ///Transmit Data Inversion
        enum class TxinvVal : unsigned {
            v0=0x00000000,     ///<Transmit data not inverted.
            v1=0x00000001,     ///<Transmit data inverted.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,TxinvVal> txinv{}; 
        namespace TxinvValC{
            constexpr Register::FieldValue<decltype(txinv)::Type,TxinvVal::v0> v0{};
            constexpr Register::FieldValue<decltype(txinv)::Type,TxinvVal::v1> v1{};
        }
        ///TXD Pin Direction in Single-Wire Mode
        enum class TxdirVal : unsigned {
            v0=0x00000000,     ///<TXD pin is an input in single-wire mode.
            v1=0x00000001,     ///<TXD pin is an output in single-wire mode.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,TxdirVal> txdir{}; 
        namespace TxdirValC{
            constexpr Register::FieldValue<decltype(txdir)::Type,TxdirVal::v0> v0{};
            constexpr Register::FieldValue<decltype(txdir)::Type,TxdirVal::v1> v1{};
        }
        ///Receive Bit 9 / Transmit Bit 8
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,unsigned> r9t8{}; 
        ///Receive Bit 8 / Transmit Bit 9
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,unsigned> r8t9{}; 
    }
    namespace Lpuart2Data{    ///<LPUART Data Register
        using Addr = Register::Address<0x4006c01c,0xffff0400,0x00000000,unsigned>;
        ///R0T0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,unsigned> r0t0{}; 
        ///R1T1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,unsigned> r1t1{}; 
        ///R2T2
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,unsigned> r2t2{}; 
        ///R3T3
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,unsigned> r3t3{}; 
        ///R4T4
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,4),Register::ReadWriteAccess,unsigned> r4t4{}; 
        ///R5T5
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,unsigned> r5t5{}; 
        ///R6T6
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,unsigned> r6t6{}; 
        ///R7T7
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::ReadWriteAccess,unsigned> r7t7{}; 
        ///R8T8
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::ReadWriteAccess,unsigned> r8t8{}; 
        ///R9T9
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,9),Register::ReadWriteAccess,unsigned> r9t9{}; 
        ///Idle Line
        enum class IdlineVal : unsigned {
            v0=0x00000000,     ///<Receiver was not idle before receiving this character.
            v1=0x00000001,     ///<Receiver was idle before receiving this character.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,IdlineVal> idline{}; 
        namespace IdlineValC{
            constexpr Register::FieldValue<decltype(idline)::Type,IdlineVal::v0> v0{};
            constexpr Register::FieldValue<decltype(idline)::Type,IdlineVal::v1> v1{};
        }
        ///Receive Buffer Empty
        enum class RxemptVal : unsigned {
            v0=0x00000000,     ///<Receive buffer contains valid data.
            v1=0x00000001,     ///<Receive buffer is empty, data returned on read is not valid.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,12),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RxemptVal> rxempt{}; 
        namespace RxemptValC{
            constexpr Register::FieldValue<decltype(rxempt)::Type,RxemptVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rxempt)::Type,RxemptVal::v1> v1{};
        }
        ///Frame Error / Transmit Special Character
        enum class FretscVal : unsigned {
            v0=0x00000000,     ///<The dataword was received without a frame error on read, or transmit a normal character on write.
            v1=0x00000001,     ///<The dataword was received with a frame error, or transmit an idle or break character on transmit.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,13),Register::ReadWriteAccess,FretscVal> fretsc{}; 
        namespace FretscValC{
            constexpr Register::FieldValue<decltype(fretsc)::Type,FretscVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fretsc)::Type,FretscVal::v1> v1{};
        }
        ///PARITYE
        enum class ParityeVal : unsigned {
            v0=0x00000000,     ///<The dataword was received without a parity error.
            v1=0x00000001,     ///<The dataword was received with a parity error.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,14),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,ParityeVal> paritye{}; 
        namespace ParityeValC{
            constexpr Register::FieldValue<decltype(paritye)::Type,ParityeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(paritye)::Type,ParityeVal::v1> v1{};
        }
        ///NOISY
        enum class NoisyVal : unsigned {
            v0=0x00000000,     ///<The dataword was received without noise.
            v1=0x00000001,     ///<The data was received with noise.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,NoisyVal> noisy{}; 
        namespace NoisyValC{
            constexpr Register::FieldValue<decltype(noisy)::Type,NoisyVal::v0> v0{};
            constexpr Register::FieldValue<decltype(noisy)::Type,NoisyVal::v1> v1{};
        }
    }
    namespace Lpuart2Match{    ///<LPUART Match Address Register
        using Addr = Register::Address<0x4006c020,0xfc00fc00,0x00000000,unsigned>;
        ///Match Address 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,0),Register::ReadWriteAccess,unsigned> ma1{}; 
        ///Match Address 2
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,16),Register::ReadWriteAccess,unsigned> ma2{}; 
    }
    namespace Lpuart2Modir{    ///<LPUART Modem IrDA Register
        using Addr = Register::Address<0x4006c024,0xfff8fcc0,0x00000000,unsigned>;
        ///Transmitter clear-to-send enable
        enum class TxctseVal : unsigned {
            v0=0x00000000,     ///<CTS has no effect on the transmitter.
            v1=0x00000001,     ///<Enables clear-to-send operation. The transmitter checks the state of CTS each time it is ready to send a character. If CTS is asserted, the character is sent. If CTS is deasserted, the signal TXD remains in the mark state and transmission is delayed until CTS is asserted. Changes in CTS as a character is being sent do not affect its transmission.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,TxctseVal> txctse{}; 
        namespace TxctseValC{
            constexpr Register::FieldValue<decltype(txctse)::Type,TxctseVal::v0> v0{};
            constexpr Register::FieldValue<decltype(txctse)::Type,TxctseVal::v1> v1{};
        }
        ///Transmitter request-to-send enable
        enum class TxrtseVal : unsigned {
            v0=0x00000000,     ///<The transmitter has no effect on RTS.
            v1=0x00000001,     ///<When a character is placed into an empty transmitter data buffer , RTS asserts one bit time before the start bit is transmitted. RTS deasserts one bit time after all characters in the transmitter data buffer and shift register are completely sent, including the last stop bit.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,TxrtseVal> txrtse{}; 
        namespace TxrtseValC{
            constexpr Register::FieldValue<decltype(txrtse)::Type,TxrtseVal::v0> v0{};
            constexpr Register::FieldValue<decltype(txrtse)::Type,TxrtseVal::v1> v1{};
        }
        ///Transmitter request-to-send polarity
        enum class TxrtspolVal : unsigned {
            v0=0x00000000,     ///<Transmitter RTS is active low.
            v1=0x00000001,     ///<Transmitter RTS is active high.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,TxrtspolVal> txrtspol{}; 
        namespace TxrtspolValC{
            constexpr Register::FieldValue<decltype(txrtspol)::Type,TxrtspolVal::v0> v0{};
            constexpr Register::FieldValue<decltype(txrtspol)::Type,TxrtspolVal::v1> v1{};
        }
        ///Receiver request-to-send enable
        enum class RxrtseVal : unsigned {
            v0=0x00000000,     ///<The receiver has no effect on RTS.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,RxrtseVal> rxrtse{}; 
        namespace RxrtseValC{
            constexpr Register::FieldValue<decltype(rxrtse)::Type,RxrtseVal::v0> v0{};
        }
        ///Transmit CTS Configuration
        enum class TxctscVal : unsigned {
            v0=0x00000000,     ///<CTS input is sampled at the start of each character.
            v1=0x00000001,     ///<CTS input is sampled when the transmitter is idle.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,4),Register::ReadWriteAccess,TxctscVal> txctsc{}; 
        namespace TxctscValC{
            constexpr Register::FieldValue<decltype(txctsc)::Type,TxctscVal::v0> v0{};
            constexpr Register::FieldValue<decltype(txctsc)::Type,TxctscVal::v1> v1{};
        }
        ///Transmit CTS Source
        enum class TxctssrcVal : unsigned {
            v0=0x00000000,     ///<CTS input is the CTS_B pin.
            v1=0x00000001,     ///<CTS input is the inverted Receiver Match result.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,TxctssrcVal> txctssrc{}; 
        namespace TxctssrcValC{
            constexpr Register::FieldValue<decltype(txctssrc)::Type,TxctssrcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(txctssrc)::Type,TxctssrcVal::v1> v1{};
        }
        ///Receive RTS Configuration
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,unsigned> rtswater{}; 
        ///Transmitter narrow pulse
        enum class TnpVal : unsigned {
            v00=0x00000000,     ///<1/OSR.
            v01=0x00000001,     ///<2/OSR.
            v10=0x00000002,     ///<3/OSR.
            v11=0x00000003,     ///<4/OSR.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,16),Register::ReadWriteAccess,TnpVal> tnp{}; 
        namespace TnpValC{
            constexpr Register::FieldValue<decltype(tnp)::Type,TnpVal::v00> v00{};
            constexpr Register::FieldValue<decltype(tnp)::Type,TnpVal::v01> v01{};
            constexpr Register::FieldValue<decltype(tnp)::Type,TnpVal::v10> v10{};
            constexpr Register::FieldValue<decltype(tnp)::Type,TnpVal::v11> v11{};
        }
        ///Infrared enable
        enum class IrenVal : unsigned {
            v0=0x00000000,     ///<IR disabled.
            v1=0x00000001,     ///<IR enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(18,18),Register::ReadWriteAccess,IrenVal> iren{}; 
        namespace IrenValC{
            constexpr Register::FieldValue<decltype(iren)::Type,IrenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(iren)::Type,IrenVal::v1> v1{};
        }
    }
    namespace Lpuart2Fifo{    ///<LPUART FIFO Register
        using Addr = Register::Address<0x4006c028,0xff3c2000,0x00000000,unsigned>;
        ///Receive FIFO. Buffer Depth
        enum class RxfifosizeVal : unsigned {
            v000=0x00000000,     ///<Receive FIFO/Buffer depth = 1 dataword.
            v001=0x00000001,     ///<Receive FIFO/Buffer depth = 4 datawords.
            v010=0x00000002,     ///<Receive FIFO/Buffer depth = 8 datawords.
            v011=0x00000003,     ///<Receive FIFO/Buffer depth = 16 datawords.
            v100=0x00000004,     ///<Receive FIFO/Buffer depth = 32 datawords.
            v101=0x00000005,     ///<Receive FIFO/Buffer depth = 64 datawords.
            v110=0x00000006,     ///<Receive FIFO/Buffer depth = 128 datawords.
            v111=0x00000007,     ///<Receive FIFO/Buffer depth = 256 datawords.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RxfifosizeVal> rxfifosize{}; 
        namespace RxfifosizeValC{
            constexpr Register::FieldValue<decltype(rxfifosize)::Type,RxfifosizeVal::v000> v000{};
            constexpr Register::FieldValue<decltype(rxfifosize)::Type,RxfifosizeVal::v001> v001{};
            constexpr Register::FieldValue<decltype(rxfifosize)::Type,RxfifosizeVal::v010> v010{};
            constexpr Register::FieldValue<decltype(rxfifosize)::Type,RxfifosizeVal::v011> v011{};
            constexpr Register::FieldValue<decltype(rxfifosize)::Type,RxfifosizeVal::v100> v100{};
            constexpr Register::FieldValue<decltype(rxfifosize)::Type,RxfifosizeVal::v101> v101{};
            constexpr Register::FieldValue<decltype(rxfifosize)::Type,RxfifosizeVal::v110> v110{};
            constexpr Register::FieldValue<decltype(rxfifosize)::Type,RxfifosizeVal::v111> v111{};
        }
        ///Receive FIFO Enable
        enum class RxfeVal : unsigned {
            v0=0x00000000,     ///<Receive FIFO is not enabled. Buffer is depth 1. (Legacy support)
            v1=0x00000001,     ///<Receive FIFO is enabled. Buffer is depth indicted by RXFIFOSIZE.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,RxfeVal> rxfe{}; 
        namespace RxfeValC{
            constexpr Register::FieldValue<decltype(rxfe)::Type,RxfeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rxfe)::Type,RxfeVal::v1> v1{};
        }
        ///Transmit FIFO. Buffer Depth
        enum class TxfifosizeVal : unsigned {
            v000=0x00000000,     ///<Transmit FIFO/Buffer depth = 1 dataword.
            v001=0x00000001,     ///<Transmit FIFO/Buffer depth = 4 datawords.
            v010=0x00000002,     ///<Transmit FIFO/Buffer depth = 8 datawords.
            v011=0x00000003,     ///<Transmit FIFO/Buffer depth = 16 datawords.
            v100=0x00000004,     ///<Transmit FIFO/Buffer depth = 32 datawords.
            v101=0x00000005,     ///<Transmit FIFO/Buffer depth = 64 datawords.
            v110=0x00000006,     ///<Transmit FIFO/Buffer depth = 128 datawords.
            v111=0x00000007,     ///<Transmit FIFO/Buffer depth = 256 datawords
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,4),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,TxfifosizeVal> txfifosize{}; 
        namespace TxfifosizeValC{
            constexpr Register::FieldValue<decltype(txfifosize)::Type,TxfifosizeVal::v000> v000{};
            constexpr Register::FieldValue<decltype(txfifosize)::Type,TxfifosizeVal::v001> v001{};
            constexpr Register::FieldValue<decltype(txfifosize)::Type,TxfifosizeVal::v010> v010{};
            constexpr Register::FieldValue<decltype(txfifosize)::Type,TxfifosizeVal::v011> v011{};
            constexpr Register::FieldValue<decltype(txfifosize)::Type,TxfifosizeVal::v100> v100{};
            constexpr Register::FieldValue<decltype(txfifosize)::Type,TxfifosizeVal::v101> v101{};
            constexpr Register::FieldValue<decltype(txfifosize)::Type,TxfifosizeVal::v110> v110{};
            constexpr Register::FieldValue<decltype(txfifosize)::Type,TxfifosizeVal::v111> v111{};
        }
        ///Transmit FIFO Enable
        enum class TxfeVal : unsigned {
            v0=0x00000000,     ///<Transmit FIFO is not enabled. Buffer is depth 1. (Legacy support).
            v1=0x00000001,     ///<Transmit FIFO is enabled. Buffer is depth indicated by TXFIFOSIZE.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::ReadWriteAccess,TxfeVal> txfe{}; 
        namespace TxfeValC{
            constexpr Register::FieldValue<decltype(txfe)::Type,TxfeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(txfe)::Type,TxfeVal::v1> v1{};
        }
        ///Receive FIFO Underflow Interrupt Enable
        enum class RxufeVal : unsigned {
            v0=0x00000000,     ///<RXUF flag does not generate an interrupt to the host.
            v1=0x00000001,     ///<RXUF flag generates an interrupt to the host.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::ReadWriteAccess,RxufeVal> rxufe{}; 
        namespace RxufeValC{
            constexpr Register::FieldValue<decltype(rxufe)::Type,RxufeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rxufe)::Type,RxufeVal::v1> v1{};
        }
        ///Transmit FIFO Overflow Interrupt Enable
        enum class TxofeVal : unsigned {
            v0=0x00000000,     ///<TXOF flag does not generate an interrupt to the host.
            v1=0x00000001,     ///<TXOF flag generates an interrupt to the host.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,9),Register::ReadWriteAccess,TxofeVal> txofe{}; 
        namespace TxofeValC{
            constexpr Register::FieldValue<decltype(txofe)::Type,TxofeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(txofe)::Type,TxofeVal::v1> v1{};
        }
        ///Receiver Idle Empty Enable
        enum class RxidenVal : unsigned {
            v000=0x00000000,     ///<Disable RDRF assertion due to partially filled FIFO when receiver is idle.
            v001=0x00000001,     ///<Enable RDRF assertion due to partially filled FIFO when receiver is idle for 1 character.
            v010=0x00000002,     ///<Enable RDRF assertion due to partially filled FIFO when receiver is idle for 2 characters.
            v011=0x00000003,     ///<Enable RDRF assertion due to partially filled FIFO when receiver is idle for 4 characters.
            v100=0x00000004,     ///<Enable RDRF assertion due to partially filled FIFO when receiver is idle for 8 characters.
            v101=0x00000005,     ///<Enable RDRF assertion due to partially filled FIFO when receiver is idle for 16 characters.
            v110=0x00000006,     ///<Enable RDRF assertion due to partially filled FIFO when receiver is idle for 32 characters.
            v111=0x00000007,     ///<Enable RDRF assertion due to partially filled FIFO when receiver is idle for 64 characters.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,10),Register::ReadWriteAccess,RxidenVal> rxiden{}; 
        namespace RxidenValC{
            constexpr Register::FieldValue<decltype(rxiden)::Type,RxidenVal::v000> v000{};
            constexpr Register::FieldValue<decltype(rxiden)::Type,RxidenVal::v001> v001{};
            constexpr Register::FieldValue<decltype(rxiden)::Type,RxidenVal::v010> v010{};
            constexpr Register::FieldValue<decltype(rxiden)::Type,RxidenVal::v011> v011{};
            constexpr Register::FieldValue<decltype(rxiden)::Type,RxidenVal::v100> v100{};
            constexpr Register::FieldValue<decltype(rxiden)::Type,RxidenVal::v101> v101{};
            constexpr Register::FieldValue<decltype(rxiden)::Type,RxidenVal::v110> v110{};
            constexpr Register::FieldValue<decltype(rxiden)::Type,RxidenVal::v111> v111{};
        }
        ///Receive FIFO/Buffer Flush
        enum class RxflushVal : unsigned {
            v0=0x00000000,     ///<No flush operation occurs.
            v1=0x00000001,     ///<All data in the receive FIFO/buffer is cleared out.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,14),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RxflushVal> rxflush{}; 
        namespace RxflushValC{
            constexpr Register::FieldValue<decltype(rxflush)::Type,RxflushVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rxflush)::Type,RxflushVal::v1> v1{};
        }
        ///Transmit FIFO/Buffer Flush
        enum class TxflushVal : unsigned {
            v0=0x00000000,     ///<No flush operation occurs.
            v1=0x00000001,     ///<All data in the transmit FIFO/Buffer is cleared out.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,TxflushVal> txflush{}; 
        namespace TxflushValC{
            constexpr Register::FieldValue<decltype(txflush)::Type,TxflushVal::v0> v0{};
            constexpr Register::FieldValue<decltype(txflush)::Type,TxflushVal::v1> v1{};
        }
        ///Receiver Buffer Underflow Flag
        enum class RxufVal : unsigned {
            v0=0x00000000,     ///<No receive buffer underflow has occurred since the last time the flag was cleared.
            v1=0x00000001,     ///<At least one receive buffer underflow has occurred since the last time the flag was cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,RxufVal> rxuf{}; 
        namespace RxufValC{
            constexpr Register::FieldValue<decltype(rxuf)::Type,RxufVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rxuf)::Type,RxufVal::v1> v1{};
        }
        ///Transmitter Buffer Overflow Flag
        enum class TxofVal : unsigned {
            v0=0x00000000,     ///<No transmit buffer overflow has occurred since the last time the flag was cleared.
            v1=0x00000001,     ///<At least one transmit buffer overflow has occurred since the last time the flag was cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,17),Register::ReadWriteAccess,TxofVal> txof{}; 
        namespace TxofValC{
            constexpr Register::FieldValue<decltype(txof)::Type,TxofVal::v0> v0{};
            constexpr Register::FieldValue<decltype(txof)::Type,TxofVal::v1> v1{};
        }
        ///Receive Buffer/FIFO Empty
        enum class RxemptVal : unsigned {
            v0=0x00000000,     ///<Receive buffer is not empty.
            v1=0x00000001,     ///<Receive buffer is empty.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,22),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RxemptVal> rxempt{}; 
        namespace RxemptValC{
            constexpr Register::FieldValue<decltype(rxempt)::Type,RxemptVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rxempt)::Type,RxemptVal::v1> v1{};
        }
        ///Transmit Buffer/FIFO Empty
        enum class TxemptVal : unsigned {
            v0=0x00000000,     ///<Transmit buffer is not empty.
            v1=0x00000001,     ///<Transmit buffer is empty.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,23),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,TxemptVal> txempt{}; 
        namespace TxemptValC{
            constexpr Register::FieldValue<decltype(txempt)::Type,TxemptVal::v0> v0{};
            constexpr Register::FieldValue<decltype(txempt)::Type,TxemptVal::v1> v1{};
        }
    }
    namespace Lpuart2Water{    ///<LPUART Watermark Register
        using Addr = Register::Address<0x4006c02c,0xf8fcf8fc,0x00000000,unsigned>;
        ///Transmit Watermark
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,0),Register::ReadWriteAccess,unsigned> txwater{}; 
        ///Transmit Counter
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> txcount{}; 
        ///Receive Watermark
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,16),Register::ReadWriteAccess,unsigned> rxwater{}; 
        ///Receive Counter
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxcount{}; 
    }
}
