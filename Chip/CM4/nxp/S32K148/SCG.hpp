#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//System Clock Generator
    namespace ScgVerid{    ///<Version ID Register
        using Addr = Register::Address<0x40064000,0x00000000,0x00000000,unsigned>;
        ///SCG Version Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> version{}; 
    }
    namespace ScgParam{    ///<Parameter Register
        using Addr = Register::Address<0x40064004,0x07ffff00,0x00000000,unsigned>;
        ///Clock Present
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> clkpres{}; 
        ///Divider Present
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,27),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> divpres{}; 
    }
    namespace ScgCsr{    ///<Clock Status Register
        using Addr = Register::Address<0x40064010,0xf0f0ff00,0x00000000,unsigned>;
        ///Slow Clock Divide Ratio
        enum class DivslowVal : unsigned {
            v0000=0x00000000,     ///<Divide-by-1
            v0001=0x00000001,     ///<Divide-by-2
            v0010=0x00000002,     ///<Divide-by-3
            v0011=0x00000003,     ///<Divide-by-4
            v0100=0x00000004,     ///<Divide-by-5
            v0101=0x00000005,     ///<Divide-by-6
            v0110=0x00000006,     ///<Divide-by-7
            v0111=0x00000007,     ///<Divide-by-8
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,DivslowVal> divslow{}; 
        namespace DivslowValC{
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0100> v0100{};
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0101> v0101{};
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0110> v0110{};
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0111> v0111{};
        }
        ///Bus Clock Divide Ratio
        enum class DivbusVal : unsigned {
            v0000=0x00000000,     ///<Divide-by-1
            v0001=0x00000001,     ///<Divide-by-2
            v0010=0x00000002,     ///<Divide-by-3
            v0011=0x00000003,     ///<Divide-by-4
            v0100=0x00000004,     ///<Divide-by-5
            v0101=0x00000005,     ///<Divide-by-6
            v0110=0x00000006,     ///<Divide-by-7
            v0111=0x00000007,     ///<Divide-by-8
            v1000=0x00000008,     ///<Divide-by-9
            v1001=0x00000009,     ///<Divide-by-10
            v1010=0x0000000a,     ///<Divide-by-11
            v1011=0x0000000b,     ///<Divide-by-12
            v1100=0x0000000c,     ///<Divide-by-13
            v1101=0x0000000d,     ///<Divide-by-14
            v1110=0x0000000e,     ///<Divide-by-15
            v1111=0x0000000f,     ///<Divide-by-16
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,4),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,DivbusVal> divbus{}; 
        namespace DivbusValC{
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0100> v0100{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0101> v0101{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0110> v0110{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0111> v0111{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1100> v1100{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1101> v1101{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1110> v1110{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1111> v1111{};
        }
        ///Core Clock Divide Ratio
        enum class DivcoreVal : unsigned {
            v0000=0x00000000,     ///<Divide-by-1
            v0001=0x00000001,     ///<Divide-by-2
            v0010=0x00000002,     ///<Divide-by-3
            v0011=0x00000003,     ///<Divide-by-4
            v0100=0x00000004,     ///<Divide-by-5
            v0101=0x00000005,     ///<Divide-by-6
            v0110=0x00000006,     ///<Divide-by-7
            v0111=0x00000007,     ///<Divide-by-8
            v1000=0x00000008,     ///<Divide-by-9
            v1001=0x00000009,     ///<Divide-by-10
            v1010=0x0000000a,     ///<Divide-by-11
            v1011=0x0000000b,     ///<Divide-by-12
            v1100=0x0000000c,     ///<Divide-by-13
            v1101=0x0000000d,     ///<Divide-by-14
            v1110=0x0000000e,     ///<Divide-by-15
            v1111=0x0000000f,     ///<Divide-by-16
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,DivcoreVal> divcore{}; 
        namespace DivcoreValC{
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0100> v0100{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0101> v0101{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0110> v0110{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0111> v0111{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1100> v1100{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1101> v1101{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1110> v1110{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1111> v1111{};
        }
        ///System Clock Source
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> scs{}; 
    }
    namespace ScgRccr{    ///<Run Clock Control Register
        using Addr = Register::Address<0x40064014,0xf0f0ff00,0x00000000,unsigned>;
        ///Slow Clock Divide Ratio
        enum class DivslowVal : unsigned {
            v0000=0x00000000,     ///<Divide-by-1
            v0001=0x00000001,     ///<Divide-by-2
            v0010=0x00000002,     ///<Divide-by-3
            v0011=0x00000003,     ///<Divide-by-4
            v0100=0x00000004,     ///<Divide-by-5
            v0101=0x00000005,     ///<Divide-by-6
            v0110=0x00000006,     ///<Divide-by-7
            v0111=0x00000007,     ///<Divide-by-8
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::ReadWriteAccess,DivslowVal> divslow{}; 
        namespace DivslowValC{
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0100> v0100{};
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0101> v0101{};
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0110> v0110{};
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0111> v0111{};
        }
        ///Bus Clock Divide Ratio
        enum class DivbusVal : unsigned {
            v0000=0x00000000,     ///<Divide-by-1
            v0001=0x00000001,     ///<Divide-by-2
            v0010=0x00000002,     ///<Divide-by-3
            v0011=0x00000003,     ///<Divide-by-4
            v0100=0x00000004,     ///<Divide-by-5
            v0101=0x00000005,     ///<Divide-by-6
            v0110=0x00000006,     ///<Divide-by-7
            v0111=0x00000007,     ///<Divide-by-8
            v1000=0x00000008,     ///<Divide-by-9
            v1001=0x00000009,     ///<Divide-by-10
            v1010=0x0000000a,     ///<Divide-by-11
            v1011=0x0000000b,     ///<Divide-by-12
            v1100=0x0000000c,     ///<Divide-by-13
            v1101=0x0000000d,     ///<Divide-by-14
            v1110=0x0000000e,     ///<Divide-by-15
            v1111=0x0000000f,     ///<Divide-by-16
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,4),Register::ReadWriteAccess,DivbusVal> divbus{}; 
        namespace DivbusValC{
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0100> v0100{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0101> v0101{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0110> v0110{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0111> v0111{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1100> v1100{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1101> v1101{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1110> v1110{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1111> v1111{};
        }
        ///Core Clock Divide Ratio
        enum class DivcoreVal : unsigned {
            v0000=0x00000000,     ///<Divide-by-1
            v0001=0x00000001,     ///<Divide-by-2
            v0010=0x00000002,     ///<Divide-by-3
            v0011=0x00000003,     ///<Divide-by-4
            v0100=0x00000004,     ///<Divide-by-5
            v0101=0x00000005,     ///<Divide-by-6
            v0110=0x00000006,     ///<Divide-by-7
            v0111=0x00000007,     ///<Divide-by-8
            v1000=0x00000008,     ///<Divide-by-9
            v1001=0x00000009,     ///<Divide-by-10
            v1010=0x0000000a,     ///<Divide-by-11
            v1011=0x0000000b,     ///<Divide-by-12
            v1100=0x0000000c,     ///<Divide-by-13
            v1101=0x0000000d,     ///<Divide-by-14
            v1110=0x0000000e,     ///<Divide-by-15
            v1111=0x0000000f,     ///<Divide-by-16
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,DivcoreVal> divcore{}; 
        namespace DivcoreValC{
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0100> v0100{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0101> v0101{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0110> v0110{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0111> v0111{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1100> v1100{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1101> v1101{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1110> v1110{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1111> v1111{};
        }
        ///System Clock Source
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,24),Register::ReadWriteAccess,unsigned> scs{}; 
    }
    namespace ScgVccr{    ///<VLPR Clock Control Register
        using Addr = Register::Address<0x40064018,0xf0f0ff00,0x00000000,unsigned>;
        ///Slow Clock Divide Ratio
        enum class DivslowVal : unsigned {
            v0000=0x00000000,     ///<Divide-by-1
            v0001=0x00000001,     ///<Divide-by-2
            v0010=0x00000002,     ///<Divide-by-3
            v0011=0x00000003,     ///<Divide-by-4
            v0100=0x00000004,     ///<Divide-by-5
            v0101=0x00000005,     ///<Divide-by-6
            v0110=0x00000006,     ///<Divide-by-7
            v0111=0x00000007,     ///<Divide-by-8
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::ReadWriteAccess,DivslowVal> divslow{}; 
        namespace DivslowValC{
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0100> v0100{};
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0101> v0101{};
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0110> v0110{};
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0111> v0111{};
        }
        ///Bus Clock Divide Ratio
        enum class DivbusVal : unsigned {
            v0000=0x00000000,     ///<Divide-by-1
            v0001=0x00000001,     ///<Divide-by-2
            v0010=0x00000002,     ///<Divide-by-3
            v0011=0x00000003,     ///<Divide-by-4
            v0100=0x00000004,     ///<Divide-by-5
            v0101=0x00000005,     ///<Divide-by-6
            v0110=0x00000006,     ///<Divide-by-7
            v0111=0x00000007,     ///<Divide-by-8
            v1000=0x00000008,     ///<Divide-by-9
            v1001=0x00000009,     ///<Divide-by-10
            v1010=0x0000000a,     ///<Divide-by-11
            v1011=0x0000000b,     ///<Divide-by-12
            v1100=0x0000000c,     ///<Divide-by-13
            v1101=0x0000000d,     ///<Divide-by-14
            v1110=0x0000000e,     ///<Divide-by-15
            v1111=0x0000000f,     ///<Divide-by-16
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,4),Register::ReadWriteAccess,DivbusVal> divbus{}; 
        namespace DivbusValC{
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0100> v0100{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0101> v0101{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0110> v0110{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0111> v0111{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1100> v1100{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1101> v1101{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1110> v1110{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1111> v1111{};
        }
        ///Core Clock Divide Ratio
        enum class DivcoreVal : unsigned {
            v0000=0x00000000,     ///<Divide-by-1
            v0001=0x00000001,     ///<Divide-by-2
            v0010=0x00000002,     ///<Divide-by-3
            v0011=0x00000003,     ///<Divide-by-4
            v0100=0x00000004,     ///<Divide-by-5
            v0101=0x00000005,     ///<Divide-by-6
            v0110=0x00000006,     ///<Divide-by-7
            v0111=0x00000007,     ///<Divide-by-8
            v1000=0x00000008,     ///<Divide-by-9
            v1001=0x00000009,     ///<Divide-by-10
            v1010=0x0000000a,     ///<Divide-by-11
            v1011=0x0000000b,     ///<Divide-by-12
            v1100=0x0000000c,     ///<Divide-by-13
            v1101=0x0000000d,     ///<Divide-by-14
            v1110=0x0000000e,     ///<Divide-by-15
            v1111=0x0000000f,     ///<Divide-by-16
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,DivcoreVal> divcore{}; 
        namespace DivcoreValC{
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0100> v0100{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0101> v0101{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0110> v0110{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0111> v0111{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1100> v1100{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1101> v1101{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1110> v1110{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1111> v1111{};
        }
        ///System Clock Source
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,24),Register::ReadWriteAccess,unsigned> scs{}; 
    }
    namespace ScgHccr{    ///<HSRUN Clock Control Register
        using Addr = Register::Address<0x4006401c,0xf0f0ff00,0x00000000,unsigned>;
        ///Slow Clock Divide Ratio
        enum class DivslowVal : unsigned {
            v0000=0x00000000,     ///<Divide-by-1
            v0001=0x00000001,     ///<Divide-by-2
            v0010=0x00000002,     ///<Divide-by-3
            v0011=0x00000003,     ///<Divide-by-4
            v0100=0x00000004,     ///<Divide-by-5
            v0101=0x00000005,     ///<Divide-by-6
            v0110=0x00000006,     ///<Divide-by-7
            v0111=0x00000007,     ///<Divide-by-8
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::ReadWriteAccess,DivslowVal> divslow{}; 
        namespace DivslowValC{
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0100> v0100{};
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0101> v0101{};
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0110> v0110{};
            constexpr Register::FieldValue<decltype(divslow)::Type,DivslowVal::v0111> v0111{};
        }
        ///Bus Clock Divide Ratio
        enum class DivbusVal : unsigned {
            v0000=0x00000000,     ///<Divide-by-1
            v0001=0x00000001,     ///<Divide-by-2
            v0010=0x00000002,     ///<Divide-by-3
            v0011=0x00000003,     ///<Divide-by-4
            v0100=0x00000004,     ///<Divide-by-5
            v0101=0x00000005,     ///<Divide-by-6
            v0110=0x00000006,     ///<Divide-by-7
            v0111=0x00000007,     ///<Divide-by-8
            v1000=0x00000008,     ///<Divide-by-9
            v1001=0x00000009,     ///<Divide-by-10
            v1010=0x0000000a,     ///<Divide-by-11
            v1011=0x0000000b,     ///<Divide-by-12
            v1100=0x0000000c,     ///<Divide-by-13
            v1101=0x0000000d,     ///<Divide-by-14
            v1110=0x0000000e,     ///<Divide-by-15
            v1111=0x0000000f,     ///<Divide-by-16
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,4),Register::ReadWriteAccess,DivbusVal> divbus{}; 
        namespace DivbusValC{
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0100> v0100{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0101> v0101{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0110> v0110{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v0111> v0111{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1100> v1100{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1101> v1101{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1110> v1110{};
            constexpr Register::FieldValue<decltype(divbus)::Type,DivbusVal::v1111> v1111{};
        }
        ///Core Clock Divide Ratio
        enum class DivcoreVal : unsigned {
            v0000=0x00000000,     ///<Divide-by-1
            v0001=0x00000001,     ///<Divide-by-2
            v0010=0x00000002,     ///<Divide-by-3
            v0011=0x00000003,     ///<Divide-by-4
            v0100=0x00000004,     ///<Divide-by-5
            v0101=0x00000005,     ///<Divide-by-6
            v0110=0x00000006,     ///<Divide-by-7
            v0111=0x00000007,     ///<Divide-by-8
            v1000=0x00000008,     ///<Divide-by-9
            v1001=0x00000009,     ///<Divide-by-10
            v1010=0x0000000a,     ///<Divide-by-11
            v1011=0x0000000b,     ///<Divide-by-12
            v1100=0x0000000c,     ///<Divide-by-13
            v1101=0x0000000d,     ///<Divide-by-14
            v1110=0x0000000e,     ///<Divide-by-15
            v1111=0x0000000f,     ///<Divide-by-16
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,DivcoreVal> divcore{}; 
        namespace DivcoreValC{
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0100> v0100{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0101> v0101{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0110> v0110{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v0111> v0111{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1100> v1100{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1101> v1101{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1110> v1110{};
            constexpr Register::FieldValue<decltype(divcore)::Type,DivcoreVal::v1111> v1111{};
        }
        ///System Clock Source
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,24),Register::ReadWriteAccess,unsigned> scs{}; 
    }
    namespace ScgClkoutcnfg{    ///<SCG CLKOUT Configuration Register
        using Addr = Register::Address<0x40064020,0xf0ffffff,0x00000000,unsigned>;
        ///SCG Clkout Select
        enum class ClkoutselVal : unsigned {
            v0000=0x00000000,     ///<SCG SLOW Clock
            v0001=0x00000001,     ///<System OSC (SOSC_CLK)
            v0010=0x00000002,     ///<Slow IRC (SIRC_CLK)
            v0011=0x00000003,     ///<Fast IRC (FIRC_CLK)
            v0110=0x00000006,     ///<System PLL (SPLL_CLK)
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,24),Register::ReadWriteAccess,ClkoutselVal> clkoutsel{}; 
        namespace ClkoutselValC{
            constexpr Register::FieldValue<decltype(clkoutsel)::Type,ClkoutselVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(clkoutsel)::Type,ClkoutselVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(clkoutsel)::Type,ClkoutselVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(clkoutsel)::Type,ClkoutselVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(clkoutsel)::Type,ClkoutselVal::v0110> v0110{};
        }
    }
    namespace ScgSosccsr{    ///<System OSC Control Status Register
        using Addr = Register::Address<0x40064100,0xf87cfffe,0x00000000,unsigned>;
        ///System OSC Enable
        enum class SoscenVal : unsigned {
            v0=0x00000000,     ///<System OSC is disabled
            v1=0x00000001,     ///<System OSC is enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,SoscenVal> soscen{}; 
        namespace SoscenValC{
            constexpr Register::FieldValue<decltype(soscen)::Type,SoscenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(soscen)::Type,SoscenVal::v1> v1{};
        }
        ///System OSC Clock Monitor
        enum class SosccmVal : unsigned {
            v0=0x00000000,     ///<System OSC Clock Monitor is disabled
            v1=0x00000001,     ///<System OSC Clock Monitor is enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,SosccmVal> sosccm{}; 
        namespace SosccmValC{
            constexpr Register::FieldValue<decltype(sosccm)::Type,SosccmVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sosccm)::Type,SosccmVal::v1> v1{};
        }
        ///System OSC Clock Monitor Reset Enable
        enum class SosccmreVal : unsigned {
            v0=0x00000000,     ///<Clock Monitor generates interrupt when error detected
            v1=0x00000001,     ///<Clock Monitor generates reset when error detected
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,17),Register::ReadWriteAccess,SosccmreVal> sosccmre{}; 
        namespace SosccmreValC{
            constexpr Register::FieldValue<decltype(sosccmre)::Type,SosccmreVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sosccmre)::Type,SosccmreVal::v1> v1{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<This Control Status Register can be written.
            v1=0x00000001,     ///<This Control Status Register cannot be written.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,23),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///System OSC Valid
        enum class SoscvldVal : unsigned {
            v0=0x00000000,     ///<System OSC is not enabled or clock is not valid
            v1=0x00000001,     ///<System OSC is enabled and output clock is valid
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,SoscvldVal> soscvld{}; 
        namespace SoscvldValC{
            constexpr Register::FieldValue<decltype(soscvld)::Type,SoscvldVal::v0> v0{};
            constexpr Register::FieldValue<decltype(soscvld)::Type,SoscvldVal::v1> v1{};
        }
        ///System OSC Selected
        enum class SoscselVal : unsigned {
            v0=0x00000000,     ///<System OSC is not the system clock source
            v1=0x00000001,     ///<System OSC is the system clock source
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,SoscselVal> soscsel{}; 
        namespace SoscselValC{
            constexpr Register::FieldValue<decltype(soscsel)::Type,SoscselVal::v0> v0{};
            constexpr Register::FieldValue<decltype(soscsel)::Type,SoscselVal::v1> v1{};
        }
        ///System OSC Clock Error
        enum class SoscerrVal : unsigned {
            v0=0x00000000,     ///<System OSC Clock Monitor is disabled or has not detected an error
            v1=0x00000001,     ///<System OSC Clock Monitor is enabled and detected an error
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,SoscerrVal> soscerr{}; 
        namespace SoscerrValC{
            constexpr Register::FieldValue<decltype(soscerr)::Type,SoscerrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(soscerr)::Type,SoscerrVal::v1> v1{};
        }
    }
    namespace ScgSoscdiv{    ///<System OSC Divide Register
        using Addr = Register::Address<0x40064104,0xfffff8f8,0x00000000,unsigned>;
        ///System OSC Clock Divide 1
        enum class Soscdiv1Val : unsigned {
            v000=0x00000000,     ///<Output disabled
            v001=0x00000001,     ///<Divide by 1
            v010=0x00000002,     ///<Divide by 2
            v011=0x00000003,     ///<Divide by 4
            v100=0x00000004,     ///<Divide by 8
            v101=0x00000005,     ///<Divide by 16
            v110=0x00000006,     ///<Divide by 32
            v111=0x00000007,     ///<Divide by 64
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,Soscdiv1Val> soscdiv1{}; 
        namespace Soscdiv1ValC{
            constexpr Register::FieldValue<decltype(soscdiv1)::Type,Soscdiv1Val::v000> v000{};
            constexpr Register::FieldValue<decltype(soscdiv1)::Type,Soscdiv1Val::v001> v001{};
            constexpr Register::FieldValue<decltype(soscdiv1)::Type,Soscdiv1Val::v010> v010{};
            constexpr Register::FieldValue<decltype(soscdiv1)::Type,Soscdiv1Val::v011> v011{};
            constexpr Register::FieldValue<decltype(soscdiv1)::Type,Soscdiv1Val::v100> v100{};
            constexpr Register::FieldValue<decltype(soscdiv1)::Type,Soscdiv1Val::v101> v101{};
            constexpr Register::FieldValue<decltype(soscdiv1)::Type,Soscdiv1Val::v110> v110{};
            constexpr Register::FieldValue<decltype(soscdiv1)::Type,Soscdiv1Val::v111> v111{};
        }
        ///System OSC Clock Divide 2
        enum class Soscdiv2Val : unsigned {
            v000=0x00000000,     ///<Output disabled
            v001=0x00000001,     ///<Divide by 1
            v010=0x00000002,     ///<Divide by 2
            v011=0x00000003,     ///<Divide by 4
            v100=0x00000004,     ///<Divide by 8
            v101=0x00000005,     ///<Divide by 16
            v110=0x00000006,     ///<Divide by 32
            v111=0x00000007,     ///<Divide by 64
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,Soscdiv2Val> soscdiv2{}; 
        namespace Soscdiv2ValC{
            constexpr Register::FieldValue<decltype(soscdiv2)::Type,Soscdiv2Val::v000> v000{};
            constexpr Register::FieldValue<decltype(soscdiv2)::Type,Soscdiv2Val::v001> v001{};
            constexpr Register::FieldValue<decltype(soscdiv2)::Type,Soscdiv2Val::v010> v010{};
            constexpr Register::FieldValue<decltype(soscdiv2)::Type,Soscdiv2Val::v011> v011{};
            constexpr Register::FieldValue<decltype(soscdiv2)::Type,Soscdiv2Val::v100> v100{};
            constexpr Register::FieldValue<decltype(soscdiv2)::Type,Soscdiv2Val::v101> v101{};
            constexpr Register::FieldValue<decltype(soscdiv2)::Type,Soscdiv2Val::v110> v110{};
            constexpr Register::FieldValue<decltype(soscdiv2)::Type,Soscdiv2Val::v111> v111{};
        }
    }
    namespace ScgSosccfg{    ///<System Oscillator Configuration Register
        using Addr = Register::Address<0x40064108,0xffffffc3,0x00000000,unsigned>;
        ///External Reference Select
        enum class ErefsVal : unsigned {
            v0=0x00000000,     ///<External reference clock selected
            v1=0x00000001,     ///<Internal crystal oscillator of OSC selected.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,ErefsVal> erefs{}; 
        namespace ErefsValC{
            constexpr Register::FieldValue<decltype(erefs)::Type,ErefsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(erefs)::Type,ErefsVal::v1> v1{};
        }
        ///High Gain Oscillator Select
        enum class HgoVal : unsigned {
            v0=0x00000000,     ///<Configure crystal oscillator for low-gain operation
            v1=0x00000001,     ///<Configure crystal oscillator for high-gain operation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,HgoVal> hgo{}; 
        namespace HgoValC{
            constexpr Register::FieldValue<decltype(hgo)::Type,HgoVal::v0> v0{};
            constexpr Register::FieldValue<decltype(hgo)::Type,HgoVal::v1> v1{};
        }
        ///System OSC Range Select
        enum class RangeVal : unsigned {
            v01=0x00000001,     ///<Low frequency range selected for the crystal oscillator
            v10=0x00000002,     ///<Medium frequency range selected for the crytstal oscillator
            v11=0x00000003,     ///<High frequency range selected for the crystal oscillator
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,4),Register::ReadWriteAccess,RangeVal> range{}; 
        namespace RangeValC{
            constexpr Register::FieldValue<decltype(range)::Type,RangeVal::v01> v01{};
            constexpr Register::FieldValue<decltype(range)::Type,RangeVal::v10> v10{};
            constexpr Register::FieldValue<decltype(range)::Type,RangeVal::v11> v11{};
        }
    }
    namespace ScgSirccsr{    ///<Slow IRC Control Status Register
        using Addr = Register::Address<0x40064200,0xfc7ffff8,0x00000000,unsigned>;
        ///Slow IRC Enable
        enum class SircenVal : unsigned {
            v0=0x00000000,     ///<Slow IRC is disabled
            v1=0x00000001,     ///<Slow IRC is enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,SircenVal> sircen{}; 
        namespace SircenValC{
            constexpr Register::FieldValue<decltype(sircen)::Type,SircenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sircen)::Type,SircenVal::v1> v1{};
        }
        ///Slow IRC Stop Enable
        enum class SircstenVal : unsigned {
            v0=0x00000000,     ///<Slow IRC is disabled in supported Stop modes
            v1=0x00000001,     ///<Slow IRC is enabled in supported Stop modes
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,SircstenVal> sircsten{}; 
        namespace SircstenValC{
            constexpr Register::FieldValue<decltype(sircsten)::Type,SircstenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sircsten)::Type,SircstenVal::v1> v1{};
        }
        ///Slow IRC Low Power Enable
        enum class SirclpenVal : unsigned {
            v0=0x00000000,     ///<Slow IRC is disabled in VLP modes
            v1=0x00000001,     ///<Slow IRC is enabled in VLP modes
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,SirclpenVal> sirclpen{}; 
        namespace SirclpenValC{
            constexpr Register::FieldValue<decltype(sirclpen)::Type,SirclpenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sirclpen)::Type,SirclpenVal::v1> v1{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Control Status Register can be written.
            v1=0x00000001,     ///<Control Status Register cannot be written.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,23),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Slow IRC Valid
        enum class SircvldVal : unsigned {
            v0=0x00000000,     ///<Slow IRC is not enabled or clock is not valid
            v1=0x00000001,     ///<Slow IRC is enabled and output clock is valid
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,SircvldVal> sircvld{}; 
        namespace SircvldValC{
            constexpr Register::FieldValue<decltype(sircvld)::Type,SircvldVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sircvld)::Type,SircvldVal::v1> v1{};
        }
        ///Slow IRC Selected
        enum class SircselVal : unsigned {
            v0=0x00000000,     ///<Slow IRC is not the system clock source
            v1=0x00000001,     ///<Slow IRC is the system clock source
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,SircselVal> sircsel{}; 
        namespace SircselValC{
            constexpr Register::FieldValue<decltype(sircsel)::Type,SircselVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sircsel)::Type,SircselVal::v1> v1{};
        }
    }
    namespace ScgSircdiv{    ///<Slow IRC Divide Register
        using Addr = Register::Address<0x40064204,0xfffff8f8,0x00000000,unsigned>;
        ///Slow IRC Clock Divide 1
        enum class Sircdiv1Val : unsigned {
            v000=0x00000000,     ///<Output disabled
            v001=0x00000001,     ///<Divide by 1
            v010=0x00000002,     ///<Divide by 2
            v011=0x00000003,     ///<Divide by 4
            v100=0x00000004,     ///<Divide by 8
            v101=0x00000005,     ///<Divide by 16
            v110=0x00000006,     ///<Divide by 32
            v111=0x00000007,     ///<Divide by 64
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,Sircdiv1Val> sircdiv1{}; 
        namespace Sircdiv1ValC{
            constexpr Register::FieldValue<decltype(sircdiv1)::Type,Sircdiv1Val::v000> v000{};
            constexpr Register::FieldValue<decltype(sircdiv1)::Type,Sircdiv1Val::v001> v001{};
            constexpr Register::FieldValue<decltype(sircdiv1)::Type,Sircdiv1Val::v010> v010{};
            constexpr Register::FieldValue<decltype(sircdiv1)::Type,Sircdiv1Val::v011> v011{};
            constexpr Register::FieldValue<decltype(sircdiv1)::Type,Sircdiv1Val::v100> v100{};
            constexpr Register::FieldValue<decltype(sircdiv1)::Type,Sircdiv1Val::v101> v101{};
            constexpr Register::FieldValue<decltype(sircdiv1)::Type,Sircdiv1Val::v110> v110{};
            constexpr Register::FieldValue<decltype(sircdiv1)::Type,Sircdiv1Val::v111> v111{};
        }
        ///Slow IRC Clock Divide 2
        enum class Sircdiv2Val : unsigned {
            v000=0x00000000,     ///<Output disabled
            v001=0x00000001,     ///<Divide by 1
            v010=0x00000002,     ///<Divide by 2
            v011=0x00000003,     ///<Divide by 4
            v100=0x00000004,     ///<Divide by 8
            v101=0x00000005,     ///<Divide by 16
            v110=0x00000006,     ///<Divide by 32
            v111=0x00000007,     ///<Divide by 64
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,Sircdiv2Val> sircdiv2{}; 
        namespace Sircdiv2ValC{
            constexpr Register::FieldValue<decltype(sircdiv2)::Type,Sircdiv2Val::v000> v000{};
            constexpr Register::FieldValue<decltype(sircdiv2)::Type,Sircdiv2Val::v001> v001{};
            constexpr Register::FieldValue<decltype(sircdiv2)::Type,Sircdiv2Val::v010> v010{};
            constexpr Register::FieldValue<decltype(sircdiv2)::Type,Sircdiv2Val::v011> v011{};
            constexpr Register::FieldValue<decltype(sircdiv2)::Type,Sircdiv2Val::v100> v100{};
            constexpr Register::FieldValue<decltype(sircdiv2)::Type,Sircdiv2Val::v101> v101{};
            constexpr Register::FieldValue<decltype(sircdiv2)::Type,Sircdiv2Val::v110> v110{};
            constexpr Register::FieldValue<decltype(sircdiv2)::Type,Sircdiv2Val::v111> v111{};
        }
    }
    namespace ScgSirccfg{    ///<Slow IRC Configuration Register
        using Addr = Register::Address<0x40064208,0xfffffffe,0x00000000,unsigned>;
        ///Frequency Range
        enum class RangeVal : unsigned {
            v0=0x00000000,     ///<Slow IRC low range clock (2 MHz)
            v1=0x00000001,     ///<Slow IRC high range clock (8 MHz )
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,RangeVal> range{}; 
        namespace RangeValC{
            constexpr Register::FieldValue<decltype(range)::Type,RangeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(range)::Type,RangeVal::v1> v1{};
        }
    }
    namespace ScgFirccsr{    ///<Fast IRC Control Status Register
        using Addr = Register::Address<0x40064300,0xf87ffff6,0x00000000,unsigned>;
        ///Fast IRC Enable
        enum class FircenVal : unsigned {
            v0=0x00000000,     ///<Fast IRC is disabled
            v1=0x00000001,     ///<Fast IRC is enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,FircenVal> fircen{}; 
        namespace FircenValC{
            constexpr Register::FieldValue<decltype(fircen)::Type,FircenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fircen)::Type,FircenVal::v1> v1{};
        }
        ///Fast IRC Regulator Enable
        enum class FircregoffVal : unsigned {
            v0=0x00000000,     ///<Fast IRC Regulator is enabled.
            v1=0x00000001,     ///<Fast IRC Regulator is disabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,FircregoffVal> fircregoff{}; 
        namespace FircregoffValC{
            constexpr Register::FieldValue<decltype(fircregoff)::Type,FircregoffVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fircregoff)::Type,FircregoffVal::v1> v1{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Control Status Register can be written.
            v1=0x00000001,     ///<Control Status Register cannot be written.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,23),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Fast IRC Valid status
        enum class FircvldVal : unsigned {
            v0=0x00000000,     ///<Fast IRC is not enabled or clock is not valid.
            v1=0x00000001,     ///<Fast IRC is enabled and output clock is valid. The clock is valid once there is an output clock from the FIRC analog.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,FircvldVal> fircvld{}; 
        namespace FircvldValC{
            constexpr Register::FieldValue<decltype(fircvld)::Type,FircvldVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fircvld)::Type,FircvldVal::v1> v1{};
        }
        ///Fast IRC Selected status
        enum class FircselVal : unsigned {
            v0=0x00000000,     ///<Fast IRC is not the system clock source
            v1=0x00000001,     ///<Fast IRC is the system clock source
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,FircselVal> fircsel{}; 
        namespace FircselValC{
            constexpr Register::FieldValue<decltype(fircsel)::Type,FircselVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fircsel)::Type,FircselVal::v1> v1{};
        }
        ///Fast IRC Clock Error
        enum class FircerrVal : unsigned {
            v0=0x00000000,     ///<Error not detected with the Fast IRC trimming.
            v1=0x00000001,     ///<Error detected with the Fast IRC trimming.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,FircerrVal> fircerr{}; 
        namespace FircerrValC{
            constexpr Register::FieldValue<decltype(fircerr)::Type,FircerrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fircerr)::Type,FircerrVal::v1> v1{};
        }
    }
    namespace ScgFircdiv{    ///<Fast IRC Divide Register
        using Addr = Register::Address<0x40064304,0xfffff8f8,0x00000000,unsigned>;
        ///Fast IRC Clock Divide 1
        enum class Fircdiv1Val : unsigned {
            v000=0x00000000,     ///<Output disabled
            v001=0x00000001,     ///<Divide by 1
            v010=0x00000002,     ///<Divide by 2
            v011=0x00000003,     ///<Divide by 4
            v100=0x00000004,     ///<Divide by 8
            v101=0x00000005,     ///<Divide by 16
            v110=0x00000006,     ///<Divide by 32
            v111=0x00000007,     ///<Divide by 64
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,Fircdiv1Val> fircdiv1{}; 
        namespace Fircdiv1ValC{
            constexpr Register::FieldValue<decltype(fircdiv1)::Type,Fircdiv1Val::v000> v000{};
            constexpr Register::FieldValue<decltype(fircdiv1)::Type,Fircdiv1Val::v001> v001{};
            constexpr Register::FieldValue<decltype(fircdiv1)::Type,Fircdiv1Val::v010> v010{};
            constexpr Register::FieldValue<decltype(fircdiv1)::Type,Fircdiv1Val::v011> v011{};
            constexpr Register::FieldValue<decltype(fircdiv1)::Type,Fircdiv1Val::v100> v100{};
            constexpr Register::FieldValue<decltype(fircdiv1)::Type,Fircdiv1Val::v101> v101{};
            constexpr Register::FieldValue<decltype(fircdiv1)::Type,Fircdiv1Val::v110> v110{};
            constexpr Register::FieldValue<decltype(fircdiv1)::Type,Fircdiv1Val::v111> v111{};
        }
        ///Fast IRC Clock Divide 2
        enum class Fircdiv2Val : unsigned {
            v000=0x00000000,     ///<Output disabled
            v001=0x00000001,     ///<Divide by 1
            v010=0x00000002,     ///<Divide by 2
            v011=0x00000003,     ///<Divide by 4
            v100=0x00000004,     ///<Divide by 8
            v101=0x00000005,     ///<Divide by 16
            v110=0x00000006,     ///<Divide by 32
            v111=0x00000007,     ///<Divide by 64
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,Fircdiv2Val> fircdiv2{}; 
        namespace Fircdiv2ValC{
            constexpr Register::FieldValue<decltype(fircdiv2)::Type,Fircdiv2Val::v000> v000{};
            constexpr Register::FieldValue<decltype(fircdiv2)::Type,Fircdiv2Val::v001> v001{};
            constexpr Register::FieldValue<decltype(fircdiv2)::Type,Fircdiv2Val::v010> v010{};
            constexpr Register::FieldValue<decltype(fircdiv2)::Type,Fircdiv2Val::v011> v011{};
            constexpr Register::FieldValue<decltype(fircdiv2)::Type,Fircdiv2Val::v100> v100{};
            constexpr Register::FieldValue<decltype(fircdiv2)::Type,Fircdiv2Val::v101> v101{};
            constexpr Register::FieldValue<decltype(fircdiv2)::Type,Fircdiv2Val::v110> v110{};
            constexpr Register::FieldValue<decltype(fircdiv2)::Type,Fircdiv2Val::v111> v111{};
        }
    }
    namespace ScgFirccfg{    ///<Fast IRC Configuration Register
        using Addr = Register::Address<0x40064308,0xfffffffc,0x00000000,unsigned>;
        ///Frequency Range
        enum class RangeVal : unsigned {
            v00=0x00000000,     ///<Fast IRC is trimmed to 48 MHz
            v01=0x00000001,     ///<Fast IRC is trimmed to 52 MHz
            v10=0x00000002,     ///<Fast IRC is trimmed to 56 MHz
            v11=0x00000003,     ///<Fast IRC is trimmed to 60 MHz
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,0),Register::ReadWriteAccess,RangeVal> range{}; 
        namespace RangeValC{
            constexpr Register::FieldValue<decltype(range)::Type,RangeVal::v00> v00{};
            constexpr Register::FieldValue<decltype(range)::Type,RangeVal::v01> v01{};
            constexpr Register::FieldValue<decltype(range)::Type,RangeVal::v10> v10{};
            constexpr Register::FieldValue<decltype(range)::Type,RangeVal::v11> v11{};
        }
    }
    namespace ScgSpllcsr{    ///<System PLL Control Status Register
        using Addr = Register::Address<0x40064600,0xf87cfffe,0x00000000,unsigned>;
        ///System PLL Enable
        enum class SpllenVal : unsigned {
            v0=0x00000000,     ///<System PLL is disabled
            v1=0x00000001,     ///<System PLL is enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,SpllenVal> spllen{}; 
        namespace SpllenValC{
            constexpr Register::FieldValue<decltype(spllen)::Type,SpllenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(spllen)::Type,SpllenVal::v1> v1{};
        }
        ///System PLL Clock Monitor
        enum class SpllcmVal : unsigned {
            v0=0x00000000,     ///<System PLL Clock Monitor is disabled
            v1=0x00000001,     ///<System PLL Clock Monitor is enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,SpllcmVal> spllcm{}; 
        namespace SpllcmValC{
            constexpr Register::FieldValue<decltype(spllcm)::Type,SpllcmVal::v0> v0{};
            constexpr Register::FieldValue<decltype(spllcm)::Type,SpllcmVal::v1> v1{};
        }
        ///System PLL Clock Monitor Reset Enable
        enum class SpllcmreVal : unsigned {
            v0=0x00000000,     ///<Clock Monitor generates interrupt when error detected
            v1=0x00000001,     ///<Clock Monitor generates reset when error detected
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,17),Register::ReadWriteAccess,SpllcmreVal> spllcmre{}; 
        namespace SpllcmreValC{
            constexpr Register::FieldValue<decltype(spllcmre)::Type,SpllcmreVal::v0> v0{};
            constexpr Register::FieldValue<decltype(spllcmre)::Type,SpllcmreVal::v1> v1{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Control Status Register can be written.
            v1=0x00000001,     ///<Control Status Register cannot be written.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,23),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///System PLL Valid
        enum class SpllvldVal : unsigned {
            v0=0x00000000,     ///<System PLL is not enabled or clock is not valid
            v1=0x00000001,     ///<System PLL is enabled and output clock is valid
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,SpllvldVal> spllvld{}; 
        namespace SpllvldValC{
            constexpr Register::FieldValue<decltype(spllvld)::Type,SpllvldVal::v0> v0{};
            constexpr Register::FieldValue<decltype(spllvld)::Type,SpllvldVal::v1> v1{};
        }
        ///System PLL Selected
        enum class SpllselVal : unsigned {
            v0=0x00000000,     ///<System PLL is not the system clock source
            v1=0x00000001,     ///<System PLL is the system clock source
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,SpllselVal> spllsel{}; 
        namespace SpllselValC{
            constexpr Register::FieldValue<decltype(spllsel)::Type,SpllselVal::v0> v0{};
            constexpr Register::FieldValue<decltype(spllsel)::Type,SpllselVal::v1> v1{};
        }
        ///System PLL Clock Error
        enum class SpllerrVal : unsigned {
            v0=0x00000000,     ///<System PLL Clock Monitor is disabled or has not detected an error
            v1=0x00000001,     ///<System PLL Clock Monitor is enabled and detected an error. System PLL Clock Error flag will not set when System OSC is selected as its source and SOSCERR has set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,SpllerrVal> spllerr{}; 
        namespace SpllerrValC{
            constexpr Register::FieldValue<decltype(spllerr)::Type,SpllerrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(spllerr)::Type,SpllerrVal::v1> v1{};
        }
    }
    namespace ScgSplldiv{    ///<System PLL Divide Register
        using Addr = Register::Address<0x40064604,0xfffff8f8,0x00000000,unsigned>;
        ///System PLL Clock Divide 1
        enum class Splldiv1Val : unsigned {
            v000=0x00000000,     ///<Clock disabled
            v001=0x00000001,     ///<Divide by 1
            v010=0x00000002,     ///<Divide by 2
            v011=0x00000003,     ///<Divide by 4
            v100=0x00000004,     ///<Divide by 8
            v101=0x00000005,     ///<Divide by 16
            v110=0x00000006,     ///<Divide by 32
            v111=0x00000007,     ///<Divide by 64
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,Splldiv1Val> splldiv1{}; 
        namespace Splldiv1ValC{
            constexpr Register::FieldValue<decltype(splldiv1)::Type,Splldiv1Val::v000> v000{};
            constexpr Register::FieldValue<decltype(splldiv1)::Type,Splldiv1Val::v001> v001{};
            constexpr Register::FieldValue<decltype(splldiv1)::Type,Splldiv1Val::v010> v010{};
            constexpr Register::FieldValue<decltype(splldiv1)::Type,Splldiv1Val::v011> v011{};
            constexpr Register::FieldValue<decltype(splldiv1)::Type,Splldiv1Val::v100> v100{};
            constexpr Register::FieldValue<decltype(splldiv1)::Type,Splldiv1Val::v101> v101{};
            constexpr Register::FieldValue<decltype(splldiv1)::Type,Splldiv1Val::v110> v110{};
            constexpr Register::FieldValue<decltype(splldiv1)::Type,Splldiv1Val::v111> v111{};
        }
        ///System PLL Clock Divide 2
        enum class Splldiv2Val : unsigned {
            v000=0x00000000,     ///<Clock disabled
            v001=0x00000001,     ///<Divide by 1
            v010=0x00000002,     ///<Divide by 2
            v011=0x00000003,     ///<Divide by 4
            v100=0x00000004,     ///<Divide by 8
            v101=0x00000005,     ///<Divide by 16
            v110=0x00000006,     ///<Divide by 32
            v111=0x00000007,     ///<Divide by 64
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,Splldiv2Val> splldiv2{}; 
        namespace Splldiv2ValC{
            constexpr Register::FieldValue<decltype(splldiv2)::Type,Splldiv2Val::v000> v000{};
            constexpr Register::FieldValue<decltype(splldiv2)::Type,Splldiv2Val::v001> v001{};
            constexpr Register::FieldValue<decltype(splldiv2)::Type,Splldiv2Val::v010> v010{};
            constexpr Register::FieldValue<decltype(splldiv2)::Type,Splldiv2Val::v011> v011{};
            constexpr Register::FieldValue<decltype(splldiv2)::Type,Splldiv2Val::v100> v100{};
            constexpr Register::FieldValue<decltype(splldiv2)::Type,Splldiv2Val::v101> v101{};
            constexpr Register::FieldValue<decltype(splldiv2)::Type,Splldiv2Val::v110> v110{};
            constexpr Register::FieldValue<decltype(splldiv2)::Type,Splldiv2Val::v111> v111{};
        }
    }
    namespace ScgSpllcfg{    ///<System PLL Configuration Register
        using Addr = Register::Address<0x40064608,0xffe0f8ff,0x00000000,unsigned>;
        ///PLL Reference Clock Divider
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,unsigned> prediv{}; 
        ///System PLL Multiplier
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,16),Register::ReadWriteAccess,unsigned> mult{}; 
    }
}
