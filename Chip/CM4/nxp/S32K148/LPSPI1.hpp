#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//The LPSPI Memory Map/Register Definition can be found here.
    namespace Lpspi1Verid{    ///<Version ID Register
        using Addr = Register::Address<0x4002d000,0x00000000,0x00000000,unsigned>;
        ///Module Identification Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> feature{}; 
        ///Minor Version Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> minor{}; 
        ///Major Version Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> major{}; 
    }
    namespace Lpspi1Param{    ///<Parameter Register
        using Addr = Register::Address<0x4002d004,0xffff0000,0x00000000,unsigned>;
        ///Transmit FIFO Size
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> txfifo{}; 
        ///Receive FIFO Size
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxfifo{}; 
    }
    namespace Lpspi1Cr{    ///<Control Register
        using Addr = Register::Address<0x4002d010,0xfffffcf0,0x00000000,unsigned>;
        ///Module Enable
        enum class MenVal : unsigned {
            v0=0x00000000,     ///<Module is disabled.
            v1=0x00000001,     ///<Module is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,MenVal> men{}; 
        namespace MenValC{
            constexpr Register::FieldValue<decltype(men)::Type,MenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(men)::Type,MenVal::v1> v1{};
        }
        ///Software Reset
        enum class RstVal : unsigned {
            v0=0x00000000,     ///<Master logic is not reset.
            v1=0x00000001,     ///<Master logic is reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,RstVal> rst{}; 
        namespace RstValC{
            constexpr Register::FieldValue<decltype(rst)::Type,RstVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rst)::Type,RstVal::v1> v1{};
        }
        ///Doze mode enable
        enum class DozenVal : unsigned {
            v0=0x00000000,     ///<Module is enabled in Doze mode.
            v1=0x00000001,     ///<Module is disabled in Doze mode.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,DozenVal> dozen{}; 
        namespace DozenValC{
            constexpr Register::FieldValue<decltype(dozen)::Type,DozenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dozen)::Type,DozenVal::v1> v1{};
        }
        ///Debug Enable
        enum class DbgenVal : unsigned {
            v0=0x00000000,     ///<Module is disabled in debug mode.
            v1=0x00000001,     ///<Module is enabled in debug mode.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,DbgenVal> dbgen{}; 
        namespace DbgenValC{
            constexpr Register::FieldValue<decltype(dbgen)::Type,DbgenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dbgen)::Type,DbgenVal::v1> v1{};
        }
        ///Reset Transmit FIFO
        enum class RtfVal : unsigned {
            v0=0x00000000,     ///<No effect.
            v1=0x00000001,     ///<Transmit FIFO is reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RtfVal> rtf{}; 
        namespace RtfValC{
            constexpr Register::FieldValue<decltype(rtf)::Type,RtfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rtf)::Type,RtfVal::v1> v1{};
        }
        ///Reset Receive FIFO
        enum class RrfVal : unsigned {
            v0=0x00000000,     ///<No effect.
            v1=0x00000001,     ///<Receive FIFO is reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,9),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RrfVal> rrf{}; 
        namespace RrfValC{
            constexpr Register::FieldValue<decltype(rrf)::Type,RrfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rrf)::Type,RrfVal::v1> v1{};
        }
    }
    namespace Lpspi1Sr{    ///<Status Register
        using Addr = Register::Address<0x4002d014,0xfeffc0fc,0x00000000,unsigned>;
        ///Transmit Data Flag
        enum class TdfVal : unsigned {
            v0=0x00000000,     ///<Transmit data not requested.
            v1=0x00000001,     ///<Transmit data is requested.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,TdfVal> tdf{}; 
        namespace TdfValC{
            constexpr Register::FieldValue<decltype(tdf)::Type,TdfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tdf)::Type,TdfVal::v1> v1{};
        }
        ///Receive Data Flag
        enum class RdfVal : unsigned {
            v0=0x00000000,     ///<Receive Data is not ready.
            v1=0x00000001,     ///<Receive data is ready.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RdfVal> rdf{}; 
        namespace RdfValC{
            constexpr Register::FieldValue<decltype(rdf)::Type,RdfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rdf)::Type,RdfVal::v1> v1{};
        }
        ///Word Complete Flag
        enum class WcfVal : unsigned {
            v0=0x00000000,     ///<Transfer word not completed.
            v1=0x00000001,     ///<Transfer word completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::ReadWriteAccess,WcfVal> wcf{}; 
        namespace WcfValC{
            constexpr Register::FieldValue<decltype(wcf)::Type,WcfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(wcf)::Type,WcfVal::v1> v1{};
        }
        ///Frame Complete Flag
        enum class FcfVal : unsigned {
            v0=0x00000000,     ///<Frame transfer has not completed.
            v1=0x00000001,     ///<Frame transfer has completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,9),Register::ReadWriteAccess,FcfVal> fcf{}; 
        namespace FcfValC{
            constexpr Register::FieldValue<decltype(fcf)::Type,FcfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fcf)::Type,FcfVal::v1> v1{};
        }
        ///Transfer Complete Flag
        enum class TcfVal : unsigned {
            v0=0x00000000,     ///<All transfers have not completed.
            v1=0x00000001,     ///<All transfers have completed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,10),Register::ReadWriteAccess,TcfVal> tcf{}; 
        namespace TcfValC{
            constexpr Register::FieldValue<decltype(tcf)::Type,TcfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tcf)::Type,TcfVal::v1> v1{};
        }
        ///Transmit Error Flag
        enum class TefVal : unsigned {
            v0=0x00000000,     ///<Transmit FIFO underrun has not occurred.
            v1=0x00000001,     ///<Transmit FIFO underrun has occurred
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,TefVal> tef{}; 
        namespace TefValC{
            constexpr Register::FieldValue<decltype(tef)::Type,TefVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tef)::Type,TefVal::v1> v1{};
        }
        ///Receive Error Flag
        enum class RefVal : unsigned {
            v0=0x00000000,     ///<Receive FIFO has not overflowed.
            v1=0x00000001,     ///<Receive FIFO has overflowed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,12),Register::ReadWriteAccess,RefVal> ref{}; 
        namespace RefValC{
            constexpr Register::FieldValue<decltype(ref)::Type,RefVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ref)::Type,RefVal::v1> v1{};
        }
        ///Data Match Flag
        enum class DmfVal : unsigned {
            v0=0x00000000,     ///<Have not received matching data.
            v1=0x00000001,     ///<Have received matching data.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,13),Register::ReadWriteAccess,DmfVal> dmf{}; 
        namespace DmfValC{
            constexpr Register::FieldValue<decltype(dmf)::Type,DmfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dmf)::Type,DmfVal::v1> v1{};
        }
        ///Module Busy Flag
        enum class MbfVal : unsigned {
            v0=0x00000000,     ///<LPSPI is idle.
            v1=0x00000001,     ///<LPSPI is busy.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,MbfVal> mbf{}; 
        namespace MbfValC{
            constexpr Register::FieldValue<decltype(mbf)::Type,MbfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(mbf)::Type,MbfVal::v1> v1{};
        }
    }
    namespace Lpspi1Ier{    ///<Interrupt Enable Register
        using Addr = Register::Address<0x4002d018,0xffffc0fc,0x00000000,unsigned>;
        ///Transmit Data Interrupt Enable
        enum class TdieVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,TdieVal> tdie{}; 
        namespace TdieValC{
            constexpr Register::FieldValue<decltype(tdie)::Type,TdieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tdie)::Type,TdieVal::v1> v1{};
        }
        ///Receive Data Interrupt Enable
        enum class RdieVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,RdieVal> rdie{}; 
        namespace RdieValC{
            constexpr Register::FieldValue<decltype(rdie)::Type,RdieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rdie)::Type,RdieVal::v1> v1{};
        }
        ///Word Complete Interrupt Enable
        enum class WcieVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::ReadWriteAccess,WcieVal> wcie{}; 
        namespace WcieValC{
            constexpr Register::FieldValue<decltype(wcie)::Type,WcieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(wcie)::Type,WcieVal::v1> v1{};
        }
        ///Frame Complete Interrupt Enable
        enum class FcieVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,9),Register::ReadWriteAccess,FcieVal> fcie{}; 
        namespace FcieValC{
            constexpr Register::FieldValue<decltype(fcie)::Type,FcieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fcie)::Type,FcieVal::v1> v1{};
        }
        ///Transfer Complete Interrupt Enable
        enum class TcieVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,10),Register::ReadWriteAccess,TcieVal> tcie{}; 
        namespace TcieValC{
            constexpr Register::FieldValue<decltype(tcie)::Type,TcieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tcie)::Type,TcieVal::v1> v1{};
        }
        ///Transmit Error Interrupt Enable
        enum class TeieVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,TeieVal> teie{}; 
        namespace TeieValC{
            constexpr Register::FieldValue<decltype(teie)::Type,TeieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(teie)::Type,TeieVal::v1> v1{};
        }
        ///Receive Error Interrupt Enable
        enum class ReieVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,12),Register::ReadWriteAccess,ReieVal> reie{}; 
        namespace ReieValC{
            constexpr Register::FieldValue<decltype(reie)::Type,ReieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(reie)::Type,ReieVal::v1> v1{};
        }
        ///Data Match Interrupt Enable
        enum class DmieVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,13),Register::ReadWriteAccess,DmieVal> dmie{}; 
        namespace DmieValC{
            constexpr Register::FieldValue<decltype(dmie)::Type,DmieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dmie)::Type,DmieVal::v1> v1{};
        }
    }
    namespace Lpspi1Der{    ///<DMA Enable Register
        using Addr = Register::Address<0x4002d01c,0xfffffffc,0x00000000,unsigned>;
        ///Transmit Data DMA Enable
        enum class TddeVal : unsigned {
            v0=0x00000000,     ///<DMA request disabled.
            v1=0x00000001,     ///<DMA request enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,TddeVal> tdde{}; 
        namespace TddeValC{
            constexpr Register::FieldValue<decltype(tdde)::Type,TddeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tdde)::Type,TddeVal::v1> v1{};
        }
        ///Receive Data DMA Enable
        enum class RddeVal : unsigned {
            v0=0x00000000,     ///<DMA request disabled.
            v1=0x00000001,     ///<DMA request enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,RddeVal> rdde{}; 
        namespace RddeValC{
            constexpr Register::FieldValue<decltype(rdde)::Type,RddeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rdde)::Type,RddeVal::v1> v1{};
        }
    }
    namespace Lpspi1Cfgr0{    ///<Configuration Register 0
        using Addr = Register::Address<0x4002d020,0xfffffcf8,0x00000000,unsigned>;
        ///Host Request Enable
        enum class HrenVal : unsigned {
            v0=0x00000000,     ///<Host request is disabled.
            v1=0x00000001,     ///<Host request is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,HrenVal> hren{}; 
        namespace HrenValC{
            constexpr Register::FieldValue<decltype(hren)::Type,HrenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(hren)::Type,HrenVal::v1> v1{};
        }
        ///Host Request Polarity
        enum class HrpolVal : unsigned {
            v0=0x00000000,     ///<Active low.
            v1=0x00000001,     ///<Active high.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,HrpolVal> hrpol{}; 
        namespace HrpolValC{
            constexpr Register::FieldValue<decltype(hrpol)::Type,HrpolVal::v0> v0{};
            constexpr Register::FieldValue<decltype(hrpol)::Type,HrpolVal::v1> v1{};
        }
        ///Host Request Select
        enum class HrselVal : unsigned {
            v0=0x00000000,     ///<Host request input is pin LPSPI_HREQ.
            v1=0x00000001,     ///<Host request input is input trigger.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,HrselVal> hrsel{}; 
        namespace HrselValC{
            constexpr Register::FieldValue<decltype(hrsel)::Type,HrselVal::v0> v0{};
            constexpr Register::FieldValue<decltype(hrsel)::Type,HrselVal::v1> v1{};
        }
        ///Circular FIFO Enable
        enum class CirfifoVal : unsigned {
            v0=0x00000000,     ///<Circular FIFO is disabled.
            v1=0x00000001,     ///<Circular FIFO is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::ReadWriteAccess,CirfifoVal> cirfifo{}; 
        namespace CirfifoValC{
            constexpr Register::FieldValue<decltype(cirfifo)::Type,CirfifoVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cirfifo)::Type,CirfifoVal::v1> v1{};
        }
        ///Receive Data Match Only
        enum class RdmoVal : unsigned {
            v0=0x00000000,     ///<Received data is stored in the receive FIFO as normal.
            v1=0x00000001,     ///<Received data is discarded unless the DMF is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,9),Register::ReadWriteAccess,RdmoVal> rdmo{}; 
        namespace RdmoValC{
            constexpr Register::FieldValue<decltype(rdmo)::Type,RdmoVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rdmo)::Type,RdmoVal::v1> v1{};
        }
    }
    namespace Lpspi1Cfgr1{    ///<Configuration Register 1
        using Addr = Register::Address<0x4002d024,0xf0f8f0f0,0x00000000,unsigned>;
        ///Master Mode
        enum class MasterVal : unsigned {
            v0=0x00000000,     ///<Slave mode.
            v1=0x00000001,     ///<Master mode.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,MasterVal> master{}; 
        namespace MasterValC{
            constexpr Register::FieldValue<decltype(master)::Type,MasterVal::v0> v0{};
            constexpr Register::FieldValue<decltype(master)::Type,MasterVal::v1> v1{};
        }
        ///Sample Point
        enum class SampleVal : unsigned {
            v0=0x00000000,     ///<Input data sampled on SCK edge.
            v1=0x00000001,     ///<Input data sampled on delayed SCK edge.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,SampleVal> sample{}; 
        namespace SampleValC{
            constexpr Register::FieldValue<decltype(sample)::Type,SampleVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sample)::Type,SampleVal::v1> v1{};
        }
        ///Automatic PCS
        enum class AutopcsVal : unsigned {
            v0=0x00000000,     ///<Automatic PCS generation disabled.
            v1=0x00000001,     ///<Automatic PCS generation enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,AutopcsVal> autopcs{}; 
        namespace AutopcsValC{
            constexpr Register::FieldValue<decltype(autopcs)::Type,AutopcsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(autopcs)::Type,AutopcsVal::v1> v1{};
        }
        ///No Stall
        enum class NostallVal : unsigned {
            v0=0x00000000,     ///<Transfers will stall when transmit FIFO is empty or receive FIFO is full.
            v1=0x00000001,     ///<Transfers will not stall, allowing transmit FIFO underrun or receive FIFO overrun to occur.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,NostallVal> nostall{}; 
        namespace NostallValC{
            constexpr Register::FieldValue<decltype(nostall)::Type,NostallVal::v0> v0{};
            constexpr Register::FieldValue<decltype(nostall)::Type,NostallVal::v1> v1{};
        }
        ///Peripheral Chip Select Polarity
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,8),Register::ReadWriteAccess,unsigned> pcspol{}; 
        ///Match Configuration
        enum class MatcfgVal : unsigned {
            v000=0x00000000,     ///<Match is disabled.
            v010=0x00000002,     ///<010b - Match is enabled, if 1st data word equals MATCH0 OR MATCH1, i.e., (1st data word = MATCH0 + MATCH1)
            v011=0x00000003,     ///<011b - Match is enabled, if any data word equals MATCH0 OR MATCH1, i.e., (any data word = MATCH0 + MATCH1)
            v100=0x00000004,     ///<100b - Match is enabled, if 1st data word equals MATCH0 AND 2nd data word equals MATCH1, i.e., [(1st data word = MATCH0) * (2nd data word = MATCH1)]
            v101=0x00000005,     ///<101b - Match is enabled, if any data word equals MATCH0 AND the next data word equals MATCH1, i.e., [(any data word = MATCH0) * (next data word = MATCH1)]
            v110=0x00000006,     ///<110b - Match is enabled, if (1st data word AND MATCH1) equals (MATCH0 AND MATCH1), i.e., [(1st data word * MATCH1) = (MATCH0 * MATCH1)]
            v111=0x00000007,     ///<111b - Match is enabled, if (any data word AND MATCH1) equals (MATCH0 AND MATCH1), i.e., [(any data word * MATCH1) = (MATCH0 * MATCH1)]
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(18,16),Register::ReadWriteAccess,MatcfgVal> matcfg{}; 
        namespace MatcfgValC{
            constexpr Register::FieldValue<decltype(matcfg)::Type,MatcfgVal::v000> v000{};
            constexpr Register::FieldValue<decltype(matcfg)::Type,MatcfgVal::v010> v010{};
            constexpr Register::FieldValue<decltype(matcfg)::Type,MatcfgVal::v011> v011{};
            constexpr Register::FieldValue<decltype(matcfg)::Type,MatcfgVal::v100> v100{};
            constexpr Register::FieldValue<decltype(matcfg)::Type,MatcfgVal::v101> v101{};
            constexpr Register::FieldValue<decltype(matcfg)::Type,MatcfgVal::v110> v110{};
            constexpr Register::FieldValue<decltype(matcfg)::Type,MatcfgVal::v111> v111{};
        }
        ///Pin Configuration
        enum class PincfgVal : unsigned {
            v00=0x00000000,     ///<SIN is used for input data and SOUT for output data.
            v01=0x00000001,     ///<SIN is used for both input and output data.
            v10=0x00000002,     ///<SOUT is used for both input and output data.
            v11=0x00000003,     ///<SOUT is used for input data and SIN for output data.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,PincfgVal> pincfg{}; 
        namespace PincfgValC{
            constexpr Register::FieldValue<decltype(pincfg)::Type,PincfgVal::v00> v00{};
            constexpr Register::FieldValue<decltype(pincfg)::Type,PincfgVal::v01> v01{};
            constexpr Register::FieldValue<decltype(pincfg)::Type,PincfgVal::v10> v10{};
            constexpr Register::FieldValue<decltype(pincfg)::Type,PincfgVal::v11> v11{};
        }
        ///Output Config
        enum class OutcfgVal : unsigned {
            v0=0x00000000,     ///<Output data retains last value when chip select is negated.
            v1=0x00000001,     ///<Output data is tristated when chip select is negated.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,OutcfgVal> outcfg{}; 
        namespace OutcfgValC{
            constexpr Register::FieldValue<decltype(outcfg)::Type,OutcfgVal::v0> v0{};
            constexpr Register::FieldValue<decltype(outcfg)::Type,OutcfgVal::v1> v1{};
        }
        ///Peripheral Chip Select Configuration
        enum class PcscfgVal : unsigned {
            v0=0x00000000,     ///<PCS[3:2] are enabled.
            v1=0x00000001,     ///<PCS[3:2] are disabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,PcscfgVal> pcscfg{}; 
        namespace PcscfgValC{
            constexpr Register::FieldValue<decltype(pcscfg)::Type,PcscfgVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pcscfg)::Type,PcscfgVal::v1> v1{};
        }
    }
    namespace Lpspi1Dmr0{    ///<Data Match Register 0
        using Addr = Register::Address<0x4002d030,0x00000000,0x00000000,unsigned>;
        ///Match 0 Value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> match0{}; 
    }
    namespace Lpspi1Dmr1{    ///<Data Match Register 1
        using Addr = Register::Address<0x4002d034,0x00000000,0x00000000,unsigned>;
        ///Match 1 Value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> match1{}; 
    }
    namespace Lpspi1Ccr{    ///<Clock Configuration Register
        using Addr = Register::Address<0x4002d040,0x00000000,0x00000000,unsigned>;
        ///SCK Divider
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> sckdiv{}; 
        ///Delay Between Transfers
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> dbt{}; 
        ///PCS to SCK Delay
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> pcssck{}; 
        ///SCK to PCS Delay
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> sckpcs{}; 
    }
    namespace Lpspi1Fcr{    ///<FIFO Control Register
        using Addr = Register::Address<0x4002d058,0xfffcfffc,0x00000000,unsigned>;
        ///Transmit FIFO Watermark
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,0),Register::ReadWriteAccess,unsigned> txwater{}; 
        ///Receive FIFO Watermark
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,16),Register::ReadWriteAccess,unsigned> rxwater{}; 
    }
    namespace Lpspi1Fsr{    ///<FIFO Status Register
        using Addr = Register::Address<0x4002d05c,0xfff8fff8,0x00000000,unsigned>;
        ///Transmit FIFO Count
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> txcount{}; 
        ///Receive FIFO Count
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(18,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxcount{}; 
    }
    namespace Lpspi1Tcr{    ///<Transmit Command Register
        using Addr = Register::Address<0x4002d060,0x0400f000,0x00000000,unsigned>;
        ///Frame Size
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,0),Register::ReadWriteAccess,unsigned> framesz{}; 
        ///Transfer Width
        enum class WidthVal : unsigned {
            v00=0x00000000,     ///<Single bit transfer.
            v01=0x00000001,     ///<Two bit transfer.
            v10=0x00000002,     ///<Four bit transfer.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,16),Register::ReadWriteAccess,WidthVal> width{}; 
        namespace WidthValC{
            constexpr Register::FieldValue<decltype(width)::Type,WidthVal::v00> v00{};
            constexpr Register::FieldValue<decltype(width)::Type,WidthVal::v01> v01{};
            constexpr Register::FieldValue<decltype(width)::Type,WidthVal::v10> v10{};
        }
        ///Transmit Data Mask
        enum class TxmskVal : unsigned {
            v0=0x00000000,     ///<Normal transfer.
            v1=0x00000001,     ///<Mask transmit data.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(18,18),Register::ReadWriteAccess,TxmskVal> txmsk{}; 
        namespace TxmskValC{
            constexpr Register::FieldValue<decltype(txmsk)::Type,TxmskVal::v0> v0{};
            constexpr Register::FieldValue<decltype(txmsk)::Type,TxmskVal::v1> v1{};
        }
        ///Receive Data Mask
        enum class RxmskVal : unsigned {
            v0=0x00000000,     ///<Normal transfer.
            v1=0x00000001,     ///<Receive data is masked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,19),Register::ReadWriteAccess,RxmskVal> rxmsk{}; 
        namespace RxmskValC{
            constexpr Register::FieldValue<decltype(rxmsk)::Type,RxmskVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rxmsk)::Type,RxmskVal::v1> v1{};
        }
        ///Continuing Command
        enum class ContcVal : unsigned {
            v0=0x00000000,     ///<Command word for start of new transfer.
            v1=0x00000001,     ///<Command word for continuing transfer.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,20),Register::ReadWriteAccess,ContcVal> contc{}; 
        namespace ContcValC{
            constexpr Register::FieldValue<decltype(contc)::Type,ContcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(contc)::Type,ContcVal::v1> v1{};
        }
        ///Continuous Transfer
        enum class ContVal : unsigned {
            v0=0x00000000,     ///<Continuous transfer disabled.
            v1=0x00000001,     ///<Continuous transfer enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(21,21),Register::ReadWriteAccess,ContVal> cont{}; 
        namespace ContValC{
            constexpr Register::FieldValue<decltype(cont)::Type,ContVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cont)::Type,ContVal::v1> v1{};
        }
        ///Byte Swap
        enum class ByswVal : unsigned {
            v0=0x00000000,     ///<Byte swap disabled.
            v1=0x00000001,     ///<Byte swap enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,22),Register::ReadWriteAccess,ByswVal> bysw{}; 
        namespace ByswValC{
            constexpr Register::FieldValue<decltype(bysw)::Type,ByswVal::v0> v0{};
            constexpr Register::FieldValue<decltype(bysw)::Type,ByswVal::v1> v1{};
        }
        ///LSB First
        enum class LsbfVal : unsigned {
            v0=0x00000000,     ///<Data is transferred MSB first.
            v1=0x00000001,     ///<Data is transferred LSB first.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,23),Register::ReadWriteAccess,LsbfVal> lsbf{}; 
        namespace LsbfValC{
            constexpr Register::FieldValue<decltype(lsbf)::Type,LsbfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lsbf)::Type,LsbfVal::v1> v1{};
        }
        ///Peripheral Chip Select
        enum class PcsVal : unsigned {
            v00=0x00000000,     ///<Transfer using LPSPI_PCS[0]
            v01=0x00000001,     ///<Transfer using LPSPI_PCS[1]
            v10=0x00000002,     ///<Transfer using LPSPI_PCS[2]
            v11=0x00000003,     ///<Transfer using LPSPI_PCS[3]
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,PcsVal> pcs{}; 
        namespace PcsValC{
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v00> v00{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v01> v01{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v10> v10{};
            constexpr Register::FieldValue<decltype(pcs)::Type,PcsVal::v11> v11{};
        }
        ///Prescaler Value
        enum class PrescaleVal : unsigned {
            v000=0x00000000,     ///<Divide by 1.
            v001=0x00000001,     ///<Divide by 2.
            v010=0x00000002,     ///<Divide by 4.
            v011=0x00000003,     ///<Divide by 8.
            v100=0x00000004,     ///<Divide by 16.
            v101=0x00000005,     ///<Divide by 32.
            v110=0x00000006,     ///<Divide by 64.
            v111=0x00000007,     ///<Divide by 128.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,27),Register::ReadWriteAccess,PrescaleVal> prescale{}; 
        namespace PrescaleValC{
            constexpr Register::FieldValue<decltype(prescale)::Type,PrescaleVal::v000> v000{};
            constexpr Register::FieldValue<decltype(prescale)::Type,PrescaleVal::v001> v001{};
            constexpr Register::FieldValue<decltype(prescale)::Type,PrescaleVal::v010> v010{};
            constexpr Register::FieldValue<decltype(prescale)::Type,PrescaleVal::v011> v011{};
            constexpr Register::FieldValue<decltype(prescale)::Type,PrescaleVal::v100> v100{};
            constexpr Register::FieldValue<decltype(prescale)::Type,PrescaleVal::v101> v101{};
            constexpr Register::FieldValue<decltype(prescale)::Type,PrescaleVal::v110> v110{};
            constexpr Register::FieldValue<decltype(prescale)::Type,PrescaleVal::v111> v111{};
        }
        ///Clock Phase
        enum class CphaVal : unsigned {
            v0=0x00000000,     ///<Data is captured on the leading edge of SCK and changed on the following edge.
            v1=0x00000001,     ///<Data is changed on the leading edge of SCK and captured on the following edge.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,30),Register::ReadWriteAccess,CphaVal> cpha{}; 
        namespace CphaValC{
            constexpr Register::FieldValue<decltype(cpha)::Type,CphaVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cpha)::Type,CphaVal::v1> v1{};
        }
        ///Clock Polarity
        enum class CpolVal : unsigned {
            v0=0x00000000,     ///<The inactive state value of SCK is low.
            v1=0x00000001,     ///<The inactive state value of SCK is high.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,CpolVal> cpol{}; 
        namespace CpolValC{
            constexpr Register::FieldValue<decltype(cpol)::Type,CpolVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cpol)::Type,CpolVal::v1> v1{};
        }
    }
    namespace Lpspi1Tdr{    ///<Transmit Data Register
        using Addr = Register::Address<0x4002d064,0x00000000,0x00000000,unsigned>;
        ///Transmit Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> data{}; 
    }
    namespace Lpspi1Rsr{    ///<Receive Status Register
        using Addr = Register::Address<0x4002d070,0xfffffffc,0x00000000,unsigned>;
        ///Start Of Frame
        enum class SofVal : unsigned {
            v0=0x00000000,     ///<Subsequent data word received after LPSPI_PCS assertion.
            v1=0x00000001,     ///<First data word received after LPSPI_PCS assertion.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,SofVal> sof{}; 
        namespace SofValC{
            constexpr Register::FieldValue<decltype(sof)::Type,SofVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sof)::Type,SofVal::v1> v1{};
        }
        ///RX FIFO Empty
        enum class RxemptyVal : unsigned {
            v0=0x00000000,     ///<RX FIFO is not empty.
            v1=0x00000001,     ///<RX FIFO is empty.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RxemptyVal> rxempty{}; 
        namespace RxemptyValC{
            constexpr Register::FieldValue<decltype(rxempty)::Type,RxemptyVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rxempty)::Type,RxemptyVal::v1> v1{};
        }
    }
    namespace Lpspi1Rdr{    ///<Receive Data Register
        using Addr = Register::Address<0x4002d074,0x00000000,0x00000000,unsigned>;
        ///Receive Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> data{}; 
    }
}
