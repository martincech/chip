#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//Reset Control Module
    namespace RcmVerid{    ///<Version ID Register
        using Addr = Register::Address<0x4007f000,0x00000000,0x00000000,unsigned>;
        ///Feature Specification Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> feature{}; 
        ///Minor Version Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> minor{}; 
        ///Major Version Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> major{}; 
    }
    namespace RcmParam{    ///<Parameter Register
        using Addr = Register::Address<0x4007f004,0xfffe5010,0x00000000,unsigned>;
        ///Existence of SRS[WAKEUP] status indication feature
        enum class EwakeupVal : unsigned {
            v0=0x00000000,     ///<The feature is not available.
            v1=0x00000001,     ///<The feature is available.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,EwakeupVal> ewakeup{}; 
        namespace EwakeupValC{
            constexpr Register::FieldValue<decltype(ewakeup)::Type,EwakeupVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ewakeup)::Type,EwakeupVal::v1> v1{};
        }
        ///Existence of SRS[LVD] status indication feature
        enum class ElvdVal : unsigned {
            v0=0x00000000,     ///<The feature is not available.
            v1=0x00000001,     ///<The feature is available.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,ElvdVal> elvd{}; 
        namespace ElvdValC{
            constexpr Register::FieldValue<decltype(elvd)::Type,ElvdVal::v0> v0{};
            constexpr Register::FieldValue<decltype(elvd)::Type,ElvdVal::v1> v1{};
        }
        ///Existence of SRS[LOC] status indication feature
        enum class ElocVal : unsigned {
            v0=0x00000000,     ///<The feature is not available.
            v1=0x00000001,     ///<The feature is available.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,ElocVal> eloc{}; 
        namespace ElocValC{
            constexpr Register::FieldValue<decltype(eloc)::Type,ElocVal::v0> v0{};
            constexpr Register::FieldValue<decltype(eloc)::Type,ElocVal::v1> v1{};
        }
        ///Existence of SRS[LOL] status indication feature
        enum class ElolVal : unsigned {
            v0=0x00000000,     ///<The feature is not available.
            v1=0x00000001,     ///<The feature is available.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,ElolVal> elol{}; 
        namespace ElolValC{
            constexpr Register::FieldValue<decltype(elol)::Type,ElolVal::v0> v0{};
            constexpr Register::FieldValue<decltype(elol)::Type,ElolVal::v1> v1{};
        }
        ///Existence of SRS[WDOG] status indication feature
        enum class EwdogVal : unsigned {
            v0=0x00000000,     ///<The feature is not available.
            v1=0x00000001,     ///<The feature is available.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,EwdogVal> ewdog{}; 
        namespace EwdogValC{
            constexpr Register::FieldValue<decltype(ewdog)::Type,EwdogVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ewdog)::Type,EwdogVal::v1> v1{};
        }
        ///Existence of SRS[PIN] status indication feature
        enum class EpinVal : unsigned {
            v0=0x00000000,     ///<The feature is not available.
            v1=0x00000001,     ///<The feature is available.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,EpinVal> epin{}; 
        namespace EpinValC{
            constexpr Register::FieldValue<decltype(epin)::Type,EpinVal::v0> v0{};
            constexpr Register::FieldValue<decltype(epin)::Type,EpinVal::v1> v1{};
        }
        ///Existence of SRS[POR] status indication feature
        enum class EporVal : unsigned {
            v0=0x00000000,     ///<The feature is not available.
            v1=0x00000001,     ///<The feature is available.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,EporVal> epor{}; 
        namespace EporValC{
            constexpr Register::FieldValue<decltype(epor)::Type,EporVal::v0> v0{};
            constexpr Register::FieldValue<decltype(epor)::Type,EporVal::v1> v1{};
        }
        ///Existence of SRS[JTAG] status indication feature
        enum class EjtagVal : unsigned {
            v0=0x00000000,     ///<The feature is not available.
            v1=0x00000001,     ///<The feature is available.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,EjtagVal> ejtag{}; 
        namespace EjtagValC{
            constexpr Register::FieldValue<decltype(ejtag)::Type,EjtagVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ejtag)::Type,EjtagVal::v1> v1{};
        }
        ///Existence of SRS[LOCKUP] status indication feature
        enum class ElockupVal : unsigned {
            v0=0x00000000,     ///<The feature is not available.
            v1=0x00000001,     ///<The feature is available.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,9),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,ElockupVal> elockup{}; 
        namespace ElockupValC{
            constexpr Register::FieldValue<decltype(elockup)::Type,ElockupVal::v0> v0{};
            constexpr Register::FieldValue<decltype(elockup)::Type,ElockupVal::v1> v1{};
        }
        ///Existence of SRS[SW] status indication feature
        enum class EswVal : unsigned {
            v0=0x00000000,     ///<The feature is not available.
            v1=0x00000001,     ///<The feature is available.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,10),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,EswVal> esw{}; 
        namespace EswValC{
            constexpr Register::FieldValue<decltype(esw)::Type,EswVal::v0> v0{};
            constexpr Register::FieldValue<decltype(esw)::Type,EswVal::v1> v1{};
        }
        ///Existence of SRS[MDM_AP] status indication feature
        enum class EmdmapVal : unsigned {
            v0=0x00000000,     ///<The feature is not available.
            v1=0x00000001,     ///<The feature is available.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,EmdmapVal> emdmAp{}; 
        namespace EmdmapValC{
            constexpr Register::FieldValue<decltype(emdmAp)::Type,EmdmapVal::v0> v0{};
            constexpr Register::FieldValue<decltype(emdmAp)::Type,EmdmapVal::v1> v1{};
        }
        ///Existence of SRS[SACKERR] status indication feature
        enum class EsackerrVal : unsigned {
            v0=0x00000000,     ///<The feature is not available.
            v1=0x00000001,     ///<The feature is available.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,13),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,EsackerrVal> esackerr{}; 
        namespace EsackerrValC{
            constexpr Register::FieldValue<decltype(esackerr)::Type,EsackerrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(esackerr)::Type,EsackerrVal::v1> v1{};
        }
        ///Existence of SRS[TAMPER] status indication feature
        enum class EtamperVal : unsigned {
            v0=0x00000000,     ///<The feature is not available.
            v1=0x00000001,     ///<The feature is available.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,EtamperVal> etamper{}; 
        namespace EtamperValC{
            constexpr Register::FieldValue<decltype(etamper)::Type,EtamperVal::v0> v0{};
            constexpr Register::FieldValue<decltype(etamper)::Type,EtamperVal::v1> v1{};
        }
        ///Existence of SRS[CORE1] status indication feature
        enum class Ecore1Val : unsigned {
            v0=0x00000000,     ///<The feature is not available.
            v1=0x00000001,     ///<The feature is available.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,Ecore1Val> ecore1{}; 
        namespace Ecore1ValC{
            constexpr Register::FieldValue<decltype(ecore1)::Type,Ecore1Val::v0> v0{};
            constexpr Register::FieldValue<decltype(ecore1)::Type,Ecore1Val::v1> v1{};
        }
    }
    namespace RcmSrs{    ///<System Reset Status Register
        using Addr = Register::Address<0x4007f008,0xffffd011,0x00000000,unsigned>;
        ///Low-Voltage Detect Reset or High-Voltage Detect Reset
        enum class LvdVal : unsigned {
            v0=0x00000000,     ///<Reset not caused by LVD trip, HVD trip or POR
            v1=0x00000001,     ///<Reset caused by LVD trip, HVD trip or POR
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,LvdVal> lvd{}; 
        namespace LvdValC{
            constexpr Register::FieldValue<decltype(lvd)::Type,LvdVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lvd)::Type,LvdVal::v1> v1{};
        }
        ///Loss-of-Clock Reset
        enum class LocVal : unsigned {
            v0=0x00000000,     ///<Reset not caused by a loss of external clock.
            v1=0x00000001,     ///<Reset caused by a loss of external clock.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,LocVal> loc{}; 
        namespace LocValC{
            constexpr Register::FieldValue<decltype(loc)::Type,LocVal::v0> v0{};
            constexpr Register::FieldValue<decltype(loc)::Type,LocVal::v1> v1{};
        }
        ///Loss-of-Lock Reset
        enum class LolVal : unsigned {
            v0=0x00000000,     ///<Reset not caused by a loss of lock in the PLL/FLL
            v1=0x00000001,     ///<Reset caused by a loss of lock in the PLL/FLL
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,LolVal> lol{}; 
        namespace LolValC{
            constexpr Register::FieldValue<decltype(lol)::Type,LolVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lol)::Type,LolVal::v1> v1{};
        }
        ///Watchdog
        enum class WdogVal : unsigned {
            v0=0x00000000,     ///<Reset not caused by watchdog timeout
            v1=0x00000001,     ///<Reset caused by watchdog timeout
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,WdogVal> wdog{}; 
        namespace WdogValC{
            constexpr Register::FieldValue<decltype(wdog)::Type,WdogVal::v0> v0{};
            constexpr Register::FieldValue<decltype(wdog)::Type,WdogVal::v1> v1{};
        }
        ///External Reset Pin
        enum class PinVal : unsigned {
            v0=0x00000000,     ///<Reset not caused by external reset pin
            v1=0x00000001,     ///<Reset caused by external reset pin
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PinVal> pin{}; 
        namespace PinValC{
            constexpr Register::FieldValue<decltype(pin)::Type,PinVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pin)::Type,PinVal::v1> v1{};
        }
        ///Power-On Reset
        enum class PorVal : unsigned {
            v0=0x00000000,     ///<Reset not caused by POR
            v1=0x00000001,     ///<Reset caused by POR
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PorVal> por{}; 
        namespace PorValC{
            constexpr Register::FieldValue<decltype(por)::Type,PorVal::v0> v0{};
            constexpr Register::FieldValue<decltype(por)::Type,PorVal::v1> v1{};
        }
        ///JTAG generated reset
        enum class JtagVal : unsigned {
            v0=0x00000000,     ///<Reset not caused by JTAG
            v1=0x00000001,     ///<Reset caused by JTAG
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,JtagVal> jtag{}; 
        namespace JtagValC{
            constexpr Register::FieldValue<decltype(jtag)::Type,JtagVal::v0> v0{};
            constexpr Register::FieldValue<decltype(jtag)::Type,JtagVal::v1> v1{};
        }
        ///Core Lockup
        enum class LockupVal : unsigned {
            v0=0x00000000,     ///<Reset not caused by core LOCKUP event
            v1=0x00000001,     ///<Reset caused by core LOCKUP event
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,9),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,LockupVal> lockup{}; 
        namespace LockupValC{
            constexpr Register::FieldValue<decltype(lockup)::Type,LockupVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lockup)::Type,LockupVal::v1> v1{};
        }
        ///Software
        enum class SwVal : unsigned {
            v0=0x00000000,     ///<Reset not caused by software setting of SYSRESETREQ bit
            v1=0x00000001,     ///<Reset caused by software setting of SYSRESETREQ bit
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,10),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,SwVal> sw{}; 
        namespace SwValC{
            constexpr Register::FieldValue<decltype(sw)::Type,SwVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sw)::Type,SwVal::v1> v1{};
        }
        ///MDM-AP System Reset Request
        enum class MdmapVal : unsigned {
            v0=0x00000000,     ///<Reset was not caused by host debugger system setting of the System Reset Request bit
            v1=0x00000001,     ///<Reset was caused by host debugger system setting of the System Reset Request bit
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,MdmapVal> mdmAp{}; 
        namespace MdmapValC{
            constexpr Register::FieldValue<decltype(mdmAp)::Type,MdmapVal::v0> v0{};
            constexpr Register::FieldValue<decltype(mdmAp)::Type,MdmapVal::v1> v1{};
        }
        ///Stop Acknowledge Error
        enum class SackerrVal : unsigned {
            v0=0x00000000,     ///<Reset not caused by peripheral failure to acknowledge attempt to enter stop mode
            v1=0x00000001,     ///<Reset caused by peripheral failure to acknowledge attempt to enter stop mode
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,13),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,SackerrVal> sackerr{}; 
        namespace SackerrValC{
            constexpr Register::FieldValue<decltype(sackerr)::Type,SackerrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sackerr)::Type,SackerrVal::v1> v1{};
        }
    }
    namespace RcmRpc{    ///<Reset Pin Control register
        using Addr = Register::Address<0x4007f00c,0xffffe0f8,0x00000000,unsigned>;
        ///Reset Pin Filter Select in Run and Wait Modes
        enum class RstfltsrwVal : unsigned {
            v00=0x00000000,     ///<All filtering disabled
            v01=0x00000001,     ///<Bus clock filter enabled for normal operation
            v10=0x00000002,     ///<LPO clock filter enabled for normal operation
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,0),Register::ReadWriteAccess,RstfltsrwVal> rstfltsrw{}; 
        namespace RstfltsrwValC{
            constexpr Register::FieldValue<decltype(rstfltsrw)::Type,RstfltsrwVal::v00> v00{};
            constexpr Register::FieldValue<decltype(rstfltsrw)::Type,RstfltsrwVal::v01> v01{};
            constexpr Register::FieldValue<decltype(rstfltsrw)::Type,RstfltsrwVal::v10> v10{};
        }
        ///Reset Pin Filter Select in Stop Mode
        enum class RstfltssVal : unsigned {
            v0=0x00000000,     ///<All filtering disabled
            v1=0x00000001,     ///<LPO clock filter enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,RstfltssVal> rstfltss{}; 
        namespace RstfltssValC{
            constexpr Register::FieldValue<decltype(rstfltss)::Type,RstfltssVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rstfltss)::Type,RstfltssVal::v1> v1{};
        }
        ///Reset Pin Filter Bus Clock Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,8),Register::ReadWriteAccess,unsigned> rstfltsel{}; 
    }
    namespace RcmSsrs{    ///<Sticky System Reset Status Register
        using Addr = Register::Address<0x4007f018,0xffffd011,0x00000000,unsigned>;
        ///Sticky Low-Voltage Detect Reset
        enum class SlvdVal : unsigned {
            v0=0x00000000,     ///<Reset not caused by LVD trip or POR
            v1=0x00000001,     ///<Reset caused by LVD trip or POR
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,SlvdVal> slvd{}; 
        namespace SlvdValC{
            constexpr Register::FieldValue<decltype(slvd)::Type,SlvdVal::v0> v0{};
            constexpr Register::FieldValue<decltype(slvd)::Type,SlvdVal::v1> v1{};
        }
        ///Sticky Loss-of-Clock Reset
        enum class SlocVal : unsigned {
            v0=0x00000000,     ///<Reset not caused by a loss of external clock.
            v1=0x00000001,     ///<Reset caused by a loss of external clock.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,SlocVal> sloc{}; 
        namespace SlocValC{
            constexpr Register::FieldValue<decltype(sloc)::Type,SlocVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sloc)::Type,SlocVal::v1> v1{};
        }
        ///Sticky Loss-of-Lock Reset
        enum class SlolVal : unsigned {
            v0=0x00000000,     ///<Reset not caused by a loss of lock in the PLL/FLL
            v1=0x00000001,     ///<Reset caused by a loss of lock in the PLL/FLL
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,SlolVal> slol{}; 
        namespace SlolValC{
            constexpr Register::FieldValue<decltype(slol)::Type,SlolVal::v0> v0{};
            constexpr Register::FieldValue<decltype(slol)::Type,SlolVal::v1> v1{};
        }
        ///Sticky Watchdog
        enum class SwdogVal : unsigned {
            v0=0x00000000,     ///<Reset not caused by watchdog timeout
            v1=0x00000001,     ///<Reset caused by watchdog timeout
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,SwdogVal> swdog{}; 
        namespace SwdogValC{
            constexpr Register::FieldValue<decltype(swdog)::Type,SwdogVal::v0> v0{};
            constexpr Register::FieldValue<decltype(swdog)::Type,SwdogVal::v1> v1{};
        }
        ///Sticky External Reset Pin
        enum class SpinVal : unsigned {
            v0=0x00000000,     ///<Reset not caused by external reset pin
            v1=0x00000001,     ///<Reset caused by external reset pin
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,SpinVal> spin{}; 
        namespace SpinValC{
            constexpr Register::FieldValue<decltype(spin)::Type,SpinVal::v0> v0{};
            constexpr Register::FieldValue<decltype(spin)::Type,SpinVal::v1> v1{};
        }
        ///Sticky Power-On Reset
        enum class SporVal : unsigned {
            v0=0x00000000,     ///<Reset not caused by POR
            v1=0x00000001,     ///<Reset caused by POR
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::ReadWriteAccess,SporVal> spor{}; 
        namespace SporValC{
            constexpr Register::FieldValue<decltype(spor)::Type,SporVal::v0> v0{};
            constexpr Register::FieldValue<decltype(spor)::Type,SporVal::v1> v1{};
        }
        ///Sticky JTAG generated reset
        enum class SjtagVal : unsigned {
            v0=0x00000000,     ///<Reset not caused by JTAG
            v1=0x00000001,     ///<Reset caused by JTAG
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::ReadWriteAccess,SjtagVal> sjtag{}; 
        namespace SjtagValC{
            constexpr Register::FieldValue<decltype(sjtag)::Type,SjtagVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sjtag)::Type,SjtagVal::v1> v1{};
        }
        ///Sticky Core Lockup
        enum class SlockupVal : unsigned {
            v0=0x00000000,     ///<Reset not caused by core LOCKUP event
            v1=0x00000001,     ///<Reset caused by core LOCKUP event
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,9),Register::ReadWriteAccess,SlockupVal> slockup{}; 
        namespace SlockupValC{
            constexpr Register::FieldValue<decltype(slockup)::Type,SlockupVal::v0> v0{};
            constexpr Register::FieldValue<decltype(slockup)::Type,SlockupVal::v1> v1{};
        }
        ///Sticky Software
        enum class SswVal : unsigned {
            v0=0x00000000,     ///<Reset not caused by software setting of SYSRESETREQ bit
            v1=0x00000001,     ///<Reset caused by software setting of SYSRESETREQ bit
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,10),Register::ReadWriteAccess,SswVal> ssw{}; 
        namespace SswValC{
            constexpr Register::FieldValue<decltype(ssw)::Type,SswVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ssw)::Type,SswVal::v1> v1{};
        }
        ///Sticky MDM-AP System Reset Request
        enum class SmdmapVal : unsigned {
            v0=0x00000000,     ///<Reset was not caused by host debugger system setting of the System Reset Request bit
            v1=0x00000001,     ///<Reset was caused by host debugger system setting of the System Reset Request bit
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,SmdmapVal> smdmAp{}; 
        namespace SmdmapValC{
            constexpr Register::FieldValue<decltype(smdmAp)::Type,SmdmapVal::v0> v0{};
            constexpr Register::FieldValue<decltype(smdmAp)::Type,SmdmapVal::v1> v1{};
        }
        ///Sticky Stop Acknowledge Error
        enum class SsackerrVal : unsigned {
            v0=0x00000000,     ///<Reset not caused by peripheral failure to acknowledge attempt to enter stop mode
            v1=0x00000001,     ///<Reset caused by peripheral failure to acknowledge attempt to enter stop mode
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,13),Register::ReadWriteAccess,SsackerrVal> ssackerr{}; 
        namespace SsackerrValC{
            constexpr Register::FieldValue<decltype(ssackerr)::Type,SsackerrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ssackerr)::Type,SsackerrVal::v1> v1{};
        }
    }
    namespace RcmSrie{    ///<System Reset Interrupt Enable Register
        using Addr = Register::Address<0x4007f01c,0xffffd010,0x00000000,unsigned>;
        ///Reset Delay Time
        enum class DelayVal : unsigned {
            v00=0x00000000,     ///<10 LPO cycles
            v01=0x00000001,     ///<34 LPO cycles
            v10=0x00000002,     ///<130 LPO cycles
            v11=0x00000003,     ///<514 LPO cycles
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,0),Register::ReadWriteAccess,DelayVal> delay{}; 
        namespace DelayValC{
            constexpr Register::FieldValue<decltype(delay)::Type,DelayVal::v00> v00{};
            constexpr Register::FieldValue<decltype(delay)::Type,DelayVal::v01> v01{};
            constexpr Register::FieldValue<decltype(delay)::Type,DelayVal::v10> v10{};
            constexpr Register::FieldValue<decltype(delay)::Type,DelayVal::v11> v11{};
        }
        ///Loss-of-Clock Interrupt
        enum class LocVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,LocVal> loc{}; 
        namespace LocValC{
            constexpr Register::FieldValue<decltype(loc)::Type,LocVal::v0> v0{};
            constexpr Register::FieldValue<decltype(loc)::Type,LocVal::v1> v1{};
        }
        ///Loss-of-Lock Interrupt
        enum class LolVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,LolVal> lol{}; 
        namespace LolValC{
            constexpr Register::FieldValue<decltype(lol)::Type,LolVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lol)::Type,LolVal::v1> v1{};
        }
        ///Watchdog Interrupt
        enum class WdogVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,WdogVal> wdog{}; 
        namespace WdogValC{
            constexpr Register::FieldValue<decltype(wdog)::Type,WdogVal::v0> v0{};
            constexpr Register::FieldValue<decltype(wdog)::Type,WdogVal::v1> v1{};
        }
        ///External Reset Pin Interrupt
        enum class PinVal : unsigned {
            v0=0x00000000,     ///<Reset not caused by external reset pin
            v1=0x00000001,     ///<Reset caused by external reset pin
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,PinVal> pin{}; 
        namespace PinValC{
            constexpr Register::FieldValue<decltype(pin)::Type,PinVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pin)::Type,PinVal::v1> v1{};
        }
        ///Global Interrupt Enable
        enum class GieVal : unsigned {
            v0=0x00000000,     ///<All interrupt sources disabled.
            v1=0x00000001,     ///<All interrupt sources enabled. Note that the individual interrupt-enable bits still need to be set to generate interrupts.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::ReadWriteAccess,GieVal> gie{}; 
        namespace GieValC{
            constexpr Register::FieldValue<decltype(gie)::Type,GieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(gie)::Type,GieVal::v1> v1{};
        }
        ///JTAG generated reset
        enum class JtagVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::ReadWriteAccess,JtagVal> jtag{}; 
        namespace JtagValC{
            constexpr Register::FieldValue<decltype(jtag)::Type,JtagVal::v0> v0{};
            constexpr Register::FieldValue<decltype(jtag)::Type,JtagVal::v1> v1{};
        }
        ///Core Lockup Interrupt
        enum class LockupVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,9),Register::ReadWriteAccess,LockupVal> lockup{}; 
        namespace LockupValC{
            constexpr Register::FieldValue<decltype(lockup)::Type,LockupVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lockup)::Type,LockupVal::v1> v1{};
        }
        ///Software Interrupt
        enum class SwVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,10),Register::ReadWriteAccess,SwVal> sw{}; 
        namespace SwValC{
            constexpr Register::FieldValue<decltype(sw)::Type,SwVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sw)::Type,SwVal::v1> v1{};
        }
        ///MDM-AP System Reset Request
        enum class MdmapVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,MdmapVal> mdmAp{}; 
        namespace MdmapValC{
            constexpr Register::FieldValue<decltype(mdmAp)::Type,MdmapVal::v0> v0{};
            constexpr Register::FieldValue<decltype(mdmAp)::Type,MdmapVal::v1> v1{};
        }
        ///Stop Acknowledge Error Interrupt
        enum class SackerrVal : unsigned {
            v0=0x00000000,     ///<Interrupt disabled.
            v1=0x00000001,     ///<Interrupt enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,13),Register::ReadWriteAccess,SackerrVal> sackerr{}; 
        namespace SackerrValC{
            constexpr Register::FieldValue<decltype(sackerr)::Type,SackerrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sackerr)::Type,SackerrVal::v1> v1{};
        }
    }
}
