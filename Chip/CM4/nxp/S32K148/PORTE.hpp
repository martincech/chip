#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//Pin Control and Interrupts
    namespace PortePcr0{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d000,0xfef078bc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Drive Strength Enable
        enum class DseVal : unsigned {
            v0=0x00000000,     ///<Low drive strength is configured on the corresponding pin, if pin is configured as a digital output.
            v1=0x00000001,     ///<High drive strength is configured on the corresponding pin, if pin is configured as a digital output.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,DseVal> dse{}; 
        namespace DseValC{
            constexpr Register::FieldValue<decltype(dse)::Type,DseVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dse)::Type,DseVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            lpspi0_sck=0x00000002,     ///<Alternative 2 (chip-specific).
            tclk1=0x00000003,     ///<Alternative 3 (chip-specific).
            lpi2c1_sda=0x00000004,     ///<Alternative 4 (chip-specific).
            lpspi1_sout=0x00000005,     ///<Alternative 5 (chip-specific).
            ftm1_flt2=0x00000006,     ///<Alternative 6 (chip-specific).
            sai0_d2=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi0_sck> lpspi0_sck{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::tclk1> tclk1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpi2c1_sda> lpi2c1_sda{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi1_sout> lpspi1_sout{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_flt2> ftm1_flt2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::sai0_d2> sai0_d2{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr1{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d004,0xfef078bc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Drive Strength Enable
        enum class DseVal : unsigned {
            v0=0x00000000,     ///<Low drive strength is configured on the corresponding pin, if pin is configured as a digital output.
            v1=0x00000001,     ///<High drive strength is configured on the corresponding pin, if pin is configured as a digital output.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,DseVal> dse{}; 
        namespace DseValC{
            constexpr Register::FieldValue<decltype(dse)::Type,DseVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dse)::Type,DseVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            lpspi0_sin=0x00000002,     ///<Alternative 2 (chip-specific).
            lpi2c0_hreq=0x00000003,     ///<Alternative 3 (chip-specific).
            lpi2c1_scl=0x00000004,     ///<Alternative 4 (chip-specific).
            lpspi1_pcs0=0x00000005,     ///<Alternative 5 (chip-specific).
            ftm1_flt1=0x00000006,     ///<Alternative 6 (chip-specific).
            sai0_d1=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi0_sin> lpspi0_sin{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpi2c0_hreq> lpi2c0_hreq{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpi2c1_scl> lpi2c1_scl{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi1_pcs0> lpspi1_pcs0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_flt1> ftm1_flt1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::sai0_d1> sai0_d1{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr2{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d008,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            lpspi0_sout=0x00000002,     ///<Alternative 2 (chip-specific).
            lptmr0_alt3=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm3_ch6=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            lpuart1_cts=0x00000006,     ///<Alternative 6 (chip-specific).
            sai1_sync=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi0_sout> lpspi0_sout{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lptmr0_alt3> lptmr0_alt3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm3_ch6> ftm3_ch6{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart1_cts> lpuart1_cts{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::sai1_sync> sai1_sync{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr3{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d00c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm0_flt0=0x00000002,     ///<Alternative 2 (chip-specific).
            lpuart2_rts=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm2_flt0=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            trgmux_in6=0x00000006,     ///<Alternative 6 (chip-specific).
            cmp0_out=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm0_flt0> ftm0_flt0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart2_rts> lpuart2_rts{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_flt0> ftm2_flt0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::trgmux_in6> trgmux_in6{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::cmp0_out> cmp0_out{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr4{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d010,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            etm_trace_d1=0x00000002,     ///<Alternative 2 (chip-specific).
            ftm2_qd_phb=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm2_ch2=0x00000004,     ///<Alternative 4 (chip-specific).
            can0_rx=0x00000005,     ///<Alternative 5 (chip-specific).
            fxio_d6=0x00000006,     ///<Alternative 6 (chip-specific).
            ewm_out_b=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::etm_trace_d1> etm_trace_d1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_qd_phb> ftm2_qd_phb{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_ch2> ftm2_ch2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::can0_rx> can0_rx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d6> fxio_d6{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ewm_out_b> ewm_out_b{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr5{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d014,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            tclk2=0x00000002,     ///<Alternative 2 (chip-specific).
            ftm2_qd_pha=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm2_ch3=0x00000004,     ///<Alternative 4 (chip-specific).
            can0_tx=0x00000005,     ///<Alternative 5 (chip-specific).
            fxio_d7=0x00000006,     ///<Alternative 6 (chip-specific).
            ewm_in=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::tclk2> tclk2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_qd_pha> ftm2_qd_pha{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_ch3> ftm2_ch3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::can0_tx> can0_tx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d7> fxio_d7{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ewm_in> ewm_in{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr6{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d018,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            lpspi0_pcs2=0x00000002,     ///<Alternative 2 (chip-specific).
            ftm7_flt1=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm3_ch7=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            lpuart1_rts=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi0_pcs2> lpspi0_pcs2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm7_flt1> ftm7_flt1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm3_ch7> ftm3_ch7{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart1_rts> lpuart1_rts{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr7{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d01c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm0_ch7=0x00000002,     ///<Alternative 2 (chip-specific).
            ftm3_flt0=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm0_ch7> ftm0_ch7{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm3_flt0> ftm3_flt0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr8{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d020,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm0_ch6=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            mii_rmii_mdc=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm0_ch6> ftm0_ch6{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::mii_rmii_mdc> mii_rmii_mdc{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr9{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d024,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm0_ch7=0x00000002,     ///<Alternative 2 (chip-specific).
            lpuart2_cts=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            enet_tmr3=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm0_ch7> ftm0_ch7{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart2_cts> lpuart2_cts{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::enet_tmr3> enet_tmr3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr10{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d028,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            clkout=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi2_pcs1=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm2_ch4=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            fxio_d4=0x00000006,     ///<Alternative 6 (chip-specific).
            trgmux_out4=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::clkout> clkout{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi2_pcs1> lpspi2_pcs1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_ch4> ftm2_ch4{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d4> fxio_d4{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::trgmux_out4> trgmux_out4{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr11{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d02c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            lpspi2_pcs0=0x00000002,     ///<Alternative 2 (chip-specific).
            lptmr0_alt1=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm2_ch5=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            fxio_d5=0x00000006,     ///<Alternative 6 (chip-specific).
            trgmux_out5=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi2_pcs0> lpspi2_pcs0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lptmr0_alt1> lptmr0_alt1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_ch5> ftm2_ch5{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d5> fxio_d5{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::trgmux_out5> trgmux_out5{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr12{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d030,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm0_flt3=0x00000002,     ///<Alternative 2 (chip-specific).
            lpuart2_tx=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm5_flt0=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm0_flt3> ftm0_flt3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart2_tx> lpuart2_tx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm5_flt0> ftm5_flt0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr13{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d034,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm4_ch5=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi2_pcs2=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm2_flt0=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm4_ch5> ftm4_ch5{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi2_pcs2> lpspi2_pcs2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_flt0> ftm2_flt0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr14{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d038,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm0_flt1=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm2_flt1=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm0_flt1> ftm0_flt1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_flt1> ftm2_flt1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr15{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d03c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            lpuart1_cts=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi2_sck=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm2_ch6=0x00000004,     ///<Alternative 4 (chip-specific).
            ftm4_flt1=0x00000005,     ///<Alternative 5 (chip-specific).
            fxio_d2=0x00000006,     ///<Alternative 6 (chip-specific).
            trgmux_out6=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart1_cts> lpuart1_cts{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi2_sck> lpspi2_sck{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_ch6> ftm2_ch6{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm4_flt1> ftm4_flt1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d2> fxio_d2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::trgmux_out6> trgmux_out6{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr16{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d040,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            lpuart1_rts=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi2_sin=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm2_ch7=0x00000004,     ///<Alternative 4 (chip-specific).
            ftm4_flt0=0x00000005,     ///<Alternative 5 (chip-specific).
            fxio_d3=0x00000006,     ///<Alternative 6 (chip-specific).
            trgmux_out7=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart1_rts> lpuart1_rts{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi2_sin> lpspi2_sin{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_ch7> ftm2_ch7{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm4_flt0> ftm4_flt0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d3> fxio_d3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::trgmux_out7> trgmux_out7{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr17{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d044,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm7_ch5=0x00000002,     ///<Alternative 2 (chip-specific).
            fxio_d5=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm7_ch5> ftm7_ch5{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d5> fxio_d5{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr18{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d048,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm7_ch6=0x00000002,     ///<Alternative 2 (chip-specific).
            fxio_d4=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm7_ch6> ftm7_ch6{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d4> fxio_d4{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr19{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d04c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm7_ch7=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm7_ch7> ftm7_ch7{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr20{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d050,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm4_ch0=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm4_ch0> ftm4_ch0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr21{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d054,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm4_ch1=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm4_ch1> ftm4_ch1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr22{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d058,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm4_ch2=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm4_ch2> ftm4_ch2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr23{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d05c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm4_ch3=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm4_ch3> ftm4_ch3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr24{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d060,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm4_ch4=0x00000002,     ///<Alternative 2 (chip-specific).
            can2_tx=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm4_ch4> ftm4_ch4{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::can2_tx> can2_tx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr25{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d064,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm4_ch5=0x00000002,     ///<Alternative 2 (chip-specific).
            can2_rx=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm4_ch5> ftm4_ch5{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::can2_rx> can2_rx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr26{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d068,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm4_ch6=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm4_ch6> ftm4_ch6{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr27{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d06c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm4_ch7=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm4_ch7> ftm4_ch7{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr28{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d070,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            v000=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            v001=0x00000001,     ///<Alternative 1 (GPIO).
            v010=0x00000002,     ///<Alternative 2 (chip-specific).
            v011=0x00000003,     ///<Alternative 3 (chip-specific).
            v100=0x00000004,     ///<Alternative 4 (chip-specific).
            v101=0x00000005,     ///<Alternative 5 (chip-specific).
            v110=0x00000006,     ///<Alternative 6 (chip-specific).
            v111=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v000> v000{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v001> v001{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v010> v010{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v011> v011{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v100> v100{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v101> v101{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v110> v110{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v111> v111{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr29{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d074,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            v000=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            v001=0x00000001,     ///<Alternative 1 (GPIO).
            v010=0x00000002,     ///<Alternative 2 (chip-specific).
            v011=0x00000003,     ///<Alternative 3 (chip-specific).
            v100=0x00000004,     ///<Alternative 4 (chip-specific).
            v101=0x00000005,     ///<Alternative 5 (chip-specific).
            v110=0x00000006,     ///<Alternative 6 (chip-specific).
            v111=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v000> v000{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v001> v001{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v010> v010{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v011> v011{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v100> v100{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v101> v101{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v110> v110{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v111> v111{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr30{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d078,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            v000=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            v001=0x00000001,     ///<Alternative 1 (GPIO).
            v010=0x00000002,     ///<Alternative 2 (chip-specific).
            v011=0x00000003,     ///<Alternative 3 (chip-specific).
            v100=0x00000004,     ///<Alternative 4 (chip-specific).
            v101=0x00000005,     ///<Alternative 5 (chip-specific).
            v110=0x00000006,     ///<Alternative 6 (chip-specific).
            v111=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v000> v000{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v001> v001{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v010> v010{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v011> v011{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v100> v100{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v101> v101{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v110> v110{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v111> v111{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortePcr31{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004d07c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            v000=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            v001=0x00000001,     ///<Alternative 1 (GPIO).
            v010=0x00000002,     ///<Alternative 2 (chip-specific).
            v011=0x00000003,     ///<Alternative 3 (chip-specific).
            v100=0x00000004,     ///<Alternative 4 (chip-specific).
            v101=0x00000005,     ///<Alternative 5 (chip-specific).
            v110=0x00000006,     ///<Alternative 6 (chip-specific).
            v111=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v000> v000{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v001> v001{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v010> v010{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v011> v011{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v100> v100{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v101> v101{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v110> v110{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::v111> v111{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PorteGpclr{    ///<Global Pin Control Low Register
        using Addr = Register::Address<0x4004d080,0x00000000,0x00000000,unsigned>;
        ///Global Pin Write Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> gpwd{}; 
        ///Global Pin Write Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,16),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> gpwe{}; 
    }
    namespace PorteGpchr{    ///<Global Pin Control High Register
        using Addr = Register::Address<0x4004d084,0x00000000,0x00000000,unsigned>;
        ///Global Pin Write Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> gpwd{}; 
        ///Global Pin Write Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,16),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> gpwe{}; 
    }
    namespace PorteGiclr{    ///<Global Interrupt Control Low Register
        using Addr = Register::Address<0x4004d088,0x00000000,0x00000000,unsigned>;
        ///Global Interrupt Write Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> giwe{}; 
        ///Global Interrupt Write Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,16),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> giwd{}; 
    }
    namespace PorteGichr{    ///<Global Interrupt Control High Register
        using Addr = Register::Address<0x4004d08c,0x00000000,0x00000000,unsigned>;
        ///Global Interrupt Write Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> giwe{}; 
        ///Global Interrupt Write Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,16),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> giwd{}; 
    }
    namespace PorteIsfr{    ///<Interrupt Status Flag Register
        using Addr = Register::Address<0x4004d0a0,0x00000000,0x00000000,unsigned>;
        ///Interrupt Status Flag
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> isf{}; 
    }
    namespace PorteDfer{    ///<Digital Filter Enable Register
        using Addr = Register::Address<0x4004d0c0,0x00000000,0x00000000,unsigned>;
        ///Digital Filter Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> dfe{}; 
    }
    namespace PorteDfcr{    ///<Digital Filter Clock Register
        using Addr = Register::Address<0x4004d0c4,0xfffffffe,0x00000000,unsigned>;
        ///Clock Source
        enum class CsVal : unsigned {
            v0=0x00000000,     ///<Digital filters are clocked by the bus clock.
            v1=0x00000001,     ///<Digital filters are clocked by the LPO clock.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,CsVal> cs{}; 
        namespace CsValC{
            constexpr Register::FieldValue<decltype(cs)::Type,CsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cs)::Type,CsVal::v1> v1{};
        }
    }
    namespace PorteDfwr{    ///<Digital Filter Width Register
        using Addr = Register::Address<0x4004d0c8,0xffffffe0,0x00000000,unsigned>;
        ///Filter Length
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,0),Register::ReadWriteAccess,unsigned> filt{}; 
    }
}
