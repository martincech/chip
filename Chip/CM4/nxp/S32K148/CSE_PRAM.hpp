#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//CSE_PRAM
    namespace CsePramEmbeddedram0{    ///<CSE PRAM 0 Register
        using Addr = Register::Address<0x14001000,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram0ll{    ///<CSE PRAM0LL register.
        using Addr = Register::Address<0x14001000,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram0lu{    ///<CSE PRAM0LU register.
        using Addr = Register::Address<0x14001001,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram0hl{    ///<CSE PRAM0HL register.
        using Addr = Register::Address<0x14001002,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram0hu{    ///<CSE PRAM0HU register.
        using Addr = Register::Address<0x14001003,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram1{    ///<CSE PRAM 1 Register
        using Addr = Register::Address<0x14001004,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram1ll{    ///<CSE PRAM1LL register.
        using Addr = Register::Address<0x14001004,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram1lu{    ///<CSE PRAM1LU register.
        using Addr = Register::Address<0x14001005,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram1hl{    ///<CSE PRAM1HL register.
        using Addr = Register::Address<0x14001006,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram1hu{    ///<CSE PRAM1HU register.
        using Addr = Register::Address<0x14001007,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram2{    ///<CSE PRAM 2 Register
        using Addr = Register::Address<0x14001008,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram2ll{    ///<CSE PRAM2LL register.
        using Addr = Register::Address<0x14001008,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram2lu{    ///<CSE PRAM2LU register.
        using Addr = Register::Address<0x14001009,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram2hl{    ///<CSE PRAM2HL register.
        using Addr = Register::Address<0x1400100a,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram2hu{    ///<CSE PRAM2HU register.
        using Addr = Register::Address<0x1400100b,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram3{    ///<CSE PRAM 3 Register
        using Addr = Register::Address<0x1400100c,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram3ll{    ///<CSE PRAM3LL register.
        using Addr = Register::Address<0x1400100c,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram3lu{    ///<CSE PRAM3LU register.
        using Addr = Register::Address<0x1400100d,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram3hl{    ///<CSE PRAM3HL register.
        using Addr = Register::Address<0x1400100e,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram3hu{    ///<CSE PRAM3HU register.
        using Addr = Register::Address<0x1400100f,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram4{    ///<CSE PRAM 4 Register
        using Addr = Register::Address<0x14001010,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram4ll{    ///<CSE PRAM4LL register.
        using Addr = Register::Address<0x14001010,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram4lu{    ///<CSE PRAM4LU register.
        using Addr = Register::Address<0x14001011,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram4hl{    ///<CSE PRAM4HL register.
        using Addr = Register::Address<0x14001012,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram4hu{    ///<CSE PRAM4HU register.
        using Addr = Register::Address<0x14001013,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram5{    ///<CSE PRAM 5 Register
        using Addr = Register::Address<0x14001014,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram5ll{    ///<CSE PRAM5LL register.
        using Addr = Register::Address<0x14001014,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram5lu{    ///<CSE PRAM5LU register.
        using Addr = Register::Address<0x14001015,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram5hl{    ///<CSE PRAM5HL register.
        using Addr = Register::Address<0x14001016,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram5hu{    ///<CSE PRAM5HU register.
        using Addr = Register::Address<0x14001017,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram6{    ///<CSE PRAM 6 Register
        using Addr = Register::Address<0x14001018,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram6ll{    ///<CSE PRAM6LL register.
        using Addr = Register::Address<0x14001018,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram6lu{    ///<CSE PRAM6LU register.
        using Addr = Register::Address<0x14001019,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram6hl{    ///<CSE PRAM6HL register.
        using Addr = Register::Address<0x1400101a,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram6hu{    ///<CSE PRAM6HU register.
        using Addr = Register::Address<0x1400101b,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram7{    ///<CSE PRAM 7 Register
        using Addr = Register::Address<0x1400101c,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram7ll{    ///<CSE PRAM7LL register.
        using Addr = Register::Address<0x1400101c,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram7lu{    ///<CSE PRAM7LU register.
        using Addr = Register::Address<0x1400101d,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram7hl{    ///<CSE PRAM7HL register.
        using Addr = Register::Address<0x1400101e,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram7hu{    ///<CSE PRAM7HU register.
        using Addr = Register::Address<0x1400101f,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram8{    ///<CSE PRAM 8 Register
        using Addr = Register::Address<0x14001020,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram8ll{    ///<CSE PRAM8LL register.
        using Addr = Register::Address<0x14001020,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram8lu{    ///<CSE PRAM8LU register.
        using Addr = Register::Address<0x14001021,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram8hl{    ///<CSE PRAM8HL register.
        using Addr = Register::Address<0x14001022,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram8hu{    ///<CSE PRAM8HU register.
        using Addr = Register::Address<0x14001023,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram9{    ///<CSE PRAM 9 Register
        using Addr = Register::Address<0x14001024,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram9ll{    ///<CSE PRAM9LL register.
        using Addr = Register::Address<0x14001024,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram9lu{    ///<CSE PRAM9LU register.
        using Addr = Register::Address<0x14001025,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram9hl{    ///<CSE PRAM9HL register.
        using Addr = Register::Address<0x14001026,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram9hu{    ///<CSE PRAM9HU register.
        using Addr = Register::Address<0x14001027,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram10{    ///<CSE PRAM 10 Register
        using Addr = Register::Address<0x14001028,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram10ll{    ///<CSE PRAM10LL register.
        using Addr = Register::Address<0x14001028,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram10lu{    ///<CSE PRAM10LU register.
        using Addr = Register::Address<0x14001029,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram10hl{    ///<CSE PRAM10HL register.
        using Addr = Register::Address<0x1400102a,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram10hu{    ///<CSE PRAM10HU register.
        using Addr = Register::Address<0x1400102b,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram11{    ///<CSE PRAM 11 Register
        using Addr = Register::Address<0x1400102c,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram11ll{    ///<CSE PRAM11LL register.
        using Addr = Register::Address<0x1400102c,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram11lu{    ///<CSE PRAM11LU register.
        using Addr = Register::Address<0x1400102d,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram11hl{    ///<CSE PRAM11HL register.
        using Addr = Register::Address<0x1400102e,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram11hu{    ///<CSE PRAM11HU register.
        using Addr = Register::Address<0x1400102f,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram12{    ///<CSE PRAM 12 Register
        using Addr = Register::Address<0x14001030,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram12ll{    ///<CSE PRAM12LL register.
        using Addr = Register::Address<0x14001030,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram12lu{    ///<CSE PRAM12LU register.
        using Addr = Register::Address<0x14001031,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram12hl{    ///<CSE PRAM12HL register.
        using Addr = Register::Address<0x14001032,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram12hu{    ///<CSE PRAM12HU register.
        using Addr = Register::Address<0x14001033,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram13{    ///<CSE PRAM 13 Register
        using Addr = Register::Address<0x14001034,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram13ll{    ///<CSE PRAM13LL register.
        using Addr = Register::Address<0x14001034,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram13lu{    ///<CSE PRAM13LU register.
        using Addr = Register::Address<0x14001035,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram13hl{    ///<CSE PRAM13HL register.
        using Addr = Register::Address<0x14001036,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram13hu{    ///<CSE PRAM13HU register.
        using Addr = Register::Address<0x14001037,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram14{    ///<CSE PRAM 14 Register
        using Addr = Register::Address<0x14001038,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram14ll{    ///<CSE PRAM14LL register.
        using Addr = Register::Address<0x14001038,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram14lu{    ///<CSE PRAM14LU register.
        using Addr = Register::Address<0x14001039,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram14hl{    ///<CSE PRAM14HL register.
        using Addr = Register::Address<0x1400103a,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram14hu{    ///<CSE PRAM14HU register.
        using Addr = Register::Address<0x1400103b,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram15{    ///<CSE PRAM 15 Register
        using Addr = Register::Address<0x1400103c,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram15ll{    ///<CSE PRAM15LL register.
        using Addr = Register::Address<0x1400103c,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram15lu{    ///<CSE PRAM15LU register.
        using Addr = Register::Address<0x1400103d,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram15hl{    ///<CSE PRAM15HL register.
        using Addr = Register::Address<0x1400103e,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram15hu{    ///<CSE PRAM15HU register.
        using Addr = Register::Address<0x1400103f,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram16{    ///<CSE PRAM 16 Register
        using Addr = Register::Address<0x14001040,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram16ll{    ///<CSE PRAM16LL register.
        using Addr = Register::Address<0x14001040,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram16lu{    ///<CSE PRAM16LU register.
        using Addr = Register::Address<0x14001041,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram16hl{    ///<CSE PRAM16HL register.
        using Addr = Register::Address<0x14001042,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram16hu{    ///<CSE PRAM16HU register.
        using Addr = Register::Address<0x14001043,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram17{    ///<CSE PRAM 17 Register
        using Addr = Register::Address<0x14001044,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram17ll{    ///<CSE PRAM17LL register.
        using Addr = Register::Address<0x14001044,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram17lu{    ///<CSE PRAM17LU register.
        using Addr = Register::Address<0x14001045,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram17hl{    ///<CSE PRAM17HL register.
        using Addr = Register::Address<0x14001046,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram17hu{    ///<CSE PRAM17HU register.
        using Addr = Register::Address<0x14001047,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram18{    ///<CSE PRAM 18 Register
        using Addr = Register::Address<0x14001048,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram18ll{    ///<CSE PRAM18LL register.
        using Addr = Register::Address<0x14001048,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram18lu{    ///<CSE PRAM18LU register.
        using Addr = Register::Address<0x14001049,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram18hl{    ///<CSE PRAM18HL register.
        using Addr = Register::Address<0x1400104a,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram18hu{    ///<CSE PRAM18HU register.
        using Addr = Register::Address<0x1400104b,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram19{    ///<CSE PRAM 19 Register
        using Addr = Register::Address<0x1400104c,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram19ll{    ///<CSE PRAM19LL register.
        using Addr = Register::Address<0x1400104c,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram19lu{    ///<CSE PRAM19LU register.
        using Addr = Register::Address<0x1400104d,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram19hl{    ///<CSE PRAM19HL register.
        using Addr = Register::Address<0x1400104e,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram19hu{    ///<CSE PRAM19HU register.
        using Addr = Register::Address<0x1400104f,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram20{    ///<CSE PRAM 20 Register
        using Addr = Register::Address<0x14001050,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram20ll{    ///<CSE PRAM20LL register.
        using Addr = Register::Address<0x14001050,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram20lu{    ///<CSE PRAM20LU register.
        using Addr = Register::Address<0x14001051,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram20hl{    ///<CSE PRAM20HL register.
        using Addr = Register::Address<0x14001052,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram20hu{    ///<CSE PRAM20HU register.
        using Addr = Register::Address<0x14001053,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram21{    ///<CSE PRAM 21 Register
        using Addr = Register::Address<0x14001054,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram21ll{    ///<CSE PRAM21LL register.
        using Addr = Register::Address<0x14001054,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram21lu{    ///<CSE PRAM21LU register.
        using Addr = Register::Address<0x14001055,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram21hl{    ///<CSE PRAM21HL register.
        using Addr = Register::Address<0x14001056,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram21hu{    ///<CSE PRAM21HU register.
        using Addr = Register::Address<0x14001057,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram22{    ///<CSE PRAM 22 Register
        using Addr = Register::Address<0x14001058,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram22ll{    ///<CSE PRAM22LL register.
        using Addr = Register::Address<0x14001058,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram22lu{    ///<CSE PRAM22LU register.
        using Addr = Register::Address<0x14001059,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram22hl{    ///<CSE PRAM22HL register.
        using Addr = Register::Address<0x1400105a,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram22hu{    ///<CSE PRAM22HU register.
        using Addr = Register::Address<0x1400105b,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram23{    ///<CSE PRAM 23 Register
        using Addr = Register::Address<0x1400105c,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram23ll{    ///<CSE PRAM23LL register.
        using Addr = Register::Address<0x1400105c,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram23lu{    ///<CSE PRAM23LU register.
        using Addr = Register::Address<0x1400105d,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram23hl{    ///<CSE PRAM23HL register.
        using Addr = Register::Address<0x1400105e,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram23hu{    ///<CSE PRAM23HU register.
        using Addr = Register::Address<0x1400105f,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram24{    ///<CSE PRAM 24 Register
        using Addr = Register::Address<0x14001060,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram24ll{    ///<CSE PRAM24LL register.
        using Addr = Register::Address<0x14001060,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram24lu{    ///<CSE PRAM24LU register.
        using Addr = Register::Address<0x14001061,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram24hl{    ///<CSE PRAM24HL register.
        using Addr = Register::Address<0x14001062,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram24hu{    ///<CSE PRAM24HU register.
        using Addr = Register::Address<0x14001063,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram25{    ///<CSE PRAM 25 Register
        using Addr = Register::Address<0x14001064,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram25ll{    ///<CSE PRAM25LL register.
        using Addr = Register::Address<0x14001064,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram25lu{    ///<CSE PRAM25LU register.
        using Addr = Register::Address<0x14001065,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram25hl{    ///<CSE PRAM25HL register.
        using Addr = Register::Address<0x14001066,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram25hu{    ///<CSE PRAM25HU register.
        using Addr = Register::Address<0x14001067,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram26{    ///<CSE PRAM 26 Register
        using Addr = Register::Address<0x14001068,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram26ll{    ///<CSE PRAM26LL register.
        using Addr = Register::Address<0x14001068,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram26lu{    ///<CSE PRAM26LU register.
        using Addr = Register::Address<0x14001069,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram26hl{    ///<CSE PRAM26HL register.
        using Addr = Register::Address<0x1400106a,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram26hu{    ///<CSE PRAM26HU register.
        using Addr = Register::Address<0x1400106b,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram27{    ///<CSE PRAM 27 Register
        using Addr = Register::Address<0x1400106c,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram27ll{    ///<CSE PRAM27LL register.
        using Addr = Register::Address<0x1400106c,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram27lu{    ///<CSE PRAM27LU register.
        using Addr = Register::Address<0x1400106d,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram27hl{    ///<CSE PRAM27HL register.
        using Addr = Register::Address<0x1400106e,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram27hu{    ///<CSE PRAM27HU register.
        using Addr = Register::Address<0x1400106f,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram28{    ///<CSE PRAM 28 Register
        using Addr = Register::Address<0x14001070,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram28ll{    ///<CSE PRAM28LL register.
        using Addr = Register::Address<0x14001070,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram28lu{    ///<CSE PRAM28LU register.
        using Addr = Register::Address<0x14001071,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram28hl{    ///<CSE PRAM28HL register.
        using Addr = Register::Address<0x14001072,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram28hu{    ///<CSE PRAM28HU register.
        using Addr = Register::Address<0x14001073,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram29{    ///<CSE PRAM 29 Register
        using Addr = Register::Address<0x14001074,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram29ll{    ///<CSE PRAM29LL register.
        using Addr = Register::Address<0x14001074,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram29lu{    ///<CSE PRAM29LU register.
        using Addr = Register::Address<0x14001075,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram29hl{    ///<CSE PRAM29HL register.
        using Addr = Register::Address<0x14001076,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram29hu{    ///<CSE PRAM29HU register.
        using Addr = Register::Address<0x14001077,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram30{    ///<CSE PRAM 30 Register
        using Addr = Register::Address<0x14001078,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram30ll{    ///<CSE PRAM30LL register.
        using Addr = Register::Address<0x14001078,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram30lu{    ///<CSE PRAM30LU register.
        using Addr = Register::Address<0x14001079,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram30hl{    ///<CSE PRAM30HL register.
        using Addr = Register::Address<0x1400107a,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram30hu{    ///<CSE PRAM30HU register.
        using Addr = Register::Address<0x1400107b,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
    namespace CsePramEmbeddedram31{    ///<CSE PRAM 31 Register
        using Addr = Register::Address<0x1400107c,0x00000000,0x00000000,unsigned>;
        ///Data byte 3 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> byte3{}; 
        ///Data byte 2 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> byte2{}; 
        ///Data byte 1 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> byte1{}; 
        ///Data byte 0 of Rx/Tx frame.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> byte0{}; 
    }
    namespace CsePramEmbeddedram31ll{    ///<CSE PRAM31LL register.
        using Addr = Register::Address<0x1400107c,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LL stores the first 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLl{}; 
    }
    namespace CsePramEmbeddedram31lu{    ///<CSE PRAM31LU register.
        using Addr = Register::Address<0x1400107d,0xffffff00,0x00000000,unsigned char>;
        ///RAM_LU stores the second 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramLu{}; 
    }
    namespace CsePramEmbeddedram31hl{    ///<CSE PRAM31HL register.
        using Addr = Register::Address<0x1400107e,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HL stores the third 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHl{}; 
    }
    namespace CsePramEmbeddedram31hu{    ///<CSE PRAM31HU register.
        using Addr = Register::Address<0x1400107f,0xffffff00,0x00000000,unsigned char>;
        ///RAM_HU stores the fourth 8 bits of the 32 bit CRC
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> ramHu{}; 
    }
}
