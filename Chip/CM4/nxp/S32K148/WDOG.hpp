#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//Watchdog timer
    namespace WdogCs{    ///<Watchdog Control and Status Register
        using Addr = Register::Address<0x40052000,0xffff0000,0x00000000,unsigned>;
        ///Stop Enable
        enum class StopVal : unsigned {
            v0=0x00000000,     ///<Watchdog disabled in chip stop mode.
            v1=0x00000001,     ///<Watchdog enabled in chip stop mode.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,StopVal> stop{}; 
        namespace StopValC{
            constexpr Register::FieldValue<decltype(stop)::Type,StopVal::v0> v0{};
            constexpr Register::FieldValue<decltype(stop)::Type,StopVal::v1> v1{};
        }
        ///Wait Enable
        enum class WaitVal : unsigned {
            v0=0x00000000,     ///<Watchdog disabled in chip wait mode.
            v1=0x00000001,     ///<Watchdog enabled in chip wait mode.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,WaitVal> wait{}; 
        namespace WaitValC{
            constexpr Register::FieldValue<decltype(wait)::Type,WaitVal::v0> v0{};
            constexpr Register::FieldValue<decltype(wait)::Type,WaitVal::v1> v1{};
        }
        ///Debug Enable
        enum class DbgVal : unsigned {
            v0=0x00000000,     ///<Watchdog disabled in chip debug mode.
            v1=0x00000001,     ///<Watchdog enabled in chip debug mode.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,DbgVal> dbg{}; 
        namespace DbgValC{
            constexpr Register::FieldValue<decltype(dbg)::Type,DbgVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dbg)::Type,DbgVal::v1> v1{};
        }
        ///Watchdog Test
        enum class TstVal : unsigned {
            v00=0x00000000,     ///<Watchdog test mode disabled.
            v01=0x00000001,     ///<Watchdog user mode enabled. (Watchdog test mode disabled.) After testing the watchdog, software should use this setting to indicate that the watchdog is functioning normally in user mode.
            v10=0x00000002,     ///<Watchdog test mode enabled, only the low byte is used. CNT[CNTLOW] is compared with TOVAL[TOVALLOW].
            v11=0x00000003,     ///<Watchdog test mode enabled, only the high byte is used. CNT[CNTHIGH] is compared with TOVAL[TOVALHIGH].
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,3),Register::ReadWriteAccess,TstVal> tst{}; 
        namespace TstValC{
            constexpr Register::FieldValue<decltype(tst)::Type,TstVal::v00> v00{};
            constexpr Register::FieldValue<decltype(tst)::Type,TstVal::v01> v01{};
            constexpr Register::FieldValue<decltype(tst)::Type,TstVal::v10> v10{};
            constexpr Register::FieldValue<decltype(tst)::Type,TstVal::v11> v11{};
        }
        ///Allow updates
        enum class UpdateVal : unsigned {
            v0=0x00000000,     ///<Updates not allowed. After the initial configuration, the watchdog cannot be later modified without forcing a reset.
            v1=0x00000001,     ///<Updates allowed. Software can modify the watchdog configuration registers within 8'd128 bus clocks after performing the unlock write sequence.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,UpdateVal> update{}; 
        namespace UpdateValC{
            constexpr Register::FieldValue<decltype(update)::Type,UpdateVal::v0> v0{};
            constexpr Register::FieldValue<decltype(update)::Type,UpdateVal::v1> v1{};
        }
        ///Watchdog Interrupt
        enum class Int_Val : unsigned {
            v0=0x00000000,     ///<Watchdog interrupts are disabled. Watchdog resets are not delayed.
            v1=0x00000001,     ///<Watchdog interrupts are enabled. Watchdog resets are delayed by 8'd128 bus clocks from the interrupt vector fetch.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,Int_Val> int_{}; 
        namespace Int_ValC{
            constexpr Register::FieldValue<decltype(int_)::Type,Int_Val::v0> v0{};
            constexpr Register::FieldValue<decltype(int_)::Type,Int_Val::v1> v1{};
        }
        ///Watchdog Enable
        enum class EnVal : unsigned {
            v0=0x00000000,     ///<Watchdog disabled.
            v1=0x00000001,     ///<Watchdog enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::ReadWriteAccess,EnVal> en{}; 
        namespace EnValC{
            constexpr Register::FieldValue<decltype(en)::Type,EnVal::v0> v0{};
            constexpr Register::FieldValue<decltype(en)::Type,EnVal::v1> v1{};
        }
        ///Watchdog Clock
        enum class ClkVal : unsigned {
            v00=0x00000000,     ///<Bus clock
            v01=0x00000001,     ///<LPO clock
            v10=0x00000002,     ///<INTCLK (internal clock)
            v11=0x00000003,     ///<ERCLK (external reference clock)
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,ClkVal> clk{}; 
        namespace ClkValC{
            constexpr Register::FieldValue<decltype(clk)::Type,ClkVal::v00> v00{};
            constexpr Register::FieldValue<decltype(clk)::Type,ClkVal::v01> v01{};
            constexpr Register::FieldValue<decltype(clk)::Type,ClkVal::v10> v10{};
            constexpr Register::FieldValue<decltype(clk)::Type,ClkVal::v11> v11{};
        }
        ///Reconfiguration Success
        enum class RcsVal : unsigned {
            v0=0x00000000,     ///<Reconfiguring WDOG.
            v1=0x00000001,     ///<Reconfiguration is successful.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,10),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RcsVal> rcs{}; 
        namespace RcsValC{
            constexpr Register::FieldValue<decltype(rcs)::Type,RcsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rcs)::Type,RcsVal::v1> v1{};
        }
        ///Unlock status
        enum class UlkVal : unsigned {
            v0=0x00000000,     ///<WDOG is locked.
            v1=0x00000001,     ///<WDOG is unlocked.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,UlkVal> ulk{}; 
        namespace UlkValC{
            constexpr Register::FieldValue<decltype(ulk)::Type,UlkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ulk)::Type,UlkVal::v1> v1{};
        }
        ///Watchdog prescaler
        enum class PresVal : unsigned {
            v0=0x00000000,     ///<256 prescaler disabled.
            v1=0x00000001,     ///<256 prescaler enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,12),Register::ReadWriteAccess,PresVal> pres{}; 
        namespace PresValC{
            constexpr Register::FieldValue<decltype(pres)::Type,PresVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pres)::Type,PresVal::v1> v1{};
        }
        ///Enables or disables WDOG support for 32-bit (otherwise 16-bit or 8-bit) refresh/unlock command write words
        enum class Cmd32enVal : unsigned {
            v0=0x00000000,     ///<Disables support for 32-bit refresh/unlock command write words. Only 16-bit or 8-bit is supported.
            v1=0x00000001,     ///<Enables support for 32-bit refresh/unlock command write words. 16-bit or 8-bit is NOT supported.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,13),Register::ReadWriteAccess,Cmd32enVal> cmd32en{}; 
        namespace Cmd32enValC{
            constexpr Register::FieldValue<decltype(cmd32en)::Type,Cmd32enVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cmd32en)::Type,Cmd32enVal::v1> v1{};
        }
        ///Watchdog Interrupt Flag
        enum class FlgVal : unsigned {
            v0=0x00000000,     ///<No interrupt occurred.
            v1=0x00000001,     ///<An interrupt occurred.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,14),Register::ReadWriteAccess,FlgVal> flg{}; 
        namespace FlgValC{
            constexpr Register::FieldValue<decltype(flg)::Type,FlgVal::v0> v0{};
            constexpr Register::FieldValue<decltype(flg)::Type,FlgVal::v1> v1{};
        }
        ///Watchdog Window
        enum class WinVal : unsigned {
            v0=0x00000000,     ///<Window mode disabled.
            v1=0x00000001,     ///<Window mode enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,WinVal> win{}; 
        namespace WinValC{
            constexpr Register::FieldValue<decltype(win)::Type,WinVal::v0> v0{};
            constexpr Register::FieldValue<decltype(win)::Type,WinVal::v1> v1{};
        }
    }
    namespace WdogCnt{    ///<Watchdog Counter Register
        using Addr = Register::Address<0x40052004,0xffff0000,0x00000000,unsigned>;
        ///Low byte of the Watchdog Counter
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> cntlow{}; 
        ///High byte of the Watchdog Counter
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> cnthigh{}; 
    }
    namespace WdogToval{    ///<Watchdog Timeout Value Register
        using Addr = Register::Address<0x40052008,0xffff0000,0x00000000,unsigned>;
        ///Low byte of the timeout value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> tovallow{}; 
        ///High byte of the timeout value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> tovalhigh{}; 
    }
    namespace WdogWin{    ///<Watchdog Window Register
        using Addr = Register::Address<0x4005200c,0xffff0000,0x00000000,unsigned>;
        ///Low byte of Watchdog Window
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> winlow{}; 
        ///High byte of Watchdog Window
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> winhigh{}; 
    }
}
