#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//PMC
    namespace PmcLvdsc1{    ///<Low Voltage Detect Status and Control 1 Register
        using Addr = Register::Address<0x4007d000,0xffffff0f,0x00000000,unsigned char>;
        ///Low Voltage Detect Reset Enable
        enum class LvdreVal : unsigned {
            v0=0x00000000,     ///<No system resets on low voltage detect events.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,4),Register::ReadWriteAccess,LvdreVal> lvdre{}; 
        namespace LvdreValC{
            constexpr Register::FieldValue<decltype(lvdre)::Type,LvdreVal::v0> v0{};
        }
        ///Low Voltage Detect Interrupt Enable
        enum class LvdieVal : unsigned {
            v0=0x00000000,     ///<Hardware interrupt disabled (use polling)
            v1=0x00000001,     ///<Request a hardware interrupt when LVDF = 1
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,LvdieVal> lvdie{}; 
        namespace LvdieValC{
            constexpr Register::FieldValue<decltype(lvdie)::Type,LvdieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lvdie)::Type,LvdieVal::v1> v1{};
        }
        ///Low Voltage Detect Acknowledge
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> lvdack{}; 
        ///Low Voltage Detect Flag
        enum class LvdfVal : unsigned {
            v0=0x00000000,     ///<Low-voltage event not detected
            v1=0x00000001,     ///<Low-voltage event detected
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,LvdfVal> lvdf{}; 
        namespace LvdfValC{
            constexpr Register::FieldValue<decltype(lvdf)::Type,LvdfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lvdf)::Type,LvdfVal::v1> v1{};
        }
    }
    namespace PmcLvdsc2{    ///<Low Voltage Detect Status and Control 2 Register
        using Addr = Register::Address<0x4007d001,0xffffff1f,0x00000000,unsigned char>;
        ///Low-Voltage Warning Interrupt Enable
        enum class LvwieVal : unsigned {
            v0=0x00000000,     ///<Hardware interrupt disabled (use polling)
            v1=0x00000001,     ///<Request a hardware interrupt when LVWF=1
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,LvwieVal> lvwie{}; 
        namespace LvwieValC{
            constexpr Register::FieldValue<decltype(lvwie)::Type,LvwieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lvwie)::Type,LvwieVal::v1> v1{};
        }
        ///Low-Voltage Warning Acknowledge
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> lvwack{}; 
        ///Low-Voltage Warning Flag
        enum class LvwfVal : unsigned {
            v0=0x00000000,     ///<Low-voltage warning event not detected
            v1=0x00000001,     ///<Low-voltage warning event detected
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,LvwfVal> lvwf{}; 
        namespace LvwfValC{
            constexpr Register::FieldValue<decltype(lvwf)::Type,LvwfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lvwf)::Type,LvwfVal::v1> v1{};
        }
    }
    namespace PmcRegsc{    ///<Regulator Status and Control Register
        using Addr = Register::Address<0x4007d002,0xffffff38,0x00000000,unsigned char>;
        ///Bias Enable Bit
        enum class BiasenVal : unsigned {
            v0=0x00000000,     ///<Biasing disabled, core logic can run in full performance
            v1=0x00000001,     ///<Biasing enabled, core logic is slower and there are restrictions in allowed system clock speed (see Data Sheet for details)
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,BiasenVal> biasen{}; 
        namespace BiasenValC{
            constexpr Register::FieldValue<decltype(biasen)::Type,BiasenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(biasen)::Type,BiasenVal::v1> v1{};
        }
        ///Clock Bias Disable Bit
        enum class ClkbiasdisVal : unsigned {
            v0=0x00000000,     ///<No effect
            v1=0x00000001,     ///<In VLPS mode, the bias currents and reference voltages for the following clock modules are disabled: SIRC, FIRC, PLL. (if available on device)
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,ClkbiasdisVal> clkbiasdis{}; 
        namespace ClkbiasdisValC{
            constexpr Register::FieldValue<decltype(clkbiasdis)::Type,ClkbiasdisVal::v0> v0{};
            constexpr Register::FieldValue<decltype(clkbiasdis)::Type,ClkbiasdisVal::v1> v1{};
        }
        ///Regulator in Full Performance Mode Status Bit
        enum class RegfpmVal : unsigned {
            v0=0x00000000,     ///<Regulator is in low power mode or transition to/from
            v1=0x00000001,     ///<Regulator is in full performance mode
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RegfpmVal> regfpm{}; 
        namespace RegfpmValC{
            constexpr Register::FieldValue<decltype(regfpm)::Type,RegfpmVal::v0> v0{};
            constexpr Register::FieldValue<decltype(regfpm)::Type,RegfpmVal::v1> v1{};
        }
        ///LPO Status Bit
        enum class LpostatVal : unsigned {
            v0=0x00000000,     ///<Low power oscillator in low phase
            v1=0x00000001,     ///<Low power oscillator in high phase
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,LpostatVal> lpostat{}; 
        namespace LpostatValC{
            constexpr Register::FieldValue<decltype(lpostat)::Type,LpostatVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lpostat)::Type,LpostatVal::v1> v1{};
        }
        ///LPO Disable Bit
        enum class LpodisVal : unsigned {
            v0=0x00000000,     ///<Low power oscillator enabled
            v1=0x00000001,     ///<Low power oscillator disabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::ReadWriteAccess,LpodisVal> lpodis{}; 
        namespace LpodisValC{
            constexpr Register::FieldValue<decltype(lpodis)::Type,LpodisVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lpodis)::Type,LpodisVal::v1> v1{};
        }
    }
    namespace PmcLpotrim{    ///<Low Power Oscillator Trim Register
        using Addr = Register::Address<0x4007d004,0xffffffe0,0x00000000,unsigned char>;
        ///LPO trimming bits
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,0),Register::ReadWriteAccess,unsigned> lpotrim{}; 
    }
}
