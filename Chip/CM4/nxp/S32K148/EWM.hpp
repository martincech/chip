#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//External Watchdog Monitor
    namespace EwmCtrl{    ///<Control Register
        using Addr = Register::Address<0x40061000,0xfffffff0,0x00000000,unsigned char>;
        ///EWM enable.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::Access<Register::AccessType::writeOnce,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> ewmen{}; 
        ///EWM_in's Assertion State Select.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::Access<Register::AccessType::writeOnce,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> assin{}; 
        ///Input Enable.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::Access<Register::AccessType::writeOnce,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> inen{}; 
        ///Interrupt Enable.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,unsigned> inten{}; 
    }
    namespace EwmServ{    ///<Service Register
        using Addr = Register::Address<0x40061001,0xffffff00,0x00000000,unsigned char>;
        ///SERVICE
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> service{}; 
    }
    namespace EwmCmpl{    ///<Compare Low Register
        using Addr = Register::Address<0x40061002,0xffffff00,0x00000000,unsigned char>;
        ///COMPAREL
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::Access<Register::AccessType::writeOnce,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> comparel{}; 
    }
    namespace EwmCmph{    ///<Compare High Register
        using Addr = Register::Address<0x40061003,0xffffff00,0x00000000,unsigned char>;
        ///COMPAREH
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::Access<Register::AccessType::writeOnce,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> compareh{}; 
    }
    namespace EwmClkprescaler{    ///<Clock Prescaler Register
        using Addr = Register::Address<0x40061005,0xffffff00,0x00000000,unsigned char>;
        ///CLK_DIV
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::Access<Register::AccessType::writeOnce,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> clkDiv{}; 
    }
}
