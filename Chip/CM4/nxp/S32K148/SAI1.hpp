#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//Synchronous Serial Interface
    namespace Sai1SaiVerid{    ///<Version ID Register
        using Addr = Register::Address<0x40055000,0x00000000,0x00000000,unsigned>;
        ///Feature Specification Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> feature{}; 
        ///Minor Version Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> minor{}; 
        ///Major Version Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> major{}; 
    }
    namespace Sai1SaiParam{    ///<Parameter Register
        using Addr = Register::Address<0x40055004,0xfff0f0f0,0x00000000,unsigned>;
        ///Number of Datalines
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dataline{}; 
        ///FIFO Size
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> fifo{}; 
        ///Frame Size
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> frame{}; 
    }
    namespace Sai1SaiTcsr{    ///<SAI Transmit Control Register
        using Addr = Register::Address<0x40055008,0x4ce0e0fc,0x00000000,unsigned>;
        ///FIFO Request DMA Enable
        enum class FrdeVal : unsigned {
            v0=0x00000000,     ///<Disables the DMA request.
            v1=0x00000001,     ///<Enables the DMA request.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,FrdeVal> frde{}; 
        namespace FrdeValC{
            constexpr Register::FieldValue<decltype(frde)::Type,FrdeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(frde)::Type,FrdeVal::v1> v1{};
        }
        ///FIFO Warning DMA Enable
        enum class FwdeVal : unsigned {
            v0=0x00000000,     ///<Disables the DMA request.
            v1=0x00000001,     ///<Enables the DMA request.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,FwdeVal> fwde{}; 
        namespace FwdeValC{
            constexpr Register::FieldValue<decltype(fwde)::Type,FwdeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fwde)::Type,FwdeVal::v1> v1{};
        }
        ///FIFO Request Interrupt Enable
        enum class FrieVal : unsigned {
            v0=0x00000000,     ///<Disables the interrupt.
            v1=0x00000001,     ///<Enables the interrupt.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::ReadWriteAccess,FrieVal> frie{}; 
        namespace FrieValC{
            constexpr Register::FieldValue<decltype(frie)::Type,FrieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(frie)::Type,FrieVal::v1> v1{};
        }
        ///FIFO Warning Interrupt Enable
        enum class FwieVal : unsigned {
            v0=0x00000000,     ///<Disables the interrupt.
            v1=0x00000001,     ///<Enables the interrupt.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,9),Register::ReadWriteAccess,FwieVal> fwie{}; 
        namespace FwieValC{
            constexpr Register::FieldValue<decltype(fwie)::Type,FwieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fwie)::Type,FwieVal::v1> v1{};
        }
        ///FIFO Error Interrupt Enable
        enum class FeieVal : unsigned {
            v0=0x00000000,     ///<Disables the interrupt.
            v1=0x00000001,     ///<Enables the interrupt.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,10),Register::ReadWriteAccess,FeieVal> feie{}; 
        namespace FeieValC{
            constexpr Register::FieldValue<decltype(feie)::Type,FeieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(feie)::Type,FeieVal::v1> v1{};
        }
        ///Sync Error Interrupt Enable
        enum class SeieVal : unsigned {
            v0=0x00000000,     ///<Disables interrupt.
            v1=0x00000001,     ///<Enables interrupt.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,SeieVal> seie{}; 
        namespace SeieValC{
            constexpr Register::FieldValue<decltype(seie)::Type,SeieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(seie)::Type,SeieVal::v1> v1{};
        }
        ///Word Start Interrupt Enable
        enum class WsieVal : unsigned {
            v0=0x00000000,     ///<Disables interrupt.
            v1=0x00000001,     ///<Enables interrupt.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,12),Register::ReadWriteAccess,WsieVal> wsie{}; 
        namespace WsieValC{
            constexpr Register::FieldValue<decltype(wsie)::Type,WsieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(wsie)::Type,WsieVal::v1> v1{};
        }
        ///FIFO Request Flag
        enum class FrfVal : unsigned {
            v0=0x00000000,     ///<Transmit FIFO watermark has not been reached.
            v1=0x00000001,     ///<Transmit FIFO watermark has been reached.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,FrfVal> frf{}; 
        namespace FrfValC{
            constexpr Register::FieldValue<decltype(frf)::Type,FrfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(frf)::Type,FrfVal::v1> v1{};
        }
        ///FIFO Warning Flag
        enum class FwfVal : unsigned {
            v0=0x00000000,     ///<No enabled transmit FIFO is empty.
            v1=0x00000001,     ///<Enabled transmit FIFO is empty.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,17),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,FwfVal> fwf{}; 
        namespace FwfValC{
            constexpr Register::FieldValue<decltype(fwf)::Type,FwfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fwf)::Type,FwfVal::v1> v1{};
        }
        ///FIFO Error Flag
        enum class FefVal : unsigned {
            v0=0x00000000,     ///<Transmit underrun not detected.
            v1=0x00000001,     ///<Transmit underrun detected.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(18,18),Register::ReadWriteAccess,FefVal> fef{}; 
        namespace FefValC{
            constexpr Register::FieldValue<decltype(fef)::Type,FefVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fef)::Type,FefVal::v1> v1{};
        }
        ///Sync Error Flag
        enum class SefVal : unsigned {
            v0=0x00000000,     ///<Sync error not detected.
            v1=0x00000001,     ///<Frame sync error detected.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,19),Register::ReadWriteAccess,SefVal> sef{}; 
        namespace SefValC{
            constexpr Register::FieldValue<decltype(sef)::Type,SefVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sef)::Type,SefVal::v1> v1{};
        }
        ///Word Start Flag
        enum class WsfVal : unsigned {
            v0=0x00000000,     ///<Start of word not detected.
            v1=0x00000001,     ///<Start of word detected.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,20),Register::ReadWriteAccess,WsfVal> wsf{}; 
        namespace WsfValC{
            constexpr Register::FieldValue<decltype(wsf)::Type,WsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(wsf)::Type,WsfVal::v1> v1{};
        }
        ///Software Reset
        enum class SrVal : unsigned {
            v0=0x00000000,     ///<No effect.
            v1=0x00000001,     ///<Software reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,SrVal> sr{}; 
        namespace SrValC{
            constexpr Register::FieldValue<decltype(sr)::Type,SrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sr)::Type,SrVal::v1> v1{};
        }
        ///FIFO Reset
        enum class FrVal : unsigned {
            v0=0x00000000,     ///<No effect.
            v1=0x00000001,     ///<FIFO reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,FrVal> fr{}; 
        namespace FrValC{
            constexpr Register::FieldValue<decltype(fr)::Type,FrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fr)::Type,FrVal::v1> v1{};
        }
        ///Bit Clock Enable
        enum class BceVal : unsigned {
            v0=0x00000000,     ///<Transmit bit clock is disabled.
            v1=0x00000001,     ///<Transmit bit clock is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,BceVal> bce{}; 
        namespace BceValC{
            constexpr Register::FieldValue<decltype(bce)::Type,BceVal::v0> v0{};
            constexpr Register::FieldValue<decltype(bce)::Type,BceVal::v1> v1{};
        }
        ///Debug Enable
        enum class DbgeVal : unsigned {
            v0=0x00000000,     ///<Transmitter is disabled in Debug mode, after completing the current frame.
            v1=0x00000001,     ///<Transmitter is enabled in Debug mode.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,DbgeVal> dbge{}; 
        namespace DbgeValC{
            constexpr Register::FieldValue<decltype(dbge)::Type,DbgeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dbge)::Type,DbgeVal::v1> v1{};
        }
        ///Transmitter Enable
        enum class TeVal : unsigned {
            v0=0x00000000,     ///<Transmitter is disabled.
            v1=0x00000001,     ///<Transmitter is enabled, or transmitter has been disabled and has not yet reached end of frame.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,TeVal> te{}; 
        namespace TeValC{
            constexpr Register::FieldValue<decltype(te)::Type,TeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(te)::Type,TeVal::v1> v1{};
        }
    }
    namespace Sai1SaiTcr1{    ///<SAI Transmit Configuration 1 Register
        using Addr = Register::Address<0x4005500c,0xfffffff8,0x00000000,unsigned>;
        ///Transmit FIFO Watermark
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> tfw{}; 
    }
    namespace Sai1SaiTcr2{    ///<SAI Transmit Configuration 2 Register
        using Addr = Register::Address<0x40055010,0x00ffff00,0x00000000,unsigned>;
        ///Bit Clock Divide
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> div{}; 
        ///Bit Clock Direction
        enum class BcdVal : unsigned {
            v0=0x00000000,     ///<Bit clock is generated externally in Slave mode.
            v1=0x00000001,     ///<Bit clock is generated internally in Master mode.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,BcdVal> bcd{}; 
        namespace BcdValC{
            constexpr Register::FieldValue<decltype(bcd)::Type,BcdVal::v0> v0{};
            constexpr Register::FieldValue<decltype(bcd)::Type,BcdVal::v1> v1{};
        }
        ///Bit Clock Polarity
        enum class BcpVal : unsigned {
            v0=0x00000000,     ///<Bit clock is active high with drive outputs on rising edge and sample inputs on falling edge.
            v1=0x00000001,     ///<Bit clock is active low with drive outputs on falling edge and sample inputs on rising edge.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,BcpVal> bcp{}; 
        namespace BcpValC{
            constexpr Register::FieldValue<decltype(bcp)::Type,BcpVal::v0> v0{};
            constexpr Register::FieldValue<decltype(bcp)::Type,BcpVal::v1> v1{};
        }
        ///MCLK Select
        enum class MselVal : unsigned {
            v00=0x00000000,     ///<Bus Clock selected.
            v01=0x00000001,     ///<Master Clock (MCLK) 1 option selected.
            v10=0x00000002,     ///<Master Clock (MCLK) 2 option selected.
            v11=0x00000003,     ///<Master Clock (MCLK) 3 option selected.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,26),Register::ReadWriteAccess,MselVal> msel{}; 
        namespace MselValC{
            constexpr Register::FieldValue<decltype(msel)::Type,MselVal::v00> v00{};
            constexpr Register::FieldValue<decltype(msel)::Type,MselVal::v01> v01{};
            constexpr Register::FieldValue<decltype(msel)::Type,MselVal::v10> v10{};
            constexpr Register::FieldValue<decltype(msel)::Type,MselVal::v11> v11{};
        }
        ///Bit Clock Input
        enum class BciVal : unsigned {
            v0=0x00000000,     ///<No effect.
            v1=0x00000001,     ///<Internal logic is clocked as if bit clock was externally generated.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,BciVal> bci{}; 
        namespace BciValC{
            constexpr Register::FieldValue<decltype(bci)::Type,BciVal::v0> v0{};
            constexpr Register::FieldValue<decltype(bci)::Type,BciVal::v1> v1{};
        }
        ///Bit Clock Swap
        enum class BcsVal : unsigned {
            v0=0x00000000,     ///<Use the normal bit clock source.
            v1=0x00000001,     ///<Swap the bit clock source.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,BcsVal> bcs{}; 
        namespace BcsValC{
            constexpr Register::FieldValue<decltype(bcs)::Type,BcsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(bcs)::Type,BcsVal::v1> v1{};
        }
        ///Synchronous Mode
        enum class SyncVal : unsigned {
            v00=0x00000000,     ///<Asynchronous mode.
            v01=0x00000001,     ///<Synchronous with receiver.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,30),Register::ReadWriteAccess,SyncVal> sync{}; 
        namespace SyncValC{
            constexpr Register::FieldValue<decltype(sync)::Type,SyncVal::v00> v00{};
            constexpr Register::FieldValue<decltype(sync)::Type,SyncVal::v01> v01{};
        }
    }
    namespace Sai1SaiTcr3{    ///<SAI Transmit Configuration 3 Register
        using Addr = Register::Address<0x40055014,0xf0f0fff0,0x00000000,unsigned>;
        ///Word Flag Configuration
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::ReadWriteAccess,unsigned> wdfl{}; 
        ///Transmit Channel Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,unsigned> tce{}; 
        ///Channel FIFO Reset
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,24),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> cfr{}; 
    }
    namespace Sai1SaiTcr4{    ///<SAI Transmit Configuration 4 Register
        using Addr = Register::Address<0x40055018,0xe0f0e0c0,0x00000000,unsigned>;
        ///Frame Sync Direction
        enum class FsdVal : unsigned {
            v0=0x00000000,     ///<Frame sync is generated externally in Slave mode.
            v1=0x00000001,     ///<Frame sync is generated internally in Master mode.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,FsdVal> fsd{}; 
        namespace FsdValC{
            constexpr Register::FieldValue<decltype(fsd)::Type,FsdVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fsd)::Type,FsdVal::v1> v1{};
        }
        ///Frame Sync Polarity
        enum class FspVal : unsigned {
            v0=0x00000000,     ///<Frame sync is active high.
            v1=0x00000001,     ///<Frame sync is active low.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,FspVal> fsp{}; 
        namespace FspValC{
            constexpr Register::FieldValue<decltype(fsp)::Type,FspVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fsp)::Type,FspVal::v1> v1{};
        }
        ///On Demand Mode
        enum class OndemVal : unsigned {
            v0=0x00000000,     ///<Internal frame sync is generated continuously.
            v1=0x00000001,     ///<Internal frame sync is generated when the FIFO warning flag is clear.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,OndemVal> ondem{}; 
        namespace OndemValC{
            constexpr Register::FieldValue<decltype(ondem)::Type,OndemVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ondem)::Type,OndemVal::v1> v1{};
        }
        ///Frame Sync Early
        enum class FseVal : unsigned {
            v0=0x00000000,     ///<Frame sync asserts with the first bit of the frame.
            v1=0x00000001,     ///<Frame sync asserts one bit before the first bit of the frame.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,FseVal> fse{}; 
        namespace FseValC{
            constexpr Register::FieldValue<decltype(fse)::Type,FseVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fse)::Type,FseVal::v1> v1{};
        }
        ///MSB First
        enum class MfVal : unsigned {
            v0=0x00000000,     ///<LSB is transmitted first.
            v1=0x00000001,     ///<MSB is transmitted first.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,4),Register::ReadWriteAccess,MfVal> mf{}; 
        namespace MfValC{
            constexpr Register::FieldValue<decltype(mf)::Type,MfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(mf)::Type,MfVal::v1> v1{};
        }
        ///Channel Mode
        enum class ChmodVal : unsigned {
            v0=0x00000000,     ///<TDM mode, transmit data pins are tri-stated when slots are masked or channels are disabled.
            v1=0x00000001,     ///<Output mode, transmit data pins are never tri-stated and will output zero when slots are masked or channels are disabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,ChmodVal> chmod{}; 
        namespace ChmodValC{
            constexpr Register::FieldValue<decltype(chmod)::Type,ChmodVal::v0> v0{};
            constexpr Register::FieldValue<decltype(chmod)::Type,ChmodVal::v1> v1{};
        }
        ///Sync Width
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,8),Register::ReadWriteAccess,unsigned> sywd{}; 
        ///Frame size
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,unsigned> frsz{}; 
        ///FIFO Packing Mode
        enum class FpackVal : unsigned {
            v00=0x00000000,     ///<FIFO packing is disabled
            v10=0x00000002,     ///<8-bit FIFO packing is enabled
            v11=0x00000003,     ///<16-bit FIFO packing is enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,FpackVal> fpack{}; 
        namespace FpackValC{
            constexpr Register::FieldValue<decltype(fpack)::Type,FpackVal::v00> v00{};
            constexpr Register::FieldValue<decltype(fpack)::Type,FpackVal::v10> v10{};
            constexpr Register::FieldValue<decltype(fpack)::Type,FpackVal::v11> v11{};
        }
        ///FIFO Combine Mode
        enum class FcombVal : unsigned {
            v00=0x00000000,     ///<FIFO combine mode disabled.
            v01=0x00000001,     ///<FIFO combine mode enabled on FIFO reads (from transmit shift registers).
            v10=0x00000002,     ///<FIFO combine mode enabled on FIFO writes (by software).
            v11=0x00000003,     ///<FIFO combine mode enabled on FIFO reads (from transmit shift registers) and writes (by software).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,26),Register::ReadWriteAccess,FcombVal> fcomb{}; 
        namespace FcombValC{
            constexpr Register::FieldValue<decltype(fcomb)::Type,FcombVal::v00> v00{};
            constexpr Register::FieldValue<decltype(fcomb)::Type,FcombVal::v01> v01{};
            constexpr Register::FieldValue<decltype(fcomb)::Type,FcombVal::v10> v10{};
            constexpr Register::FieldValue<decltype(fcomb)::Type,FcombVal::v11> v11{};
        }
        ///FIFO Continue on Error
        enum class FcontVal : unsigned {
            v0=0x00000000,     ///<On FIFO error, the SAI will continue from the start of the next frame after the FIFO error flag has been cleared.
            v1=0x00000001,     ///<On FIFO error, the SAI will continue from the same word that caused the FIFO error to set after the FIFO warning flag has been cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,FcontVal> fcont{}; 
        namespace FcontValC{
            constexpr Register::FieldValue<decltype(fcont)::Type,FcontVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fcont)::Type,FcontVal::v1> v1{};
        }
    }
    namespace Sai1SaiTcr5{    ///<SAI Transmit Configuration 5 Register
        using Addr = Register::Address<0x4005501c,0xe0e0e0ff,0x00000000,unsigned>;
        ///First Bit Shifted
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,8),Register::ReadWriteAccess,unsigned> fbt{}; 
        ///Word 0 Width
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,16),Register::ReadWriteAccess,unsigned> w0w{}; 
        ///Word N Width
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,24),Register::ReadWriteAccess,unsigned> wnw{}; 
    }
    namespace Sai1SaiTdr0{    ///<SAI Transmit Data Register
        using Addr = Register::Address<0x40055020,0x00000000,0x00000000,unsigned>;
        ///Transmit Data Register
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> tdr{}; 
    }
    namespace Sai1SaiTdr1{    ///<SAI Transmit Data Register
        using Addr = Register::Address<0x40055024,0x00000000,0x00000000,unsigned>;
        ///Transmit Data Register
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> tdr{}; 
    }
    namespace Sai1SaiTdr2{    ///<SAI Transmit Data Register
        using Addr = Register::Address<0x40055028,0x00000000,0x00000000,unsigned>;
        ///Transmit Data Register
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> tdr{}; 
    }
    namespace Sai1SaiTdr3{    ///<SAI Transmit Data Register
        using Addr = Register::Address<0x4005502c,0x00000000,0x00000000,unsigned>;
        ///Transmit Data Register
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> tdr{}; 
    }
    namespace Sai1SaiTfr0{    ///<SAI Transmit FIFO Register
        using Addr = Register::Address<0x40055040,0x7ff0fff0,0x00000000,unsigned>;
        ///Read FIFO Pointer
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rfp{}; 
        ///Write FIFO Pointer
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> wfp{}; 
        ///Write Channel Pointer
        enum class WcpVal : unsigned {
            v0=0x00000000,     ///<No effect.
            v1=0x00000001,     ///<FIFO combine is enabled for FIFO writes and this FIFO will be written on the next FIFO write.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,WcpVal> wcp{}; 
        namespace WcpValC{
            constexpr Register::FieldValue<decltype(wcp)::Type,WcpVal::v0> v0{};
            constexpr Register::FieldValue<decltype(wcp)::Type,WcpVal::v1> v1{};
        }
    }
    namespace Sai1SaiTfr1{    ///<SAI Transmit FIFO Register
        using Addr = Register::Address<0x40055044,0x7ff0fff0,0x00000000,unsigned>;
        ///Read FIFO Pointer
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rfp{}; 
        ///Write FIFO Pointer
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> wfp{}; 
        ///Write Channel Pointer
        enum class WcpVal : unsigned {
            v0=0x00000000,     ///<No effect.
            v1=0x00000001,     ///<FIFO combine is enabled for FIFO writes and this FIFO will be written on the next FIFO write.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,WcpVal> wcp{}; 
        namespace WcpValC{
            constexpr Register::FieldValue<decltype(wcp)::Type,WcpVal::v0> v0{};
            constexpr Register::FieldValue<decltype(wcp)::Type,WcpVal::v1> v1{};
        }
    }
    namespace Sai1SaiTfr2{    ///<SAI Transmit FIFO Register
        using Addr = Register::Address<0x40055048,0x7ff0fff0,0x00000000,unsigned>;
        ///Read FIFO Pointer
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rfp{}; 
        ///Write FIFO Pointer
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> wfp{}; 
        ///Write Channel Pointer
        enum class WcpVal : unsigned {
            v0=0x00000000,     ///<No effect.
            v1=0x00000001,     ///<FIFO combine is enabled for FIFO writes and this FIFO will be written on the next FIFO write.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,WcpVal> wcp{}; 
        namespace WcpValC{
            constexpr Register::FieldValue<decltype(wcp)::Type,WcpVal::v0> v0{};
            constexpr Register::FieldValue<decltype(wcp)::Type,WcpVal::v1> v1{};
        }
    }
    namespace Sai1SaiTfr3{    ///<SAI Transmit FIFO Register
        using Addr = Register::Address<0x4005504c,0x7ff0fff0,0x00000000,unsigned>;
        ///Read FIFO Pointer
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rfp{}; 
        ///Write FIFO Pointer
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> wfp{}; 
        ///Write Channel Pointer
        enum class WcpVal : unsigned {
            v0=0x00000000,     ///<No effect.
            v1=0x00000001,     ///<FIFO combine is enabled for FIFO writes and this FIFO will be written on the next FIFO write.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,WcpVal> wcp{}; 
        namespace WcpValC{
            constexpr Register::FieldValue<decltype(wcp)::Type,WcpVal::v0> v0{};
            constexpr Register::FieldValue<decltype(wcp)::Type,WcpVal::v1> v1{};
        }
    }
    namespace Sai1SaiTmr{    ///<SAI Transmit Mask Register
        using Addr = Register::Address<0x40055060,0xffff0000,0x00000000,unsigned>;
        ///Transmit Word Mask
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::ReadWriteAccess,unsigned> twm{}; 
    }
    namespace Sai1SaiRcsr{    ///<SAI Receive Control Register
        using Addr = Register::Address<0x40055088,0x4ce0e0fc,0x00000000,unsigned>;
        ///FIFO Request DMA Enable
        enum class FrdeVal : unsigned {
            v0=0x00000000,     ///<Disables the DMA request.
            v1=0x00000001,     ///<Enables the DMA request.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,FrdeVal> frde{}; 
        namespace FrdeValC{
            constexpr Register::FieldValue<decltype(frde)::Type,FrdeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(frde)::Type,FrdeVal::v1> v1{};
        }
        ///FIFO Warning DMA Enable
        enum class FwdeVal : unsigned {
            v0=0x00000000,     ///<Disables the DMA request.
            v1=0x00000001,     ///<Enables the DMA request.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,FwdeVal> fwde{}; 
        namespace FwdeValC{
            constexpr Register::FieldValue<decltype(fwde)::Type,FwdeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fwde)::Type,FwdeVal::v1> v1{};
        }
        ///FIFO Request Interrupt Enable
        enum class FrieVal : unsigned {
            v0=0x00000000,     ///<Disables the interrupt.
            v1=0x00000001,     ///<Enables the interrupt.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::ReadWriteAccess,FrieVal> frie{}; 
        namespace FrieValC{
            constexpr Register::FieldValue<decltype(frie)::Type,FrieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(frie)::Type,FrieVal::v1> v1{};
        }
        ///FIFO Warning Interrupt Enable
        enum class FwieVal : unsigned {
            v0=0x00000000,     ///<Disables the interrupt.
            v1=0x00000001,     ///<Enables the interrupt.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,9),Register::ReadWriteAccess,FwieVal> fwie{}; 
        namespace FwieValC{
            constexpr Register::FieldValue<decltype(fwie)::Type,FwieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fwie)::Type,FwieVal::v1> v1{};
        }
        ///FIFO Error Interrupt Enable
        enum class FeieVal : unsigned {
            v0=0x00000000,     ///<Disables the interrupt.
            v1=0x00000001,     ///<Enables the interrupt.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,10),Register::ReadWriteAccess,FeieVal> feie{}; 
        namespace FeieValC{
            constexpr Register::FieldValue<decltype(feie)::Type,FeieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(feie)::Type,FeieVal::v1> v1{};
        }
        ///Sync Error Interrupt Enable
        enum class SeieVal : unsigned {
            v0=0x00000000,     ///<Disables interrupt.
            v1=0x00000001,     ///<Enables interrupt.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,SeieVal> seie{}; 
        namespace SeieValC{
            constexpr Register::FieldValue<decltype(seie)::Type,SeieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(seie)::Type,SeieVal::v1> v1{};
        }
        ///Word Start Interrupt Enable
        enum class WsieVal : unsigned {
            v0=0x00000000,     ///<Disables interrupt.
            v1=0x00000001,     ///<Enables interrupt.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,12),Register::ReadWriteAccess,WsieVal> wsie{}; 
        namespace WsieValC{
            constexpr Register::FieldValue<decltype(wsie)::Type,WsieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(wsie)::Type,WsieVal::v1> v1{};
        }
        ///FIFO Request Flag
        enum class FrfVal : unsigned {
            v0=0x00000000,     ///<Receive FIFO watermark not reached.
            v1=0x00000001,     ///<Receive FIFO watermark has been reached.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,FrfVal> frf{}; 
        namespace FrfValC{
            constexpr Register::FieldValue<decltype(frf)::Type,FrfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(frf)::Type,FrfVal::v1> v1{};
        }
        ///FIFO Warning Flag
        enum class FwfVal : unsigned {
            v0=0x00000000,     ///<No enabled receive FIFO is full.
            v1=0x00000001,     ///<Enabled receive FIFO is full.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,17),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,FwfVal> fwf{}; 
        namespace FwfValC{
            constexpr Register::FieldValue<decltype(fwf)::Type,FwfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fwf)::Type,FwfVal::v1> v1{};
        }
        ///FIFO Error Flag
        enum class FefVal : unsigned {
            v0=0x00000000,     ///<Receive overflow not detected.
            v1=0x00000001,     ///<Receive overflow detected.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(18,18),Register::ReadWriteAccess,FefVal> fef{}; 
        namespace FefValC{
            constexpr Register::FieldValue<decltype(fef)::Type,FefVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fef)::Type,FefVal::v1> v1{};
        }
        ///Sync Error Flag
        enum class SefVal : unsigned {
            v0=0x00000000,     ///<Sync error not detected.
            v1=0x00000001,     ///<Frame sync error detected.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,19),Register::ReadWriteAccess,SefVal> sef{}; 
        namespace SefValC{
            constexpr Register::FieldValue<decltype(sef)::Type,SefVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sef)::Type,SefVal::v1> v1{};
        }
        ///Word Start Flag
        enum class WsfVal : unsigned {
            v0=0x00000000,     ///<Start of word not detected.
            v1=0x00000001,     ///<Start of word detected.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,20),Register::ReadWriteAccess,WsfVal> wsf{}; 
        namespace WsfValC{
            constexpr Register::FieldValue<decltype(wsf)::Type,WsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(wsf)::Type,WsfVal::v1> v1{};
        }
        ///Software Reset
        enum class SrVal : unsigned {
            v0=0x00000000,     ///<No effect.
            v1=0x00000001,     ///<Software reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,SrVal> sr{}; 
        namespace SrValC{
            constexpr Register::FieldValue<decltype(sr)::Type,SrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sr)::Type,SrVal::v1> v1{};
        }
        ///FIFO Reset
        enum class FrVal : unsigned {
            v0=0x00000000,     ///<No effect.
            v1=0x00000001,     ///<FIFO reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,FrVal> fr{}; 
        namespace FrValC{
            constexpr Register::FieldValue<decltype(fr)::Type,FrVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fr)::Type,FrVal::v1> v1{};
        }
        ///Bit Clock Enable
        enum class BceVal : unsigned {
            v0=0x00000000,     ///<Receive bit clock is disabled.
            v1=0x00000001,     ///<Receive bit clock is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,BceVal> bce{}; 
        namespace BceValC{
            constexpr Register::FieldValue<decltype(bce)::Type,BceVal::v0> v0{};
            constexpr Register::FieldValue<decltype(bce)::Type,BceVal::v1> v1{};
        }
        ///Debug Enable
        enum class DbgeVal : unsigned {
            v0=0x00000000,     ///<Receiver is disabled in Debug mode, after completing the current frame.
            v1=0x00000001,     ///<Receiver is enabled in Debug mode.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,DbgeVal> dbge{}; 
        namespace DbgeValC{
            constexpr Register::FieldValue<decltype(dbge)::Type,DbgeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dbge)::Type,DbgeVal::v1> v1{};
        }
        ///Receiver Enable
        enum class ReVal : unsigned {
            v0=0x00000000,     ///<Receiver is disabled.
            v1=0x00000001,     ///<Receiver is enabled, or receiver has been disabled and has not yet reached end of frame.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,ReVal> re{}; 
        namespace ReValC{
            constexpr Register::FieldValue<decltype(re)::Type,ReVal::v0> v0{};
            constexpr Register::FieldValue<decltype(re)::Type,ReVal::v1> v1{};
        }
    }
    namespace Sai1SaiRcr1{    ///<SAI Receive Configuration 1 Register
        using Addr = Register::Address<0x4005508c,0xfffffff8,0x00000000,unsigned>;
        ///Receive FIFO Watermark
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> rfw{}; 
    }
    namespace Sai1SaiRcr2{    ///<SAI Receive Configuration 2 Register
        using Addr = Register::Address<0x40055090,0x00ffff00,0x00000000,unsigned>;
        ///Bit Clock Divide
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> div{}; 
        ///Bit Clock Direction
        enum class BcdVal : unsigned {
            v0=0x00000000,     ///<Bit clock is generated externally in Slave mode.
            v1=0x00000001,     ///<Bit clock is generated internally in Master mode.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,BcdVal> bcd{}; 
        namespace BcdValC{
            constexpr Register::FieldValue<decltype(bcd)::Type,BcdVal::v0> v0{};
            constexpr Register::FieldValue<decltype(bcd)::Type,BcdVal::v1> v1{};
        }
        ///Bit Clock Polarity
        enum class BcpVal : unsigned {
            v0=0x00000000,     ///<Bit Clock is active high with drive outputs on rising edge and sample inputs on falling edge.
            v1=0x00000001,     ///<Bit Clock is active low with drive outputs on falling edge and sample inputs on rising edge.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,BcpVal> bcp{}; 
        namespace BcpValC{
            constexpr Register::FieldValue<decltype(bcp)::Type,BcpVal::v0> v0{};
            constexpr Register::FieldValue<decltype(bcp)::Type,BcpVal::v1> v1{};
        }
        ///MCLK Select
        enum class MselVal : unsigned {
            v00=0x00000000,     ///<Bus Clock selected.
            v01=0x00000001,     ///<Master Clock (MCLK) 1 option selected.
            v10=0x00000002,     ///<Master Clock (MCLK) 2 option selected.
            v11=0x00000003,     ///<Master Clock (MCLK) 3 option selected.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,26),Register::ReadWriteAccess,MselVal> msel{}; 
        namespace MselValC{
            constexpr Register::FieldValue<decltype(msel)::Type,MselVal::v00> v00{};
            constexpr Register::FieldValue<decltype(msel)::Type,MselVal::v01> v01{};
            constexpr Register::FieldValue<decltype(msel)::Type,MselVal::v10> v10{};
            constexpr Register::FieldValue<decltype(msel)::Type,MselVal::v11> v11{};
        }
        ///Bit Clock Input
        enum class BciVal : unsigned {
            v0=0x00000000,     ///<No effect.
            v1=0x00000001,     ///<Internal logic is clocked as if bit clock was externally generated.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,BciVal> bci{}; 
        namespace BciValC{
            constexpr Register::FieldValue<decltype(bci)::Type,BciVal::v0> v0{};
            constexpr Register::FieldValue<decltype(bci)::Type,BciVal::v1> v1{};
        }
        ///Bit Clock Swap
        enum class BcsVal : unsigned {
            v0=0x00000000,     ///<Use the normal bit clock source.
            v1=0x00000001,     ///<Swap the bit clock source.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,29),Register::ReadWriteAccess,BcsVal> bcs{}; 
        namespace BcsValC{
            constexpr Register::FieldValue<decltype(bcs)::Type,BcsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(bcs)::Type,BcsVal::v1> v1{};
        }
        ///Synchronous Mode
        enum class SyncVal : unsigned {
            v00=0x00000000,     ///<Asynchronous mode.
            v01=0x00000001,     ///<Synchronous with transmitter.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,30),Register::ReadWriteAccess,SyncVal> sync{}; 
        namespace SyncValC{
            constexpr Register::FieldValue<decltype(sync)::Type,SyncVal::v00> v00{};
            constexpr Register::FieldValue<decltype(sync)::Type,SyncVal::v01> v01{};
        }
    }
    namespace Sai1SaiRcr3{    ///<SAI Receive Configuration 3 Register
        using Addr = Register::Address<0x40055094,0xf0f0fff0,0x00000000,unsigned>;
        ///Word Flag Configuration
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::ReadWriteAccess,unsigned> wdfl{}; 
        ///Receive Channel Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,unsigned> rce{}; 
        ///Channel FIFO Reset
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,24),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> cfr{}; 
    }
    namespace Sai1SaiRcr4{    ///<SAI Receive Configuration 4 Register
        using Addr = Register::Address<0x40055098,0xe0f0e0e0,0x00000000,unsigned>;
        ///Frame Sync Direction
        enum class FsdVal : unsigned {
            v0=0x00000000,     ///<Frame Sync is generated externally in Slave mode.
            v1=0x00000001,     ///<Frame Sync is generated internally in Master mode.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,FsdVal> fsd{}; 
        namespace FsdValC{
            constexpr Register::FieldValue<decltype(fsd)::Type,FsdVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fsd)::Type,FsdVal::v1> v1{};
        }
        ///Frame Sync Polarity
        enum class FspVal : unsigned {
            v0=0x00000000,     ///<Frame sync is active high.
            v1=0x00000001,     ///<Frame sync is active low.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,FspVal> fsp{}; 
        namespace FspValC{
            constexpr Register::FieldValue<decltype(fsp)::Type,FspVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fsp)::Type,FspVal::v1> v1{};
        }
        ///On Demand Mode
        enum class OndemVal : unsigned {
            v0=0x00000000,     ///<Internal frame sync is generated continuously.
            v1=0x00000001,     ///<Internal frame sync is generated when the FIFO warning flag is clear.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,OndemVal> ondem{}; 
        namespace OndemValC{
            constexpr Register::FieldValue<decltype(ondem)::Type,OndemVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ondem)::Type,OndemVal::v1> v1{};
        }
        ///Frame Sync Early
        enum class FseVal : unsigned {
            v0=0x00000000,     ///<Frame sync asserts with the first bit of the frame.
            v1=0x00000001,     ///<Frame sync asserts one bit before the first bit of the frame.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,FseVal> fse{}; 
        namespace FseValC{
            constexpr Register::FieldValue<decltype(fse)::Type,FseVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fse)::Type,FseVal::v1> v1{};
        }
        ///MSB First
        enum class MfVal : unsigned {
            v0=0x00000000,     ///<LSB is received first.
            v1=0x00000001,     ///<MSB is received first.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,4),Register::ReadWriteAccess,MfVal> mf{}; 
        namespace MfValC{
            constexpr Register::FieldValue<decltype(mf)::Type,MfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(mf)::Type,MfVal::v1> v1{};
        }
        ///Sync Width
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,8),Register::ReadWriteAccess,unsigned> sywd{}; 
        ///Frame Size
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,unsigned> frsz{}; 
        ///FIFO Packing Mode
        enum class FpackVal : unsigned {
            v00=0x00000000,     ///<FIFO packing is disabled
            v10=0x00000002,     ///<8-bit FIFO packing is enabled
            v11=0x00000003,     ///<16-bit FIFO packing is enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,FpackVal> fpack{}; 
        namespace FpackValC{
            constexpr Register::FieldValue<decltype(fpack)::Type,FpackVal::v00> v00{};
            constexpr Register::FieldValue<decltype(fpack)::Type,FpackVal::v10> v10{};
            constexpr Register::FieldValue<decltype(fpack)::Type,FpackVal::v11> v11{};
        }
        ///FIFO Combine Mode
        enum class FcombVal : unsigned {
            v00=0x00000000,     ///<FIFO combine mode disabled.
            v01=0x00000001,     ///<FIFO combine mode enabled on FIFO writes (from receive shift registers).
            v10=0x00000002,     ///<FIFO combine mode enabled on FIFO reads (by software).
            v11=0x00000003,     ///<FIFO combine mode enabled on FIFO writes (from receive shift registers) and reads (by software).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,26),Register::ReadWriteAccess,FcombVal> fcomb{}; 
        namespace FcombValC{
            constexpr Register::FieldValue<decltype(fcomb)::Type,FcombVal::v00> v00{};
            constexpr Register::FieldValue<decltype(fcomb)::Type,FcombVal::v01> v01{};
            constexpr Register::FieldValue<decltype(fcomb)::Type,FcombVal::v10> v10{};
            constexpr Register::FieldValue<decltype(fcomb)::Type,FcombVal::v11> v11{};
        }
        ///FIFO Continue on Error
        enum class FcontVal : unsigned {
            v0=0x00000000,     ///<On FIFO error, the SAI will continue from the start of the next frame after the FIFO error flag has been cleared.
            v1=0x00000001,     ///<On FIFO error, the SAI will continue from the same word that caused the FIFO error to set after the FIFO warning flag has been cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,FcontVal> fcont{}; 
        namespace FcontValC{
            constexpr Register::FieldValue<decltype(fcont)::Type,FcontVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fcont)::Type,FcontVal::v1> v1{};
        }
    }
    namespace Sai1SaiRcr5{    ///<SAI Receive Configuration 5 Register
        using Addr = Register::Address<0x4005509c,0xe0e0e0ff,0x00000000,unsigned>;
        ///First Bit Shifted
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,8),Register::ReadWriteAccess,unsigned> fbt{}; 
        ///Word 0 Width
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,16),Register::ReadWriteAccess,unsigned> w0w{}; 
        ///Word N Width
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,24),Register::ReadWriteAccess,unsigned> wnw{}; 
    }
    namespace Sai1SaiRdr0{    ///<SAI Receive Data Register
        using Addr = Register::Address<0x400550a0,0x00000000,0x00000000,unsigned>;
        ///Receive Data Register
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rdr{}; 
    }
    namespace Sai1SaiRdr1{    ///<SAI Receive Data Register
        using Addr = Register::Address<0x400550a4,0x00000000,0x00000000,unsigned>;
        ///Receive Data Register
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rdr{}; 
    }
    namespace Sai1SaiRdr2{    ///<SAI Receive Data Register
        using Addr = Register::Address<0x400550a8,0x00000000,0x00000000,unsigned>;
        ///Receive Data Register
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rdr{}; 
    }
    namespace Sai1SaiRdr3{    ///<SAI Receive Data Register
        using Addr = Register::Address<0x400550ac,0x00000000,0x00000000,unsigned>;
        ///Receive Data Register
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rdr{}; 
    }
    namespace Sai1SaiRfr0{    ///<SAI Receive FIFO Register
        using Addr = Register::Address<0x400550c0,0xfff07ff0,0x00000000,unsigned>;
        ///Read FIFO Pointer
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rfp{}; 
        ///Receive Channel Pointer
        enum class RcpVal : unsigned {
            v0=0x00000000,     ///<No effect.
            v1=0x00000001,     ///<FIFO combine is enabled for FIFO reads and this FIFO will be read on the next FIFO read.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RcpVal> rcp{}; 
        namespace RcpValC{
            constexpr Register::FieldValue<decltype(rcp)::Type,RcpVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rcp)::Type,RcpVal::v1> v1{};
        }
        ///Write FIFO Pointer
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> wfp{}; 
    }
    namespace Sai1SaiRfr1{    ///<SAI Receive FIFO Register
        using Addr = Register::Address<0x400550c4,0xfff07ff0,0x00000000,unsigned>;
        ///Read FIFO Pointer
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rfp{}; 
        ///Receive Channel Pointer
        enum class RcpVal : unsigned {
            v0=0x00000000,     ///<No effect.
            v1=0x00000001,     ///<FIFO combine is enabled for FIFO reads and this FIFO will be read on the next FIFO read.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RcpVal> rcp{}; 
        namespace RcpValC{
            constexpr Register::FieldValue<decltype(rcp)::Type,RcpVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rcp)::Type,RcpVal::v1> v1{};
        }
        ///Write FIFO Pointer
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> wfp{}; 
    }
    namespace Sai1SaiRfr2{    ///<SAI Receive FIFO Register
        using Addr = Register::Address<0x400550c8,0xfff07ff0,0x00000000,unsigned>;
        ///Read FIFO Pointer
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rfp{}; 
        ///Receive Channel Pointer
        enum class RcpVal : unsigned {
            v0=0x00000000,     ///<No effect.
            v1=0x00000001,     ///<FIFO combine is enabled for FIFO reads and this FIFO will be read on the next FIFO read.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RcpVal> rcp{}; 
        namespace RcpValC{
            constexpr Register::FieldValue<decltype(rcp)::Type,RcpVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rcp)::Type,RcpVal::v1> v1{};
        }
        ///Write FIFO Pointer
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> wfp{}; 
    }
    namespace Sai1SaiRfr3{    ///<SAI Receive FIFO Register
        using Addr = Register::Address<0x400550cc,0xfff07ff0,0x00000000,unsigned>;
        ///Read FIFO Pointer
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rfp{}; 
        ///Receive Channel Pointer
        enum class RcpVal : unsigned {
            v0=0x00000000,     ///<No effect.
            v1=0x00000001,     ///<FIFO combine is enabled for FIFO reads and this FIFO will be read on the next FIFO read.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RcpVal> rcp{}; 
        namespace RcpValC{
            constexpr Register::FieldValue<decltype(rcp)::Type,RcpVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rcp)::Type,RcpVal::v1> v1{};
        }
        ///Write FIFO Pointer
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> wfp{}; 
    }
    namespace Sai1SaiRmr{    ///<SAI Receive Mask Register
        using Addr = Register::Address<0x400550e0,0xffff0000,0x00000000,unsigned>;
        ///Receive Word Mask
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::ReadWriteAccess,unsigned> rwm{}; 
    }
}
