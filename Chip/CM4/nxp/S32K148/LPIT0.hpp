#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//Low Power Periodic Interrupt Timer (LPIT)
    namespace Lpit0Verid{    ///<Version ID Register
        using Addr = Register::Address<0x40037000,0x00000000,0x00000000,unsigned>;
        ///Feature Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> feature{}; 
        ///Minor Version Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> minor{}; 
        ///Major Version Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> major{}; 
    }
    namespace Lpit0Param{    ///<Parameter Register
        using Addr = Register::Address<0x40037004,0xffff0000,0x00000000,unsigned>;
        ///Number of Timer Channels
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> channel{}; 
        ///Number of External Trigger Inputs
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> extTrig{}; 
    }
    namespace Lpit0Mcr{    ///<Module Control Register
        using Addr = Register::Address<0x40037008,0xfffffff0,0x00000000,unsigned>;
        ///Module Clock Enable
        enum class McenVal : unsigned {
            v0=0x00000000,     ///<Peripheral clock to timers is disabled
            v1=0x00000001,     ///<Peripheral clock to timers is enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,McenVal> mCen{}; 
        namespace McenValC{
            constexpr Register::FieldValue<decltype(mCen)::Type,McenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(mCen)::Type,McenVal::v1> v1{};
        }
        ///Software Reset Bit
        enum class SwrstVal : unsigned {
            v0=0x00000000,     ///<Timer channels and registers are not reset
            v1=0x00000001,     ///<Timer channels and registers are reset
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,SwrstVal> swRst{}; 
        namespace SwrstValC{
            constexpr Register::FieldValue<decltype(swRst)::Type,SwrstVal::v0> v0{};
            constexpr Register::FieldValue<decltype(swRst)::Type,SwrstVal::v1> v1{};
        }
        ///DOZE Mode Enable Bit
        enum class DozeenVal : unsigned {
            v0=0x00000000,     ///<Timer channels are stopped in DOZE mode
            v1=0x00000001,     ///<Timer channels continue to run in DOZE mode
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,DozeenVal> dozeEn{}; 
        namespace DozeenValC{
            constexpr Register::FieldValue<decltype(dozeEn)::Type,DozeenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dozeEn)::Type,DozeenVal::v1> v1{};
        }
        ///Debug Enable Bit
        enum class DbgenVal : unsigned {
            v0=0x00000000,     ///<Timer channels are stopped in Debug mode
            v1=0x00000001,     ///<Timer channels continue to run in Debug mode
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,DbgenVal> dbgEn{}; 
        namespace DbgenValC{
            constexpr Register::FieldValue<decltype(dbgEn)::Type,DbgenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dbgEn)::Type,DbgenVal::v1> v1{};
        }
    }
    namespace Lpit0Msr{    ///<Module Status Register
        using Addr = Register::Address<0x4003700c,0xfffffff0,0x00000000,unsigned>;
        ///Channel 0 Timer Interrupt Flag
        enum class Tif0Val : unsigned {
            v0=0x00000000,     ///<Timer has not timed out
            v1=0x00000001,     ///<Timeout has occurred
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,Tif0Val> tif0{}; 
        namespace Tif0ValC{
            constexpr Register::FieldValue<decltype(tif0)::Type,Tif0Val::v0> v0{};
            constexpr Register::FieldValue<decltype(tif0)::Type,Tif0Val::v1> v1{};
        }
        ///Channel 1 Timer Interrupt Flag
        enum class Tif1Val : unsigned {
            v0=0x00000000,     ///<Timer has not timed out
            v1=0x00000001,     ///<Timeout has occurred
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,Tif1Val> tif1{}; 
        namespace Tif1ValC{
            constexpr Register::FieldValue<decltype(tif1)::Type,Tif1Val::v0> v0{};
            constexpr Register::FieldValue<decltype(tif1)::Type,Tif1Val::v1> v1{};
        }
        ///Channel 2 Timer Interrupt Flag
        enum class Tif2Val : unsigned {
            v0=0x00000000,     ///<Timer has not timed out
            v1=0x00000001,     ///<Timeout has occurred
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,Tif2Val> tif2{}; 
        namespace Tif2ValC{
            constexpr Register::FieldValue<decltype(tif2)::Type,Tif2Val::v0> v0{};
            constexpr Register::FieldValue<decltype(tif2)::Type,Tif2Val::v1> v1{};
        }
        ///Channel 3 Timer Interrupt Flag
        enum class Tif3Val : unsigned {
            v0=0x00000000,     ///<Timer has not timed out
            v1=0x00000001,     ///<Timeout has occurred
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,Tif3Val> tif3{}; 
        namespace Tif3ValC{
            constexpr Register::FieldValue<decltype(tif3)::Type,Tif3Val::v0> v0{};
            constexpr Register::FieldValue<decltype(tif3)::Type,Tif3Val::v1> v1{};
        }
    }
    namespace Lpit0Mier{    ///<Module Interrupt Enable Register
        using Addr = Register::Address<0x40037010,0xfffffff0,0x00000000,unsigned>;
        ///Channel 0 Timer Interrupt Enable
        enum class Tie0Val : unsigned {
            v0=0x00000000,     ///<Interrupt generation is disabled
            v1=0x00000001,     ///<Interrupt generation is enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,Tie0Val> tie0{}; 
        namespace Tie0ValC{
            constexpr Register::FieldValue<decltype(tie0)::Type,Tie0Val::v0> v0{};
            constexpr Register::FieldValue<decltype(tie0)::Type,Tie0Val::v1> v1{};
        }
        ///Channel 1 Timer Interrupt Enable
        enum class Tie1Val : unsigned {
            v0=0x00000000,     ///<Interrupt generation is disabled
            v1=0x00000001,     ///<Interrupt generation is enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,Tie1Val> tie1{}; 
        namespace Tie1ValC{
            constexpr Register::FieldValue<decltype(tie1)::Type,Tie1Val::v0> v0{};
            constexpr Register::FieldValue<decltype(tie1)::Type,Tie1Val::v1> v1{};
        }
        ///Channel 2 Timer Interrupt Enable
        enum class Tie2Val : unsigned {
            v0=0x00000000,     ///<Interrupt generation is disabled
            v1=0x00000001,     ///<Interrupt generation is enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,Tie2Val> tie2{}; 
        namespace Tie2ValC{
            constexpr Register::FieldValue<decltype(tie2)::Type,Tie2Val::v0> v0{};
            constexpr Register::FieldValue<decltype(tie2)::Type,Tie2Val::v1> v1{};
        }
        ///Channel 3 Timer Interrupt Enable
        enum class Tie3Val : unsigned {
            v0=0x00000000,     ///<Interrupt generation is disabled
            v1=0x00000001,     ///<Interrupt generation is enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,Tie3Val> tie3{}; 
        namespace Tie3ValC{
            constexpr Register::FieldValue<decltype(tie3)::Type,Tie3Val::v0> v0{};
            constexpr Register::FieldValue<decltype(tie3)::Type,Tie3Val::v1> v1{};
        }
    }
    namespace Lpit0Setten{    ///<Set Timer Enable Register
        using Addr = Register::Address<0x40037014,0xfffffff0,0x00000000,unsigned>;
        ///Set Timer 0 Enable
        enum class Setten0Val : unsigned {
            v0=0x00000000,     ///<No effect
            v1=0x00000001,     ///<Enables the Timer Channel 0
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,Setten0Val> setTEn0{}; 
        namespace Setten0ValC{
            constexpr Register::FieldValue<decltype(setTEn0)::Type,Setten0Val::v0> v0{};
            constexpr Register::FieldValue<decltype(setTEn0)::Type,Setten0Val::v1> v1{};
        }
        ///Set Timer 1 Enable
        enum class Setten1Val : unsigned {
            v0=0x00000000,     ///<No Effect
            v1=0x00000001,     ///<Enables the Timer Channel 1
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,Setten1Val> setTEn1{}; 
        namespace Setten1ValC{
            constexpr Register::FieldValue<decltype(setTEn1)::Type,Setten1Val::v0> v0{};
            constexpr Register::FieldValue<decltype(setTEn1)::Type,Setten1Val::v1> v1{};
        }
        ///Set Timer 2 Enable
        enum class Setten2Val : unsigned {
            v0=0x00000000,     ///<No Effect
            v1=0x00000001,     ///<Enables the Timer Channel 2
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,Setten2Val> setTEn2{}; 
        namespace Setten2ValC{
            constexpr Register::FieldValue<decltype(setTEn2)::Type,Setten2Val::v0> v0{};
            constexpr Register::FieldValue<decltype(setTEn2)::Type,Setten2Val::v1> v1{};
        }
        ///Set Timer 3 Enable
        enum class Setten3Val : unsigned {
            v0=0x00000000,     ///<No effect
            v1=0x00000001,     ///<Enables the Timer Channel 3
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,Setten3Val> setTEn3{}; 
        namespace Setten3ValC{
            constexpr Register::FieldValue<decltype(setTEn3)::Type,Setten3Val::v0> v0{};
            constexpr Register::FieldValue<decltype(setTEn3)::Type,Setten3Val::v1> v1{};
        }
    }
    namespace Lpit0Clrten{    ///<Clear Timer Enable Register
        using Addr = Register::Address<0x40037018,0xfffffff0,0x00000000,unsigned>;
        ///Clear Timer 0 Enable
        enum class Clrten0Val : unsigned {
            v0=0x00000000,     ///<No action
            v1=0x00000001,     ///<Clear T_EN bit for Timer Channel 0
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,Clrten0Val> clrTEn0{}; 
        namespace Clrten0ValC{
            constexpr Register::FieldValue<decltype(clrTEn0)::Type,Clrten0Val::v0> v0{};
            constexpr Register::FieldValue<decltype(clrTEn0)::Type,Clrten0Val::v1> v1{};
        }
        ///Clear Timer 1 Enable
        enum class Clrten1Val : unsigned {
            v0=0x00000000,     ///<No Action
            v1=0x00000001,     ///<Clear T_EN bit for Timer Channel 1
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,Clrten1Val> clrTEn1{}; 
        namespace Clrten1ValC{
            constexpr Register::FieldValue<decltype(clrTEn1)::Type,Clrten1Val::v0> v0{};
            constexpr Register::FieldValue<decltype(clrTEn1)::Type,Clrten1Val::v1> v1{};
        }
        ///Clear Timer 2 Enable
        enum class Clrten2Val : unsigned {
            v0=0x00000000,     ///<No Action
            v1=0x00000001,     ///<Clear T_EN bit for Timer Channel 2
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,Clrten2Val> clrTEn2{}; 
        namespace Clrten2ValC{
            constexpr Register::FieldValue<decltype(clrTEn2)::Type,Clrten2Val::v0> v0{};
            constexpr Register::FieldValue<decltype(clrTEn2)::Type,Clrten2Val::v1> v1{};
        }
        ///Clear Timer 3 Enable
        enum class Clrten3Val : unsigned {
            v0=0x00000000,     ///<No Action
            v1=0x00000001,     ///<Clear T_EN bit for Timer Channel 3
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,Clrten3Val> clrTEn3{}; 
        namespace Clrten3ValC{
            constexpr Register::FieldValue<decltype(clrTEn3)::Type,Clrten3Val::v0> v0{};
            constexpr Register::FieldValue<decltype(clrTEn3)::Type,Clrten3Val::v1> v1{};
        }
    }
    namespace Lpit0Tval0{    ///<Timer Value Register
        using Addr = Register::Address<0x40037020,0x00000000,0x00000000,unsigned>;
        ///Timer Value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> tmrVal{}; 
    }
    namespace Lpit0Cval0{    ///<Current Timer Value
        using Addr = Register::Address<0x40037024,0x00000000,0x00000000,unsigned>;
        ///Current Timer Value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> tmrCurVal{}; 
    }
    namespace Lpit0Tctrl0{    ///<Timer Control Register
        using Addr = Register::Address<0x40037028,0xf078fff0,0x00000000,unsigned>;
        ///Timer Enable
        enum class TenVal : unsigned {
            v0=0x00000000,     ///<Timer Channel is disabled
            v1=0x00000001,     ///<Timer Channel is enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,TenVal> tEn{}; 
        namespace TenValC{
            constexpr Register::FieldValue<decltype(tEn)::Type,TenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tEn)::Type,TenVal::v1> v1{};
        }
        ///Chain Channel
        enum class ChainVal : unsigned {
            v0=0x00000000,     ///<Channel Chaining is disabled. Channel Timer runs independently.
            v1=0x00000001,     ///<Channel Chaining is enabled. Timer decrements on previous channel's timeout
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,ChainVal> chain{}; 
        namespace ChainValC{
            constexpr Register::FieldValue<decltype(chain)::Type,ChainVal::v0> v0{};
            constexpr Register::FieldValue<decltype(chain)::Type,ChainVal::v1> v1{};
        }
        ///Timer Operation Mode
        enum class ModeVal : unsigned {
            v0=0x00000000,     ///<32-bit Periodic Counter
            v1=0x00000001,     ///<Dual 16-bit Periodic Counter
            v10=0x00000002,     ///<32-bit Trigger Accumulator
            v11=0x00000003,     ///<32-bit Trigger Input Capture
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,2),Register::ReadWriteAccess,ModeVal> mode{}; 
        namespace ModeValC{
            constexpr Register::FieldValue<decltype(mode)::Type,ModeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(mode)::Type,ModeVal::v1> v1{};
            constexpr Register::FieldValue<decltype(mode)::Type,ModeVal::v10> v10{};
            constexpr Register::FieldValue<decltype(mode)::Type,ModeVal::v11> v11{};
        }
        ///Timer Start On Trigger
        enum class TsotVal : unsigned {
            v0=0x00000000,     ///<Timer starts to decrement immediately based on restart condition (controlled by TSOI bit)
            v1=0x00000001,     ///<Timer starts to decrement when rising edge on selected trigger is detected
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,TsotVal> tsot{}; 
        namespace TsotValC{
            constexpr Register::FieldValue<decltype(tsot)::Type,TsotVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tsot)::Type,TsotVal::v1> v1{};
        }
        ///Timer Stop On Interrupt
        enum class TsoiVal : unsigned {
            v0=0x00000000,     ///<The channel timer does not stop after timeout.
            v1=0x00000001,     ///<The channel timer will stop after a timeout, and the channel timer will restart based on TSOT. When TSOT = 0, the channel timer will restart after a rising edge on the T_EN bit is detected (which means that the timer channel is disabled and then enabled); when TSOT = 1, the channel timer will restart after a rising edge on the selected trigger is detected.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,17),Register::ReadWriteAccess,TsoiVal> tsoi{}; 
        namespace TsoiValC{
            constexpr Register::FieldValue<decltype(tsoi)::Type,TsoiVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tsoi)::Type,TsoiVal::v1> v1{};
        }
        ///Timer Reload On Trigger
        enum class TrotVal : unsigned {
            v0=0x00000000,     ///<Timer will not reload on selected trigger
            v1=0x00000001,     ///<Timer will reload on selected trigger
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(18,18),Register::ReadWriteAccess,TrotVal> trot{}; 
        namespace TrotValC{
            constexpr Register::FieldValue<decltype(trot)::Type,TrotVal::v0> v0{};
            constexpr Register::FieldValue<decltype(trot)::Type,TrotVal::v1> v1{};
        }
        ///Trigger Source
        enum class TrgsrcVal : unsigned {
            v0=0x00000000,     ///<Trigger source selected in external
            v1=0x00000001,     ///<Trigger source selected is the internal trigger
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,23),Register::ReadWriteAccess,TrgsrcVal> trgSrc{}; 
        namespace TrgsrcValC{
            constexpr Register::FieldValue<decltype(trgSrc)::Type,TrgsrcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(trgSrc)::Type,TrgsrcVal::v1> v1{};
        }
        ///Trigger Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,24),Register::ReadWriteAccess,unsigned> trgSel{}; 
    }
    namespace Lpit0Tval1{    ///<Timer Value Register
        using Addr = Register::Address<0x40037030,0x00000000,0x00000000,unsigned>;
        ///Timer Value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> tmrVal{}; 
    }
    namespace Lpit0Cval1{    ///<Current Timer Value
        using Addr = Register::Address<0x40037034,0x00000000,0x00000000,unsigned>;
        ///Current Timer Value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> tmrCurVal{}; 
    }
    namespace Lpit0Tctrl1{    ///<Timer Control Register
        using Addr = Register::Address<0x40037038,0xf078fff0,0x00000000,unsigned>;
        ///Timer Enable
        enum class TenVal : unsigned {
            v0=0x00000000,     ///<Timer Channel is disabled
            v1=0x00000001,     ///<Timer Channel is enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,TenVal> tEn{}; 
        namespace TenValC{
            constexpr Register::FieldValue<decltype(tEn)::Type,TenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tEn)::Type,TenVal::v1> v1{};
        }
        ///Chain Channel
        enum class ChainVal : unsigned {
            v0=0x00000000,     ///<Channel Chaining is disabled. Channel Timer runs independently.
            v1=0x00000001,     ///<Channel Chaining is enabled. Timer decrements on previous channel's timeout
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,ChainVal> chain{}; 
        namespace ChainValC{
            constexpr Register::FieldValue<decltype(chain)::Type,ChainVal::v0> v0{};
            constexpr Register::FieldValue<decltype(chain)::Type,ChainVal::v1> v1{};
        }
        ///Timer Operation Mode
        enum class ModeVal : unsigned {
            v0=0x00000000,     ///<32-bit Periodic Counter
            v1=0x00000001,     ///<Dual 16-bit Periodic Counter
            v10=0x00000002,     ///<32-bit Trigger Accumulator
            v11=0x00000003,     ///<32-bit Trigger Input Capture
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,2),Register::ReadWriteAccess,ModeVal> mode{}; 
        namespace ModeValC{
            constexpr Register::FieldValue<decltype(mode)::Type,ModeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(mode)::Type,ModeVal::v1> v1{};
            constexpr Register::FieldValue<decltype(mode)::Type,ModeVal::v10> v10{};
            constexpr Register::FieldValue<decltype(mode)::Type,ModeVal::v11> v11{};
        }
        ///Timer Start On Trigger
        enum class TsotVal : unsigned {
            v0=0x00000000,     ///<Timer starts to decrement immediately based on restart condition (controlled by TSOI bit)
            v1=0x00000001,     ///<Timer starts to decrement when rising edge on selected trigger is detected
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,TsotVal> tsot{}; 
        namespace TsotValC{
            constexpr Register::FieldValue<decltype(tsot)::Type,TsotVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tsot)::Type,TsotVal::v1> v1{};
        }
        ///Timer Stop On Interrupt
        enum class TsoiVal : unsigned {
            v0=0x00000000,     ///<The channel timer does not stop after timeout.
            v1=0x00000001,     ///<The channel timer will stop after a timeout, and the channel timer will restart based on TSOT. When TSOT = 0, the channel timer will restart after a rising edge on the T_EN bit is detected (which means that the timer channel is disabled and then enabled); when TSOT = 1, the channel timer will restart after a rising edge on the selected trigger is detected.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,17),Register::ReadWriteAccess,TsoiVal> tsoi{}; 
        namespace TsoiValC{
            constexpr Register::FieldValue<decltype(tsoi)::Type,TsoiVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tsoi)::Type,TsoiVal::v1> v1{};
        }
        ///Timer Reload On Trigger
        enum class TrotVal : unsigned {
            v0=0x00000000,     ///<Timer will not reload on selected trigger
            v1=0x00000001,     ///<Timer will reload on selected trigger
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(18,18),Register::ReadWriteAccess,TrotVal> trot{}; 
        namespace TrotValC{
            constexpr Register::FieldValue<decltype(trot)::Type,TrotVal::v0> v0{};
            constexpr Register::FieldValue<decltype(trot)::Type,TrotVal::v1> v1{};
        }
        ///Trigger Source
        enum class TrgsrcVal : unsigned {
            v0=0x00000000,     ///<Trigger source selected in external
            v1=0x00000001,     ///<Trigger source selected is the internal trigger
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,23),Register::ReadWriteAccess,TrgsrcVal> trgSrc{}; 
        namespace TrgsrcValC{
            constexpr Register::FieldValue<decltype(trgSrc)::Type,TrgsrcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(trgSrc)::Type,TrgsrcVal::v1> v1{};
        }
        ///Trigger Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,24),Register::ReadWriteAccess,unsigned> trgSel{}; 
    }
    namespace Lpit0Tval2{    ///<Timer Value Register
        using Addr = Register::Address<0x40037040,0x00000000,0x00000000,unsigned>;
        ///Timer Value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> tmrVal{}; 
    }
    namespace Lpit0Cval2{    ///<Current Timer Value
        using Addr = Register::Address<0x40037044,0x00000000,0x00000000,unsigned>;
        ///Current Timer Value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> tmrCurVal{}; 
    }
    namespace Lpit0Tctrl2{    ///<Timer Control Register
        using Addr = Register::Address<0x40037048,0xf078fff0,0x00000000,unsigned>;
        ///Timer Enable
        enum class TenVal : unsigned {
            v0=0x00000000,     ///<Timer Channel is disabled
            v1=0x00000001,     ///<Timer Channel is enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,TenVal> tEn{}; 
        namespace TenValC{
            constexpr Register::FieldValue<decltype(tEn)::Type,TenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tEn)::Type,TenVal::v1> v1{};
        }
        ///Chain Channel
        enum class ChainVal : unsigned {
            v0=0x00000000,     ///<Channel Chaining is disabled. Channel Timer runs independently.
            v1=0x00000001,     ///<Channel Chaining is enabled. Timer decrements on previous channel's timeout
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,ChainVal> chain{}; 
        namespace ChainValC{
            constexpr Register::FieldValue<decltype(chain)::Type,ChainVal::v0> v0{};
            constexpr Register::FieldValue<decltype(chain)::Type,ChainVal::v1> v1{};
        }
        ///Timer Operation Mode
        enum class ModeVal : unsigned {
            v0=0x00000000,     ///<32-bit Periodic Counter
            v1=0x00000001,     ///<Dual 16-bit Periodic Counter
            v10=0x00000002,     ///<32-bit Trigger Accumulator
            v11=0x00000003,     ///<32-bit Trigger Input Capture
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,2),Register::ReadWriteAccess,ModeVal> mode{}; 
        namespace ModeValC{
            constexpr Register::FieldValue<decltype(mode)::Type,ModeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(mode)::Type,ModeVal::v1> v1{};
            constexpr Register::FieldValue<decltype(mode)::Type,ModeVal::v10> v10{};
            constexpr Register::FieldValue<decltype(mode)::Type,ModeVal::v11> v11{};
        }
        ///Timer Start On Trigger
        enum class TsotVal : unsigned {
            v0=0x00000000,     ///<Timer starts to decrement immediately based on restart condition (controlled by TSOI bit)
            v1=0x00000001,     ///<Timer starts to decrement when rising edge on selected trigger is detected
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,TsotVal> tsot{}; 
        namespace TsotValC{
            constexpr Register::FieldValue<decltype(tsot)::Type,TsotVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tsot)::Type,TsotVal::v1> v1{};
        }
        ///Timer Stop On Interrupt
        enum class TsoiVal : unsigned {
            v0=0x00000000,     ///<The channel timer does not stop after timeout.
            v1=0x00000001,     ///<The channel timer will stop after a timeout, and the channel timer will restart based on TSOT. When TSOT = 0, the channel timer will restart after a rising edge on the T_EN bit is detected (which means that the timer channel is disabled and then enabled); when TSOT = 1, the channel timer will restart after a rising edge on the selected trigger is detected.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,17),Register::ReadWriteAccess,TsoiVal> tsoi{}; 
        namespace TsoiValC{
            constexpr Register::FieldValue<decltype(tsoi)::Type,TsoiVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tsoi)::Type,TsoiVal::v1> v1{};
        }
        ///Timer Reload On Trigger
        enum class TrotVal : unsigned {
            v0=0x00000000,     ///<Timer will not reload on selected trigger
            v1=0x00000001,     ///<Timer will reload on selected trigger
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(18,18),Register::ReadWriteAccess,TrotVal> trot{}; 
        namespace TrotValC{
            constexpr Register::FieldValue<decltype(trot)::Type,TrotVal::v0> v0{};
            constexpr Register::FieldValue<decltype(trot)::Type,TrotVal::v1> v1{};
        }
        ///Trigger Source
        enum class TrgsrcVal : unsigned {
            v0=0x00000000,     ///<Trigger source selected in external
            v1=0x00000001,     ///<Trigger source selected is the internal trigger
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,23),Register::ReadWriteAccess,TrgsrcVal> trgSrc{}; 
        namespace TrgsrcValC{
            constexpr Register::FieldValue<decltype(trgSrc)::Type,TrgsrcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(trgSrc)::Type,TrgsrcVal::v1> v1{};
        }
        ///Trigger Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,24),Register::ReadWriteAccess,unsigned> trgSel{}; 
    }
    namespace Lpit0Tval3{    ///<Timer Value Register
        using Addr = Register::Address<0x40037050,0x00000000,0x00000000,unsigned>;
        ///Timer Value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> tmrVal{}; 
    }
    namespace Lpit0Cval3{    ///<Current Timer Value
        using Addr = Register::Address<0x40037054,0x00000000,0x00000000,unsigned>;
        ///Current Timer Value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> tmrCurVal{}; 
    }
    namespace Lpit0Tctrl3{    ///<Timer Control Register
        using Addr = Register::Address<0x40037058,0xf078fff0,0x00000000,unsigned>;
        ///Timer Enable
        enum class TenVal : unsigned {
            v0=0x00000000,     ///<Timer Channel is disabled
            v1=0x00000001,     ///<Timer Channel is enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,TenVal> tEn{}; 
        namespace TenValC{
            constexpr Register::FieldValue<decltype(tEn)::Type,TenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tEn)::Type,TenVal::v1> v1{};
        }
        ///Chain Channel
        enum class ChainVal : unsigned {
            v0=0x00000000,     ///<Channel Chaining is disabled. Channel Timer runs independently.
            v1=0x00000001,     ///<Channel Chaining is enabled. Timer decrements on previous channel's timeout
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,ChainVal> chain{}; 
        namespace ChainValC{
            constexpr Register::FieldValue<decltype(chain)::Type,ChainVal::v0> v0{};
            constexpr Register::FieldValue<decltype(chain)::Type,ChainVal::v1> v1{};
        }
        ///Timer Operation Mode
        enum class ModeVal : unsigned {
            v0=0x00000000,     ///<32-bit Periodic Counter
            v1=0x00000001,     ///<Dual 16-bit Periodic Counter
            v10=0x00000002,     ///<32-bit Trigger Accumulator
            v11=0x00000003,     ///<32-bit Trigger Input Capture
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,2),Register::ReadWriteAccess,ModeVal> mode{}; 
        namespace ModeValC{
            constexpr Register::FieldValue<decltype(mode)::Type,ModeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(mode)::Type,ModeVal::v1> v1{};
            constexpr Register::FieldValue<decltype(mode)::Type,ModeVal::v10> v10{};
            constexpr Register::FieldValue<decltype(mode)::Type,ModeVal::v11> v11{};
        }
        ///Timer Start On Trigger
        enum class TsotVal : unsigned {
            v0=0x00000000,     ///<Timer starts to decrement immediately based on restart condition (controlled by TSOI bit)
            v1=0x00000001,     ///<Timer starts to decrement when rising edge on selected trigger is detected
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,TsotVal> tsot{}; 
        namespace TsotValC{
            constexpr Register::FieldValue<decltype(tsot)::Type,TsotVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tsot)::Type,TsotVal::v1> v1{};
        }
        ///Timer Stop On Interrupt
        enum class TsoiVal : unsigned {
            v0=0x00000000,     ///<The channel timer does not stop after timeout.
            v1=0x00000001,     ///<The channel timer will stop after a timeout, and the channel timer will restart based on TSOT. When TSOT = 0, the channel timer will restart after a rising edge on the T_EN bit is detected (which means that the timer channel is disabled and then enabled); when TSOT = 1, the channel timer will restart after a rising edge on the selected trigger is detected.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,17),Register::ReadWriteAccess,TsoiVal> tsoi{}; 
        namespace TsoiValC{
            constexpr Register::FieldValue<decltype(tsoi)::Type,TsoiVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tsoi)::Type,TsoiVal::v1> v1{};
        }
        ///Timer Reload On Trigger
        enum class TrotVal : unsigned {
            v0=0x00000000,     ///<Timer will not reload on selected trigger
            v1=0x00000001,     ///<Timer will reload on selected trigger
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(18,18),Register::ReadWriteAccess,TrotVal> trot{}; 
        namespace TrotValC{
            constexpr Register::FieldValue<decltype(trot)::Type,TrotVal::v0> v0{};
            constexpr Register::FieldValue<decltype(trot)::Type,TrotVal::v1> v1{};
        }
        ///Trigger Source
        enum class TrgsrcVal : unsigned {
            v0=0x00000000,     ///<Trigger source selected in external
            v1=0x00000001,     ///<Trigger source selected is the internal trigger
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,23),Register::ReadWriteAccess,TrgsrcVal> trgSrc{}; 
        namespace TrgsrcValC{
            constexpr Register::FieldValue<decltype(trgSrc)::Type,TrgsrcVal::v0> v0{};
            constexpr Register::FieldValue<decltype(trgSrc)::Type,TrgsrcVal::v1> v1{};
        }
        ///Trigger Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,24),Register::ReadWriteAccess,unsigned> trgSel{}; 
    }
}
