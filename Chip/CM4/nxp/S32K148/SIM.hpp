#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//System Integration Module
    namespace SimChipctl{    ///<Chip Control register
        using Addr = Register::Address<0x40048004,0xffc0c000,0x00000000,unsigned>;
        ///ADC interleave channel enable
        enum class AdcinterleaveenVal : unsigned {
            v0000=0x00000000,     ///<Interleaving disabled. No channel pair interleaved. Interleaved channels are individually connected to pins. PTC0 is connected to ADC0_SE8. PTC1 is connected to ADC0_SE9. PTB15 is connected to ADC1_SE14. PTB16 is connected to ADC1_SE15. PTB0 is connected to ADC0_SE4. PTB1 is connected to ADC0_SE5. PTB13 is connected to ADC1_SE8. PTB14 is connected to ADC1_SE9.
            v1xxx=0x00000008,     ///<PTB14 to ADC1_SE9 and ADC0_SE9
            x1xx=0x00000004,     ///<PTB13 to ADC1_SE8 and ADC0_SE8
            xx1x=0x00000002,     ///<PTB1 to ADC0_SE5 and ADC1_SE15
            xxx1=0x00000001,     ///<PTB0 to ADC0_SE4 and ADC1_SE14
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::ReadWriteAccess,AdcinterleaveenVal> adcInterleaveEn{}; 
        namespace AdcinterleaveenValC{
            constexpr Register::FieldValue<decltype(adcInterleaveEn)::Type,AdcinterleaveenVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(adcInterleaveEn)::Type,AdcinterleaveenVal::v1xxx> v1xxx{};
            constexpr Register::FieldValue<decltype(adcInterleaveEn)::Type,AdcinterleaveenVal::x1xx> x1xx{};
            constexpr Register::FieldValue<decltype(adcInterleaveEn)::Type,AdcinterleaveenVal::xx1x> xx1x{};
            constexpr Register::FieldValue<decltype(adcInterleaveEn)::Type,AdcinterleaveenVal::xxx1> xxx1{};
        }
        ///CLKOUT Select
        enum class ClkoutselVal : unsigned {
            v0000=0x00000000,     ///<SCG CLKOUT
            v0010=0x00000002,     ///<SOSC DIV2 CLK
            v0100=0x00000004,     ///<SIRC DIV2 CLK
            v0101=0x00000005,     ///<For S32K148: QSPI SFIF_CLK_HYP: Divide by 2 clock (configured through SCLKCONFIG[5]) for HyperRAM going to sfif clock to QSPI; For others: Reserved
            v0110=0x00000006,     ///<FIRC DIV2 CLK
            v0111=0x00000007,     ///<HCLK
            v1000=0x00000008,     ///<SPLL DIV2 CLK
            v1001=0x00000009,     ///<BUS_CLK
            v1010=0x0000000a,     ///<LPO128K_CLK
            v1011=0x0000000b,     ///<For S32K148: QSPI IPG_CLK; For others: Reserved
            v1100=0x0000000c,     ///<LPO_CLK as selected by SIM_LPOCLKS[LPOCLKSEL]
            v1101=0x0000000d,     ///<For S32K148: QSPI IPG_CLK_SFIF; For others: Reserved
            v1110=0x0000000e,     ///<RTC_CLK as selected by SIM_LPOCLKS[RTCCLKSEL]
            v1111=0x0000000f,     ///<For S32K148: QSPI IPG_CLK_2XSFIF; For others: Reserved
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,4),Register::ReadWriteAccess,ClkoutselVal> clkoutsel{}; 
        namespace ClkoutselValC{
            constexpr Register::FieldValue<decltype(clkoutsel)::Type,ClkoutselVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(clkoutsel)::Type,ClkoutselVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(clkoutsel)::Type,ClkoutselVal::v0100> v0100{};
            constexpr Register::FieldValue<decltype(clkoutsel)::Type,ClkoutselVal::v0101> v0101{};
            constexpr Register::FieldValue<decltype(clkoutsel)::Type,ClkoutselVal::v0110> v0110{};
            constexpr Register::FieldValue<decltype(clkoutsel)::Type,ClkoutselVal::v0111> v0111{};
            constexpr Register::FieldValue<decltype(clkoutsel)::Type,ClkoutselVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(clkoutsel)::Type,ClkoutselVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(clkoutsel)::Type,ClkoutselVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(clkoutsel)::Type,ClkoutselVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(clkoutsel)::Type,ClkoutselVal::v1100> v1100{};
            constexpr Register::FieldValue<decltype(clkoutsel)::Type,ClkoutselVal::v1101> v1101{};
            constexpr Register::FieldValue<decltype(clkoutsel)::Type,ClkoutselVal::v1110> v1110{};
            constexpr Register::FieldValue<decltype(clkoutsel)::Type,ClkoutselVal::v1111> v1111{};
        }
        ///CLKOUT Divide Ratio
        enum class ClkoutdivVal : unsigned {
            v000=0x00000000,     ///<Divide by 1
            v001=0x00000001,     ///<Divide by 2
            v010=0x00000002,     ///<Divide by 3
            v011=0x00000003,     ///<Divide by 4
            v100=0x00000004,     ///<Divide by 5
            v101=0x00000005,     ///<Divide by 6
            v110=0x00000006,     ///<Divide by 7
            v111=0x00000007,     ///<Divide by 8
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,ClkoutdivVal> clkoutdiv{}; 
        namespace ClkoutdivValC{
            constexpr Register::FieldValue<decltype(clkoutdiv)::Type,ClkoutdivVal::v000> v000{};
            constexpr Register::FieldValue<decltype(clkoutdiv)::Type,ClkoutdivVal::v001> v001{};
            constexpr Register::FieldValue<decltype(clkoutdiv)::Type,ClkoutdivVal::v010> v010{};
            constexpr Register::FieldValue<decltype(clkoutdiv)::Type,ClkoutdivVal::v011> v011{};
            constexpr Register::FieldValue<decltype(clkoutdiv)::Type,ClkoutdivVal::v100> v100{};
            constexpr Register::FieldValue<decltype(clkoutdiv)::Type,ClkoutdivVal::v101> v101{};
            constexpr Register::FieldValue<decltype(clkoutdiv)::Type,ClkoutdivVal::v110> v110{};
            constexpr Register::FieldValue<decltype(clkoutdiv)::Type,ClkoutdivVal::v111> v111{};
        }
        ///CLKOUT enable
        enum class ClkoutenVal : unsigned {
            v0=0x00000000,     ///<Clockout disable
            v1=0x00000001,     ///<Clockout enable
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,ClkoutenVal> clkouten{}; 
        namespace ClkoutenValC{
            constexpr Register::FieldValue<decltype(clkouten)::Type,ClkoutenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(clkouten)::Type,ClkoutenVal::v1> v1{};
        }
        ///Debug trace clock select
        enum class TraceclkselVal : unsigned {
            v0=0x00000000,     ///<Core clock
            v1=0x00000001,     ///<Platform clock
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,12),Register::ReadWriteAccess,TraceclkselVal> traceclkSel{}; 
        namespace TraceclkselValC{
            constexpr Register::FieldValue<decltype(traceclkSel)::Type,TraceclkselVal::v0> v0{};
            constexpr Register::FieldValue<decltype(traceclkSel)::Type,TraceclkselVal::v1> v1{};
        }
        ///PDB back-to-back select
        enum class PdbbbselVal : unsigned {
            v0=0x00000000,     ///<PDB0 channel 0 back-to-back operation with ADC0 COCO[7:0] and PDB1 channel 0 back-to-back operation with ADC1 COCO[7:0]
            v1=0x00000001,     ///<Channel 0 of PDB0 and PDB1 back-to-back operation with COCO[7:0] of ADC0 and ADC1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,13),Register::ReadWriteAccess,PdbbbselVal> pdbBbSel{}; 
        namespace PdbbbselValC{
            constexpr Register::FieldValue<decltype(pdbBbSel)::Type,PdbbbselVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pdbBbSel)::Type,PdbbbselVal::v1> v1{};
        }
        ///ADC_SUPPLY
        enum class AdcsupplyVal : unsigned {
            v000=0x00000000,     ///<5 V input VDD supply (VDD)
            v001=0x00000001,     ///<5 V input analog supply (VDDA)
            v010=0x00000002,     ///<ADC Reference Supply (VREFH)
            v011=0x00000003,     ///<3.3 V Oscillator Regulator Output (VDD_3V)
            v100=0x00000004,     ///<3.3 V flash regulator output (VDD_flash_3V)
            v101=0x00000005,     ///<1.2 V core regulator output (VDD_LV)
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(18,16),Register::ReadWriteAccess,AdcsupplyVal> adcSupply{}; 
        namespace AdcsupplyValC{
            constexpr Register::FieldValue<decltype(adcSupply)::Type,AdcsupplyVal::v000> v000{};
            constexpr Register::FieldValue<decltype(adcSupply)::Type,AdcsupplyVal::v001> v001{};
            constexpr Register::FieldValue<decltype(adcSupply)::Type,AdcsupplyVal::v010> v010{};
            constexpr Register::FieldValue<decltype(adcSupply)::Type,AdcsupplyVal::v011> v011{};
            constexpr Register::FieldValue<decltype(adcSupply)::Type,AdcsupplyVal::v100> v100{};
            constexpr Register::FieldValue<decltype(adcSupply)::Type,AdcsupplyVal::v101> v101{};
        }
        ///ADC_SUPPLYEN
        enum class AdcsupplyenVal : unsigned {
            v0=0x00000000,     ///<Disable internal supply monitoring
            v1=0x00000001,     ///<Enable internal supply monitoring
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,19),Register::ReadWriteAccess,AdcsupplyenVal> adcSupplyen{}; 
        namespace AdcsupplyenValC{
            constexpr Register::FieldValue<decltype(adcSupplyen)::Type,AdcsupplyenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(adcSupplyen)::Type,AdcsupplyenVal::v1> v1{};
        }
        ///SRAMU_RETEN
        enum class SramuretenVal : unsigned {
            v0=0x00000000,     ///<SRAMU contents are retained across resets
            v1=0x00000001,     ///<No SRAMU retention
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,20),Register::ReadWriteAccess,SramuretenVal> sramuReten{}; 
        namespace SramuretenValC{
            constexpr Register::FieldValue<decltype(sramuReten)::Type,SramuretenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sramuReten)::Type,SramuretenVal::v1> v1{};
        }
        ///SRAML_RETEN
        enum class SramlretenVal : unsigned {
            v0=0x00000000,     ///<SRAML contents are retained across resets
            v1=0x00000001,     ///<No SRAML retention
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(21,21),Register::ReadWriteAccess,SramlretenVal> sramlReten{}; 
        namespace SramlretenValC{
            constexpr Register::FieldValue<decltype(sramlReten)::Type,SramlretenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sramlReten)::Type,SramlretenVal::v1> v1{};
        }
    }
    namespace SimFtmopt0{    ///<FTM Option Register 0
        using Addr = Register::Address<0x4004800c,0x00008888,0x00000000,unsigned>;
        ///FTM0 Fault X Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> ftm0fltxsel{}; 
        ///FTM1 Fault X Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,4),Register::ReadWriteAccess,unsigned> ftm1fltxsel{}; 
        ///FTM2 Fault X Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,unsigned> ftm2fltxsel{}; 
        ///FTM3 Fault X Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,12),Register::ReadWriteAccess,unsigned> ftm3fltxsel{}; 
        ///FTM4 External Clock Pin Select
        enum class Ftm4clkselVal : unsigned {
            v00=0x00000000,     ///<FTM4 external clock driven by TCLK0 pin.
            v01=0x00000001,     ///<FTM4 external clock driven by TCLK1 pin.
            v10=0x00000002,     ///<FTM4 external clock driven by TCLK2 pin.
            v11=0x00000003,     ///<No clock input
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,16),Register::ReadWriteAccess,Ftm4clkselVal> ftm4clksel{}; 
        namespace Ftm4clkselValC{
            constexpr Register::FieldValue<decltype(ftm4clksel)::Type,Ftm4clkselVal::v00> v00{};
            constexpr Register::FieldValue<decltype(ftm4clksel)::Type,Ftm4clkselVal::v01> v01{};
            constexpr Register::FieldValue<decltype(ftm4clksel)::Type,Ftm4clkselVal::v10> v10{};
            constexpr Register::FieldValue<decltype(ftm4clksel)::Type,Ftm4clkselVal::v11> v11{};
        }
        ///FTM5 External Clock Pin Select
        enum class Ftm5clkselVal : unsigned {
            v00=0x00000000,     ///<FTM5 external clock driven by TCLK0 pin.
            v01=0x00000001,     ///<FTM5 external clock driven by TCLK1 pin.
            v10=0x00000002,     ///<FTM5 external clock driven by TCLK2 pin.
            v11=0x00000003,     ///<No clock input
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,18),Register::ReadWriteAccess,Ftm5clkselVal> ftm5clksel{}; 
        namespace Ftm5clkselValC{
            constexpr Register::FieldValue<decltype(ftm5clksel)::Type,Ftm5clkselVal::v00> v00{};
            constexpr Register::FieldValue<decltype(ftm5clksel)::Type,Ftm5clkselVal::v01> v01{};
            constexpr Register::FieldValue<decltype(ftm5clksel)::Type,Ftm5clkselVal::v10> v10{};
            constexpr Register::FieldValue<decltype(ftm5clksel)::Type,Ftm5clkselVal::v11> v11{};
        }
        ///FTM6 External Clock Pin Select
        enum class Ftm6clkselVal : unsigned {
            v00=0x00000000,     ///<FTM6 external clock driven by TCLK0 pin.
            v01=0x00000001,     ///<FTM6 external clock driven by TCLK1 pin.
            v10=0x00000002,     ///<FTM6 external clock driven by TCLK2 pin.
            v11=0x00000003,     ///<No clock input
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(21,20),Register::ReadWriteAccess,Ftm6clkselVal> ftm6clksel{}; 
        namespace Ftm6clkselValC{
            constexpr Register::FieldValue<decltype(ftm6clksel)::Type,Ftm6clkselVal::v00> v00{};
            constexpr Register::FieldValue<decltype(ftm6clksel)::Type,Ftm6clkselVal::v01> v01{};
            constexpr Register::FieldValue<decltype(ftm6clksel)::Type,Ftm6clkselVal::v10> v10{};
            constexpr Register::FieldValue<decltype(ftm6clksel)::Type,Ftm6clkselVal::v11> v11{};
        }
        ///FTM7 External Clock Pin Select
        enum class Ftm7clkselVal : unsigned {
            v00=0x00000000,     ///<FTM7 external clock driven by TCLK0 pin.
            v01=0x00000001,     ///<FTM7 external clock driven by TCLK1 pin.
            v10=0x00000002,     ///<FTM7 external clock driven by TCLK2 pin.
            v11=0x00000003,     ///<No clock input
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,22),Register::ReadWriteAccess,Ftm7clkselVal> ftm7clksel{}; 
        namespace Ftm7clkselValC{
            constexpr Register::FieldValue<decltype(ftm7clksel)::Type,Ftm7clkselVal::v00> v00{};
            constexpr Register::FieldValue<decltype(ftm7clksel)::Type,Ftm7clkselVal::v01> v01{};
            constexpr Register::FieldValue<decltype(ftm7clksel)::Type,Ftm7clkselVal::v10> v10{};
            constexpr Register::FieldValue<decltype(ftm7clksel)::Type,Ftm7clkselVal::v11> v11{};
        }
        ///FTM0 External Clock Pin Select
        enum class Ftm0clkselVal : unsigned {
            v00=0x00000000,     ///<FTM0 external clock driven by TCLK0 pin.
            v01=0x00000001,     ///<FTM0 external clock driven by TCLK1 pin.
            v10=0x00000002,     ///<FTM0 external clock driven by TCLK2 pin.
            v11=0x00000003,     ///<No clock input
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Ftm0clkselVal> ftm0clksel{}; 
        namespace Ftm0clkselValC{
            constexpr Register::FieldValue<decltype(ftm0clksel)::Type,Ftm0clkselVal::v00> v00{};
            constexpr Register::FieldValue<decltype(ftm0clksel)::Type,Ftm0clkselVal::v01> v01{};
            constexpr Register::FieldValue<decltype(ftm0clksel)::Type,Ftm0clkselVal::v10> v10{};
            constexpr Register::FieldValue<decltype(ftm0clksel)::Type,Ftm0clkselVal::v11> v11{};
        }
        ///FTM1 External Clock Pin Select
        enum class Ftm1clkselVal : unsigned {
            v00=0x00000000,     ///<FTM1 external clock driven by TCLK0 pin.
            v01=0x00000001,     ///<FTM1 external clock driven by TCLK1 pin.
            v10=0x00000002,     ///<FTM1 external clock driven by TCLK2 pin.
            v11=0x00000003,     ///<No clock input
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,26),Register::ReadWriteAccess,Ftm1clkselVal> ftm1clksel{}; 
        namespace Ftm1clkselValC{
            constexpr Register::FieldValue<decltype(ftm1clksel)::Type,Ftm1clkselVal::v00> v00{};
            constexpr Register::FieldValue<decltype(ftm1clksel)::Type,Ftm1clkselVal::v01> v01{};
            constexpr Register::FieldValue<decltype(ftm1clksel)::Type,Ftm1clkselVal::v10> v10{};
            constexpr Register::FieldValue<decltype(ftm1clksel)::Type,Ftm1clkselVal::v11> v11{};
        }
        ///FTM2 External Clock Pin Select
        enum class Ftm2clkselVal : unsigned {
            v00=0x00000000,     ///<FTM2 external clock driven by TCLK0 pin.
            v01=0x00000001,     ///<FTM2 external clock driven by TCLK1 pin.
            v10=0x00000002,     ///<FTM2 external clock driven by TCLK2 pin.
            v11=0x00000003,     ///<No clock input
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(29,28),Register::ReadWriteAccess,Ftm2clkselVal> ftm2clksel{}; 
        namespace Ftm2clkselValC{
            constexpr Register::FieldValue<decltype(ftm2clksel)::Type,Ftm2clkselVal::v00> v00{};
            constexpr Register::FieldValue<decltype(ftm2clksel)::Type,Ftm2clkselVal::v01> v01{};
            constexpr Register::FieldValue<decltype(ftm2clksel)::Type,Ftm2clkselVal::v10> v10{};
            constexpr Register::FieldValue<decltype(ftm2clksel)::Type,Ftm2clkselVal::v11> v11{};
        }
        ///FTM3 External Clock Pin Select
        enum class Ftm3clkselVal : unsigned {
            v00=0x00000000,     ///<FTM3 external clock driven by TCLK0 pin.
            v01=0x00000001,     ///<FTM3 external clock driven by TCLK1 pin.
            v10=0x00000002,     ///<FTM3 external clock driven by TCLK2 pin.
            v11=0x00000003,     ///<No clock input
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,30),Register::ReadWriteAccess,Ftm3clkselVal> ftm3clksel{}; 
        namespace Ftm3clkselValC{
            constexpr Register::FieldValue<decltype(ftm3clksel)::Type,Ftm3clkselVal::v00> v00{};
            constexpr Register::FieldValue<decltype(ftm3clksel)::Type,Ftm3clkselVal::v01> v01{};
            constexpr Register::FieldValue<decltype(ftm3clksel)::Type,Ftm3clkselVal::v10> v10{};
            constexpr Register::FieldValue<decltype(ftm3clksel)::Type,Ftm3clkselVal::v11> v11{};
        }
    }
    namespace SimLpoclks{    ///<LPO Clock Select Register
        using Addr = Register::Address<0x40048010,0xffffffc0,0x00000000,unsigned>;
        ///1 kHz LPO_CLK enable
        enum class Lpo1kclkenVal : unsigned {
            v0=0x00000000,     ///<Disable 1 kHz LPO_CLK output
            v1=0x00000001,     ///<Enable 1 kHz LPO_CLK output
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,Lpo1kclkenVal> lpo1kclken{}; 
        namespace Lpo1kclkenValC{
            constexpr Register::FieldValue<decltype(lpo1kclken)::Type,Lpo1kclkenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lpo1kclken)::Type,Lpo1kclkenVal::v1> v1{};
        }
        ///32 kHz LPO_CLK enable
        enum class Lpo32kclkenVal : unsigned {
            v0=0x00000000,     ///<Disable 32 kHz LPO_CLK output
            v1=0x00000001,     ///<Enable 32 kHz LPO_CLK output
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,Lpo32kclkenVal> lpo32kclken{}; 
        namespace Lpo32kclkenValC{
            constexpr Register::FieldValue<decltype(lpo32kclken)::Type,Lpo32kclkenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lpo32kclken)::Type,Lpo32kclkenVal::v1> v1{};
        }
        ///LPO clock source select
        enum class LpoclkselVal : unsigned {
            v00=0x00000000,     ///<128 kHz LPO_CLK
            v01=0x00000001,     ///<No clock
            v10=0x00000002,     ///<32 kHz LPO_CLK which is derived from the 128 kHz LPO_CLK
            v11=0x00000003,     ///<1 kHz LPO_CLK which is derived from the 128 kHz LPO_CLK
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,2),Register::ReadWriteAccess,LpoclkselVal> lpoclksel{}; 
        namespace LpoclkselValC{
            constexpr Register::FieldValue<decltype(lpoclksel)::Type,LpoclkselVal::v00> v00{};
            constexpr Register::FieldValue<decltype(lpoclksel)::Type,LpoclkselVal::v01> v01{};
            constexpr Register::FieldValue<decltype(lpoclksel)::Type,LpoclkselVal::v10> v10{};
            constexpr Register::FieldValue<decltype(lpoclksel)::Type,LpoclkselVal::v11> v11{};
        }
        ///32 kHz clock source select
        enum class RtcclkselVal : unsigned {
            v00=0x00000000,     ///<SOSCDIV1_CLK
            v01=0x00000001,     ///<32 kHz LPO_CLK
            v10=0x00000002,     ///<RTC_CLKIN clock
            v11=0x00000003,     ///<FIRCDIV1_CLK
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,4),Register::ReadWriteAccess,RtcclkselVal> rtcclksel{}; 
        namespace RtcclkselValC{
            constexpr Register::FieldValue<decltype(rtcclksel)::Type,RtcclkselVal::v00> v00{};
            constexpr Register::FieldValue<decltype(rtcclksel)::Type,RtcclkselVal::v01> v01{};
            constexpr Register::FieldValue<decltype(rtcclksel)::Type,RtcclkselVal::v10> v10{};
            constexpr Register::FieldValue<decltype(rtcclksel)::Type,RtcclkselVal::v11> v11{};
        }
    }
    namespace SimAdcopt{    ///<ADC Options Register
        using Addr = Register::Address<0x40048018,0xffffc0c0,0x00000000,unsigned>;
        ///ADC0 trigger source select
        enum class Adc0trgselVal : unsigned {
            v0=0x00000000,     ///<PDB output
            v1=0x00000001,     ///<TRGMUX output
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,Adc0trgselVal> adc0trgsel{}; 
        namespace Adc0trgselValC{
            constexpr Register::FieldValue<decltype(adc0trgsel)::Type,Adc0trgselVal::v0> v0{};
            constexpr Register::FieldValue<decltype(adc0trgsel)::Type,Adc0trgselVal::v1> v1{};
        }
        ///ADC0 software pretrigger sources
        enum class Adc0swpretrgVal : unsigned {
            v000=0x00000000,     ///<Software pretrigger disabled
            v001=0x00000001,     ///<Reserved (do not use)
            v010=0x00000002,     ///<Reserved (do not use)
            v011=0x00000003,     ///<Reserved (do not use)
            v100=0x00000004,     ///<Software pretrigger 0
            v101=0x00000005,     ///<Software pretrigger 1
            v110=0x00000006,     ///<Software pretrigger 2
            v111=0x00000007,     ///<Software pretrigger 3
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,1),Register::ReadWriteAccess,Adc0swpretrgVal> adc0swpretrg{}; 
        namespace Adc0swpretrgValC{
            constexpr Register::FieldValue<decltype(adc0swpretrg)::Type,Adc0swpretrgVal::v000> v000{};
            constexpr Register::FieldValue<decltype(adc0swpretrg)::Type,Adc0swpretrgVal::v001> v001{};
            constexpr Register::FieldValue<decltype(adc0swpretrg)::Type,Adc0swpretrgVal::v010> v010{};
            constexpr Register::FieldValue<decltype(adc0swpretrg)::Type,Adc0swpretrgVal::v011> v011{};
            constexpr Register::FieldValue<decltype(adc0swpretrg)::Type,Adc0swpretrgVal::v100> v100{};
            constexpr Register::FieldValue<decltype(adc0swpretrg)::Type,Adc0swpretrgVal::v101> v101{};
            constexpr Register::FieldValue<decltype(adc0swpretrg)::Type,Adc0swpretrgVal::v110> v110{};
            constexpr Register::FieldValue<decltype(adc0swpretrg)::Type,Adc0swpretrgVal::v111> v111{};
        }
        ///ADC0 pretrigger source select
        enum class Adc0pretrgselVal : unsigned {
            v00=0x00000000,     ///<PDB pretrigger (default)
            v01=0x00000001,     ///<TRGMUX pretrigger
            v10=0x00000002,     ///<Software pretrigger
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,4),Register::ReadWriteAccess,Adc0pretrgselVal> adc0pretrgsel{}; 
        namespace Adc0pretrgselValC{
            constexpr Register::FieldValue<decltype(adc0pretrgsel)::Type,Adc0pretrgselVal::v00> v00{};
            constexpr Register::FieldValue<decltype(adc0pretrgsel)::Type,Adc0pretrgselVal::v01> v01{};
            constexpr Register::FieldValue<decltype(adc0pretrgsel)::Type,Adc0pretrgselVal::v10> v10{};
        }
        ///ADC1 trigger source select
        enum class Adc1trgselVal : unsigned {
            v0=0x00000000,     ///<PDB output
            v1=0x00000001,     ///<TRGMUX output
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::ReadWriteAccess,Adc1trgselVal> adc1trgsel{}; 
        namespace Adc1trgselValC{
            constexpr Register::FieldValue<decltype(adc1trgsel)::Type,Adc1trgselVal::v0> v0{};
            constexpr Register::FieldValue<decltype(adc1trgsel)::Type,Adc1trgselVal::v1> v1{};
        }
        ///ADC1 software pretrigger sources
        enum class Adc1swpretrgVal : unsigned {
            v000=0x00000000,     ///<Software pretrigger disabled
            v001=0x00000001,     ///<Reserved (do not use)
            v010=0x00000002,     ///<Reserved (do not use)
            v011=0x00000003,     ///<Reserved (do not use)
            v100=0x00000004,     ///<Software pretrigger 0
            v101=0x00000005,     ///<Software pretrigger 1
            v110=0x00000006,     ///<Software pretrigger 2
            v111=0x00000007,     ///<Software pretrigger 3
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,9),Register::ReadWriteAccess,Adc1swpretrgVal> adc1swpretrg{}; 
        namespace Adc1swpretrgValC{
            constexpr Register::FieldValue<decltype(adc1swpretrg)::Type,Adc1swpretrgVal::v000> v000{};
            constexpr Register::FieldValue<decltype(adc1swpretrg)::Type,Adc1swpretrgVal::v001> v001{};
            constexpr Register::FieldValue<decltype(adc1swpretrg)::Type,Adc1swpretrgVal::v010> v010{};
            constexpr Register::FieldValue<decltype(adc1swpretrg)::Type,Adc1swpretrgVal::v011> v011{};
            constexpr Register::FieldValue<decltype(adc1swpretrg)::Type,Adc1swpretrgVal::v100> v100{};
            constexpr Register::FieldValue<decltype(adc1swpretrg)::Type,Adc1swpretrgVal::v101> v101{};
            constexpr Register::FieldValue<decltype(adc1swpretrg)::Type,Adc1swpretrgVal::v110> v110{};
            constexpr Register::FieldValue<decltype(adc1swpretrg)::Type,Adc1swpretrgVal::v111> v111{};
        }
        ///ADC1 pretrigger source select
        enum class Adc1pretrgselVal : unsigned {
            v00=0x00000000,     ///<PDB pretrigger (default)
            v01=0x00000001,     ///<TRGMUX pretrigger
            v10=0x00000002,     ///<Software pretrigger
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,12),Register::ReadWriteAccess,Adc1pretrgselVal> adc1pretrgsel{}; 
        namespace Adc1pretrgselValC{
            constexpr Register::FieldValue<decltype(adc1pretrgsel)::Type,Adc1pretrgselVal::v00> v00{};
            constexpr Register::FieldValue<decltype(adc1pretrgsel)::Type,Adc1pretrgselVal::v01> v01{};
            constexpr Register::FieldValue<decltype(adc1pretrgsel)::Type,Adc1pretrgselVal::v10> v10{};
        }
    }
    namespace SimFtmopt1{    ///<FTM Option Register 1
        using Addr = Register::Address<0x4004801c,0x00000600,0x00000000,unsigned>;
        ///FTM0 Sync Bit
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,unsigned> ftm0syncbit{}; 
        ///FTM1 Sync Bit
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,unsigned> ftm1syncbit{}; 
        ///FTM2 Sync Bit
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,unsigned> ftm2syncbit{}; 
        ///FTM3 Sync Bit
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,unsigned> ftm3syncbit{}; 
        ///FTM1 CH0 Select
        enum class Ftm1ch0selVal : unsigned {
            v00=0x00000000,     ///<FTM1_CH0 input
            v01=0x00000001,     ///<CMP0 output
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,4),Register::ReadWriteAccess,Ftm1ch0selVal> ftm1ch0sel{}; 
        namespace Ftm1ch0selValC{
            constexpr Register::FieldValue<decltype(ftm1ch0sel)::Type,Ftm1ch0selVal::v00> v00{};
            constexpr Register::FieldValue<decltype(ftm1ch0sel)::Type,Ftm1ch0selVal::v01> v01{};
        }
        ///FTM2 CH0 Select
        enum class Ftm2ch0selVal : unsigned {
            v00=0x00000000,     ///<FTM2_CH0 input
            v01=0x00000001,     ///<CMP0 output
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,6),Register::ReadWriteAccess,Ftm2ch0selVal> ftm2ch0sel{}; 
        namespace Ftm2ch0selValC{
            constexpr Register::FieldValue<decltype(ftm2ch0sel)::Type,Ftm2ch0selVal::v00> v00{};
            constexpr Register::FieldValue<decltype(ftm2ch0sel)::Type,Ftm2ch0selVal::v01> v01{};
        }
        ///FTM2 CH1 Select
        enum class Ftm2ch1selVal : unsigned {
            v0=0x00000000,     ///<FTM2_CH1 input
            v1=0x00000001,     ///<exclusive OR of FTM2_CH0,FTM2_CH1,and FTM1_CH1
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::ReadWriteAccess,Ftm2ch1selVal> ftm2ch1sel{}; 
        namespace Ftm2ch1selValC{
            constexpr Register::FieldValue<decltype(ftm2ch1sel)::Type,Ftm2ch1selVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ftm2ch1sel)::Type,Ftm2ch1selVal::v1> v1{};
        }
        ///FTM4 Sync Bit
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::ReadWriteAccess,unsigned> ftm4syncbit{}; 
        ///FTM5 Sync Bit
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,12),Register::ReadWriteAccess,unsigned> ftm5syncbit{}; 
        ///FTM6 Sync Bit
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,13),Register::ReadWriteAccess,unsigned> ftm6syncbit{}; 
        ///FTM7 Sync Bit
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,14),Register::ReadWriteAccess,unsigned> ftm7syncbit{}; 
        ///FTM global load enable
        enum class FtmgldokVal : unsigned {
            v0=0x00000000,     ///<FTM Global load mechanism disabled.
            v1=0x00000001,     ///<FTM Global load mechanism enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,FtmgldokVal> ftmgldok{}; 
        namespace FtmgldokValC{
            constexpr Register::FieldValue<decltype(ftmgldok)::Type,FtmgldokVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ftmgldok)::Type,FtmgldokVal::v1> v1{};
        }
        ///FTM0 channel modulation select with FTM1_CH1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> ftm0Outsel{}; 
        ///FTM3 channel modulation select with FTM2_CH1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> ftm3Outsel{}; 
    }
    namespace SimMisctrl0{    ///<Miscellaneous control register 0
        using Addr = Register::Address<0x40048020,0xf800bfff,0x00000000,unsigned>;
        ///FTM GTB split enable/disable bit
        enum class FtmgtbsplitenVal : unsigned {
            v0=0x00000000,     ///<All the FTMs have a single global time-base
            v1=0x00000001,     ///<FTM0-3 have a common time-base and others have a different common time-base. Please refer 'FTM global time base' in FTM chapter for implementation details.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,14),Register::ReadWriteAccess,FtmgtbsplitenVal> ftmGtbSplitEn{}; 
        namespace FtmgtbsplitenValC{
            constexpr Register::FieldValue<decltype(ftmGtbSplitEn)::Type,FtmgtbsplitenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ftmGtbSplitEn)::Type,FtmgtbsplitenVal::v1> v1{};
        }
        ///FTM0 OBE CTRL bit
        enum class Ftm0obectrlVal : unsigned {
            v0=0x00000000,     ///<The FTM channel output is put to safe state when the FTM counter is enabled and the FTM channel output is enabled by Fault Control (FTM_MODE[FAULTM]!=2'b00 and FTM_FLTCTRL[FSTATE]=1'b0) and PWM is enabled (FTM_SC[PWMENn] = 1'b1). Otherwise the channel output is tristated.
            v1=0x00000001,     ///<The FTM channel output state is retained when the channel is in output mode. The output channel is tristated when the channel is in input capture [DECAPEN=1'b0, COMBINE=1'b0, MSnB:MSnA=2'b00] or dual edge capture mode [DECAPEN=1'b1].
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,Ftm0obectrlVal> ftm0ObeCtrl{}; 
        namespace Ftm0obectrlValC{
            constexpr Register::FieldValue<decltype(ftm0ObeCtrl)::Type,Ftm0obectrlVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ftm0ObeCtrl)::Type,Ftm0obectrlVal::v1> v1{};
        }
        ///FTM1 OBE CTRL bit
        enum class Ftm1obectrlVal : unsigned {
            v0=0x00000000,     ///<The FTM channel output is put to safe state when the FTM counter is enabled and the FTM channel output is enabled by Fault Control (FTM_MODE[FAULTM]!=2'b00 and FTM_FLTCTRL[FSTATE]=1'b0) and PWM is enabled (FTM_SC[PWMENn] = 1'b1). Otherwise the channel output is tristated.
            v1=0x00000001,     ///<The FTM channel output state is retained when the channel is in output mode. The output channel is tristated when the channel is in input capture [DECAPEN=1'b0, COMBINE=1'b0, MSnB:MSnA=2'b00] or dual edge capture mode [DECAPEN=1'b1].
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,17),Register::ReadWriteAccess,Ftm1obectrlVal> ftm1ObeCtrl{}; 
        namespace Ftm1obectrlValC{
            constexpr Register::FieldValue<decltype(ftm1ObeCtrl)::Type,Ftm1obectrlVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ftm1ObeCtrl)::Type,Ftm1obectrlVal::v1> v1{};
        }
        ///FTM2 OBE CTRL bit
        enum class Ftm2obectrlVal : unsigned {
            v0=0x00000000,     ///<The FTM channel output is put to safe state when the FTM counter is enabled and the FTM channel output is enabled by Fault Control (FTM_MODE[FAULTM]!=2'b00 and FTM_FLTCTRL[FSTATE]=1'b0) and PWM is enabled (FTM_SC[PWMENn] = 1'b1). Otherwise the channel output is tristated.
            v1=0x00000001,     ///<The FTM channel output state is retained when the channel is in output mode. The output channel is tristated when the channel is in input capture [DECAPEN=1'b0, COMBINE=1'b0, MSnB:MSnA=2'b00] or dual edge capture mode [DECAPEN=1'b1].
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(18,18),Register::ReadWriteAccess,Ftm2obectrlVal> ftm2ObeCtrl{}; 
        namespace Ftm2obectrlValC{
            constexpr Register::FieldValue<decltype(ftm2ObeCtrl)::Type,Ftm2obectrlVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ftm2ObeCtrl)::Type,Ftm2obectrlVal::v1> v1{};
        }
        ///FTM3 OBE CTRL bit
        enum class Ftm3obectrlVal : unsigned {
            v0=0x00000000,     ///<The FTM channel output is put to safe state when the FTM counter is enabled and the FTM channel output is enabled by Fault Control (FTM_MODE[FAULTM]!=2'b00 and FTM_FLTCTRL[FSTATE]=1'b0) and PWM is enabled (FTM_SC[PWMENn] = 1'b1). Otherwise the channel output is tristated.
            v1=0x00000001,     ///<The FTM channel output state is retained when the channel is in output mode. The output channel is tristated when the channel is in input capture [DECAPEN=1'b0, COMBINE=1'b0, MSnB:MSnA=2'b00] or dual edge capture mode [DECAPEN=1'b1].
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,19),Register::ReadWriteAccess,Ftm3obectrlVal> ftm3ObeCtrl{}; 
        namespace Ftm3obectrlValC{
            constexpr Register::FieldValue<decltype(ftm3ObeCtrl)::Type,Ftm3obectrlVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ftm3ObeCtrl)::Type,Ftm3obectrlVal::v1> v1{};
        }
        ///FTM4 OBE CTRL bit
        enum class Ftm4obectrlVal : unsigned {
            v0=0x00000000,     ///<The FTM channel output is put to safe state when the FTM counter is enabled and the FTM channel output is enabled by Fault Control (FTM_MODE[FAULTM]!=2'b00 and FTM_FLTCTRL[FSTATE]=1'b0) and PWM is enabled (FTM_SC[PWMENn] = 1'b1). Otherwise the channel output is tristated.
            v1=0x00000001,     ///<The FTM channel output state is retained when the channel is in output mode. The output channel is tristated when the channel is in input capture [DECAPEN=1'b0, COMBINE=1'b0, MSnB:MSnA=2'b00] or dual edge capture mode [DECAPEN=1'b1].
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(20,20),Register::ReadWriteAccess,Ftm4obectrlVal> ftm4ObeCtrl{}; 
        namespace Ftm4obectrlValC{
            constexpr Register::FieldValue<decltype(ftm4ObeCtrl)::Type,Ftm4obectrlVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ftm4ObeCtrl)::Type,Ftm4obectrlVal::v1> v1{};
        }
        ///FTM5 OBE CTRL bit
        enum class Ftm5obectrlVal : unsigned {
            v0=0x00000000,     ///<The FTM channel output is put to safe state when the FTM counter is enabled and the FTM channel output is enabled by Fault Control (FTM_MODE[FAULTM]!=2'b00 and FTM_FLTCTRL[FSTATE]=1'b0) and PWM is enabled (FTM_SC[PWMENn] = 1'b1). Otherwise the channel output is tristated.
            v1=0x00000001,     ///<The FTM channel output state is retained when the channel is in output mode. The output channel is tristated when the channel is in input capture [DECAPEN=1'b0, COMBINE=1'b0, MSnB:MSnA=2'b00] or dual edge capture mode [DECAPEN=1'b1].
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(21,21),Register::ReadWriteAccess,Ftm5obectrlVal> ftm5ObeCtrl{}; 
        namespace Ftm5obectrlValC{
            constexpr Register::FieldValue<decltype(ftm5ObeCtrl)::Type,Ftm5obectrlVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ftm5ObeCtrl)::Type,Ftm5obectrlVal::v1> v1{};
        }
        ///FTM6 OBE CTRL bit
        enum class Ftm6obectrlVal : unsigned {
            v0=0x00000000,     ///<The FTM channel output is put to safe state when the FTM counter is enabled and the FTM channel output is enabled by Fault Control (FTM_MODE[FAULTM]!=2'b00 and FTM_FLTCTRL[FSTATE]=1'b0) and PWM is enabled (FTM_SC[PWMENn] = 1'b1). Otherwise the channel output is tristated.
            v1=0x00000001,     ///<The FTM channel output state is retained when the channel is in output mode. The output channel is tristated when the channel is in input capture [DECAPEN=1'b0, COMBINE=1'b0, MSnB:MSnA=2'b00] or dual edge capture mode [DECAPEN=1'b1].
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,22),Register::ReadWriteAccess,Ftm6obectrlVal> ftm6ObeCtrl{}; 
        namespace Ftm6obectrlValC{
            constexpr Register::FieldValue<decltype(ftm6ObeCtrl)::Type,Ftm6obectrlVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ftm6ObeCtrl)::Type,Ftm6obectrlVal::v1> v1{};
        }
        ///FTM7 OBE CTRL bit
        enum class Ftm7obectrlVal : unsigned {
            v0=0x00000000,     ///<The FTM channel output is put to safe state when the FTM counter is enabled and the FTM channel output is enabled by Fault Control (FTM_MODE[FAULTM]!=2'b00 and FTM_FLTCTRL[FSTATE]=1'b0) and PWM is enabled (FTM_SC[PWMENn] = 1'b1). Otherwise the channel output is tristated.
            v1=0x00000001,     ///<The FTM channel output state is retained when the channel is in output mode. The output channel is tristated when the channel is in input capture [DECAPEN=1'b0, COMBINE=1'b0, MSnB:MSnA=2'b00] or dual edge capture mode [DECAPEN=1'b1].
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,23),Register::ReadWriteAccess,Ftm7obectrlVal> ftm7ObeCtrl{}; 
        namespace Ftm7obectrlValC{
            constexpr Register::FieldValue<decltype(ftm7ObeCtrl)::Type,Ftm7obectrlVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ftm7ObeCtrl)::Type,Ftm7obectrlVal::v1> v1{};
        }
        ///RMII CLK OBE bit
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,unsigned> rmiiClkObe{}; 
        ///RMII CLK Select bit
        enum class RmiiclkselVal : unsigned {
            v0=0x00000000,     ///<FIRCDIV1_CLK
            v1=0x00000001,     ///<SOSCDIV1_CLK
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,RmiiclkselVal> rmiiClkSel{}; 
        namespace RmiiclkselValC{
            constexpr Register::FieldValue<decltype(rmiiClkSel)::Type,RmiiclkselVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rmiiClkSel)::Type,RmiiclkselVal::v1> v1{};
        }
        ///QSPI CLK Select bit
        enum class QspiclkselVal : unsigned {
            v0=0x00000000,     ///<QuadSPI internal reference clock is gated.
            v1=0x00000001,     ///<QuadSPI internal reference clock is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,QspiclkselVal> qspiClkSel{}; 
        namespace QspiclkselValC{
            constexpr Register::FieldValue<decltype(qspiClkSel)::Type,QspiclkselVal::v0> v0{};
            constexpr Register::FieldValue<decltype(qspiClkSel)::Type,QspiclkselVal::v1> v1{};
        }
    }
    namespace SimSdid{    ///<System Device Identification Register
        using Addr = Register::Address<0x40048024,0x00000000,0x00000000,unsigned>;
        ///Features
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> features{}; 
        ///Package
        enum class PackageVal : unsigned {
            v0010=0x00000002,     ///<48 LQFP
            v0011=0x00000003,     ///<64 LQFP
            v0100=0x00000004,     ///<100 LQFP
            v0110=0x00000006,     ///<144 LQFP
            v0111=0x00000007,     ///<176 LQFP
            v1000=0x00000008,     ///<100 MAP BGA
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,PackageVal> package{}; 
        namespace PackageValC{
            constexpr Register::FieldValue<decltype(package)::Type,PackageVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(package)::Type,PackageVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(package)::Type,PackageVal::v0100> v0100{};
            constexpr Register::FieldValue<decltype(package)::Type,PackageVal::v0110> v0110{};
            constexpr Register::FieldValue<decltype(package)::Type,PackageVal::v0111> v0111{};
            constexpr Register::FieldValue<decltype(package)::Type,PackageVal::v1000> v1000{};
        }
        ///Device revision number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,12),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> revid{}; 
        ///RAM size
        enum class RamsizeVal : unsigned {
            v0111=0x00000007,     ///<128 KB (S32K148), Reserved (others)
            v1001=0x00000009,     ///<160 KB (S32K148) , Reserved (others)
            v1011=0x0000000b,     ///<192 KB (S32K148), 16 KB (S32K142), Reserved (others)
            v1101=0x0000000d,     ///<48 KB (S32K144), 24 KB (S32K142), Reserved (others)
            v1111=0x0000000f,     ///<256 KB (S32K148), 64 KB (S32K144), 32 KB (S32K142)
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,RamsizeVal> ramsize{}; 
        namespace RamsizeValC{
            constexpr Register::FieldValue<decltype(ramsize)::Type,RamsizeVal::v0111> v0111{};
            constexpr Register::FieldValue<decltype(ramsize)::Type,RamsizeVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(ramsize)::Type,RamsizeVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(ramsize)::Type,RamsizeVal::v1101> v1101{};
            constexpr Register::FieldValue<decltype(ramsize)::Type,RamsizeVal::v1111> v1111{};
        }
        ///Derivate
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,20),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> derivate{}; 
        ///Subseries
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> subseries{}; 
        ///S32K product series generation
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,28),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> generation{}; 
    }
    namespace SimPlatcgc{    ///<Platform Clock Gating Control Register
        using Addr = Register::Address<0x40048040,0xffffffe0,0x00000000,unsigned>;
        ///MSCM Clock Gating Control
        enum class CgcmscmVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,CgcmscmVal> cgcmscm{}; 
        namespace CgcmscmValC{
            constexpr Register::FieldValue<decltype(cgcmscm)::Type,CgcmscmVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgcmscm)::Type,CgcmscmVal::v1> v1{};
        }
        ///MPU Clock Gating Control
        enum class CgcmpuVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,CgcmpuVal> cgcmpu{}; 
        namespace CgcmpuValC{
            constexpr Register::FieldValue<decltype(cgcmpu)::Type,CgcmpuVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgcmpu)::Type,CgcmpuVal::v1> v1{};
        }
        ///DMA Clock Gating Control
        enum class CgcdmaVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,CgcdmaVal> cgcdma{}; 
        namespace CgcdmaValC{
            constexpr Register::FieldValue<decltype(cgcdma)::Type,CgcdmaVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgcdma)::Type,CgcdmaVal::v1> v1{};
        }
        ///ERM Clock Gating Control
        enum class CgcermVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,CgcermVal> cgcerm{}; 
        namespace CgcermValC{
            constexpr Register::FieldValue<decltype(cgcerm)::Type,CgcermVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgcerm)::Type,CgcermVal::v1> v1{};
        }
        ///EIM Clock Gating Control
        enum class CgceimVal : unsigned {
            v0=0x00000000,     ///<Clock disabled
            v1=0x00000001,     ///<Clock enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,4),Register::ReadWriteAccess,CgceimVal> cgceim{}; 
        namespace CgceimValC{
            constexpr Register::FieldValue<decltype(cgceim)::Type,CgceimVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cgceim)::Type,CgceimVal::v1> v1{};
        }
    }
    namespace SimFcfg1{    ///<Flash Configuration Register 1
        using Addr = Register::Address<0x4004804c,0xfff00fff,0x00000000,unsigned>;
        ///FlexNVM partition
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,12),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> depart{}; 
        ///EEE SRAM SIZE
        enum class EeeramsizeVal : unsigned {
            v0010=0x00000002,     ///<4 KB
            v0011=0x00000003,     ///<2 KB
            v0100=0x00000004,     ///<1 KB
            v0101=0x00000005,     ///<512 Bytes
            v0110=0x00000006,     ///<256 Bytes
            v0111=0x00000007,     ///<128 Bytes
            v1000=0x00000008,     ///<64 Bytes
            v1001=0x00000009,     ///<32 Bytes
            v1111=0x0000000f,     ///<0 Bytes
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,EeeramsizeVal> eeeramsize{}; 
        namespace EeeramsizeValC{
            constexpr Register::FieldValue<decltype(eeeramsize)::Type,EeeramsizeVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(eeeramsize)::Type,EeeramsizeVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(eeeramsize)::Type,EeeramsizeVal::v0100> v0100{};
            constexpr Register::FieldValue<decltype(eeeramsize)::Type,EeeramsizeVal::v0101> v0101{};
            constexpr Register::FieldValue<decltype(eeeramsize)::Type,EeeramsizeVal::v0110> v0110{};
            constexpr Register::FieldValue<decltype(eeeramsize)::Type,EeeramsizeVal::v0111> v0111{};
            constexpr Register::FieldValue<decltype(eeeramsize)::Type,EeeramsizeVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(eeeramsize)::Type,EeeramsizeVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(eeeramsize)::Type,EeeramsizeVal::v1111> v1111{};
        }
    }
    namespace SimUidh{    ///<Unique Identification Register High
        using Addr = Register::Address<0x40048054,0x00000000,0x00000000,unsigned>;
        ///Unique Identification
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> uid12796{}; 
    }
    namespace SimUidmh{    ///<Unique Identification Register Mid-High
        using Addr = Register::Address<0x40048058,0x00000000,0x00000000,unsigned>;
        ///Unique Identification
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> uid9564{}; 
    }
    namespace SimUidml{    ///<Unique Identification Register Mid Low
        using Addr = Register::Address<0x4004805c,0x00000000,0x00000000,unsigned>;
        ///Unique Identification
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> uid6332{}; 
    }
    namespace SimUidl{    ///<Unique Identification Register Low
        using Addr = Register::Address<0x40048060,0x00000000,0x00000000,unsigned>;
        ///Unique Identification
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> uid310{}; 
    }
    namespace SimClkdiv4{    ///<System Clock Divider Register 4
        using Addr = Register::Address<0x40048068,0xeffffff0,0x00000000,unsigned>;
        ///Trace Clock Divider fraction To configure TRACEDIV and TRACEFRAC, you must first clear TRACEDIVEN to disable the trace clock divide function.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,unsigned> tracefrac{}; 
        ///Trace Clock Divider value To configure TRACEDIV, you must first disable TRACEDIVEN, then enable it after setting TRACEDIV.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,1),Register::ReadWriteAccess,unsigned> tracediv{}; 
        ///Debug Trace Divider control
        enum class TracedivenVal : unsigned {
            v0=0x00000000,     ///<Debug trace divider disabled
            v1=0x00000001,     ///<Debug trace divider enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::ReadWriteAccess,TracedivenVal> tracediven{}; 
        namespace TracedivenValC{
            constexpr Register::FieldValue<decltype(tracediven)::Type,TracedivenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tracediven)::Type,TracedivenVal::v1> v1{};
        }
    }
    namespace SimMisctrl1{    ///<Miscellaneous Control register 1
        using Addr = Register::Address<0x4004806c,0xfffffffe,0x00000000,unsigned>;
        ///Software trigger to TRGMUX. Writing to this bit generates software trigger to peripherals through TRGMUX (Refer to Figure: Trigger interconnectivity).
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,unsigned> swTrg{}; 
    }
}
