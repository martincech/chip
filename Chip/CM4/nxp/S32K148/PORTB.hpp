#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//Pin Control and Interrupts
    namespace PortbPcr0{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a000,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            lpuart0_rx=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi0_pcs0=0x00000003,     ///<Alternative 3 (chip-specific).
            lptmr0_alt3=0x00000004,     ///<Alternative 4 (chip-specific).
            can0_rx=0x00000005,     ///<Alternative 5 (chip-specific).
            ftm4_ch6=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart0_rx> lpuart0_rx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi0_pcs0> lpspi0_pcs0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lptmr0_alt3> lptmr0_alt3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::can0_rx> can0_rx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm4_ch6> ftm4_ch6{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr1{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a004,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            lpuart0_tx=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi0_sout=0x00000003,     ///<Alternative 3 (chip-specific).
            tclk0=0x00000004,     ///<Alternative 4 (chip-specific).
            can0_tx=0x00000005,     ///<Alternative 5 (chip-specific).
            ftm4_ch5=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart0_tx> lpuart0_tx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi0_sout> lpspi0_sout{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::tclk0> tclk0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::can0_tx> can0_tx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm4_ch5> ftm4_ch5{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr2{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a008,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm1_ch0=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi0_sck=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm1_qd_phb=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            trgmux_in3=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_ch0> ftm1_ch0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi0_sck> lpspi0_sck{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_qd_phb> ftm1_qd_phb{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::trgmux_in3> trgmux_in3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr3{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a00c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm1_ch1=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi0_sin=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm1_qd_pha=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            trgmux_in2=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_ch1> ftm1_ch1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi0_sin> lpspi0_sin{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_qd_pha> ftm1_qd_pha{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::trgmux_in2> trgmux_in2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr4{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a010,0xfef078bc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Drive Strength Enable
        enum class DseVal : unsigned {
            v0=0x00000000,     ///<Low drive strength is configured on the corresponding pin, if pin is configured as a digital output.
            v1=0x00000001,     ///<High drive strength is configured on the corresponding pin, if pin is configured as a digital output.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,DseVal> dse{}; 
        namespace DseValC{
            constexpr Register::FieldValue<decltype(dse)::Type,DseVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dse)::Type,DseVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm0_ch4=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi0_sout=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            mii_rmii_mdio=0x00000005,     ///<Alternative 5 (chip-specific).
            trgmux_in1=0x00000006,     ///<Alternative 6 (chip-specific).
            qspi_b_io0=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm0_ch4> ftm0_ch4{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi0_sout> lpspi0_sout{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::mii_rmii_mdio> mii_rmii_mdio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::trgmux_in1> trgmux_in1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::qspi_b_io0> qspi_b_io0{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr5{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a014,0xfef078bc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Drive Strength Enable
        enum class DseVal : unsigned {
            v0=0x00000000,     ///<Low drive strength is configured on the corresponding pin, if pin is configured as a digital output.
            v1=0x00000001,     ///<High drive strength is configured on the corresponding pin, if pin is configured as a digital output.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,DseVal> dse{}; 
        namespace DseValC{
            constexpr Register::FieldValue<decltype(dse)::Type,DseVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dse)::Type,DseVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm0_ch5=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi0_pcs1=0x00000003,     ///<Alternative 3 (chip-specific).
            lpspi0_pcs0=0x00000004,     ///<Alternative 4 (chip-specific).
            clkout=0x00000005,     ///<Alternative 5 (chip-specific).
            trgmux_in0=0x00000006,     ///<Alternative 6 (chip-specific).
            mii_rmii_mdc=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm0_ch5> ftm0_ch5{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi0_pcs1> lpspi0_pcs1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi0_pcs0> lpspi0_pcs0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::clkout> clkout{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::trgmux_in0> trgmux_in0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::mii_rmii_mdc> mii_rmii_mdc{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr6{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a018,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            lpi2c0_sda=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpi2c0_sda> lpi2c0_sda{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr7{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a01c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            lpi2c0_scl=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpi2c0_scl> lpi2c0_scl{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr8{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a020,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm3_ch0=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            sai1_bclk=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm3_ch0> ftm3_ch0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::sai1_bclk> sai1_bclk{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr9{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a024,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm3_ch1=0x00000002,     ///<Alternative 2 (chip-specific).
            lpi2c0_scls=0x00000003,     ///<Alternative 3 (chip-specific).
            sai1_d0=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm3_ch1> ftm3_ch1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpi2c0_scls> lpi2c0_scls{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::sai1_d0> sai1_d0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr10{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a028,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm3_ch2=0x00000002,     ///<Alternative 2 (chip-specific).
            lpi2c0_sdas=0x00000003,     ///<Alternative 3 (chip-specific).
            sai1_mclk=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm3_ch2> ftm3_ch2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpi2c0_sdas> lpi2c0_sdas{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::sai1_mclk> sai1_mclk{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr11{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a02c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm3_ch3=0x00000002,     ///<Alternative 2 (chip-specific).
            lpi2c0_hreq=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm3_ch3> ftm3_ch3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpi2c0_hreq> lpi2c0_hreq{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr12{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a030,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm0_ch0=0x00000002,     ///<Alternative 2 (chip-specific).
            ftm3_flt2=0x00000003,     ///<Alternative 3 (chip-specific).
            can2_rx=0x00000004,     ///<Alternative 4 (chip-specific).
            ftm6_flt1=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm0_ch0> ftm0_ch0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm3_flt2> ftm3_flt2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::can2_rx> can2_rx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm6_flt1> ftm6_flt1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr13{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a034,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm0_ch1=0x00000002,     ///<Alternative 2 (chip-specific).
            ftm3_flt1=0x00000003,     ///<Alternative 3 (chip-specific).
            can2_tx=0x00000004,     ///<Alternative 4 (chip-specific).
            ftm6_flt0=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm0_ch1> ftm0_ch1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm3_flt1> ftm3_flt1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::can2_tx> can2_tx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm6_flt0> ftm6_flt0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr14{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a038,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm0_ch2=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi1_sck=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm0_ch2> ftm0_ch2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi1_sck> lpspi1_sck{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr15{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a03c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm0_ch3=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi1_sin=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm0_ch3> ftm0_ch3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi1_sin> lpspi1_sin{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr16{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a040,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm0_ch4=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi1_sout=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm0_ch4> ftm0_ch4{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi1_sout> lpspi1_sout{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr17{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a044,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm0_ch5=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi1_pcs3=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm5_flt1=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm0_ch5> ftm0_ch5{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi1_pcs3> lpspi1_pcs3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm5_flt1> ftm5_flt1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr18{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a048,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm5_ch7=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            lpspi1_pcs1=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm5_ch7> ftm5_ch7{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi1_pcs1> lpspi1_pcs1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr19{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a04c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm5_ch7=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm5_ch7> ftm5_ch7{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr20{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a050,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm6_ch0=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm6_ch0> ftm6_ch0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr21{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a054,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm6_ch1=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm6_ch1> ftm6_ch1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr22{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a058,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm6_ch2=0x00000002,     ///<Alternative 2 (chip-specific).
            mii_crs=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            lpuart1_tx=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm6_ch2> ftm6_ch2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::mii_crs> mii_crs{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart1_tx> lpuart1_tx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr23{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a05c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm6_ch3=0x00000002,     ///<Alternative 2 (chip-specific).
            lpuart1_rx=0x00000003,     ///<Alternative 3 (chip-specific).
            mii_col=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm6_ch3> ftm6_ch3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart1_rx> lpuart1_rx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::mii_col> mii_col{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr24{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a060,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm6_ch4=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm6_ch4> ftm6_ch4{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr25{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a064,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm6_ch5=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            lpspi2_pcs0=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm6_ch5> ftm6_ch5{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi2_pcs0> lpspi2_pcs0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr26{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a068,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm6_ch6=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm6_ch6> ftm6_ch6{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr27{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a06c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm6_ch7=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            lpspi2_sout=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm6_ch7> ftm6_ch7{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi2_sout> lpspi2_sout{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr28{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a070,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm7_ch0=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            lpspi2_sin=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm7_ch0> ftm7_ch0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi2_sin> lpspi2_sin{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr29{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a074,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm7_ch1=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            lpspi2_sck=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm7_ch1> ftm7_ch1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi2_sck> lpspi2_sck{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr30{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a078,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm7_ch2=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm7_ch2> ftm7_ch2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbPcr31{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004a07c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm7_ch3=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm7_ch3> ftm7_ch3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortbGpclr{    ///<Global Pin Control Low Register
        using Addr = Register::Address<0x4004a080,0x00000000,0x00000000,unsigned>;
        ///Global Pin Write Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> gpwd{}; 
        ///Global Pin Write Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,16),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> gpwe{}; 
    }
    namespace PortbGpchr{    ///<Global Pin Control High Register
        using Addr = Register::Address<0x4004a084,0x00000000,0x00000000,unsigned>;
        ///Global Pin Write Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> gpwd{}; 
        ///Global Pin Write Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,16),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> gpwe{}; 
    }
    namespace PortbGiclr{    ///<Global Interrupt Control Low Register
        using Addr = Register::Address<0x4004a088,0x00000000,0x00000000,unsigned>;
        ///Global Interrupt Write Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> giwe{}; 
        ///Global Interrupt Write Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,16),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> giwd{}; 
    }
    namespace PortbGichr{    ///<Global Interrupt Control High Register
        using Addr = Register::Address<0x4004a08c,0x00000000,0x00000000,unsigned>;
        ///Global Interrupt Write Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> giwe{}; 
        ///Global Interrupt Write Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,16),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> giwd{}; 
    }
    namespace PortbIsfr{    ///<Interrupt Status Flag Register
        using Addr = Register::Address<0x4004a0a0,0x00000000,0x00000000,unsigned>;
        ///Interrupt Status Flag
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> isf{}; 
    }
    namespace PortbDfer{    ///<Digital Filter Enable Register
        using Addr = Register::Address<0x4004a0c0,0x00000000,0x00000000,unsigned>;
        ///Digital Filter Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> dfe{}; 
    }
    namespace PortbDfcr{    ///<Digital Filter Clock Register
        using Addr = Register::Address<0x4004a0c4,0xfffffffe,0x00000000,unsigned>;
        ///Clock Source
        enum class CsVal : unsigned {
            v0=0x00000000,     ///<Digital filters are clocked by the bus clock.
            v1=0x00000001,     ///<Digital filters are clocked by the LPO clock.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,CsVal> cs{}; 
        namespace CsValC{
            constexpr Register::FieldValue<decltype(cs)::Type,CsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cs)::Type,CsVal::v1> v1{};
        }
    }
    namespace PortbDfwr{    ///<Digital Filter Width Register
        using Addr = Register::Address<0x4004a0c8,0xffffffe0,0x00000000,unsigned>;
        ///Filter Length
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,0),Register::ReadWriteAccess,unsigned> filt{}; 
    }
}
