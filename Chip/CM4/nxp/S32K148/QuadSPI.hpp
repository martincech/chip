#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//QuadSPI
    namespace QuadspiMcr{    ///<Module Configuration Register
        using Addr = Register::Address<0x40076000,0x00f03200,0x00000000,unsigned>;
        ///Software reset for serial flash domain
        enum class SwrstsdVal : unsigned {
            v0=0x00000000,     ///<No action
            v1=0x00000001,     ///<Serial Flash domain flops are reset. Does not reset configuration registers. It is advisable to reset both the serial flash domain and AHB domain at the same time. Resetting only one domain might lead to side effects. The software resets need the clock to be running to propagate to the design. The MCR[MDIS] should therefore be set to 0 when the software reset bits are asserted. Also, before they can be deasserted again (by setting MCR[SWRSTSD] to 0), it is recommended to set the MCR[MDIS] bit to 1. Once the software resets have been deasserted, the normal operation can be started by setting the MCR[MDIS] bit to 0.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,SwrstsdVal> swrstsd{}; 
        namespace SwrstsdValC{
            constexpr Register::FieldValue<decltype(swrstsd)::Type,SwrstsdVal::v0> v0{};
            constexpr Register::FieldValue<decltype(swrstsd)::Type,SwrstsdVal::v1> v1{};
        }
        ///Software reset for AHB domain
        enum class SwrsthdVal : unsigned {
            v0=0x00000000,     ///<No action
            v1=0x00000001,     ///<AHB domain flops are reset. Does not reset configuration registers. It is advisable to reset both the serial flash domain and AHB domain at the same time. Resetting only one domain might lead to side effects. The software resets need the clock to be running to propagate to the design. The MCR[MDIS] should therefore be set to 0 when the software reset bits are asserted. Also, before they can be deasserted again (by setting MCR[SWRSTHD] to 0), it is recommended to set the MCR[MDIS] bit to 1. Once the software resets have been deasserted, the normal operation can be started by setting the MCR[MDIS] bit to 0.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,SwrsthdVal> swrsthd{}; 
        namespace SwrsthdValC{
            constexpr Register::FieldValue<decltype(swrsthd)::Type,SwrsthdVal::v0> v0{};
            constexpr Register::FieldValue<decltype(swrsthd)::Type,SwrsthdVal::v1> v1{};
        }
        ///Defines the endianness of the QuadSPI module. For more details refer to Byte Ordering Endianess
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,2),Register::ReadWriteAccess,unsigned> endCfg{}; 
        ///This field is valid when Data Strobe is also used as an output from controller during Write data phase
        enum class DqsoutenVal : unsigned {
            v0=0x00000000,     ///<DQS as an output from controller is disabled
            v1=0x00000001,     ///<DQS as an output from controller is enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,4),Register::ReadWriteAccess,DqsoutenVal> dqsOutEn{}; 
        namespace DqsoutenValC{
            constexpr Register::FieldValue<decltype(dqsOutEn)::Type,DqsoutenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dqsOutEn)::Type,DqsoutenVal::v1> v1{};
        }
        ///DQS Latency Enable
        enum class DqslatenVal : unsigned {
            v0=0x00000000,     ///<DQS Latency disabled
            v1=0x00000001,     ///<DQS feature with latency included enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,DqslatenVal> dqsLatEn{}; 
        namespace DqslatenValC{
            constexpr Register::FieldValue<decltype(dqsLatEn)::Type,DqslatenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dqsLatEn)::Type,DqslatenVal::v1> v1{};
        }
        ///DQS enable
        enum class DqsenVal : unsigned {
            v0=0x00000000,     ///<DQS disabled.
            v1=0x00000001,     ///<DQS enabled. When enabled, the incoming data is sampled on both the edges of DQS input when QSPI_MCR[DDR_EN] is set, else, on only one edge when QSPI_MCR[DDR_EN] is 0. The QSPI_SMPR[DDR_SMP] values are ignored.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,DqsenVal> dqsEn{}; 
        namespace DqsenValC{
            constexpr Register::FieldValue<decltype(dqsEn)::Type,DqsenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dqsEn)::Type,DqsenVal::v1> v1{};
        }
        ///DDR mode enable
        enum class DdrenVal : unsigned {
            v0=0x00000000,     ///<2x clock are disabled for SDR instructions only
            v1=0x00000001,     ///<2x clock are enabled supports both SDR and DDR instruction.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::ReadWriteAccess,DdrenVal> ddrEn{}; 
        namespace DdrenValC{
            constexpr Register::FieldValue<decltype(ddrEn)::Type,DdrenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ddrEn)::Type,DdrenVal::v1> v1{};
        }
        ///This field is used to enable variable latency feature in the controller
        enum class VarlatenVal : unsigned {
            v0=0x00000000,     ///<Fixed latency: Twice + 1 latency enable
            v1=0x00000001,     ///<Variable latency: 'once' or 'twice + 1' the initial latency based on Data strobe during CA phase. If enabled also need to ensure QSPI_FLSHCR[TCSS] must be >= 2.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::ReadWriteAccess,VarlatenVal> varLatEn{}; 
        namespace VarlatenValC{
            constexpr Register::FieldValue<decltype(varLatEn)::Type,VarlatenVal::v0> v0{};
            constexpr Register::FieldValue<decltype(varLatEn)::Type,VarlatenVal::v1> v1{};
        }
        ///Clear RX FIFO. Invalidates the RX Buffer. This is a self-clearing field.
        enum class ClrrxfVal : unsigned {
            v0=0x00000000,     ///<No action.
            v1=0x00000001,     ///<Read and write pointers of the RX Buffer are reset to 0. QSPI_RBSR[RDBFL] is reset to 0.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,10),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,ClrrxfVal> clrRxf{}; 
        namespace ClrrxfValC{
            constexpr Register::FieldValue<decltype(clrRxf)::Type,ClrrxfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(clrRxf)::Type,ClrrxfVal::v1> v1{};
        }
        ///Clear TX FIFO/Buffer. Invalidates the TX Buffer content. This is a self-clearing field.
        enum class ClrtxfVal : unsigned {
            v0=0x00000000,     ///<No action.
            v1=0x00000001,     ///<Read and write pointers of the TX Buffer are reset to 0. QSPI_TBSR[TRCTR] is reset to 0.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,ClrtxfVal> clrTxf{}; 
        namespace ClrtxfValC{
            constexpr Register::FieldValue<decltype(clrTxf)::Type,ClrtxfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(clrTxf)::Type,ClrtxfVal::v1> v1{};
        }
        ///Module Disable
        enum class MdisVal : unsigned {
            v0=0x00000000,     ///<Enable QuadSPI clocks.
            v1=0x00000001,     ///<Allow external logic to disable QuadSPI clocks.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,14),Register::ReadWriteAccess,MdisVal> mdis{}; 
        namespace MdisValC{
            constexpr Register::FieldValue<decltype(mdis)::Type,MdisVal::v0> v0{};
            constexpr Register::FieldValue<decltype(mdis)::Type,MdisVal::v1> v1{};
        }
        ///Doze Enable
        enum class DozeVal : unsigned {
            v0=0x00000000,     ///<A doze request will be ignored by the QuadSPI module
            v1=0x00000001,     ///<A doze request will be processed by the QuadSPI module
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,DozeVal> doze{}; 
        namespace DozeValC{
            constexpr Register::FieldValue<decltype(doze)::Type,DozeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(doze)::Type,DozeVal::v1> v1{};
        }
        ///Idle Signal Drive IOFA[2] Flash A
        enum class Isd2faVal : unsigned {
            v0=0x00000000,     ///<IOFA[2] is driven to logic L
            v1=0x00000001,     ///<IOFA[2] is driven to logic H
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,Isd2faVal> isd2fa{}; 
        namespace Isd2faValC{
            constexpr Register::FieldValue<decltype(isd2fa)::Type,Isd2faVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isd2fa)::Type,Isd2faVal::v1> v1{};
        }
        ///Idle Signal Drive IOFA[3] Flash A
        enum class Isd3faVal : unsigned {
            v0=0x00000000,     ///<IOFA[3] is driven to logic L
            v1=0x00000001,     ///<IOFA[3] is driven to logic H
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,17),Register::ReadWriteAccess,Isd3faVal> isd3fa{}; 
        namespace Isd3faValC{
            constexpr Register::FieldValue<decltype(isd3fa)::Type,Isd3faVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isd3fa)::Type,Isd3faVal::v1> v1{};
        }
        ///Idle Signal Drive IOFB[2] Flash B
        enum class Isd2fbVal : unsigned {
            v0=0x00000000,     ///<IOFB[2] is driven to logic L
            v1=0x00000001,     ///<IOFB[2] is driven to logic H
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(18,18),Register::ReadWriteAccess,Isd2fbVal> isd2fb{}; 
        namespace Isd2fbValC{
            constexpr Register::FieldValue<decltype(isd2fb)::Type,Isd2fbVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isd2fb)::Type,Isd2fbVal::v1> v1{};
        }
        ///Idle Signal Drive IOFB[3] Flash B
        enum class Isd3fbVal : unsigned {
            v0=0x00000000,     ///<IOFB[3] is driven to logic L
            v1=0x00000001,     ///<IOFB[3] is driven to logic H
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,19),Register::ReadWriteAccess,Isd3fbVal> isd3fb{}; 
        namespace Isd3fbValC{
            constexpr Register::FieldValue<decltype(isd3fb)::Type,Isd3fbVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isd3fb)::Type,Isd3fbVal::v1> v1{};
        }
        ///Serial Clock Configuration
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::ReadWriteAccess,unsigned> sclkcfg{}; 
    }
    namespace QuadspiIpcr{    ///<IP Configuration Register
        using Addr = Register::Address<0x40076008,0xf0ff0000,0x00000000,unsigned>;
        ///IP data transfer size. Defines the data transfer size in bytes of the IP command.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::ReadWriteAccess,unsigned> idatsz{}; 
        ///Points to a sequence in the Look-up table
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,24),Register::ReadWriteAccess,unsigned> seqid{}; 
    }
    namespace QuadspiFlshcr{    ///<Flash Configuration Register
        using Addr = Register::Address<0x4007600c,0xfffcf0f0,0x00000000,unsigned>;
        ///Serial flash CS setup time in terms of serial flash clock cycles
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::ReadWriteAccess,unsigned> tcss{}; 
        ///Serial flash CS hold time in terms of serial flash clock cycles
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,8),Register::ReadWriteAccess,unsigned> tcsh{}; 
        ///Serial flash data in hold time
        enum class TdhVal : unsigned {
            v00=0x00000000,     ///<Data aligned with the posedge of Internal reference clock of QuadSPI
            v01=0x00000001,     ///<Data aligned with 2x serial flash half clock
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,16),Register::ReadWriteAccess,TdhVal> tdh{}; 
        namespace TdhValC{
            constexpr Register::FieldValue<decltype(tdh)::Type,TdhVal::v00> v00{};
            constexpr Register::FieldValue<decltype(tdh)::Type,TdhVal::v01> v01{};
        }
    }
    namespace QuadspiBuf0cr{    ///<Buffer0 Configuration Register
        using Addr = Register::Address<0x40076010,0x7fff00f0,0x00000000,unsigned>;
        ///Master ID
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::ReadWriteAccess,unsigned> mstrid{}; 
        ///AHB data transfer size
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> adatsz{}; 
        ///High Priority Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,unsigned> hpEn{}; 
    }
    namespace QuadspiBuf1cr{    ///<Buffer1 Configuration Register
        using Addr = Register::Address<0x40076014,0xffff00f0,0x00000000,unsigned>;
        ///Master ID
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::ReadWriteAccess,unsigned> mstrid{}; 
        ///AHB data transfer size
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> adatsz{}; 
    }
    namespace QuadspiBuf2cr{    ///<Buffer2 Configuration Register
        using Addr = Register::Address<0x40076018,0xffff00f0,0x00000000,unsigned>;
        ///Master ID
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::ReadWriteAccess,unsigned> mstrid{}; 
        ///AHB data transfer size
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> adatsz{}; 
    }
    namespace QuadspiBuf3cr{    ///<Buffer3 Configuration Register
        using Addr = Register::Address<0x4007601c,0x7fff00f0,0x00000000,unsigned>;
        ///Master ID
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::ReadWriteAccess,unsigned> mstrid{}; 
        ///AHB data transfer size
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> adatsz{}; 
        ///All master enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,unsigned> allmst{}; 
    }
    namespace QuadspiBfgencr{    ///<Buffer Generic Configuration Register
        using Addr = Register::Address<0x40076020,0xffff0fff,0x00000000,unsigned>;
        ///Points to a sequence in the Look-up-table
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,12),Register::ReadWriteAccess,unsigned> seqid{}; 
    }
    namespace QuadspiSoccr{    ///<SOC Configuration Register
        using Addr = Register::Address<0x40076024,0x00000000,0x00000000,unsigned>;
        ///SOC Configuration For details, refer to chip-specific QuadSPI information.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> soccfg{}; 
    }
    namespace QuadspiBuf0ind{    ///<Buffer0 Top Index Register
        using Addr = Register::Address<0x40076030,0x00000007,0x00000000,unsigned>;
        ///Top index of buffer 0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,3),Register::ReadWriteAccess,unsigned> tpindx0{}; 
    }
    namespace QuadspiBuf1ind{    ///<Buffer1 Top Index Register
        using Addr = Register::Address<0x40076034,0x00000007,0x00000000,unsigned>;
        ///Top index of buffer 1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,3),Register::ReadWriteAccess,unsigned> tpindx1{}; 
    }
    namespace QuadspiBuf2ind{    ///<Buffer2 Top Index Register
        using Addr = Register::Address<0x40076038,0x00000007,0x00000000,unsigned>;
        ///Top index of buffer 2.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,3),Register::ReadWriteAccess,unsigned> tpindx2{}; 
    }
    namespace QuadspiSfar{    ///<Serial Flash Address Register
        using Addr = Register::Address<0x40076100,0x00000000,0x00000000,unsigned>;
        ///Serial Flash Address. The register content is used as byte address for all following IP Commands.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> sfadr{}; 
    }
    namespace QuadspiSfacr{    ///<Serial Flash Address Configuration Register
        using Addr = Register::Address<0x40076104,0xfffefff0,0x00000000,unsigned>;
        ///Column Address Space
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::ReadWriteAccess,unsigned> cas{}; 
        ///Word Addressable
        enum class WaVal : unsigned {
            v0=0x00000000,     ///<Byte addressable serial flash mode.
            v1=0x00000001,     ///<Word (2 byte) addressable serial flash mode.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,WaVal> wa{}; 
        namespace WaValC{
            constexpr Register::FieldValue<decltype(wa)::Type,WaVal::v0> v0{};
            constexpr Register::FieldValue<decltype(wa)::Type,WaVal::v1> v1{};
        }
    }
    namespace QuadspiSmpr{    ///<Sampling Register
        using Addr = Register::Address<0x40076108,0xffffff9f,0x00000000,unsigned>;
        ///Full Speed Phase selection for SDR instructions.
        enum class FsphsVal : unsigned {
            v0=0x00000000,     ///<Select sampling at non-inverted clock
            v1=0x00000001,     ///<Select sampling at inverted clock.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,FsphsVal> fsphs{}; 
        namespace FsphsValC{
            constexpr Register::FieldValue<decltype(fsphs)::Type,FsphsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fsphs)::Type,FsphsVal::v1> v1{};
        }
        ///Full Speed Delay selection for SDR instructions. Select the delay with respect to the reference edge for the sample point valid for full speed commands.
        enum class FsdlyVal : unsigned {
            v0=0x00000000,     ///<One clock cycle delay
            v1=0x00000001,     ///<Two clock cycles delay.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,FsdlyVal> fsdly{}; 
        namespace FsdlyValC{
            constexpr Register::FieldValue<decltype(fsdly)::Type,FsdlyVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fsdly)::Type,FsdlyVal::v1> v1{};
        }
    }
    namespace QuadspiRbsr{    ///<RX Buffer Status Register
        using Addr = Register::Address<0x4007610c,0x0000c0ff,0x00000000,unsigned>;
        ///RX Buffer Fill Level
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rdbfl{}; 
        ///Read Counter
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rdctr{}; 
    }
    namespace QuadspiRbct{    ///<RX Buffer Control Register
        using Addr = Register::Address<0x40076110,0xfffffee0,0x00000000,unsigned>;
        ///RX Buffer Watermark
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,0),Register::ReadWriteAccess,unsigned> wmrk{}; 
        ///RX Buffer Readout. This field specifies the access scheme for the RX Buffer readout.
        enum class RxbrdVal : unsigned {
            v0=0x00000000,     ///<RX Buffer content is read using the AHB Bus registers QSPI_ARDB0 to QSPI_ARDB31. For details, refer to Exclusive Access to Serial Flash for AHB Commands.
            v1=0x00000001,     ///<RX Buffer content is read using the IP Bus registers QSPI_RBDR0 to QSPI_RBDR31.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::ReadWriteAccess,RxbrdVal> rxbrd{}; 
        namespace RxbrdValC{
            constexpr Register::FieldValue<decltype(rxbrd)::Type,RxbrdVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rxbrd)::Type,RxbrdVal::v1> v1{};
        }
    }
    namespace QuadspiTbsr{    ///<TX Buffer Status Register
        using Addr = Register::Address<0x40076150,0x0000c0ff,0x00000000,unsigned>;
        ///TX Buffer Fill Level
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> trbfl{}; 
        ///Transmit Counter
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> trctr{}; 
    }
    namespace QuadspiTbdr{    ///<TX Buffer Data Register
        using Addr = Register::Address<0x40076154,0x00000000,0x00000000,unsigned>;
        ///TX Data On write access the data is written into the next available entry of the TX Buffer and the QPSI_TBSR[TRBFL] field is updated accordingly
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> txdata{}; 
    }
    namespace QuadspiTbct{    ///<Tx Buffer Control Register
        using Addr = Register::Address<0x40076158,0xffffffe0,0x00000000,unsigned>;
        ///Determines the watermark for the TX Buffer
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,0),Register::ReadWriteAccess,unsigned> wmrk{}; 
    }
    namespace QuadspiSr{    ///<Status Register
        using Addr = Register::Address<0x4007615c,0xf0768018,0x00000000,unsigned>;
        ///Module Busy
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> busy{}; 
        ///IP Access. Asserted when transaction currently executed was initiated by IP bus.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> ipAcc{}; 
        ///AHB Access. Asserted when the transaction currently executed was initiated by AHB bus.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> ahbAcc{}; 
        ///AHB Command priority Granted: Asserted when another module has been granted priority of AHB Commands against IP Commands
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> ahbgnt{}; 
        ///AHB Access Transaction pending
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> ahbtrn{}; 
        ///AHB 0 Buffer Not Empty. Asserted when AHB 0 buffer contains data.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> ahb0ne{}; 
        ///AHB 1 Buffer Not Empty. Asserted when AHB 1 buffer contains data.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> ahb1ne{}; 
        ///AHB 2 Buffer Not Empty. Asserted when AHB 2 buffer contains data.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,9),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> ahb2ne{}; 
        ///AHB 3 Buffer Not Empty. Asserted when AHB 3 buffer contains data.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,10),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> ahb3ne{}; 
        ///AHB 0 Buffer Full. Asserted when AHB 0 buffer is full.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,11),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> ahb0ful{}; 
        ///AHB 1 Buffer Full. Asserted when AHB 1 buffer is full.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,12),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> ahb1ful{}; 
        ///AHB 2 Buffer Full. Asserted when AHB 2 buffer is full.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,13),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> ahb2ful{}; 
        ///AHB 3 Buffer Full. Asserted when AHB 3 buffer is full.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,14),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> ahb3ful{}; 
        ///RX Buffer Watermark Exceeded
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxwe{}; 
        ///RX Buffer Full
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,19),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxfull{}; 
        ///RX Buffer DMA. Asserted when RX Buffer read out via DMA is active i.e DMA is requested or running.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,23),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdma{}; 
        ///Tx Buffer Enough Data Available
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> txeda{}; 
        ///TX Buffer watermark Available
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> txwa{}; 
        ///TXDMA
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> txdma{}; 
        ///TX Buffer Full. Asserted when no more data can be stored.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> txfull{}; 
    }
    namespace QuadspiFr{    ///<Flag Register
        using Addr = Register::Address<0x40076160,0xf37c0f2e,0x00000000,unsigned>;
        ///IP Command Transaction Finished Flag
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,unsigned> tff{}; 
        ///IP Command Trigger during AHB Grant Error Flag
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,4),Register::ReadWriteAccess,unsigned> ipgef{}; 
        ///IP Command Trigger could not be executed Error Flag
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,unsigned> ipief{}; 
        ///IP Command Trigger during AHB Access Error Flag
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::ReadWriteAccess,unsigned> ipaef{}; 
        ///AHB Buffer Overflow Flag
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,12),Register::ReadWriteAccess,unsigned> abof{}; 
        ///AHB Illegal Burst Size Error Flag
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,13),Register::ReadWriteAccess,unsigned> aibsef{}; 
        ///AHB Illegal transaction error flag
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,14),Register::ReadWriteAccess,unsigned> aitef{}; 
        ///AHB Sequence Error Flag
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,unsigned> absef{}; 
        ///RX Buffer Drain Flag
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,unsigned> rbdf{}; 
        ///RX Buffer Overflow Flag
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,17),Register::ReadWriteAccess,unsigned> rbof{}; 
        ///Illegal Instruction Error Flag
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,23),Register::ReadWriteAccess,unsigned> illine{}; 
        ///TX Buffer Underrun Flag
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,unsigned> tbuf{}; 
        ///TX Buffer Fill Flag
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,unsigned> tbff{}; 
    }
    namespace QuadspiRser{    ///<Interrupt and DMA Request Select and Enable Register
        using Addr = Register::Address<0x40076164,0xf15c0f2e,0x00000000,unsigned>;
        ///Transaction Finished Interrupt Enable
        enum class TfieVal : unsigned {
            v0=0x00000000,     ///<No TFF interrupt will be generated
            v1=0x00000001,     ///<TFF interrupt will be generated
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,TfieVal> tfie{}; 
        namespace TfieValC{
            constexpr Register::FieldValue<decltype(tfie)::Type,TfieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tfie)::Type,TfieVal::v1> v1{};
        }
        ///IP Command Trigger during AHB Grant Error Interrupt Enable
        enum class IpgeieVal : unsigned {
            v0=0x00000000,     ///<No IPGEF interrupt will be generated
            v1=0x00000001,     ///<IPGEF interrupt will be generated
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,4),Register::ReadWriteAccess,IpgeieVal> ipgeie{}; 
        namespace IpgeieValC{
            constexpr Register::FieldValue<decltype(ipgeie)::Type,IpgeieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ipgeie)::Type,IpgeieVal::v1> v1{};
        }
        ///IP Command Trigger during IP Access Error Interrupt Enable
        enum class IpieieVal : unsigned {
            v0=0x00000000,     ///<No IPIEF interrupt will be generated
            v1=0x00000001,     ///<IPIEF interrupt will be generated
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,IpieieVal> ipieie{}; 
        namespace IpieieValC{
            constexpr Register::FieldValue<decltype(ipieie)::Type,IpieieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ipieie)::Type,IpieieVal::v1> v1{};
        }
        ///IP Command Trigger during AHB Access Error Interrupt Enable
        enum class IpaeieVal : unsigned {
            v0=0x00000000,     ///<No IPAEF interrupt will be generated
            v1=0x00000001,     ///<IPAEF interrupt will be generated
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::ReadWriteAccess,IpaeieVal> ipaeie{}; 
        namespace IpaeieValC{
            constexpr Register::FieldValue<decltype(ipaeie)::Type,IpaeieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ipaeie)::Type,IpaeieVal::v1> v1{};
        }
        ///AHB Buffer Overflow Interrupt Enable
        enum class AboieVal : unsigned {
            v0=0x00000000,     ///<No ABOF interrupt will be generated
            v1=0x00000001,     ///<ABOF interrupt will be generated
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,12),Register::ReadWriteAccess,AboieVal> aboie{}; 
        namespace AboieValC{
            constexpr Register::FieldValue<decltype(aboie)::Type,AboieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(aboie)::Type,AboieVal::v1> v1{};
        }
        ///AHB Illegal Burst Size Interrupt Enable
        enum class AibsieVal : unsigned {
            v0=0x00000000,     ///<No AIBSEF interrupt will be generated
            v1=0x00000001,     ///<AIBSEF interrupt will be generated
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(13,13),Register::ReadWriteAccess,AibsieVal> aibsie{}; 
        namespace AibsieValC{
            constexpr Register::FieldValue<decltype(aibsie)::Type,AibsieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(aibsie)::Type,AibsieVal::v1> v1{};
        }
        ///AHB Illegal transaction interrupt enable.
        enum class AitieVal : unsigned {
            v0=0x00000000,     ///<No AITEF interrupt will be generated
            v1=0x00000001,     ///<AITEF interrupt will be generated
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,14),Register::ReadWriteAccess,AitieVal> aitie{}; 
        namespace AitieValC{
            constexpr Register::FieldValue<decltype(aitie)::Type,AitieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(aitie)::Type,AitieVal::v1> v1{};
        }
        ///AHB Sequence Error Interrupt Enable: Triggered by ABSEF flags of QSPI_FR
        enum class AbseieVal : unsigned {
            v0=0x00000000,     ///<No ABSEF interrupt will be generated
            v1=0x00000001,     ///<ABSEF interrupt will be generated
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,AbseieVal> abseie{}; 
        namespace AbseieValC{
            constexpr Register::FieldValue<decltype(abseie)::Type,AbseieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(abseie)::Type,AbseieVal::v1> v1{};
        }
        ///RX Buffer Drain Interrupt Enable: Enables generation of IRQ requests for RX Buffer Drain
        enum class RbdieVal : unsigned {
            v0=0x00000000,     ///<No RBDF interrupt will be generated
            v1=0x00000001,     ///<RBDF Interrupt will be generated
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,RbdieVal> rbdie{}; 
        namespace RbdieValC{
            constexpr Register::FieldValue<decltype(rbdie)::Type,RbdieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rbdie)::Type,RbdieVal::v1> v1{};
        }
        ///RX Buffer Overflow Interrupt Enable
        enum class RboieVal : unsigned {
            v0=0x00000000,     ///<No RBOF interrupt will be generated
            v1=0x00000001,     ///<RBOF interrupt will be generated
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(17,17),Register::ReadWriteAccess,RboieVal> rboie{}; 
        namespace RboieValC{
            constexpr Register::FieldValue<decltype(rboie)::Type,RboieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rboie)::Type,RboieVal::v1> v1{};
        }
        ///RX Buffer Drain DMA Enable: Enables generation of DMA requests for RX Buffer Drain
        enum class RbddeVal : unsigned {
            v0=0x00000000,     ///<No DMA request will be generated
            v1=0x00000001,     ///<DMA request will be generated
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(21,21),Register::ReadWriteAccess,RbddeVal> rbdde{}; 
        namespace RbddeValC{
            constexpr Register::FieldValue<decltype(rbdde)::Type,RbddeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(rbdde)::Type,RbddeVal::v1> v1{};
        }
        ///Illegal Instruction Error Interrupt Enable. Triggered by ILLINE flag in QSPI_FR
        enum class IllinieVal : unsigned {
            v0=0x00000000,     ///<No ILLINE interrupt will be generated
            v1=0x00000001,     ///<ILLINE interrupt will be generated
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,23),Register::ReadWriteAccess,IllinieVal> illinie{}; 
        namespace IllinieValC{
            constexpr Register::FieldValue<decltype(illinie)::Type,IllinieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(illinie)::Type,IllinieVal::v1> v1{};
        }
        ///TX Buffer Fill DMA Enable
        enum class TbfdeVal : unsigned {
            v0=0x00000000,     ///<No DMA request will be generated
            v1=0x00000001,     ///<DMA request will be generated
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,25),Register::ReadWriteAccess,TbfdeVal> tbfde{}; 
        namespace TbfdeValC{
            constexpr Register::FieldValue<decltype(tbfde)::Type,TbfdeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tbfde)::Type,TbfdeVal::v1> v1{};
        }
        ///TX Buffer Underrun Interrupt Enable
        enum class TbuieVal : unsigned {
            v0=0x00000000,     ///<No TBUF interrupt will be generated
            v1=0x00000001,     ///<TBUF interrupt will be generated
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(26,26),Register::ReadWriteAccess,TbuieVal> tbuie{}; 
        namespace TbuieValC{
            constexpr Register::FieldValue<decltype(tbuie)::Type,TbuieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tbuie)::Type,TbuieVal::v1> v1{};
        }
        ///TX Buffer Fill Interrupt Enable
        enum class TbfieVal : unsigned {
            v0=0x00000000,     ///<No TBFF interrupt will be generated
            v1=0x00000001,     ///<TBFF interrupt will be generated
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,27),Register::ReadWriteAccess,TbfieVal> tbfie{}; 
        namespace TbfieValC{
            constexpr Register::FieldValue<decltype(tbfie)::Type,TbfieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tbfie)::Type,TbfieVal::v1> v1{};
        }
    }
    namespace QuadspiSpndst{    ///<Sequence Suspend Status Register
        using Addr = Register::Address<0x40076168,0xffff013e,0x00000000,unsigned>;
        ///When set, it signifies that a sequence is in suspended state
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> suspnd{}; 
        ///Suspended Buffer: Provides the suspended buffer number. Valid only when SUSPND is set to 1'b1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,6),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> spdbuf{}; 
        ///Data left: Provides information about the amount of data left to be read in the suspended sequence
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,9),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> datlft{}; 
    }
    namespace QuadspiSptrclr{    ///<Sequence Pointer Clear Register
        using Addr = Register::Address<0x4007616c,0xfffffefe,0x00000000,unsigned>;
        ///Buffer Pointer Clear: 1: Clears the sequence pointer for AHB accesses as defined in QuadSPI_BFGENCR
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> bfptrc{}; 
        ///IP Pointer Clear: 1: Clears the sequence pointer for IP accesses as defined in QuadSPI_IPCR This is a self-clearing field
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(8,8),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> ipptrc{}; 
    }
    namespace QuadspiSfa1ad{    ///<Serial Flash A1 Top Address
        using Addr = Register::Address<0x40076180,0x000003ff,0x00000000,unsigned>;
        ///Top address for Serial Flash A1. In effect, TPADxx is the first location of the next memory.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,10),Register::ReadWriteAccess,unsigned> tpada1{}; 
    }
    namespace QuadspiSfa2ad{    ///<Serial Flash A2 Top Address
        using Addr = Register::Address<0x40076184,0x000003ff,0x00000000,unsigned>;
        ///Top address for Serial Flash A2. In effect, TPxxAD is the first location of the next memory.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,10),Register::ReadWriteAccess,unsigned> tpada2{}; 
    }
    namespace QuadspiSfb1ad{    ///<Serial Flash B1 Top Address
        using Addr = Register::Address<0x40076188,0x000003ff,0x00000000,unsigned>;
        ///Top address for Serial Flash B1.In effect, TPxxAD is the first location of the next memory.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,10),Register::ReadWriteAccess,unsigned> tpadb1{}; 
    }
    namespace QuadspiSfb2ad{    ///<Serial Flash B2 Top Address
        using Addr = Register::Address<0x4007618c,0x000003ff,0x00000000,unsigned>;
        ///Top address for Serial Flash B2. In effect, TPxxAD is the first location of the next memory.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,10),Register::ReadWriteAccess,unsigned> tpadb2{}; 
    }
    namespace QuadspiLutkey{    ///<LUT Key Register
        using Addr = Register::Address<0x40076300,0x00000000,0x00000000,unsigned>;
        ///The key to lock or unlock the LUT. The KEY is 0x5AF05AF0. The read value is always 0x5AF05AF0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> key{}; 
    }
    namespace QuadspiLckcr{    ///<LUT Lock Configuration Register
        using Addr = Register::Address<0x40076304,0xfffffffc,0x00000000,unsigned>;
        ///Locks the LUT when the following condition is met: This register is written just after the LUTKEYLUT Key Register The LUT key register was written with 0x5AF05AF0 key
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,unsigned> lock{}; 
        ///Unlocks the LUT when the following two conditions are met: 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,unsigned> unlock{}; 
    }
    namespace QuadspiRbdr0{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x40076200,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr1{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x40076204,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr2{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x40076208,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr3{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x4007620c,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr4{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x40076210,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr5{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x40076214,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr6{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x40076218,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr7{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x4007621c,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr8{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x40076220,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr9{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x40076224,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr10{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x40076228,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr11{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x4007622c,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr12{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x40076230,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr13{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x40076234,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr14{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x40076238,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr15{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x4007623c,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr16{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x40076240,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr17{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x40076244,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr18{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x40076248,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr19{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x4007624c,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr20{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x40076250,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr21{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x40076254,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr22{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x40076258,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr23{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x4007625c,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr24{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x40076260,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr25{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x40076264,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr26{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x40076268,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr27{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x4007626c,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr28{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x40076270,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr29{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x40076274,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr30{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x40076278,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiRbdr31{    ///<RX Buffer Data Register
        using Addr = Register::Address<0x4007627c,0x00000000,0x00000000,unsigned>;
        ///RX Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rxdata{}; 
    }
    namespace QuadspiLut0{    ///<Look-up Table register
        using Addr = Register::Address<0x40076310,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut1{    ///<Look-up Table register
        using Addr = Register::Address<0x40076314,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut2{    ///<Look-up Table register
        using Addr = Register::Address<0x40076318,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut3{    ///<Look-up Table register
        using Addr = Register::Address<0x4007631c,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut4{    ///<Look-up Table register
        using Addr = Register::Address<0x40076320,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut5{    ///<Look-up Table register
        using Addr = Register::Address<0x40076324,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut6{    ///<Look-up Table register
        using Addr = Register::Address<0x40076328,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut7{    ///<Look-up Table register
        using Addr = Register::Address<0x4007632c,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut8{    ///<Look-up Table register
        using Addr = Register::Address<0x40076330,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut9{    ///<Look-up Table register
        using Addr = Register::Address<0x40076334,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut10{    ///<Look-up Table register
        using Addr = Register::Address<0x40076338,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut11{    ///<Look-up Table register
        using Addr = Register::Address<0x4007633c,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut12{    ///<Look-up Table register
        using Addr = Register::Address<0x40076340,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut13{    ///<Look-up Table register
        using Addr = Register::Address<0x40076344,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut14{    ///<Look-up Table register
        using Addr = Register::Address<0x40076348,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut15{    ///<Look-up Table register
        using Addr = Register::Address<0x4007634c,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut16{    ///<Look-up Table register
        using Addr = Register::Address<0x40076350,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut17{    ///<Look-up Table register
        using Addr = Register::Address<0x40076354,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut18{    ///<Look-up Table register
        using Addr = Register::Address<0x40076358,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut19{    ///<Look-up Table register
        using Addr = Register::Address<0x4007635c,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut20{    ///<Look-up Table register
        using Addr = Register::Address<0x40076360,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut21{    ///<Look-up Table register
        using Addr = Register::Address<0x40076364,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut22{    ///<Look-up Table register
        using Addr = Register::Address<0x40076368,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut23{    ///<Look-up Table register
        using Addr = Register::Address<0x4007636c,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut24{    ///<Look-up Table register
        using Addr = Register::Address<0x40076370,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut25{    ///<Look-up Table register
        using Addr = Register::Address<0x40076374,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut26{    ///<Look-up Table register
        using Addr = Register::Address<0x40076378,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut27{    ///<Look-up Table register
        using Addr = Register::Address<0x4007637c,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut28{    ///<Look-up Table register
        using Addr = Register::Address<0x40076380,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut29{    ///<Look-up Table register
        using Addr = Register::Address<0x40076384,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut30{    ///<Look-up Table register
        using Addr = Register::Address<0x40076388,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut31{    ///<Look-up Table register
        using Addr = Register::Address<0x4007638c,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut32{    ///<Look-up Table register
        using Addr = Register::Address<0x40076390,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut33{    ///<Look-up Table register
        using Addr = Register::Address<0x40076394,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut34{    ///<Look-up Table register
        using Addr = Register::Address<0x40076398,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut35{    ///<Look-up Table register
        using Addr = Register::Address<0x4007639c,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut36{    ///<Look-up Table register
        using Addr = Register::Address<0x400763a0,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut37{    ///<Look-up Table register
        using Addr = Register::Address<0x400763a4,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut38{    ///<Look-up Table register
        using Addr = Register::Address<0x400763a8,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut39{    ///<Look-up Table register
        using Addr = Register::Address<0x400763ac,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut40{    ///<Look-up Table register
        using Addr = Register::Address<0x400763b0,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut41{    ///<Look-up Table register
        using Addr = Register::Address<0x400763b4,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut42{    ///<Look-up Table register
        using Addr = Register::Address<0x400763b8,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut43{    ///<Look-up Table register
        using Addr = Register::Address<0x400763bc,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut44{    ///<Look-up Table register
        using Addr = Register::Address<0x400763c0,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut45{    ///<Look-up Table register
        using Addr = Register::Address<0x400763c4,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut46{    ///<Look-up Table register
        using Addr = Register::Address<0x400763c8,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut47{    ///<Look-up Table register
        using Addr = Register::Address<0x400763cc,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut48{    ///<Look-up Table register
        using Addr = Register::Address<0x400763d0,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut49{    ///<Look-up Table register
        using Addr = Register::Address<0x400763d4,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut50{    ///<Look-up Table register
        using Addr = Register::Address<0x400763d8,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut51{    ///<Look-up Table register
        using Addr = Register::Address<0x400763dc,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut52{    ///<Look-up Table register
        using Addr = Register::Address<0x400763e0,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut53{    ///<Look-up Table register
        using Addr = Register::Address<0x400763e4,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut54{    ///<Look-up Table register
        using Addr = Register::Address<0x400763e8,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut55{    ///<Look-up Table register
        using Addr = Register::Address<0x400763ec,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut56{    ///<Look-up Table register
        using Addr = Register::Address<0x400763f0,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut57{    ///<Look-up Table register
        using Addr = Register::Address<0x400763f4,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut58{    ///<Look-up Table register
        using Addr = Register::Address<0x400763f8,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut59{    ///<Look-up Table register
        using Addr = Register::Address<0x400763fc,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut60{    ///<Look-up Table register
        using Addr = Register::Address<0x40076400,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut61{    ///<Look-up Table register
        using Addr = Register::Address<0x40076404,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut62{    ///<Look-up Table register
        using Addr = Register::Address<0x40076408,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
    namespace QuadspiLut63{    ///<Look-up Table register
        using Addr = Register::Address<0x4007640c,0x00000000,0x00000000,unsigned>;
        ///Operand for INSTR0.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,unsigned> oprnd0{}; 
        ///Pad information for INSTR0.
        enum class Pad0Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::ReadWriteAccess,Pad0Val> pad0{}; 
        namespace Pad0ValC{
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad0)::Type,Pad0Val::v11> v11{};
        }
        ///Instruction 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,10),Register::ReadWriteAccess,unsigned> instr0{}; 
        ///Operand for INSTR1.
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::ReadWriteAccess,unsigned> oprnd1{}; 
        ///Pad information for INSTR1.
        enum class Pad1Val : unsigned {
            v00=0x00000000,     ///<1 Pad
            v01=0x00000001,     ///<2 Pads
            v10=0x00000002,     ///<4 Pads
            v11=0x00000003,     ///<8 Pads
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(25,24),Register::ReadWriteAccess,Pad1Val> pad1{}; 
        namespace Pad1ValC{
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v00> v00{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v01> v01{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v10> v10{};
            constexpr Register::FieldValue<decltype(pad1)::Type,Pad1Val::v11> v11{};
        }
        ///Instruction 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,26),Register::ReadWriteAccess,unsigned> instr1{}; 
    }
}
