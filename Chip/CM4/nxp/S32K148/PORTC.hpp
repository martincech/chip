#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//Pin Control and Interrupts
    namespace PortcPcr0{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b000,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm0_ch0=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi2_sin=0x00000003,     ///<Alternative 3 (chip-specific).
            mii_rmii_rxd[1]=0x00000004,     ///<Alternative 4 (chip-specific).
            mii_rmii_rxd[0]=0x00000005,     ///<Alternative 5 (chip-specific).
            ftm1_ch6=0x00000006,     ///<Alternative 6 (chip-specific).
            qspi_b_rwds=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm0_ch0> ftm0_ch0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi2_sin> lpspi2_sin{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::mii_rmii_rxd[1]> mii_rmii_rxd[1]{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::mii_rmii_rxd[0]> mii_rmii_rxd[0]{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_ch6> ftm1_ch6{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::qspi_b_rwds> qspi_b_rwds{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr1{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b004,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm0_ch1=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi2_sout=0x00000003,     ///<Alternative 3 (chip-specific).
            mii_rmii_rxd[1]=0x00000004,     ///<Alternative 4 (chip-specific).
            mii_rmii_rxd[0]=0x00000005,     ///<Alternative 5 (chip-specific).
            ftm1_ch7=0x00000006,     ///<Alternative 6 (chip-specific).
            qspi_b_sck=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm0_ch1> ftm0_ch1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi2_sout> lpspi2_sout{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::mii_rmii_rxd[1]> mii_rmii_rxd[1]{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::mii_rmii_rxd[0]> mii_rmii_rxd[0]{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_ch7> ftm1_ch7{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::qspi_b_sck> qspi_b_sck{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr2{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b008,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm0_ch2=0x00000002,     ///<Alternative 2 (chip-specific).
            can0_rx=0x00000003,     ///<Alternative 3 (chip-specific).
            lpuart0_rx=0x00000004,     ///<Alternative 4 (chip-specific).
            mii_rmii_txd[0]=0x00000005,     ///<Alternative 5 (chip-specific).
            etm_trace_clkout=0x00000006,     ///<Alternative 6 (chip-specific).
            qspi_a_io3=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm0_ch2> ftm0_ch2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::can0_rx> can0_rx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart0_rx> lpuart0_rx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::mii_rmii_txd[0]> mii_rmii_txd[0]{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::etm_trace_clkout> etm_trace_clkout{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::qspi_a_io3> qspi_a_io3{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr3{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b00c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm0_ch3=0x00000002,     ///<Alternative 2 (chip-specific).
            can0_tx=0x00000003,     ///<Alternative 3 (chip-specific).
            lpuart0_tx=0x00000004,     ///<Alternative 4 (chip-specific).
            mii_tx_er=0x00000005,     ///<Alternative 5 (chip-specific).
            qspi_a_cs=0x00000006,     ///<Alternative 6 (chip-specific).
            qspi_b_io3=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm0_ch3> ftm0_ch3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::can0_tx> can0_tx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart0_tx> lpuart0_tx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::mii_tx_er> mii_tx_er{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::qspi_a_cs> qspi_a_cs{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::qspi_b_io3> qspi_b_io3{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr4{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b010,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm1_ch0=0x00000002,     ///<Alternative 2 (chip-specific).
            rtc_clkout=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            ewm_in=0x00000005,     ///<Alternative 5 (chip-specific).
            ftm1_qd_phb=0x00000006,     ///<Alternative 6 (chip-specific).
            jtag_tclk_swd_clk=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_ch0> ftm1_ch0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::rtc_clkout> rtc_clkout{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ewm_in> ewm_in{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_qd_phb> ftm1_qd_phb{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::jtag_tclk_swd_clk> jtag_tclk_swd_clk{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr5{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b014,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm2_ch0=0x00000002,     ///<Alternative 2 (chip-specific).
            rtc_clkout=0x00000003,     ///<Alternative 3 (chip-specific).
            lpi2c1_hreq=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            ftm2_qd_phb=0x00000006,     ///<Alternative 6 (chip-specific).
            jtag_tdi=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_ch0> ftm2_ch0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::rtc_clkout> rtc_clkout{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpi2c1_hreq> lpi2c1_hreq{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_qd_phb> ftm2_qd_phb{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::jtag_tdi> jtag_tdi{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr6{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b018,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            lpuart1_rx=0x00000002,     ///<Alternative 2 (chip-specific).
            can1_rx=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm3_ch2=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            ftm1_qd_phb=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart1_rx> lpuart1_rx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::can1_rx> can1_rx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm3_ch2> ftm3_ch2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_qd_phb> ftm1_qd_phb{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr7{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b01c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            lpuart1_tx=0x00000002,     ///<Alternative 2 (chip-specific).
            can1_tx=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm3_ch3=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            ftm1_qd_pha=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart1_tx> lpuart1_tx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::can1_tx> can1_tx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm3_ch3> ftm3_ch3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_qd_pha> ftm1_qd_pha{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr8{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b020,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            lpuart1_rx=0x00000002,     ///<Alternative 2 (chip-specific).
            ftm1_flt0=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm5_ch1=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            lpuart0_cts=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart1_rx> lpuart1_rx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_flt0> ftm1_flt0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm5_ch1> ftm5_ch1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart0_cts> lpuart0_cts{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr9{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b024,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            lpuart1_tx=0x00000002,     ///<Alternative 2 (chip-specific).
            ftm1_flt1=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm5_ch0=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            lpuart0_rts=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart1_tx> lpuart1_tx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_flt1> ftm1_flt1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm5_ch0> ftm5_ch0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart0_rts> lpuart0_rts{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr10{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b028,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm3_ch4=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            trgmux_in11=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm3_ch4> ftm3_ch4{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::trgmux_in11> trgmux_in11{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr11{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b02c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm3_ch5=0x00000002,     ///<Alternative 2 (chip-specific).
            ftm4_ch2=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            trgmux_in10=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm3_ch5> ftm3_ch5{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm4_ch2> ftm4_ch2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::trgmux_in10> trgmux_in10{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr12{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b030,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm3_ch6=0x00000002,     ///<Alternative 2 (chip-specific).
            ftm2_ch6=0x00000003,     ///<Alternative 3 (chip-specific).
            lpuart2_cts=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm3_ch6> ftm3_ch6{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_ch6> ftm2_ch6{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart2_cts> lpuart2_cts{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr13{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b034,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm3_ch7=0x00000002,     ///<Alternative 2 (chip-specific).
            ftm2_ch7=0x00000003,     ///<Alternative 3 (chip-specific).
            lpuart2_rts=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm3_ch7> ftm3_ch7{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_ch7> ftm2_ch7{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart2_rts> lpuart2_rts{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr14{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b038,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm1_ch2=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi2_pcs0=0x00000003,     ///<Alternative 3 (chip-specific).
            mii_col=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            trgmux_in9=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_ch2> ftm1_ch2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi2_pcs0> lpspi2_pcs0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::mii_col> mii_col{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::trgmux_in9> trgmux_in9{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr15{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b03c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm1_ch3=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi2_sck=0x00000003,     ///<Alternative 3 (chip-specific).
            mii_crs=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            trgmux_in8=0x00000006,     ///<Alternative 6 (chip-specific).
            qspi_b_cs=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_ch3> ftm1_ch3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi2_sck> lpspi2_sck{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::mii_crs> mii_crs{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::trgmux_in8> trgmux_in8{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::qspi_b_cs> qspi_b_cs{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr16{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b040,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm1_flt2=0x00000002,     ///<Alternative 2 (chip-specific).
            can2_rx=0x00000003,     ///<Alternative 3 (chip-specific).
            lpi2c1_sdas=0x00000004,     ///<Alternative 4 (chip-specific).
            mii_rmii_rx_er=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            qspi_b_io7=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_flt2> ftm1_flt2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::can2_rx> can2_rx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpi2c1_sdas> lpi2c1_sdas{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::mii_rmii_rx_er> mii_rmii_rx_er{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::qspi_b_io7> qspi_b_io7{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr17{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b044,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm1_flt3=0x00000002,     ///<Alternative 2 (chip-specific).
            can2_tx=0x00000003,     ///<Alternative 3 (chip-specific).
            lpi2c1_scls=0x00000004,     ///<Alternative 4 (chip-specific).
            mii_rmii_rx_dv=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            qspi_b_io6=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_flt3> ftm1_flt3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::can2_tx> can2_tx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpi2c1_scls> lpi2c1_scls{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::mii_rmii_rx_dv> mii_rmii_rx_dv{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::qspi_b_io6> qspi_b_io6{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr18{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b048,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm7_ch4=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm7_ch4> ftm7_ch4{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr19{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b04c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm7_ch5=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            lpspi2_pcs1=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm7_ch5> ftm7_ch5{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi2_pcs1> lpspi2_pcs1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr20{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b050,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm7_ch6=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm7_ch6> ftm7_ch6{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr21{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b054,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm7_ch7=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm7_flt0=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm7_ch7> ftm7_ch7{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm7_flt0> ftm7_flt0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr22{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b058,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm7_flt1=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm7_flt1> ftm7_flt1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr23{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b05c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            lpspi0_sck=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi0_sck> lpspi0_sck{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr24{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b060,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm4_ch0=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm4_ch0> ftm4_ch0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr25{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b064,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm4_ch1=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm4_ch1> ftm4_ch1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr26{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b068,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm4_ch3=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm4_ch3> ftm4_ch3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr27{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b06c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm4_ch4=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm4_ch4> ftm4_ch4{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr28{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b070,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm4_ch7=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm4_ch7> ftm4_ch7{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr29{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b074,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm5_ch2=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm5_ch2> ftm5_ch2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr30{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b078,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm5_ch4=0x00000002,     ///<Alternative 2 (chip-specific).
            fxio_d0=0x00000003,     ///<Alternative 3 (chip-specific).
            lpi2c1_sdas=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm5_ch4> ftm5_ch4{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d0> fxio_d0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpi2c1_sdas> lpi2c1_sdas{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcPcr31{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004b07c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm5_ch6=0x00000002,     ///<Alternative 2 (chip-specific).
            fxio_d1=0x00000003,     ///<Alternative 3 (chip-specific).
            lpi2c1_sda=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm5_ch6> ftm5_ch6{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d1> fxio_d1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpi2c1_sda> lpi2c1_sda{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortcGpclr{    ///<Global Pin Control Low Register
        using Addr = Register::Address<0x4004b080,0x00000000,0x00000000,unsigned>;
        ///Global Pin Write Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> gpwd{}; 
        ///Global Pin Write Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,16),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> gpwe{}; 
    }
    namespace PortcGpchr{    ///<Global Pin Control High Register
        using Addr = Register::Address<0x4004b084,0x00000000,0x00000000,unsigned>;
        ///Global Pin Write Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> gpwd{}; 
        ///Global Pin Write Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,16),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> gpwe{}; 
    }
    namespace PortcGiclr{    ///<Global Interrupt Control Low Register
        using Addr = Register::Address<0x4004b088,0x00000000,0x00000000,unsigned>;
        ///Global Interrupt Write Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> giwe{}; 
        ///Global Interrupt Write Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,16),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> giwd{}; 
    }
    namespace PortcGichr{    ///<Global Interrupt Control High Register
        using Addr = Register::Address<0x4004b08c,0x00000000,0x00000000,unsigned>;
        ///Global Interrupt Write Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> giwe{}; 
        ///Global Interrupt Write Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,16),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> giwd{}; 
    }
    namespace PortcIsfr{    ///<Interrupt Status Flag Register
        using Addr = Register::Address<0x4004b0a0,0x00000000,0x00000000,unsigned>;
        ///Interrupt Status Flag
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> isf{}; 
    }
    namespace PortcDfer{    ///<Digital Filter Enable Register
        using Addr = Register::Address<0x4004b0c0,0x00000000,0x00000000,unsigned>;
        ///Digital Filter Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> dfe{}; 
    }
    namespace PortcDfcr{    ///<Digital Filter Clock Register
        using Addr = Register::Address<0x4004b0c4,0xfffffffe,0x00000000,unsigned>;
        ///Clock Source
        enum class CsVal : unsigned {
            v0=0x00000000,     ///<Digital filters are clocked by the bus clock.
            v1=0x00000001,     ///<Digital filters are clocked by the LPO clock.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,CsVal> cs{}; 
        namespace CsValC{
            constexpr Register::FieldValue<decltype(cs)::Type,CsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cs)::Type,CsVal::v1> v1{};
        }
    }
    namespace PortcDfwr{    ///<Digital Filter Width Register
        using Addr = Register::Address<0x4004b0c8,0xffffffe0,0x00000000,unsigned>;
        ///Filter Length
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,0),Register::ReadWriteAccess,unsigned> filt{}; 
    }
}
