#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//System Mode Controller
    namespace SmcVerid{    ///<SMC Version ID Register
        using Addr = Register::Address<0x4007e000,0x00000000,0x00000000,unsigned>;
        ///Feature Specification Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> feature{}; 
        ///Minor Version Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> minor{}; 
        ///Major Version Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> major{}; 
    }
    namespace SmcParam{    ///<SMC Parameter Register
        using Addr = Register::Address<0x4007e004,0xffffff96,0x00000000,unsigned>;
        ///Existence of HSRUN feature
        enum class EhsrunVal : unsigned {
            v0=0x00000000,     ///<The feature is not available.
            v1=0x00000001,     ///<The feature is available.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,EhsrunVal> ehsrun{}; 
        namespace EhsrunValC{
            constexpr Register::FieldValue<decltype(ehsrun)::Type,EhsrunVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ehsrun)::Type,EhsrunVal::v1> v1{};
        }
        ///Existence of LLS feature
        enum class EllsVal : unsigned {
            v0=0x00000000,     ///<The feature is not available.
            v1=0x00000001,     ///<The feature is available.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,EllsVal> ells{}; 
        namespace EllsValC{
            constexpr Register::FieldValue<decltype(ells)::Type,EllsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ells)::Type,EllsVal::v1> v1{};
        }
        ///Existence of LLS2 feature
        enum class Ells2Val : unsigned {
            v0=0x00000000,     ///<The feature is not available.
            v1=0x00000001,     ///<The feature is available.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,Ells2Val> ells2{}; 
        namespace Ells2ValC{
            constexpr Register::FieldValue<decltype(ells2)::Type,Ells2Val::v0> v0{};
            constexpr Register::FieldValue<decltype(ells2)::Type,Ells2Val::v1> v1{};
        }
        ///Existence of VLLS0 feature
        enum class Evlls0Val : unsigned {
            v0=0x00000000,     ///<The feature is not available.
            v1=0x00000001,     ///<The feature is available.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,Evlls0Val> evlls0{}; 
        namespace Evlls0ValC{
            constexpr Register::FieldValue<decltype(evlls0)::Type,Evlls0Val::v0> v0{};
            constexpr Register::FieldValue<decltype(evlls0)::Type,Evlls0Val::v1> v1{};
        }
    }
    namespace SmcPmprot{    ///<Power Mode Protection register
        using Addr = Register::Address<0x4007e008,0xffffff5f,0x00000000,unsigned>;
        ///Allow Very-Low-Power Modes
        enum class AvlpVal : unsigned {
            v0=0x00000000,     ///<VLPR and VLPS are not allowed.
            v1=0x00000001,     ///<VLPR and VLPS are allowed.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,AvlpVal> avlp{}; 
        namespace AvlpValC{
            constexpr Register::FieldValue<decltype(avlp)::Type,AvlpVal::v0> v0{};
            constexpr Register::FieldValue<decltype(avlp)::Type,AvlpVal::v1> v1{};
        }
        ///Allow High Speed Run mode
        enum class AhsrunVal : unsigned {
            v0=0x00000000,     ///<HSRUN is not allowed
            v1=0x00000001,     ///<HSRUN is allowed
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::ReadWriteAccess,AhsrunVal> ahsrun{}; 
        namespace AhsrunValC{
            constexpr Register::FieldValue<decltype(ahsrun)::Type,AhsrunVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ahsrun)::Type,AhsrunVal::v1> v1{};
        }
    }
    namespace SmcPmctrl{    ///<Power Mode Control register
        using Addr = Register::Address<0x4007e00c,0xffffff90,0x00000000,unsigned>;
        ///Stop Mode Control
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,0),Register::ReadWriteAccess,unsigned> stopm{}; 
        ///Very Low Power Stop Aborted
        enum class VlpsaVal : unsigned {
            v0=0x00000000,     ///<The previous stop mode entry was successful.
            v1=0x00000001,     ///<The previous stop mode entry was aborted.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,VlpsaVal> vlpsa{}; 
        namespace VlpsaValC{
            constexpr Register::FieldValue<decltype(vlpsa)::Type,VlpsaVal::v0> v0{};
            constexpr Register::FieldValue<decltype(vlpsa)::Type,VlpsaVal::v1> v1{};
        }
        ///Run Mode Control
        enum class RunmVal : unsigned {
            v00=0x00000000,     ///<Normal Run mode (RUN)
            v10=0x00000002,     ///<Very-Low-Power Run mode (VLPR)
            v11=0x00000003,     ///<High Speed Run mode (HSRUN)
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,5),Register::ReadWriteAccess,RunmVal> runm{}; 
        namespace RunmValC{
            constexpr Register::FieldValue<decltype(runm)::Type,RunmVal::v00> v00{};
            constexpr Register::FieldValue<decltype(runm)::Type,RunmVal::v10> v10{};
            constexpr Register::FieldValue<decltype(runm)::Type,RunmVal::v11> v11{};
        }
    }
    namespace SmcStopctrl{    ///<Stop Control Register
        using Addr = Register::Address<0x4007e010,0xffffff3f,0x00000000,unsigned>;
        ///Stop Option
        enum class StopoVal : unsigned {
            v01=0x00000001,     ///<STOP1 - Stop with both system and bus clocks disabled
            v10=0x00000002,     ///<STOP2 - Stop with system clock disabled and bus clock enabled
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,6),Register::ReadWriteAccess,StopoVal> stopo{}; 
        namespace StopoValC{
            constexpr Register::FieldValue<decltype(stopo)::Type,StopoVal::v01> v01{};
            constexpr Register::FieldValue<decltype(stopo)::Type,StopoVal::v10> v10{};
        }
    }
    namespace SmcPmstat{    ///<Power Mode Status register
        using Addr = Register::Address<0x4007e014,0xffffff00,0x00000000,unsigned>;
        ///Power Mode Status
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> pmstat{}; 
    }
}
