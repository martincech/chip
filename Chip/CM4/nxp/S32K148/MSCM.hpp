#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//MSCM
    namespace MscmCpxtype{    ///<Processor X Type Register
        using Addr = Register::Address<0x40001000,0x00000000,0x00000000,unsigned>;
        ///Processor x Revision
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rypz{}; 
        ///Processor x Personality
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> personality{}; 
    }
    namespace MscmCpxnum{    ///<Processor X Number Register
        using Addr = Register::Address<0x40001004,0xfffffffe,0x00000000,unsigned>;
        ///Processor x Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> cpn{}; 
    }
    namespace MscmCpxmaster{    ///<Processor X Master Register
        using Addr = Register::Address<0x40001008,0xffffffc0,0x00000000,unsigned>;
        ///Processor x Physical Master Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> ppmn{}; 
    }
    namespace MscmCpxcount{    ///<Processor X Count Register
        using Addr = Register::Address<0x4000100c,0xfffffffc,0x00000000,unsigned>;
        ///Processor Count
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> pcnt{}; 
    }
    namespace MscmCpxcfg0{    ///<Processor X Configuration Register 0
        using Addr = Register::Address<0x40001010,0x00000000,0x00000000,unsigned>;
        ///Level 1 Data Cache Ways
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dcwy{}; 
        ///Level 1 Data Cache Size
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dcsz{}; 
        ///Level 1 Instruction Cache Ways
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> icwy{}; 
        ///Level 1 Instruction Cache Size
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> icsz{}; 
    }
    namespace MscmCpxcfg1{    ///<Processor X Configuration Register 1
        using Addr = Register::Address<0x40001014,0x0000ffff,0x00000000,unsigned>;
        ///Level 2 Instruction Cache Ways
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> l2wy{}; 
        ///Level 2 Instruction Cache Size
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> l2sz{}; 
    }
    namespace MscmCpxcfg2{    ///<Processor X Configuration Register 2
        using Addr = Register::Address<0x40001018,0x00ff00ff,0x00000000,unsigned>;
        ///Tightly-coupled Memory Upper Size
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> tmusz{}; 
        ///Tightly-coupled Memory Lower Size
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> tmlsz{}; 
    }
    namespace MscmCpxcfg3{    ///<Processor X Configuration Register 3
        using Addr = Register::Address<0x4000101c,0xfffffc80,0x00000000,unsigned>;
        ///Floating Point Unit
        enum class FpuVal : unsigned {
            v0=0x00000000,     ///<FPU support is not included.
            v1=0x00000001,     ///<FPU support is included.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,FpuVal> fpu{}; 
        namespace FpuValC{
            constexpr Register::FieldValue<decltype(fpu)::Type,FpuVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fpu)::Type,FpuVal::v1> v1{};
        }
        ///SIMD/NEON instruction support
        enum class SimdVal : unsigned {
            v0=0x00000000,     ///<SIMD/NEON support is not included.
            v1=0x00000001,     ///<SIMD/NEON support is included.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,SimdVal> simd{}; 
        namespace SimdValC{
            constexpr Register::FieldValue<decltype(simd)::Type,SimdVal::v0> v0{};
            constexpr Register::FieldValue<decltype(simd)::Type,SimdVal::v1> v1{};
        }
        ///Jazelle support
        enum class JazVal : unsigned {
            v0=0x00000000,     ///<Jazelle support is not included.
            v1=0x00000001,     ///<Jazelle support is included.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,JazVal> jaz{}; 
        namespace JazValC{
            constexpr Register::FieldValue<decltype(jaz)::Type,JazVal::v0> v0{};
            constexpr Register::FieldValue<decltype(jaz)::Type,JazVal::v1> v1{};
        }
        ///Memory Management Unit
        enum class MmuVal : unsigned {
            v0=0x00000000,     ///<MMU support is not included.
            v1=0x00000001,     ///<MMU support is included.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,MmuVal> mmu{}; 
        namespace MmuValC{
            constexpr Register::FieldValue<decltype(mmu)::Type,MmuVal::v0> v0{};
            constexpr Register::FieldValue<decltype(mmu)::Type,MmuVal::v1> v1{};
        }
        ///Trust Zone
        enum class TzVal : unsigned {
            v0=0x00000000,     ///<Trust Zone support is not included.
            v1=0x00000001,     ///<Trust Zone support is included.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,4),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,TzVal> tz{}; 
        namespace TzValC{
            constexpr Register::FieldValue<decltype(tz)::Type,TzVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tz)::Type,TzVal::v1> v1{};
        }
        ///Core Memory Protection unit
        enum class CmpVal : unsigned {
            v0=0x00000000,     ///<Core Memory Protection is not included.
            v1=0x00000001,     ///<Core Memory Protection is included.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CmpVal> cmp{}; 
        namespace CmpValC{
            constexpr Register::FieldValue<decltype(cmp)::Type,CmpVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cmp)::Type,CmpVal::v1> v1{};
        }
        ///Bit Banding
        enum class BbVal : unsigned {
            v0=0x00000000,     ///<Bit Banding is not supported.
            v1=0x00000001,     ///<Bit Banding is supported.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,BbVal> bb{}; 
        namespace BbValC{
            constexpr Register::FieldValue<decltype(bb)::Type,BbVal::v0> v0{};
            constexpr Register::FieldValue<decltype(bb)::Type,BbVal::v1> v1{};
        }
        ///System Bus Ports
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> sbp{}; 
    }
    namespace MscmCp0type{    ///<Processor 0 Type Register
        using Addr = Register::Address<0x40001020,0x00000000,0x00000000,unsigned>;
        ///Processor 0 Revision
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> rypz{}; 
        ///Processor 0 Personality
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> personality{}; 
    }
    namespace MscmCp0num{    ///<Processor 0 Number Register
        using Addr = Register::Address<0x40001024,0xfffffffe,0x00000000,unsigned>;
        ///Processor 0 Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> cpn{}; 
    }
    namespace MscmCp0master{    ///<Processor 0 Master Register
        using Addr = Register::Address<0x40001028,0xffffffc0,0x00000000,unsigned>;
        ///Processor 0 Physical Master Number
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> ppmn{}; 
    }
    namespace MscmCp0count{    ///<Processor 0 Count Register
        using Addr = Register::Address<0x4000102c,0xfffffffc,0x00000000,unsigned>;
        ///Processor Count
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> pcnt{}; 
    }
    namespace MscmCp0cfg0{    ///<Processor 0 Configuration Register 0
        using Addr = Register::Address<0x40001030,0x00000000,0x00000000,unsigned>;
        ///Level 1 Data Cache Ways
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dcwy{}; 
        ///Level 1 Data Cache Size
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> dcsz{}; 
        ///Level 1 Instruction Cache Ways
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> icwy{}; 
        ///Level 1 Instruction Cache Size
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> icsz{}; 
    }
    namespace MscmCp0cfg1{    ///<Processor 0 Configuration Register 1
        using Addr = Register::Address<0x40001034,0x0000ffff,0x00000000,unsigned>;
        ///Level 2 Instruction Cache Ways
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> l2wy{}; 
        ///Level 2 Instruction Cache Size
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> l2sz{}; 
    }
    namespace MscmCp0cfg2{    ///<Processor 0 Configuration Register 2
        using Addr = Register::Address<0x40001038,0x00ff00ff,0x00000000,unsigned>;
        ///Tightly-coupled Memory Upper Size
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> tmusz{}; 
        ///Tightly-coupled Memory Lower Size
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> tmlsz{}; 
    }
    namespace MscmCp0cfg3{    ///<Processor 0 Configuration Register 3
        using Addr = Register::Address<0x4000103c,0xfffffc80,0x00000000,unsigned>;
        ///Floating Point Unit
        enum class FpuVal : unsigned {
            v0=0x00000000,     ///<FPU support is not included.
            v1=0x00000001,     ///<FPU support is included.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,FpuVal> fpu{}; 
        namespace FpuValC{
            constexpr Register::FieldValue<decltype(fpu)::Type,FpuVal::v0> v0{};
            constexpr Register::FieldValue<decltype(fpu)::Type,FpuVal::v1> v1{};
        }
        ///SIMD/NEON instruction support
        enum class SimdVal : unsigned {
            v0=0x00000000,     ///<SIMD/NEON support is not included.
            v1=0x00000001,     ///<SIMD/NEON support is included.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,SimdVal> simd{}; 
        namespace SimdValC{
            constexpr Register::FieldValue<decltype(simd)::Type,SimdVal::v0> v0{};
            constexpr Register::FieldValue<decltype(simd)::Type,SimdVal::v1> v1{};
        }
        ///Jazelle support
        enum class JazVal : unsigned {
            v0=0x00000000,     ///<Jazelle support is not included.
            v1=0x00000001,     ///<Jazelle support is included.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,JazVal> jaz{}; 
        namespace JazValC{
            constexpr Register::FieldValue<decltype(jaz)::Type,JazVal::v0> v0{};
            constexpr Register::FieldValue<decltype(jaz)::Type,JazVal::v1> v1{};
        }
        ///Memory Management Unit
        enum class MmuVal : unsigned {
            v0=0x00000000,     ///<MMU support is not included.
            v1=0x00000001,     ///<MMU support is included.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,MmuVal> mmu{}; 
        namespace MmuValC{
            constexpr Register::FieldValue<decltype(mmu)::Type,MmuVal::v0> v0{};
            constexpr Register::FieldValue<decltype(mmu)::Type,MmuVal::v1> v1{};
        }
        ///Trust Zone
        enum class TzVal : unsigned {
            v0=0x00000000,     ///<Trust Zone support is not included.
            v1=0x00000001,     ///<Trust Zone support is included.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,4),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,TzVal> tz{}; 
        namespace TzValC{
            constexpr Register::FieldValue<decltype(tz)::Type,TzVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tz)::Type,TzVal::v1> v1{};
        }
        ///Core Memory Protection unit
        enum class CmpVal : unsigned {
            v0=0x00000000,     ///<Core Memory Protection is not included.
            v1=0x00000001,     ///<Core Memory Protection is included.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,CmpVal> cmp{}; 
        namespace CmpValC{
            constexpr Register::FieldValue<decltype(cmp)::Type,CmpVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cmp)::Type,CmpVal::v1> v1{};
        }
        ///Bit Banding
        enum class BbVal : unsigned {
            v0=0x00000000,     ///<Bit Banding is not supported.
            v1=0x00000001,     ///<Bit Banding is supported.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,BbVal> bb{}; 
        namespace BbValC{
            constexpr Register::FieldValue<decltype(bb)::Type,BbVal::v0> v0{};
            constexpr Register::FieldValue<decltype(bb)::Type,BbVal::v1> v1{};
        }
        ///System Bus Ports
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(9,8),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> sbp{}; 
    }
    namespace MscmOcmdr0{    ///<On-Chip Memory Descriptor Register
        using Addr = Register::Address<0x40001400,0x60f00000,0x00000000,unsigned>;
        ///OCMEM Control Field 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::ReadWriteAccess,unsigned> ocm0{}; 
        ///OCMEM Control Field 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,4),Register::ReadWriteAccess,unsigned> ocm1{}; 
        ///OCMEM Control Field 2
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,8),Register::ReadWriteAccess,unsigned> ocm2{}; 
        ///OCMPU
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,12),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> ocmpu{}; 
        ///OCMT
        enum class OcmtVal : unsigned {
            v000=0x00000000,     ///<OCMEMn is a System RAM.
            v001=0x00000001,     ///<OCMEMn is a Graphics RAM.
            v011=0x00000003,     ///<OCMEMn is a ROM.
            v100=0x00000004,     ///<OCMEMn is a Program Flash.
            v101=0x00000005,     ///<OCMEMn is a Data Flash.
            v110=0x00000006,     ///<OCMEMn is an EEE.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,13),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,OcmtVal> ocmt{}; 
        namespace OcmtValC{
            constexpr Register::FieldValue<decltype(ocmt)::Type,OcmtVal::v000> v000{};
            constexpr Register::FieldValue<decltype(ocmt)::Type,OcmtVal::v001> v001{};
            constexpr Register::FieldValue<decltype(ocmt)::Type,OcmtVal::v011> v011{};
            constexpr Register::FieldValue<decltype(ocmt)::Type,OcmtVal::v100> v100{};
            constexpr Register::FieldValue<decltype(ocmt)::Type,OcmtVal::v101> v101{};
            constexpr Register::FieldValue<decltype(ocmt)::Type,OcmtVal::v110> v110{};
        }
        ///RO
        enum class RoVal : unsigned {
            v0=0x00000000,     ///<Writes to the OCMDRn[11:0] are allowed
            v1=0x00000001,     ///<Writes to the OCMDRn[11:0] are ignored
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,RoVal> ro{}; 
        namespace RoValC{
            constexpr Register::FieldValue<decltype(ro)::Type,RoVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ro)::Type,RoVal::v1> v1{};
        }
        ///OCMW
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,17),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> ocmw{}; 
        ///OCMSZ
        enum class OcmszVal : unsigned {
            v0000=0x00000000,     ///<no OCMEMn
            v0001=0x00000001,     ///<1KB OCMEMn
            v0010=0x00000002,     ///<2KB OCMEMn
            v0011=0x00000003,     ///<4KB OCMEMn
            v0100=0x00000004,     ///<8KB OCMEMn
            v0101=0x00000005,     ///<16KB OCMEMn
            v0110=0x00000006,     ///<32KB OCMEMn
            v0111=0x00000007,     ///<64KB OCMEMn
            v1000=0x00000008,     ///<128KB OCMEMn
            v1001=0x00000009,     ///<256KB OCMEMn
            v1010=0x0000000a,     ///<512KB OCMEMn
            v1011=0x0000000b,     ///<1MB OCMEMn
            v1100=0x0000000c,     ///<2MB OCMEMn
            v1101=0x0000000d,     ///<4MB OCMEMn
            v1110=0x0000000e,     ///<8MB OCMEMn
            v1111=0x0000000f,     ///<16MB OCMEMn
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,OcmszVal> ocmsz{}; 
        namespace OcmszValC{
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0100> v0100{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0101> v0101{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0110> v0110{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0111> v0111{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1100> v1100{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1101> v1101{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1110> v1110{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1111> v1111{};
        }
        ///OCMSZH
        enum class OcmszhVal : unsigned {
            v0=0x00000000,     ///<OCMEMn is a power-of-2 capacity.
            v1=0x00000001,     ///<OCMEMn is not a power-of-2, with a capacity is 0.75 * OCMSZ.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,OcmszhVal> ocmszh{}; 
        namespace OcmszhValC{
            constexpr Register::FieldValue<decltype(ocmszh)::Type,OcmszhVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ocmszh)::Type,OcmszhVal::v1> v1{};
        }
        ///V
        enum class VVal : unsigned {
            v0=0x00000000,     ///<OCMEMn is not present.
            v1=0x00000001,     ///<OCMEMn is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,VVal> v{}; 
        namespace VValC{
            constexpr Register::FieldValue<decltype(v)::Type,VVal::v0> v0{};
            constexpr Register::FieldValue<decltype(v)::Type,VVal::v1> v1{};
        }
    }
    namespace MscmOcmdr1{    ///<On-Chip Memory Descriptor Register
        using Addr = Register::Address<0x40001404,0x60f00000,0x00000000,unsigned>;
        ///OCMEM Control Field 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::ReadWriteAccess,unsigned> ocm0{}; 
        ///OCMEM Control Field 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,4),Register::ReadWriteAccess,unsigned> ocm1{}; 
        ///OCMEM Control Field 2
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,8),Register::ReadWriteAccess,unsigned> ocm2{}; 
        ///OCMPU
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,12),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> ocmpu{}; 
        ///OCMT
        enum class OcmtVal : unsigned {
            v000=0x00000000,     ///<OCMEMn is a System RAM.
            v001=0x00000001,     ///<OCMEMn is a Graphics RAM.
            v011=0x00000003,     ///<OCMEMn is a ROM.
            v100=0x00000004,     ///<OCMEMn is a Program Flash.
            v101=0x00000005,     ///<OCMEMn is a Data Flash.
            v110=0x00000006,     ///<OCMEMn is an EEE.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,13),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,OcmtVal> ocmt{}; 
        namespace OcmtValC{
            constexpr Register::FieldValue<decltype(ocmt)::Type,OcmtVal::v000> v000{};
            constexpr Register::FieldValue<decltype(ocmt)::Type,OcmtVal::v001> v001{};
            constexpr Register::FieldValue<decltype(ocmt)::Type,OcmtVal::v011> v011{};
            constexpr Register::FieldValue<decltype(ocmt)::Type,OcmtVal::v100> v100{};
            constexpr Register::FieldValue<decltype(ocmt)::Type,OcmtVal::v101> v101{};
            constexpr Register::FieldValue<decltype(ocmt)::Type,OcmtVal::v110> v110{};
        }
        ///RO
        enum class RoVal : unsigned {
            v0=0x00000000,     ///<Writes to the OCMDRn[11:0] are allowed
            v1=0x00000001,     ///<Writes to the OCMDRn[11:0] are ignored
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,RoVal> ro{}; 
        namespace RoValC{
            constexpr Register::FieldValue<decltype(ro)::Type,RoVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ro)::Type,RoVal::v1> v1{};
        }
        ///OCMW
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,17),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> ocmw{}; 
        ///OCMSZ
        enum class OcmszVal : unsigned {
            v0000=0x00000000,     ///<no OCMEMn
            v0001=0x00000001,     ///<1KB OCMEMn
            v0010=0x00000002,     ///<2KB OCMEMn
            v0011=0x00000003,     ///<4KB OCMEMn
            v0100=0x00000004,     ///<8KB OCMEMn
            v0101=0x00000005,     ///<16KB OCMEMn
            v0110=0x00000006,     ///<32KB OCMEMn
            v0111=0x00000007,     ///<64KB OCMEMn
            v1000=0x00000008,     ///<128KB OCMEMn
            v1001=0x00000009,     ///<256KB OCMEMn
            v1010=0x0000000a,     ///<512KB OCMEMn
            v1011=0x0000000b,     ///<1MB OCMEMn
            v1100=0x0000000c,     ///<2MB OCMEMn
            v1101=0x0000000d,     ///<4MB OCMEMn
            v1110=0x0000000e,     ///<8MB OCMEMn
            v1111=0x0000000f,     ///<16MB OCMEMn
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,OcmszVal> ocmsz{}; 
        namespace OcmszValC{
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0100> v0100{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0101> v0101{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0110> v0110{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0111> v0111{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1100> v1100{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1101> v1101{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1110> v1110{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1111> v1111{};
        }
        ///OCMSZH
        enum class OcmszhVal : unsigned {
            v0=0x00000000,     ///<OCMEMn is a power-of-2 capacity.
            v1=0x00000001,     ///<OCMEMn is not a power-of-2, with a capacity is 0.75 * OCMSZ.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,OcmszhVal> ocmszh{}; 
        namespace OcmszhValC{
            constexpr Register::FieldValue<decltype(ocmszh)::Type,OcmszhVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ocmszh)::Type,OcmszhVal::v1> v1{};
        }
        ///V
        enum class VVal : unsigned {
            v0=0x00000000,     ///<OCMEMn is not present.
            v1=0x00000001,     ///<OCMEMn is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,VVal> v{}; 
        namespace VValC{
            constexpr Register::FieldValue<decltype(v)::Type,VVal::v0> v0{};
            constexpr Register::FieldValue<decltype(v)::Type,VVal::v1> v1{};
        }
    }
    namespace MscmOcmdr2{    ///<On-Chip Memory Descriptor Register
        using Addr = Register::Address<0x40001408,0x60f00000,0x00000000,unsigned>;
        ///OCMEM Control Field 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::ReadWriteAccess,unsigned> ocm0{}; 
        ///OCMEM Control Field 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,4),Register::ReadWriteAccess,unsigned> ocm1{}; 
        ///OCMEM Control Field 2
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,8),Register::ReadWriteAccess,unsigned> ocm2{}; 
        ///OCMPU
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,12),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> ocmpu{}; 
        ///OCMT
        enum class OcmtVal : unsigned {
            v000=0x00000000,     ///<OCMEMn is a System RAM.
            v001=0x00000001,     ///<OCMEMn is a Graphics RAM.
            v011=0x00000003,     ///<OCMEMn is a ROM.
            v100=0x00000004,     ///<OCMEMn is a Program Flash.
            v101=0x00000005,     ///<OCMEMn is a Data Flash.
            v110=0x00000006,     ///<OCMEMn is an EEE.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,13),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,OcmtVal> ocmt{}; 
        namespace OcmtValC{
            constexpr Register::FieldValue<decltype(ocmt)::Type,OcmtVal::v000> v000{};
            constexpr Register::FieldValue<decltype(ocmt)::Type,OcmtVal::v001> v001{};
            constexpr Register::FieldValue<decltype(ocmt)::Type,OcmtVal::v011> v011{};
            constexpr Register::FieldValue<decltype(ocmt)::Type,OcmtVal::v100> v100{};
            constexpr Register::FieldValue<decltype(ocmt)::Type,OcmtVal::v101> v101{};
            constexpr Register::FieldValue<decltype(ocmt)::Type,OcmtVal::v110> v110{};
        }
        ///RO
        enum class RoVal : unsigned {
            v0=0x00000000,     ///<Writes to the OCMDRn[11:0] are allowed
            v1=0x00000001,     ///<Writes to the OCMDRn[11:0] are ignored
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,RoVal> ro{}; 
        namespace RoValC{
            constexpr Register::FieldValue<decltype(ro)::Type,RoVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ro)::Type,RoVal::v1> v1{};
        }
        ///OCMW
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,17),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> ocmw{}; 
        ///OCMSZ
        enum class OcmszVal : unsigned {
            v0000=0x00000000,     ///<no OCMEMn
            v0001=0x00000001,     ///<1KB OCMEMn
            v0010=0x00000002,     ///<2KB OCMEMn
            v0011=0x00000003,     ///<4KB OCMEMn
            v0100=0x00000004,     ///<8KB OCMEMn
            v0101=0x00000005,     ///<16KB OCMEMn
            v0110=0x00000006,     ///<32KB OCMEMn
            v0111=0x00000007,     ///<64KB OCMEMn
            v1000=0x00000008,     ///<128KB OCMEMn
            v1001=0x00000009,     ///<256KB OCMEMn
            v1010=0x0000000a,     ///<512KB OCMEMn
            v1011=0x0000000b,     ///<1MB OCMEMn
            v1100=0x0000000c,     ///<2MB OCMEMn
            v1101=0x0000000d,     ///<4MB OCMEMn
            v1110=0x0000000e,     ///<8MB OCMEMn
            v1111=0x0000000f,     ///<16MB OCMEMn
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,OcmszVal> ocmsz{}; 
        namespace OcmszValC{
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0100> v0100{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0101> v0101{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0110> v0110{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0111> v0111{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1100> v1100{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1101> v1101{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1110> v1110{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1111> v1111{};
        }
        ///OCMSZH
        enum class OcmszhVal : unsigned {
            v0=0x00000000,     ///<OCMEMn is a power-of-2 capacity.
            v1=0x00000001,     ///<OCMEMn is not a power-of-2, with a capacity is 0.75 * OCMSZ.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,OcmszhVal> ocmszh{}; 
        namespace OcmszhValC{
            constexpr Register::FieldValue<decltype(ocmszh)::Type,OcmszhVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ocmszh)::Type,OcmszhVal::v1> v1{};
        }
        ///V
        enum class VVal : unsigned {
            v0=0x00000000,     ///<OCMEMn is not present.
            v1=0x00000001,     ///<OCMEMn is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,VVal> v{}; 
        namespace VValC{
            constexpr Register::FieldValue<decltype(v)::Type,VVal::v0> v0{};
            constexpr Register::FieldValue<decltype(v)::Type,VVal::v1> v1{};
        }
    }
    namespace MscmOcmdr3{    ///<On-Chip Memory Descriptor Register
        using Addr = Register::Address<0x4000140c,0x60f00000,0x00000000,unsigned>;
        ///OCMEM Control Field 0
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,0),Register::ReadWriteAccess,unsigned> ocm0{}; 
        ///OCMEM Control Field 1
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,4),Register::ReadWriteAccess,unsigned> ocm1{}; 
        ///OCMEM Control Field 2
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(11,8),Register::ReadWriteAccess,unsigned> ocm2{}; 
        ///OCMPU
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(12,12),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> ocmpu{}; 
        ///OCMT
        enum class OcmtVal : unsigned {
            v000=0x00000000,     ///<OCMEMn is a System RAM.
            v001=0x00000001,     ///<OCMEMn is a Graphics RAM.
            v011=0x00000003,     ///<OCMEMn is a ROM.
            v100=0x00000004,     ///<OCMEMn is a Program Flash.
            v101=0x00000005,     ///<OCMEMn is a Data Flash.
            v110=0x00000006,     ///<OCMEMn is an EEE.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,13),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,OcmtVal> ocmt{}; 
        namespace OcmtValC{
            constexpr Register::FieldValue<decltype(ocmt)::Type,OcmtVal::v000> v000{};
            constexpr Register::FieldValue<decltype(ocmt)::Type,OcmtVal::v001> v001{};
            constexpr Register::FieldValue<decltype(ocmt)::Type,OcmtVal::v011> v011{};
            constexpr Register::FieldValue<decltype(ocmt)::Type,OcmtVal::v100> v100{};
            constexpr Register::FieldValue<decltype(ocmt)::Type,OcmtVal::v101> v101{};
            constexpr Register::FieldValue<decltype(ocmt)::Type,OcmtVal::v110> v110{};
        }
        ///RO
        enum class RoVal : unsigned {
            v0=0x00000000,     ///<Writes to the OCMDRn[11:0] are allowed
            v1=0x00000001,     ///<Writes to the OCMDRn[11:0] are ignored
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(16,16),Register::ReadWriteAccess,RoVal> ro{}; 
        namespace RoValC{
            constexpr Register::FieldValue<decltype(ro)::Type,RoVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ro)::Type,RoVal::v1> v1{};
        }
        ///OCMW
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,17),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> ocmw{}; 
        ///OCMSZ
        enum class OcmszVal : unsigned {
            v0000=0x00000000,     ///<no OCMEMn
            v0001=0x00000001,     ///<1KB OCMEMn
            v0010=0x00000002,     ///<2KB OCMEMn
            v0011=0x00000003,     ///<4KB OCMEMn
            v0100=0x00000004,     ///<8KB OCMEMn
            v0101=0x00000005,     ///<16KB OCMEMn
            v0110=0x00000006,     ///<32KB OCMEMn
            v0111=0x00000007,     ///<64KB OCMEMn
            v1000=0x00000008,     ///<128KB OCMEMn
            v1001=0x00000009,     ///<256KB OCMEMn
            v1010=0x0000000a,     ///<512KB OCMEMn
            v1011=0x0000000b,     ///<1MB OCMEMn
            v1100=0x0000000c,     ///<2MB OCMEMn
            v1101=0x0000000d,     ///<4MB OCMEMn
            v1110=0x0000000e,     ///<8MB OCMEMn
            v1111=0x0000000f,     ///<16MB OCMEMn
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(27,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,OcmszVal> ocmsz{}; 
        namespace OcmszValC{
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0100> v0100{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0101> v0101{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0110> v0110{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v0111> v0111{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1100> v1100{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1101> v1101{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1110> v1110{};
            constexpr Register::FieldValue<decltype(ocmsz)::Type,OcmszVal::v1111> v1111{};
        }
        ///OCMSZH
        enum class OcmszhVal : unsigned {
            v0=0x00000000,     ///<OCMEMn is a power-of-2 capacity.
            v1=0x00000001,     ///<OCMEMn is not a power-of-2, with a capacity is 0.75 * OCMSZ.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(28,28),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,OcmszhVal> ocmszh{}; 
        namespace OcmszhValC{
            constexpr Register::FieldValue<decltype(ocmszh)::Type,OcmszhVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ocmszh)::Type,OcmszhVal::v1> v1{};
        }
        ///V
        enum class VVal : unsigned {
            v0=0x00000000,     ///<OCMEMn is not present.
            v1=0x00000001,     ///<OCMEMn is present.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,VVal> v{}; 
        namespace VValC{
            constexpr Register::FieldValue<decltype(v)::Type,VVal::v0> v0{};
            constexpr Register::FieldValue<decltype(v)::Type,VVal::v1> v1{};
        }
    }
}
