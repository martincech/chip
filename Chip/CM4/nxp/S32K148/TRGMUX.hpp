#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//TRGMUX
    namespace TrgmuxTrgmuxDmamux0{    ///<TRGMUX DMAMUX0 Register
        using Addr = Register::Address<0x40063000,0x00808080,0x00000000,unsigned>;
        ///Trigger MUX Input 0 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> sel0{}; 
        ///Trigger MUX Input 1 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,8),Register::ReadWriteAccess,unsigned> sel1{}; 
        ///Trigger MUX Input 2 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,16),Register::ReadWriteAccess,unsigned> sel2{}; 
        ///Trigger MUX Input 3 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,24),Register::ReadWriteAccess,unsigned> sel3{}; 
        ///TRGMUX register lock.
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Register can be written.
            v1=0x00000001,     ///<Register cannot be written until the next system Reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
    }
    namespace TrgmuxTrgmuxExtout0{    ///<TRGMUX EXTOUT0 Register
        using Addr = Register::Address<0x40063004,0x00808080,0x00000000,unsigned>;
        ///Trigger MUX Input 0 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> sel0{}; 
        ///Trigger MUX Input 1 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,8),Register::ReadWriteAccess,unsigned> sel1{}; 
        ///Trigger MUX Input 2 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,16),Register::ReadWriteAccess,unsigned> sel2{}; 
        ///Trigger MUX Input 3 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,24),Register::ReadWriteAccess,unsigned> sel3{}; 
        ///TRGMUX register lock.
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Register can be written.
            v1=0x00000001,     ///<Register cannot be written until the next system Reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
    }
    namespace TrgmuxTrgmuxExtout1{    ///<TRGMUX EXTOUT1 Register
        using Addr = Register::Address<0x40063008,0x00808080,0x00000000,unsigned>;
        ///Trigger MUX Input 0 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> sel0{}; 
        ///Trigger MUX Input 1 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,8),Register::ReadWriteAccess,unsigned> sel1{}; 
        ///Trigger MUX Input 2 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,16),Register::ReadWriteAccess,unsigned> sel2{}; 
        ///Trigger MUX Input 3 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,24),Register::ReadWriteAccess,unsigned> sel3{}; 
        ///TRGMUX register lock.
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Register can be written.
            v1=0x00000001,     ///<Register cannot be written until the next system Reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
    }
    namespace TrgmuxTrgmuxAdc0{    ///<TRGMUX ADC0 Register
        using Addr = Register::Address<0x4006300c,0x00808080,0x00000000,unsigned>;
        ///Trigger MUX Input 0 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> sel0{}; 
        ///Trigger MUX Input 1 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,8),Register::ReadWriteAccess,unsigned> sel1{}; 
        ///Trigger MUX Input 2 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,16),Register::ReadWriteAccess,unsigned> sel2{}; 
        ///Trigger MUX Input 3 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,24),Register::ReadWriteAccess,unsigned> sel3{}; 
        ///TRGMUX register lock.
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Register can be written.
            v1=0x00000001,     ///<Register cannot be written until the next system Reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
    }
    namespace TrgmuxTrgmuxAdc1{    ///<TRGMUX ADC1 Register
        using Addr = Register::Address<0x40063010,0x00808080,0x00000000,unsigned>;
        ///Trigger MUX Input 0 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> sel0{}; 
        ///Trigger MUX Input 1 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,8),Register::ReadWriteAccess,unsigned> sel1{}; 
        ///Trigger MUX Input 2 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,16),Register::ReadWriteAccess,unsigned> sel2{}; 
        ///Trigger MUX Input 3 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,24),Register::ReadWriteAccess,unsigned> sel3{}; 
        ///TRGMUX register lock.
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Register can be written.
            v1=0x00000001,     ///<Register cannot be written until the next system Reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
    }
    namespace TrgmuxTrgmuxCmp0{    ///<TRGMUX CMP0 Register
        using Addr = Register::Address<0x4006301c,0x7fffff80,0x00000000,unsigned>;
        ///Trigger MUX Input 0 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> sel0{}; 
        ///TRGMUX register lock.
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Register can be written.
            v1=0x00000001,     ///<Register cannot be written until the next system Reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
    }
    namespace TrgmuxTrgmuxFtm0{    ///<TRGMUX FTM0 Register
        using Addr = Register::Address<0x40063028,0x00808080,0x00000000,unsigned>;
        ///Trigger MUX Input 0 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> sel0{}; 
        ///Trigger MUX Input 1 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,8),Register::ReadWriteAccess,unsigned> sel1{}; 
        ///Trigger MUX Input 2 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,16),Register::ReadWriteAccess,unsigned> sel2{}; 
        ///Trigger MUX Input 3 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,24),Register::ReadWriteAccess,unsigned> sel3{}; 
        ///TRGMUX register lock.
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Register can be written.
            v1=0x00000001,     ///<Register cannot be written until the next system Reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
    }
    namespace TrgmuxTrgmuxFtm1{    ///<TRGMUX FTM1 Register
        using Addr = Register::Address<0x4006302c,0x00808080,0x00000000,unsigned>;
        ///Trigger MUX Input 0 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> sel0{}; 
        ///Trigger MUX Input 1 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,8),Register::ReadWriteAccess,unsigned> sel1{}; 
        ///Trigger MUX Input 2 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,16),Register::ReadWriteAccess,unsigned> sel2{}; 
        ///Trigger MUX Input 3 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,24),Register::ReadWriteAccess,unsigned> sel3{}; 
        ///TRGMUX register lock.
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Register can be written.
            v1=0x00000001,     ///<Register cannot be written until the next system Reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
    }
    namespace TrgmuxTrgmuxFtm2{    ///<TRGMUX FTM2 Register
        using Addr = Register::Address<0x40063030,0x00808080,0x00000000,unsigned>;
        ///Trigger MUX Input 0 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> sel0{}; 
        ///Trigger MUX Input 1 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,8),Register::ReadWriteAccess,unsigned> sel1{}; 
        ///Trigger MUX Input 2 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,16),Register::ReadWriteAccess,unsigned> sel2{}; 
        ///Trigger MUX Input 3 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,24),Register::ReadWriteAccess,unsigned> sel3{}; 
        ///TRGMUX register lock.
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Register can be written.
            v1=0x00000001,     ///<Register cannot be written until the next system Reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
    }
    namespace TrgmuxTrgmuxFtm3{    ///<TRGMUX FTM3 Register
        using Addr = Register::Address<0x40063034,0x00808080,0x00000000,unsigned>;
        ///Trigger MUX Input 0 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> sel0{}; 
        ///Trigger MUX Input 1 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,8),Register::ReadWriteAccess,unsigned> sel1{}; 
        ///Trigger MUX Input 2 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,16),Register::ReadWriteAccess,unsigned> sel2{}; 
        ///Trigger MUX Input 3 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,24),Register::ReadWriteAccess,unsigned> sel3{}; 
        ///TRGMUX register lock.
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Register can be written.
            v1=0x00000001,     ///<Register cannot be written until the next system Reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
    }
    namespace TrgmuxTrgmuxPdb0{    ///<TRGMUX PDB0 Register
        using Addr = Register::Address<0x40063038,0x7fffff80,0x00000000,unsigned>;
        ///Trigger MUX Input 0 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> sel0{}; 
        ///TRGMUX register lock.
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Register can be written.
            v1=0x00000001,     ///<Register cannot be written until the next system Reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
    }
    namespace TrgmuxTrgmuxPdb1{    ///<TRGMUX PDB1 Register
        using Addr = Register::Address<0x4006303c,0x7fffff80,0x00000000,unsigned>;
        ///Trigger MUX Input 0 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> sel0{}; 
        ///TRGMUX register lock.
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Register can be written.
            v1=0x00000001,     ///<Register cannot be written until the next system Reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
    }
    namespace TrgmuxTrgmuxFlexio{    ///<TRGMUX FLEXIO Register
        using Addr = Register::Address<0x40063044,0x00808080,0x00000000,unsigned>;
        ///Trigger MUX Input 0 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> sel0{}; 
        ///Trigger MUX Input 1 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,8),Register::ReadWriteAccess,unsigned> sel1{}; 
        ///Trigger MUX Input 2 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,16),Register::ReadWriteAccess,unsigned> sel2{}; 
        ///Trigger MUX Input 3 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,24),Register::ReadWriteAccess,unsigned> sel3{}; 
        ///TRGMUX register lock.
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Register can be written.
            v1=0x00000001,     ///<Register cannot be written until the next system Reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
    }
    namespace TrgmuxTrgmuxLpit0{    ///<TRGMUX LPIT0 Register
        using Addr = Register::Address<0x40063048,0x00808080,0x00000000,unsigned>;
        ///Trigger MUX Input 0 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> sel0{}; 
        ///Trigger MUX Input 1 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(14,8),Register::ReadWriteAccess,unsigned> sel1{}; 
        ///Trigger MUX Input 2 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(22,16),Register::ReadWriteAccess,unsigned> sel2{}; 
        ///Trigger MUX Input 3 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(30,24),Register::ReadWriteAccess,unsigned> sel3{}; 
        ///TRGMUX register lock.
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Register can be written.
            v1=0x00000001,     ///<Register cannot be written until the next system Reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
    }
    namespace TrgmuxTrgmuxLpuart0{    ///<TRGMUX LPUART0 Register
        using Addr = Register::Address<0x4006304c,0x7fffff80,0x00000000,unsigned>;
        ///Trigger MUX Input 0 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> sel0{}; 
        ///TRGMUX register lock.
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Register can be written.
            v1=0x00000001,     ///<Register cannot be written until the next system Reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
    }
    namespace TrgmuxTrgmuxLpuart1{    ///<TRGMUX LPUART1 Register
        using Addr = Register::Address<0x40063050,0x7fffff80,0x00000000,unsigned>;
        ///Trigger MUX Input 0 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> sel0{}; 
        ///TRGMUX register lock.
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Register can be written.
            v1=0x00000001,     ///<Register cannot be written until the next system Reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
    }
    namespace TrgmuxTrgmuxLpi2c0{    ///<TRGMUX LPI2C0 Register
        using Addr = Register::Address<0x40063054,0x7fffff80,0x00000000,unsigned>;
        ///Trigger MUX Input 0 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> sel0{}; 
        ///TRGMUX register lock.
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Register can be written.
            v1=0x00000001,     ///<Register cannot be written until the next system Reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
    }
    namespace TrgmuxTrgmuxLpspi0{    ///<TRGMUX LPSPI0 Register
        using Addr = Register::Address<0x4006305c,0x7fffff80,0x00000000,unsigned>;
        ///Trigger MUX Input 0 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> sel0{}; 
        ///TRGMUX register lock.
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Register can be written.
            v1=0x00000001,     ///<Register cannot be written until the next system Reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
    }
    namespace TrgmuxTrgmuxLpspi1{    ///<TRGMUX LPSPI1 Register
        using Addr = Register::Address<0x40063060,0x7fffff80,0x00000000,unsigned>;
        ///Trigger MUX Input 0 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> sel0{}; 
        ///TRGMUX register lock.
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Register can be written.
            v1=0x00000001,     ///<Register cannot be written until the next system Reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
    }
    namespace TrgmuxTrgmuxLptmr0{    ///<TRGMUX LPTMR0 Register
        using Addr = Register::Address<0x40063064,0x7fffff80,0x00000000,unsigned>;
        ///Trigger MUX Input 0 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> sel0{}; 
        ///TRGMUX register lock.
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Register can be written.
            v1=0x00000001,     ///<Register cannot be written until the next system Reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
    }
    namespace TrgmuxTrgmuxLpi2c1{    ///<TRGMUX LPI2C1 Register
        using Addr = Register::Address<0x4006306c,0x7fffff80,0x00000000,unsigned>;
        ///Trigger MUX Input 0 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> sel0{}; 
        ///TRGMUX register lock.
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Register can be written.
            v1=0x00000001,     ///<Register cannot be written until the next system Reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
    }
    namespace TrgmuxTrgmuxFtm4{    ///<TRGMUX FTM4 Register
        using Addr = Register::Address<0x40063070,0x7fffff80,0x00000000,unsigned>;
        ///Trigger MUX Input 0 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> sel0{}; 
        ///TRGMUX register lock.
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Register can be written.
            v1=0x00000001,     ///<Register cannot be written until the next system Reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
    }
    namespace TrgmuxTrgmuxFtm5{    ///<TRGMUX FTM5 Register
        using Addr = Register::Address<0x40063074,0x7fffff80,0x00000000,unsigned>;
        ///Trigger MUX Input 0 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> sel0{}; 
        ///TRGMUX register lock.
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Register can be written.
            v1=0x00000001,     ///<Register cannot be written until the next system Reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
    }
    namespace TrgmuxTrgmuxFtm6{    ///<TRGMUX FTM6 Register
        using Addr = Register::Address<0x40063078,0x7fffff80,0x00000000,unsigned>;
        ///Trigger MUX Input 0 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> sel0{}; 
        ///TRGMUX register lock.
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Register can be written.
            v1=0x00000001,     ///<Register cannot be written until the next system Reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
    }
    namespace TrgmuxTrgmuxFtm7{    ///<TRGMUX FTM7 Register
        using Addr = Register::Address<0x4006307c,0x7fffff80,0x00000000,unsigned>;
        ///Trigger MUX Input 0 Source Select
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,0),Register::ReadWriteAccess,unsigned> sel0{}; 
        ///TRGMUX register lock.
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Register can be written.
            v1=0x00000001,     ///<Register cannot be written until the next system Reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,31),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
    }
}
