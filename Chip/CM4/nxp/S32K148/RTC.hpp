#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//Secure Real Time Clock
    namespace RtcTsr{    ///<RTC Time Seconds Register
        using Addr = Register::Address<0x4003d000,0x00000000,0x00000000,unsigned>;
        ///Time Seconds Register
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> tsr{}; 
    }
    namespace RtcTpr{    ///<RTC Time Prescaler Register
        using Addr = Register::Address<0x4003d004,0xffff0000,0x00000000,unsigned>;
        ///Time Prescaler Register
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::ReadWriteAccess,unsigned> tpr{}; 
    }
    namespace RtcTar{    ///<RTC Time Alarm Register
        using Addr = Register::Address<0x4003d008,0x00000000,0x00000000,unsigned>;
        ///Time Alarm Register
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> tar{}; 
    }
    namespace RtcTcr{    ///<RTC Time Compensation Register
        using Addr = Register::Address<0x4003d00c,0x00000000,0x00000000,unsigned>;
        ///Time Compensation Register
        enum class TcrVal : unsigned {
            v10000000=0x00000080,     ///<Time Prescaler Register overflows every 32896 clock cycles.
            v10000001=0x00000081,     ///<Time Prescaler Register overflows every 32895 clock cycles.
            v11111111=0x000000ff,     ///<Time Prescaler Register overflows every 32769 clock cycles.
            v00000000=0x00000000,     ///<Time Prescaler Register overflows every 32768 clock cycles.
            v00000001=0x00000001,     ///<Time Prescaler Register overflows every 32767 clock cycles.
            v01111110=0x0000007e,     ///<Time Prescaler Register overflows every 32642 clock cycles.
            v01111111=0x0000007f,     ///<Time Prescaler Register overflows every 32641 clock cycles.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,0),Register::ReadWriteAccess,TcrVal> tcr{}; 
        namespace TcrValC{
            constexpr Register::FieldValue<decltype(tcr)::Type,TcrVal::v10000000> v10000000{};
            constexpr Register::FieldValue<decltype(tcr)::Type,TcrVal::v10000001> v10000001{};
            constexpr Register::FieldValue<decltype(tcr)::Type,TcrVal::v11111111> v11111111{};
            constexpr Register::FieldValue<decltype(tcr)::Type,TcrVal::v00000000> v00000000{};
            constexpr Register::FieldValue<decltype(tcr)::Type,TcrVal::v00000001> v00000001{};
            constexpr Register::FieldValue<decltype(tcr)::Type,TcrVal::v01111110> v01111110{};
            constexpr Register::FieldValue<decltype(tcr)::Type,TcrVal::v01111111> v01111111{};
        }
        ///Compensation Interval Register
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,8),Register::ReadWriteAccess,unsigned> cir{}; 
        ///Time Compensation Value
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(23,16),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> tcv{}; 
        ///Compensation Interval Counter
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,24),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> cic{}; 
    }
    namespace RtcCr{    ///<RTC Control Register
        using Addr = Register::Address<0x4003d010,0xfeffff52,0x00000000,unsigned>;
        ///Software Reset
        enum class SwrVal : unsigned {
            v0=0x00000000,     ///<No effect.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,SwrVal> swr{}; 
        namespace SwrValC{
            constexpr Register::FieldValue<decltype(swr)::Type,SwrVal::v0> v0{};
        }
        ///Supervisor Access
        enum class SupVal : unsigned {
            v0=0x00000000,     ///<Non-supervisor mode write accesses are not supported and generate a bus error.
            v1=0x00000001,     ///<Non-supervisor mode write accesses are supported.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,SupVal> sup{}; 
        namespace SupValC{
            constexpr Register::FieldValue<decltype(sup)::Type,SupVal::v0> v0{};
            constexpr Register::FieldValue<decltype(sup)::Type,SupVal::v1> v1{};
        }
        ///Update Mode
        enum class UmVal : unsigned {
            v0=0x00000000,     ///<Registers cannot be written when locked.
            v1=0x00000001,     ///<Registers can be written when locked under limited conditions.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,UmVal> um{}; 
        namespace UmValC{
            constexpr Register::FieldValue<decltype(um)::Type,UmVal::v0> v0{};
            constexpr Register::FieldValue<decltype(um)::Type,UmVal::v1> v1{};
        }
        ///Clock Pin Select
        enum class CpsVal : unsigned {
            v0=0x00000000,     ///<The prescaler output clock (as configured by TSIC) is output on RTC_CLKOUT.
            v1=0x00000001,     ///<The RTC 32kHz crystal clock is output on RTC_CLKOUT, provided it is output to other peripherals.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,CpsVal> cps{}; 
        namespace CpsValC{
            constexpr Register::FieldValue<decltype(cps)::Type,CpsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cps)::Type,CpsVal::v1> v1{};
        }
        ///LPO Select
        enum class LposVal : unsigned {
            v0=0x00000000,     ///<RTC prescaler increments using 32kHz crystal.
            v1=0x00000001,     ///<RTC prescaler increments using 1kHz LPO, bits [4:0] of the prescaler are ignored.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(7,7),Register::ReadWriteAccess,LposVal> lpos{}; 
        namespace LposValC{
            constexpr Register::FieldValue<decltype(lpos)::Type,LposVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lpos)::Type,LposVal::v1> v1{};
        }
        ///Clock Pin Enable
        enum class CpeVal : unsigned {
            v0=0x00000000,     ///<Disable RTC_CLKOUT pin.
            v1=0x00000001,     ///<Enable RTC_CLKOUT pin.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,CpeVal> cpe{}; 
        namespace CpeValC{
            constexpr Register::FieldValue<decltype(cpe)::Type,CpeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cpe)::Type,CpeVal::v1> v1{};
        }
    }
    namespace RtcSr{    ///<RTC Status Register
        using Addr = Register::Address<0x4003d014,0xffffffe8,0x00000000,unsigned>;
        ///Time Invalid Flag
        enum class TifVal : unsigned {
            v0=0x00000000,     ///<Time is valid.
            v1=0x00000001,     ///<Time is invalid and time counter is read as zero.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,TifVal> tif{}; 
        namespace TifValC{
            constexpr Register::FieldValue<decltype(tif)::Type,TifVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tif)::Type,TifVal::v1> v1{};
        }
        ///Time Overflow Flag
        enum class TofVal : unsigned {
            v0=0x00000000,     ///<Time overflow has not occurred.
            v1=0x00000001,     ///<Time overflow has occurred and time counter is read as zero.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,TofVal> tof{}; 
        namespace TofValC{
            constexpr Register::FieldValue<decltype(tof)::Type,TofVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tof)::Type,TofVal::v1> v1{};
        }
        ///Time Alarm Flag
        enum class TafVal : unsigned {
            v0=0x00000000,     ///<Time alarm has not occurred.
            v1=0x00000001,     ///<Time alarm has occurred.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::Access<Register::AccessType::readOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,TafVal> taf{}; 
        namespace TafValC{
            constexpr Register::FieldValue<decltype(taf)::Type,TafVal::v0> v0{};
            constexpr Register::FieldValue<decltype(taf)::Type,TafVal::v1> v1{};
        }
        ///Time Counter Enable
        enum class TceVal : unsigned {
            v0=0x00000000,     ///<Time counter is disabled.
            v1=0x00000001,     ///<Time counter is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,4),Register::ReadWriteAccess,TceVal> tce{}; 
        namespace TceValC{
            constexpr Register::FieldValue<decltype(tce)::Type,TceVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tce)::Type,TceVal::v1> v1{};
        }
    }
    namespace RtcLr{    ///<RTC Lock Register
        using Addr = Register::Address<0x4003d018,0xffffff87,0x00000000,unsigned>;
        ///Time Compensation Lock
        enum class TclVal : unsigned {
            v0=0x00000000,     ///<Time Compensation Register is locked and writes are ignored.
            v1=0x00000001,     ///<Time Compensation Register is not locked and writes complete as normal.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(3,3),Register::ReadWriteAccess,TclVal> tcl{}; 
        namespace TclValC{
            constexpr Register::FieldValue<decltype(tcl)::Type,TclVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tcl)::Type,TclVal::v1> v1{};
        }
        ///Control Register Lock
        enum class CrlVal : unsigned {
            v0=0x00000000,     ///<Control Register is locked and writes are ignored.
            v1=0x00000001,     ///<Control Register is not locked and writes complete as normal.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,4),Register::ReadWriteAccess,CrlVal> crl{}; 
        namespace CrlValC{
            constexpr Register::FieldValue<decltype(crl)::Type,CrlVal::v0> v0{};
            constexpr Register::FieldValue<decltype(crl)::Type,CrlVal::v1> v1{};
        }
        ///Status Register Lock
        enum class SrlVal : unsigned {
            v0=0x00000000,     ///<Status Register is locked and writes are ignored.
            v1=0x00000001,     ///<Status Register is not locked and writes complete as normal.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(5,5),Register::ReadWriteAccess,SrlVal> srl{}; 
        namespace SrlValC{
            constexpr Register::FieldValue<decltype(srl)::Type,SrlVal::v0> v0{};
            constexpr Register::FieldValue<decltype(srl)::Type,SrlVal::v1> v1{};
        }
        ///Lock Register Lock
        enum class LrlVal : unsigned {
            v0=0x00000000,     ///<Lock Register is locked and writes are ignored.
            v1=0x00000001,     ///<Lock Register is not locked and writes complete as normal.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,LrlVal> lrl{}; 
        namespace LrlValC{
            constexpr Register::FieldValue<decltype(lrl)::Type,LrlVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lrl)::Type,LrlVal::v1> v1{};
        }
    }
    namespace RtcIer{    ///<RTC Interrupt Enable Register
        using Addr = Register::Address<0x4003d01c,0xfff8ffe8,0x00000000,unsigned>;
        ///Time Invalid Interrupt Enable
        enum class TiieVal : unsigned {
            v0=0x00000000,     ///<Time invalid flag does not generate an interrupt.
            v1=0x00000001,     ///<Time invalid flag does generate an interrupt.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,TiieVal> tiie{}; 
        namespace TiieValC{
            constexpr Register::FieldValue<decltype(tiie)::Type,TiieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tiie)::Type,TiieVal::v1> v1{};
        }
        ///Time Overflow Interrupt Enable
        enum class ToieVal : unsigned {
            v0=0x00000000,     ///<Time overflow flag does not generate an interrupt.
            v1=0x00000001,     ///<Time overflow flag does generate an interrupt.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,ToieVal> toie{}; 
        namespace ToieValC{
            constexpr Register::FieldValue<decltype(toie)::Type,ToieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(toie)::Type,ToieVal::v1> v1{};
        }
        ///Time Alarm Interrupt Enable
        enum class TaieVal : unsigned {
            v0=0x00000000,     ///<Time alarm flag does not generate an interrupt.
            v1=0x00000001,     ///<Time alarm flag does generate an interrupt.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(2,2),Register::ReadWriteAccess,TaieVal> taie{}; 
        namespace TaieValC{
            constexpr Register::FieldValue<decltype(taie)::Type,TaieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(taie)::Type,TaieVal::v1> v1{};
        }
        ///Time Seconds Interrupt Enable
        enum class TsieVal : unsigned {
            v0=0x00000000,     ///<Seconds interrupt is disabled.
            v1=0x00000001,     ///<Seconds interrupt is enabled.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,4),Register::ReadWriteAccess,TsieVal> tsie{}; 
        namespace TsieValC{
            constexpr Register::FieldValue<decltype(tsie)::Type,TsieVal::v0> v0{};
            constexpr Register::FieldValue<decltype(tsie)::Type,TsieVal::v1> v1{};
        }
        ///Timer Seconds Interrupt Configuration
        enum class TsicVal : unsigned {
            v000=0x00000000,     ///<1 Hz.
            v001=0x00000001,     ///<2 Hz.
            v010=0x00000002,     ///<4 Hz.
            v011=0x00000003,     ///<8 Hz.
            v100=0x00000004,     ///<16 Hz.
            v101=0x00000005,     ///<32 Hz.
            v110=0x00000006,     ///<64 Hz.
            v111=0x00000007,     ///<128 Hz.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(18,16),Register::ReadWriteAccess,TsicVal> tsic{}; 
        namespace TsicValC{
            constexpr Register::FieldValue<decltype(tsic)::Type,TsicVal::v000> v000{};
            constexpr Register::FieldValue<decltype(tsic)::Type,TsicVal::v001> v001{};
            constexpr Register::FieldValue<decltype(tsic)::Type,TsicVal::v010> v010{};
            constexpr Register::FieldValue<decltype(tsic)::Type,TsicVal::v011> v011{};
            constexpr Register::FieldValue<decltype(tsic)::Type,TsicVal::v100> v100{};
            constexpr Register::FieldValue<decltype(tsic)::Type,TsicVal::v101> v101{};
            constexpr Register::FieldValue<decltype(tsic)::Type,TsicVal::v110> v110{};
            constexpr Register::FieldValue<decltype(tsic)::Type,TsicVal::v111> v111{};
        }
    }
}
