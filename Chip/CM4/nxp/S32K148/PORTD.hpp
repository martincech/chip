#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//Pin Control and Interrupts
    namespace PortdPcr0{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c000,0xfef078bc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Drive Strength Enable
        enum class DseVal : unsigned {
            v0=0x00000000,     ///<Low drive strength is configured on the corresponding pin, if pin is configured as a digital output.
            v1=0x00000001,     ///<High drive strength is configured on the corresponding pin, if pin is configured as a digital output.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,DseVal> dse{}; 
        namespace DseValC{
            constexpr Register::FieldValue<decltype(dse)::Type,DseVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dse)::Type,DseVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm0_ch2=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi1_sck=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm2_ch0=0x00000004,     ///<Alternative 4 (chip-specific).
            etm_trace_d0=0x00000005,     ///<Alternative 5 (chip-specific).
            fxio_d0=0x00000006,     ///<Alternative 6 (chip-specific).
            trgmux_out1=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm0_ch2> ftm0_ch2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi1_sck> lpspi1_sck{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_ch0> ftm2_ch0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::etm_trace_d0> etm_trace_d0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d0> fxio_d0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::trgmux_out1> trgmux_out1{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr1{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c004,0xfef078bc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Drive Strength Enable
        enum class DseVal : unsigned {
            v0=0x00000000,     ///<Low drive strength is configured on the corresponding pin, if pin is configured as a digital output.
            v1=0x00000001,     ///<High drive strength is configured on the corresponding pin, if pin is configured as a digital output.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,DseVal> dse{}; 
        namespace DseValC{
            constexpr Register::FieldValue<decltype(dse)::Type,DseVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dse)::Type,DseVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm0_ch3=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi1_sin=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm2_ch1=0x00000004,     ///<Alternative 4 (chip-specific).
            sai0_mclk=0x00000005,     ///<Alternative 5 (chip-specific).
            fxio_d1=0x00000006,     ///<Alternative 6 (chip-specific).
            trgmux_out2=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm0_ch3> ftm0_ch3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi1_sin> lpspi1_sin{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_ch1> ftm2_ch1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::sai0_mclk> sai0_mclk{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d1> fxio_d1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::trgmux_out2> trgmux_out2{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr2{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c008,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm3_ch4=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi1_sout=0x00000003,     ///<Alternative 3 (chip-specific).
            fxio_d4=0x00000004,     ///<Alternative 4 (chip-specific).
            fxio_d6=0x00000005,     ///<Alternative 5 (chip-specific).
            trgmux_in5=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm3_ch4> ftm3_ch4{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi1_sout> lpspi1_sout{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d4> fxio_d4{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d6> fxio_d6{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::trgmux_in5> trgmux_in5{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr3{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c00c,0xfef078ec,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Passive Filter Enable
        enum class PfeVal : unsigned {
            v0=0x00000000,     ///<Passive input filter is disabled on the corresponding pin.
            v1=0x00000001,     ///<Passive input filter is enabled on the corresponding pin, if the pin is configured as a digital input. Refer to the device data sheet for filter characteristics.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,4),Register::ReadWriteAccess,PfeVal> pfe{}; 
        namespace PfeValC{
            constexpr Register::FieldValue<decltype(pfe)::Type,PfeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pfe)::Type,PfeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm3_ch5=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi1_pcs0=0x00000003,     ///<Alternative 3 (chip-specific).
            fxio_d5=0x00000004,     ///<Alternative 4 (chip-specific).
            fxio_d7=0x00000005,     ///<Alternative 5 (chip-specific).
            trgmux_in4=0x00000006,     ///<Alternative 6 (chip-specific).
            nmi_b=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm3_ch5> ftm3_ch5{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi1_pcs0> lpspi1_pcs0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d5> fxio_d5{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d7> fxio_d7{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::trgmux_in4> trgmux_in4{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::nmi_b> nmi_b{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr4{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c010,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm0_flt3=0x00000002,     ///<Alternative 2 (chip-specific).
            ftm3_flt3=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm0_flt3> ftm0_flt3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm3_flt3> ftm3_flt3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr5{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c014,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm2_ch3=0x00000002,     ///<Alternative 2 (chip-specific).
            lptmr0_alt2=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm2_flt1=0x00000004,     ///<Alternative 4 (chip-specific).
            mii_txd3=0x00000005,     ///<Alternative 5 (chip-specific).
            trgmux_in7=0x00000006,     ///<Alternative 6 (chip-specific).
            qspi_b_io2=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_ch3> ftm2_ch3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lptmr0_alt2> lptmr0_alt2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_flt1> ftm2_flt1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::mii_txd3> mii_txd3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::trgmux_in7> trgmux_in7{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::qspi_b_io2> qspi_b_io2{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr6{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c018,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            lpuart2_rx=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm2_flt2=0x00000004,     ///<Alternative 4 (chip-specific).
            mii_txd2=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            qspi_b_io1=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart2_rx> lpuart2_rx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_flt2> ftm2_flt2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::mii_txd2> mii_txd2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::qspi_b_io1> qspi_b_io1{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr7{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c01c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            lpuart2_tx=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm2_flt3=0x00000004,     ///<Alternative 4 (chip-specific).
            mii_rmii_txd[1]=0x00000005,     ///<Alternative 5 (chip-specific).
            etm_trace_d0=0x00000006,     ///<Alternative 6 (chip-specific).
            qspi_a_io1=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart2_tx> lpuart2_tx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_flt3> ftm2_flt3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::mii_rmii_txd[1]> mii_rmii_txd[1]{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::etm_trace_d0> etm_trace_d0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::qspi_a_io1> qspi_a_io1{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr8{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c020,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            lpi2c1_sda=0x00000002,     ///<Alternative 2 (chip-specific).
            mii_rxd3=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm2_flt2=0x00000004,     ///<Alternative 4 (chip-specific).
            fxio_d1=0x00000005,     ///<Alternative 5 (chip-specific).
            ftm1_ch4=0x00000006,     ///<Alternative 6 (chip-specific).
            qspi_b_io5=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpi2c1_sda> lpi2c1_sda{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::mii_rxd3> mii_rxd3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_flt2> ftm2_flt2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d1> fxio_d1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_ch4> ftm1_ch4{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::qspi_b_io5> qspi_b_io5{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr9{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c024,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            lpi2c1_scl=0x00000002,     ///<Alternative 2 (chip-specific).
            fxio_d0=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm2_flt3=0x00000004,     ///<Alternative 4 (chip-specific).
            mii_rxd2=0x00000005,     ///<Alternative 5 (chip-specific).
            ftm1_ch5=0x00000006,     ///<Alternative 6 (chip-specific).
            qspi_b_io4=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpi2c1_scl> lpi2c1_scl{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d0> fxio_d0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_flt3> ftm2_flt3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::mii_rxd2> mii_rxd2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_ch5> ftm1_ch5{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::qspi_b_io4> qspi_b_io4{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr10{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c028,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm2_ch0=0x00000002,     ///<Alternative 2 (chip-specific).
            ftm2_qd_phb=0x00000003,     ///<Alternative 3 (chip-specific).
            etm_trace_d3=0x00000004,     ///<Alternative 4 (chip-specific).
            mii_rx_clk=0x00000005,     ///<Alternative 5 (chip-specific).
            clkout=0x00000006,     ///<Alternative 6 (chip-specific).
            qspi_a_sck=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_ch0> ftm2_ch0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_qd_phb> ftm2_qd_phb{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::etm_trace_d3> etm_trace_d3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::mii_rx_clk> mii_rx_clk{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::clkout> clkout{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::qspi_a_sck> qspi_a_sck{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr11{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c02c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm2_ch1=0x00000002,     ///<Alternative 2 (chip-specific).
            ftm2_qd_pha=0x00000003,     ///<Alternative 3 (chip-specific).
            etm_trace_d2=0x00000004,     ///<Alternative 4 (chip-specific).
            mii_rmii_tx_clk=0x00000005,     ///<Alternative 5 (chip-specific).
            lpuart2_cts=0x00000006,     ///<Alternative 6 (chip-specific).
            qspi_a_io0=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_ch1> ftm2_ch1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_qd_pha> ftm2_qd_pha{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::etm_trace_d2> etm_trace_d2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::mii_rmii_tx_clk> mii_rmii_tx_clk{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart2_cts> lpuart2_cts{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::qspi_a_io0> qspi_a_io0{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr12{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c030,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm2_ch2=0x00000002,     ///<Alternative 2 (chip-specific).
            lpi2c1_hreq=0x00000003,     ///<Alternative 3 (chip-specific).
            etm_trace_d1=0x00000004,     ///<Alternative 4 (chip-specific).
            mii_rmii_tx_en=0x00000005,     ///<Alternative 5 (chip-specific).
            lpuart2_rts=0x00000006,     ///<Alternative 6 (chip-specific).
            qspi_a_io2=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_ch2> ftm2_ch2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpi2c1_hreq> lpi2c1_hreq{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::etm_trace_d1> etm_trace_d1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::mii_rmii_tx_en> mii_rmii_tx_en{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart2_rts> lpuart2_rts{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::qspi_a_io2> qspi_a_io2{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr13{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c034,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm2_ch4=0x00000002,     ///<Alternative 2 (chip-specific).
            lpuart1_rx=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            enet_tmr1=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            rtc_clkout=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_ch4> ftm2_ch4{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart1_rx> lpuart1_rx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::enet_tmr1> enet_tmr1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::rtc_clkout> rtc_clkout{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr14{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c038,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm2_ch5=0x00000002,     ///<Alternative 2 (chip-specific).
            lpuart1_tx=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            enet_tmr0=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            clkout=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_ch5> ftm2_ch5{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart1_tx> lpuart1_tx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::enet_tmr0> enet_tmr0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::clkout> clkout{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr15{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c03c,0xfef078bc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Drive Strength Enable
        enum class DseVal : unsigned {
            v0=0x00000000,     ///<Low drive strength is configured on the corresponding pin, if pin is configured as a digital output.
            v1=0x00000001,     ///<High drive strength is configured on the corresponding pin, if pin is configured as a digital output.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,DseVal> dse{}; 
        namespace DseValC{
            constexpr Register::FieldValue<decltype(dse)::Type,DseVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dse)::Type,DseVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm0_ch0=0x00000002,     ///<Alternative 2 (chip-specific).
            etm_trace_d3=0x00000003,     ///<Alternative 3 (chip-specific).
            lpspi0_sck=0x00000004,     ///<Alternative 4 (chip-specific).
            enet_tmr2=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm0_ch0> ftm0_ch0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::etm_trace_d3> etm_trace_d3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi0_sck> lpspi0_sck{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::enet_tmr2> enet_tmr2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr16{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c040,0xfef078bc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Drive Strength Enable
        enum class DseVal : unsigned {
            v0=0x00000000,     ///<Low drive strength is configured on the corresponding pin, if pin is configured as a digital output.
            v1=0x00000001,     ///<High drive strength is configured on the corresponding pin, if pin is configured as a digital output.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(6,6),Register::ReadWriteAccess,DseVal> dse{}; 
        namespace DseValC{
            constexpr Register::FieldValue<decltype(dse)::Type,DseVal::v0> v0{};
            constexpr Register::FieldValue<decltype(dse)::Type,DseVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm0_ch1=0x00000002,     ///<Alternative 2 (chip-specific).
            etm_trace_d2=0x00000003,     ///<Alternative 3 (chip-specific).
            lpspi0_sin=0x00000004,     ///<Alternative 4 (chip-specific).
            cmp0_rrt=0x00000005,     ///<Alternative 5 (chip-specific).
            etm_trace_clkout=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm0_ch1> ftm0_ch1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::etm_trace_d2> etm_trace_d2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi0_sin> lpspi0_sin{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::cmp0_rrt> cmp0_rrt{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::etm_trace_clkout> etm_trace_clkout{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr17{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c044,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm0_flt2=0x00000002,     ///<Alternative 2 (chip-specific).
            lpuart2_rx=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm5_flt1=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm0_flt2> ftm0_flt2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart2_rx> lpuart2_rx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm5_flt1> ftm5_flt1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr18{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c048,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm5_ch7=0x00000002,     ///<Alternative 2 (chip-specific).
            fxio_d2=0x00000003,     ///<Alternative 3 (chip-specific).
            lpi2c1_scls=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm5_ch7> ftm5_ch7{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d2> fxio_d2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpi2c1_scls> lpi2c1_scls{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr19{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c04c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm6_ch0=0x00000002,     ///<Alternative 2 (chip-specific).
            fxio_d3=0x00000003,     ///<Alternative 3 (chip-specific).
            lpi2c1_scl=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm6_ch0> ftm6_ch0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d3> fxio_d3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpi2c1_scl> lpi2c1_scl{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr20{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c050,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm6_ch1=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm6_ch1> ftm6_ch1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr21{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c054,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm6_ch2=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm6_ch2> ftm6_ch2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr22{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c058,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm6_ch3=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm6_ch3> ftm6_ch3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr23{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c05c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm6_ch4=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm6_ch4> ftm6_ch4{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr24{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c060,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm6_ch5=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm6_ch5> ftm6_ch5{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr25{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c064,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm6_ch6=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm6_ch6> ftm6_ch6{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr26{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c068,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm6_ch7=0x00000002,     ///<Alternative 2 (chip-specific).
            fxio_d7=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm6_ch7> ftm6_ch7{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d7> fxio_d7{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr27{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c06c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm7_ch0=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm7_ch0> ftm7_ch0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr28{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c070,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm7_ch1=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm7_ch1> ftm7_ch1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr29{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c074,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm7_ch2=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm7_ch2> ftm7_ch2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr30{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c078,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm7_ch3=0x00000002,     ///<Alternative 2 (chip-specific).
            ftm6_flt1=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm7_ch3> ftm7_ch3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm6_flt1> ftm6_flt1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdPcr31{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004c07c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm7_ch4=0x00000002,     ///<Alternative 2 (chip-specific).
            fxio_d6=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            ftm6_flt0=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm7_ch4> ftm7_ch4{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d6> fxio_d6{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm6_flt0> ftm6_flt0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortdGpclr{    ///<Global Pin Control Low Register
        using Addr = Register::Address<0x4004c080,0x00000000,0x00000000,unsigned>;
        ///Global Pin Write Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> gpwd{}; 
        ///Global Pin Write Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,16),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> gpwe{}; 
    }
    namespace PortdGpchr{    ///<Global Pin Control High Register
        using Addr = Register::Address<0x4004c084,0x00000000,0x00000000,unsigned>;
        ///Global Pin Write Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> gpwd{}; 
        ///Global Pin Write Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,16),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> gpwe{}; 
    }
    namespace PortdGiclr{    ///<Global Interrupt Control Low Register
        using Addr = Register::Address<0x4004c088,0x00000000,0x00000000,unsigned>;
        ///Global Interrupt Write Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> giwe{}; 
        ///Global Interrupt Write Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,16),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> giwd{}; 
    }
    namespace PortdGichr{    ///<Global Interrupt Control High Register
        using Addr = Register::Address<0x4004c08c,0x00000000,0x00000000,unsigned>;
        ///Global Interrupt Write Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> giwe{}; 
        ///Global Interrupt Write Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,16),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> giwd{}; 
    }
    namespace PortdIsfr{    ///<Interrupt Status Flag Register
        using Addr = Register::Address<0x4004c0a0,0x00000000,0x00000000,unsigned>;
        ///Interrupt Status Flag
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> isf{}; 
    }
    namespace PortdDfer{    ///<Digital Filter Enable Register
        using Addr = Register::Address<0x4004c0c0,0x00000000,0x00000000,unsigned>;
        ///Digital Filter Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> dfe{}; 
    }
    namespace PortdDfcr{    ///<Digital Filter Clock Register
        using Addr = Register::Address<0x4004c0c4,0xfffffffe,0x00000000,unsigned>;
        ///Clock Source
        enum class CsVal : unsigned {
            v0=0x00000000,     ///<Digital filters are clocked by the bus clock.
            v1=0x00000001,     ///<Digital filters are clocked by the LPO clock.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,CsVal> cs{}; 
        namespace CsValC{
            constexpr Register::FieldValue<decltype(cs)::Type,CsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cs)::Type,CsVal::v1> v1{};
        }
    }
    namespace PortdDfwr{    ///<Digital Filter Width Register
        using Addr = Register::Address<0x4004c0c8,0xffffffe0,0x00000000,unsigned>;
        ///Filter Length
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,0),Register::ReadWriteAccess,unsigned> filt{}; 
    }
}
