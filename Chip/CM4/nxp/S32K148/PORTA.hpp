#pragma once 
#include <Register/Utility.hpp>
namespace Kvasir {
//Pin Control and Interrupts
    namespace PortaPcr0{    ///<Pin Control Register n
        using Addr = Register::Address<0x40049000,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm2_ch1=0x00000002,     ///<Alternative 2 (chip-specific).
            lpi2c0_scls=0x00000003,     ///<Alternative 3 (chip-specific).
            fxio_d2=0x00000004,     ///<Alternative 4 (chip-specific).
            ftm2_qd_pha=0x00000005,     ///<Alternative 5 (chip-specific).
            lpuart0_cts=0x00000006,     ///<Alternative 6 (chip-specific).
            trgmux_out3=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_ch1> ftm2_ch1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpi2c0_scls> lpi2c0_scls{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d2> fxio_d2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_qd_pha> ftm2_qd_pha{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart0_cts> lpuart0_cts{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::trgmux_out3> trgmux_out3{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr1{    ///<Pin Control Register n
        using Addr = Register::Address<0x40049004,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm1_ch1=0x00000002,     ///<Alternative 2 (chip-specific).
            lpi2c0_sdas=0x00000003,     ///<Alternative 3 (chip-specific).
            fxio_d3=0x00000004,     ///<Alternative 4 (chip-specific).
            ftm1_qd_pha=0x00000005,     ///<Alternative 5 (chip-specific).
            lpuart0_rts=0x00000006,     ///<Alternative 6 (chip-specific).
            trgmux_out0=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_ch1> ftm1_ch1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpi2c0_sdas> lpi2c0_sdas{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d3> fxio_d3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_qd_pha> ftm1_qd_pha{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart0_rts> lpuart0_rts{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::trgmux_out0> trgmux_out0{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr2{    ///<Pin Control Register n
        using Addr = Register::Address<0x40049008,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm3_ch0=0x00000002,     ///<Alternative 2 (chip-specific).
            lpi2c0_sda=0x00000003,     ///<Alternative 3 (chip-specific).
            ewm_out_b=0x00000004,     ///<Alternative 4 (chip-specific).
            fxio_d4=0x00000005,     ///<Alternative 5 (chip-specific).
            lpuart0_rx=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm3_ch0> ftm3_ch0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpi2c0_sda> lpi2c0_sda{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ewm_out_b> ewm_out_b{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d4> fxio_d4{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart0_rx> lpuart0_rx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr3{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004900c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm3_ch1=0x00000002,     ///<Alternative 2 (chip-specific).
            lpi2c0_scl=0x00000003,     ///<Alternative 3 (chip-specific).
            ewm_in=0x00000004,     ///<Alternative 4 (chip-specific).
            fxio_d5=0x00000005,     ///<Alternative 5 (chip-specific).
            lpuart0_tx=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm3_ch1> ftm3_ch1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpi2c0_scl> lpi2c0_scl{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ewm_in> ewm_in{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d5> fxio_d5{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart0_tx> lpuart0_tx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr4{    ///<Pin Control Register n
        using Addr = Register::Address<0x40049010,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            disabled=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            cmp0_out=0x00000004,     ///<Alternative 4 (chip-specific).
            ewm_out_b=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            jtag_tms_swd_dio=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::cmp0_out> cmp0_out{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ewm_out_b> ewm_out_b{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::jtag_tms_swd_dio> jtag_tms_swd_dio{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr5{    ///<Pin Control Register n
        using Addr = Register::Address<0x40049014,0xfef078ec,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Passive Filter Enable
        enum class PfeVal : unsigned {
            v0=0x00000000,     ///<Passive input filter is disabled on the corresponding pin.
            v1=0x00000001,     ///<Passive input filter is enabled on the corresponding pin, if the pin is configured as a digital input. Refer to the device data sheet for filter characteristics.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,4),Register::ReadWriteAccess,PfeVal> pfe{}; 
        namespace PfeValC{
            constexpr Register::FieldValue<decltype(pfe)::Type,PfeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pfe)::Type,PfeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            disabled=0x00000002,     ///<Alternative 2 (chip-specific).
            tclk1=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            reset_b=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::tclk1> tclk1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::reset_b> reset_b{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr6{    ///<Pin Control Register n
        using Addr = Register::Address<0x40049018,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm0_flt1=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi1_pcs1=0x00000003,     ///<Alternative 3 (chip-specific).
            ftm5_ch5=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            lpuart1_cts=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm0_flt1> ftm0_flt1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi1_pcs1> lpspi1_pcs1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm5_ch5> ftm5_ch5{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart1_cts> lpuart1_cts{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr7{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004901c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm0_flt2=0x00000002,     ///<Alternative 2 (chip-specific).
            ftm5_ch3=0x00000003,     ///<Alternative 3 (chip-specific).
            rtc_clkin=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            lpuart1_rts=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm0_flt2> ftm0_flt2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm5_ch3> ftm5_ch3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::rtc_clkin> rtc_clkin{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart1_rts> lpuart1_rts{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr8{    ///<Pin Control Register n
        using Addr = Register::Address<0x40049020,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            lpuart2_rx=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi2_sout=0x00000003,     ///<Alternative 3 (chip-specific).
            fxio_d6=0x00000004,     ///<Alternative 4 (chip-specific).
            ftm3_flt3=0x00000005,     ///<Alternative 5 (chip-specific).
            ftm4_flt1=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart2_rx> lpuart2_rx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi2_sout> lpspi2_sout{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d6> fxio_d6{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm3_flt3> ftm3_flt3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm4_flt1> ftm4_flt1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr9{    ///<Pin Control Register n
        using Addr = Register::Address<0x40049024,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            lpuart2_tx=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi2_pcs0=0x00000003,     ///<Alternative 3 (chip-specific).
            fxio_d7=0x00000004,     ///<Alternative 4 (chip-specific).
            ftm3_flt2=0x00000005,     ///<Alternative 5 (chip-specific).
            ftm1_flt3=0x00000006,     ///<Alternative 6 (chip-specific).
            ftm4_flt0=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart2_tx> lpuart2_tx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi2_pcs0> lpspi2_pcs0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d7> fxio_d7{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm3_flt2> ftm3_flt2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_flt3> ftm1_flt3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm4_flt0> ftm4_flt0{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr10{    ///<Pin Control Register n
        using Addr = Register::Address<0x40049028,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm1_ch4=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            fxio_d0=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            jtag_tdo_noetm_trace_swo=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_ch4> ftm1_ch4{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d0> fxio_d0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::jtag_tdo_noetm_trace_swo> jtag_tdo_noetm_trace_swo{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr11{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004902c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm1_ch5=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            fxio_d1=0x00000004,     ///<Alternative 4 (chip-specific).
            cmp0_rrt=0x00000005,     ///<Alternative 5 (chip-specific).
            sai0_sync=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_ch5> ftm1_ch5{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d1> fxio_d1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::cmp0_rrt> cmp0_rrt{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::sai0_sync> sai0_sync{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr12{    ///<Pin Control Register n
        using Addr = Register::Address<0x40049030,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm1_ch6=0x00000002,     ///<Alternative 2 (chip-specific).
            can1_rx=0x00000003,     ///<Alternative 3 (chip-specific).
            lpi2c1_sdas=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            ftm2_qd_phb=0x00000006,     ///<Alternative 6 (chip-specific).
            sai0_bclk=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_ch6> ftm1_ch6{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::can1_rx> can1_rx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpi2c1_sdas> lpi2c1_sdas{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_qd_phb> ftm2_qd_phb{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::sai0_bclk> sai0_bclk{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr13{    ///<Pin Control Register n
        using Addr = Register::Address<0x40049034,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm1_ch7=0x00000002,     ///<Alternative 2 (chip-specific).
            can1_tx=0x00000003,     ///<Alternative 3 (chip-specific).
            lpi2c1_scls=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            ftm2_qd_pha=0x00000006,     ///<Alternative 6 (chip-specific).
            sai0_d0=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_ch7> ftm1_ch7{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::can1_tx> can1_tx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpi2c1_scls> lpi2c1_scls{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm2_qd_pha> ftm2_qd_pha{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::sai0_d0> sai0_d0{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr14{    ///<Pin Control Register n
        using Addr = Register::Address<0x40049038,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm0_flt0=0x00000002,     ///<Alternative 2 (chip-specific).
            ftm3_flt1=0x00000003,     ///<Alternative 3 (chip-specific).
            ewm_in=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            ftm1_flt0=0x00000006,     ///<Alternative 6 (chip-specific).
            sai0_d3=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm0_flt0> ftm0_flt0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm3_flt1> ftm3_flt1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ewm_in> ewm_in{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_flt0> ftm1_flt0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::sai0_d3> sai0_d3{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr15{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004903c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm1_ch2=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi0_pcs3=0x00000003,     ///<Alternative 3 (chip-specific).
            lpspi2_pcs3=0x00000004,     ///<Alternative 4 (chip-specific).
            ftm7_flt0=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_ch2> ftm1_ch2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi0_pcs3> lpspi0_pcs3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi2_pcs3> lpspi2_pcs3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm7_flt0> ftm7_flt0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr16{    ///<Pin Control Register n
        using Addr = Register::Address<0x40049040,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm1_ch3=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi1_pcs2=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm1_ch3> ftm1_ch3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi1_pcs2> lpspi1_pcs2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr17{    ///<Pin Control Register n
        using Addr = Register::Address<0x40049044,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm0_ch6=0x00000002,     ///<Alternative 2 (chip-specific).
            ftm3_flt0=0x00000003,     ///<Alternative 3 (chip-specific).
            ewm_out_b=0x00000004,     ///<Alternative 4 (chip-specific).
            ftm5_flt0=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm0_ch6> ftm0_ch6{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm3_flt0> ftm3_flt0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ewm_out_b> ewm_out_b{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm5_flt0> ftm5_flt0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr18{    ///<Pin Control Register n
        using Addr = Register::Address<0x40049048,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm4_ch0=0x00000002,     ///<Alternative 2 (chip-specific).
            lpuart1_tx=0x00000003,     ///<Alternative 3 (chip-specific).
            lpspi1_sout=0x00000004,     ///<Alternative 4 (chip-specific).
            ftm6_ch0=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm4_ch0> ftm4_ch0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart1_tx> lpuart1_tx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi1_sout> lpspi1_sout{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm6_ch0> ftm6_ch0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr19{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004904c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm4_ch1=0x00000002,     ///<Alternative 2 (chip-specific).
            lpuart1_rx=0x00000003,     ///<Alternative 3 (chip-specific).
            lpspi1_sck=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm4_ch1> ftm4_ch1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart1_rx> lpuart1_rx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi1_sck> lpspi1_sck{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr20{    ///<Pin Control Register n
        using Addr = Register::Address<0x40049050,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm4_ch2=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            lpspi1_sin=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm4_ch2> ftm4_ch2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi1_sin> lpspi1_sin{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr21{    ///<Pin Control Register n
        using Addr = Register::Address<0x40049054,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm4_ch3=0x00000002,     ///<Alternative 2 (chip-specific).
            fxio_d0=0x00000003,     ///<Alternative 3 (chip-specific).
            lpspi1_pcs0=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm4_ch3> ftm4_ch3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d0> fxio_d0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi1_pcs0> lpspi1_pcs0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr22{    ///<Pin Control Register n
        using Addr = Register::Address<0x40049058,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm4_ch4=0x00000002,     ///<Alternative 2 (chip-specific).
            fxio_d1=0x00000003,     ///<Alternative 3 (chip-specific).
            lpspi1_pcs1=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm4_ch4> ftm4_ch4{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d1> fxio_d1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi1_pcs1> lpspi1_pcs1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr23{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004905c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm4_ch6=0x00000002,     ///<Alternative 2 (chip-specific).
            fxio_d2=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm4_ch6> ftm4_ch6{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d2> fxio_d2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr24{    ///<Pin Control Register n
        using Addr = Register::Address<0x40049060,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm4_ch7=0x00000002,     ///<Alternative 2 (chip-specific).
            fxio_d3=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm4_ch7> ftm4_ch7{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::fxio_d3> fxio_d3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr25{    ///<Pin Control Register n
        using Addr = Register::Address<0x40049064,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm5_ch0=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            disabled=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm5_ch0> ftm5_ch0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr26{    ///<Pin Control Register n
        using Addr = Register::Address<0x40049068,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm5_ch1=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi1_pcs0=0x00000003,     ///<Alternative 3 (chip-specific).
            lpspi0_pcs0=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm5_ch1> ftm5_ch1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi1_pcs0> lpspi1_pcs0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi0_pcs0> lpspi0_pcs0{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr27{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004906c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm5_ch2=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi1_sout=0x00000003,     ///<Alternative 3 (chip-specific).
            lpuart0_tx=0x00000004,     ///<Alternative 4 (chip-specific).
            can0_tx=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm5_ch2> ftm5_ch2{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi1_sout> lpspi1_sout{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart0_tx> lpuart0_tx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::can0_tx> can0_tx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr28{    ///<Pin Control Register n
        using Addr = Register::Address<0x40049070,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm5_ch3=0x00000002,     ///<Alternative 2 (chip-specific).
            lpspi1_sck=0x00000003,     ///<Alternative 3 (chip-specific).
            lpuart0_rx=0x00000004,     ///<Alternative 4 (chip-specific).
            can0_rx=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm5_ch3> ftm5_ch3{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi1_sck> lpspi1_sck{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart0_rx> lpuart0_rx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::can0_rx> can0_rx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr29{    ///<Pin Control Register n
        using Addr = Register::Address<0x40049074,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm5_ch4=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            lpuart2_tx=0x00000004,     ///<Alternative 4 (chip-specific).
            lpspi1_sin=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm5_ch4> ftm5_ch4{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart2_tx> lpuart2_tx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi1_sin> lpspi1_sin{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr30{    ///<Pin Control Register n
        using Addr = Register::Address<0x40049078,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm5_ch5=0x00000002,     ///<Alternative 2 (chip-specific).
            lpuart2_rx=0x00000003,     ///<Alternative 3 (chip-specific).
            lpspi0_sout=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm5_ch5> ftm5_ch5{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpuart2_rx> lpuart2_rx{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi0_sout> lpspi0_sout{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaPcr31{    ///<Pin Control Register n
        using Addr = Register::Address<0x4004907c,0xfef078fc,0x00000000,unsigned>;
        ///Pull Select
        enum class PsVal : unsigned {
            v0=0x00000000,     ///<Internal pulldown resistor is enabled on the corresponding pin, if the corresponding PE field is set.
            v1=0x00000001,     ///<Internal pullup resistor is enabled on the corresponding pin, if the corresponding PE field is set.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,PsVal> ps{}; 
        namespace PsValC{
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(ps)::Type,PsVal::v1> v1{};
        }
        ///Pull Enable
        enum class PeVal : unsigned {
            v0=0x00000000,     ///<Internal pullup or pulldown resistor is not enabled on the corresponding pin.
            v1=0x00000001,     ///<Internal pullup or pulldown resistor is enabled on the corresponding pin, if the pin is configured as a digital input.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(1,1),Register::ReadWriteAccess,PeVal> pe{}; 
        namespace PeValC{
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v0> v0{};
            constexpr Register::FieldValue<decltype(pe)::Type,PeVal::v1> v1{};
        }
        ///Pin Mux Control
        enum class MuxVal : unsigned {
            disabled=0x00000000,     ///<Pin disabled (Alternative 0) (analog).
            gpio=0x00000001,     ///<Alternative 1 (GPIO).
            ftm5_ch6=0x00000002,     ///<Alternative 2 (chip-specific).
            disabled=0x00000003,     ///<Alternative 3 (chip-specific).
            lpspi0_pcs1=0x00000004,     ///<Alternative 4 (chip-specific).
            disabled=0x00000005,     ///<Alternative 5 (chip-specific).
            disabled=0x00000006,     ///<Alternative 6 (chip-specific).
            disabled=0x00000007,     ///<Alternative 7 (chip-specific).
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(10,8),Register::ReadWriteAccess,MuxVal> mux{}; 
        namespace MuxValC{
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::gpio> gpio{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::ftm5_ch6> ftm5_ch6{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::lpspi0_pcs1> lpspi0_pcs1{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
            constexpr Register::FieldValue<decltype(mux)::Type,MuxVal::disabled> disabled{};
        }
        ///Lock Register
        enum class LkVal : unsigned {
            v0=0x00000000,     ///<Pin Control Register fields [15:0] are not locked.
            v1=0x00000001,     ///<Pin Control Register fields [15:0] are locked and cannot be updated until the next system reset.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,15),Register::ReadWriteAccess,LkVal> lk{}; 
        namespace LkValC{
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v0> v0{};
            constexpr Register::FieldValue<decltype(lk)::Type,LkVal::v1> v1{};
        }
        ///Interrupt Configuration
        enum class IrqcVal : unsigned {
            v0000=0x00000000,     ///<Interrupt Status Flag (ISF) is disabled.
            v0001=0x00000001,     ///<ISF flag and DMA request on rising edge.
            v0010=0x00000002,     ///<ISF flag and DMA request on falling edge.
            v0011=0x00000003,     ///<ISF flag and DMA request on either edge.
            v1000=0x00000008,     ///<ISF flag and Interrupt when logic 0.
            v1001=0x00000009,     ///<ISF flag and Interrupt on rising-edge.
            v1010=0x0000000a,     ///<ISF flag and Interrupt on falling-edge.
            v1011=0x0000000b,     ///<ISF flag and Interrupt on either edge.
            v1100=0x0000000c,     ///<ISF flag and Interrupt when logic 1.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(19,16),Register::ReadWriteAccess,IrqcVal> irqc{}; 
        namespace IrqcValC{
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0000> v0000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0001> v0001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0010> v0010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v0011> v0011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1000> v1000{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1001> v1001{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1010> v1010{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1011> v1011{};
            constexpr Register::FieldValue<decltype(irqc)::Type,IrqcVal::v1100> v1100{};
        }
        ///Interrupt Status Flag
        enum class IsfVal : unsigned {
            v0=0x00000000,     ///<Configured interrupt is not detected.
            v1=0x00000001,     ///<Configured interrupt is detected. If the pin is configured to generate a DMA request, then the corresponding flag will be cleared automatically at the completion of the requested DMA transfer. Otherwise, the flag remains set until a logic 1 is written to the flag. If the pin is configured for a level sensitive interrupt and the pin remains asserted, then the flag is set again immediately after it is cleared.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(24,24),Register::ReadWriteAccess,IsfVal> isf{}; 
        namespace IsfValC{
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v0> v0{};
            constexpr Register::FieldValue<decltype(isf)::Type,IsfVal::v1> v1{};
        }
    }
    namespace PortaGpclr{    ///<Global Pin Control Low Register
        using Addr = Register::Address<0x40049080,0x00000000,0x00000000,unsigned>;
        ///Global Pin Write Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> gpwd{}; 
        ///Global Pin Write Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,16),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> gpwe{}; 
    }
    namespace PortaGpchr{    ///<Global Pin Control High Register
        using Addr = Register::Address<0x40049084,0x00000000,0x00000000,unsigned>;
        ///Global Pin Write Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> gpwd{}; 
        ///Global Pin Write Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,16),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> gpwe{}; 
    }
    namespace PortaGiclr{    ///<Global Interrupt Control Low Register
        using Addr = Register::Address<0x40049088,0x00000000,0x00000000,unsigned>;
        ///Global Interrupt Write Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> giwe{}; 
        ///Global Interrupt Write Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,16),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> giwd{}; 
    }
    namespace PortaGichr{    ///<Global Interrupt Control High Register
        using Addr = Register::Address<0x4004908c,0x00000000,0x00000000,unsigned>;
        ///Global Interrupt Write Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(15,0),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> giwe{}; 
        ///Global Interrupt Write Data
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,16),Register::Access<Register::AccessType::writeOnly,Register::ReadActionType::normal,Register::ModifiedWriteValueType::normal>,unsigned> giwd{}; 
    }
    namespace PortaIsfr{    ///<Interrupt Status Flag Register
        using Addr = Register::Address<0x400490a0,0x00000000,0x00000000,unsigned>;
        ///Interrupt Status Flag
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> isf{}; 
    }
    namespace PortaDfer{    ///<Digital Filter Enable Register
        using Addr = Register::Address<0x400490c0,0x00000000,0x00000000,unsigned>;
        ///Digital Filter Enable
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(31,0),Register::ReadWriteAccess,unsigned> dfe{}; 
    }
    namespace PortaDfcr{    ///<Digital Filter Clock Register
        using Addr = Register::Address<0x400490c4,0xfffffffe,0x00000000,unsigned>;
        ///Clock Source
        enum class CsVal : unsigned {
            v0=0x00000000,     ///<Digital filters are clocked by the bus clock.
            v1=0x00000001,     ///<Digital filters are clocked by the LPO clock.
        };
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(0,0),Register::ReadWriteAccess,CsVal> cs{}; 
        namespace CsValC{
            constexpr Register::FieldValue<decltype(cs)::Type,CsVal::v0> v0{};
            constexpr Register::FieldValue<decltype(cs)::Type,CsVal::v1> v1{};
        }
    }
    namespace PortaDfwr{    ///<Digital Filter Width Register
        using Addr = Register::Address<0x400490c8,0xffffffe0,0x00000000,unsigned>;
        ///Filter Length
        constexpr Register::FieldLocation<Addr,Register::maskFromRange(4,0),Register::ReadWriteAccess,unsigned> filt{}; 
    }
}
