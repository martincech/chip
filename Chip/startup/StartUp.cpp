#include "cmsis_compiler.h"
#include "Core.hpp"
 
typedef void (*ptr_func_t)();

// bss
extern unsigned __bss_start__;
extern unsigned __bss_end__;
// data
extern unsigned __data_start__;
extern unsigned __data_end__;
extern unsigned __data_load__;
// constructors array
extern ptr_func_t __preinit_array_start[];
extern ptr_func_t __preinit_array_end[];
extern ptr_func_t __init_array_start[];
extern ptr_func_t __init_array_end[];
// destructors array
extern ptr_func_t __fini_array_start[];
extern ptr_func_t __fini_array_end[];
// heap / stack
extern unsigned __HeapBase;
extern unsigned __HeapLimit;
extern unsigned __StackTop;

inline void init_base_regs() {
   __set_MSP(reinterpret_cast<uint32_t>(&__StackTop));   
   /* Init the rest of the registers */
   __ASM volatile("ldr     r1,=0");
   __ASM volatile("ldr     r2,=0");
   __ASM volatile("ldr     r3,=0");
   __ASM volatile("ldr     r4,=0");
   __ASM volatile("ldr     r5,=0");
   __ASM volatile("ldr     r6,=0");
   __ASM volatile("ldr     r7,=0");
   __ASM volatile("mov     r8,r7");
   __ASM volatile("mov     r9,r7");
   __ASM volatile("mov     r10,r7");
   __ASM volatile("mov     r11,r7");
   __ASM volatile("mov     r12,r7");  
}

/** Zero static variables with zero or undefined value
 */
inline void zero_bss() {
    auto dst = &__bss_start__;
    while (dst < &__bss_end__) {
        *dst++ = 0;
    }
}
/** Copy statically defined variables
 */
inline void copy_data() {
    auto src = &__data_load__;
    auto dst = &__data_start__;
    while (dst < &__data_end__) {
        *dst++ = *src++;
    }
}


/** call destructors for static objects
 */
inline void call_fini_array() {
    auto array = __fini_array_start;
    while (array < __fini_array_end) {
        (*array)();
        array++;
    }
}

/** call constructors for static objects
 */
inline void call_init_array() {
    auto array = __preinit_array_start;
    while (array < __preinit_array_end) {
        (*array)();
        array++;
    }

    array = __init_array_start;
    while (array < __init_array_end) {
        (*array)();
        array++;
    }
}

/** Clear/fill with pattern content of HEAP memory
 */
inline void fill_heap(unsigned fill=0xDEADBEEF) {
    auto dst = &__HeapBase;
    while (dst < &__HeapLimit) {
        *dst++ = fill;
    }
}


extern "C" {
   void Reset_Handler(void) {
      extern int main(void);
      /* Mask interrupts */
      __disable_irq();
      init_base_regs();
      __ISB();
      copy_data();   
      zero_bss();
      fill_heap();
      call_init_array();   
      __DSB();
      __enable_irq();
      __ISB();
#pragma GCC diagnostic ignored "-Wpedantic"
      main();
#pragma GCC diagnostic pop
      while (1) ;
   }



}