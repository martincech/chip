# Portability of Kvasir Chips
This repo contains chip specific files for the Kvasir library. 
Python generator for the chip files from SWD included.

Kvasir works on essentially every ARM Cortex (we generate the chip specific code from the vendor provided CMSIS-SWD files). 
Although different chips will still have different peripherals because of Kvasirs level of abstraction it is still far easier and safer when porting.